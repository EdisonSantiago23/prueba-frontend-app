import {enableProdMode} from '@angular/core';
import {platformBrowserDynamic} from '@angular/platform-browser-dynamic';

import {AppModule} from './app/app.module';
import {environment} from './environments/environment';

if (environment.production) {
  enableProdMode();
}

const oaStyle = [
  'background: transparent',
  'color: #87bf48',
  'display: block',
  'text-align: center',
  'font-family: \'Roboto-Regular\', sans-serif',
  'font-size: 14px'
].join(';');

platformBrowserDynamic().bootstrapModule(AppModule)
  .then(res => console.log('%c ' + String.fromCodePoint(0x1F680) + ' Admin up and running ', oaStyle), err => console.error(err));
