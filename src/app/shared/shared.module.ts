import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { SharedRoutingModule } from './shared-routing.module';
import { HttpConfigInterceptor } from './interceptors/httpconfig.interceptor';
import { FormatMoneyEcPipe } from './pipes/format-money-ec.pipe';
import { NgSelectModule } from '@ng-select/ng-select';
import { AngularMyDatePickerModule } from 'angular-mydatepicker';
import {
  SharedComponent,
} from './components';
import { NgxUiLoaderModule } from 'ngx-ui-loader';


@NgModule({
  declarations: [SharedComponent,
    FormatMoneyEcPipe,],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedRoutingModule, NgxUiLoaderModule,
    NgSelectModule,
    AngularMyDatePickerModule
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: HttpConfigInterceptor, multi: true }
  ],
  exports: [
    FormatMoneyEcPipe

  ]
})
export class SharedModule {
}
