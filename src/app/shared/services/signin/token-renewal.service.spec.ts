import {TestBed} from '@angular/core/testing';

import {TokenRenewalService} from './token-renewal.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';

describe('TokenRenewalService', () => {
  let service: TokenRenewalService;

  beforeEach(() => {
    TestBed.configureTestingModule({imports: [HttpClientTestingModule]});
    service = TestBed.inject(TokenRenewalService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
