import {Injectable} from '@angular/core';
import {
  HttpInterceptor,
  HttpRequest,
  HttpResponse,
  HttpHandler,
  HttpEvent,
  HttpErrorResponse
} from '@angular/common/http';

import {Observable, throwError} from 'rxjs';
import {map, catchError, switchMap} from 'rxjs/operators';

import {GlobalErrorHandler} from '../classes/GlobalErrorHandler';
import {LocalStorageCommon} from '../classes/LocalStorageCommon';
import {TokenRenewalService} from '../services/signin/token-renewal.service';

@Injectable()
export class HttpConfigInterceptor implements HttpInterceptor {
  flag = false;

  constructor(
    public errorHandler: GlobalErrorHandler,
    private localStorageCommon: LocalStorageCommon,
    private tokenRenewalServices: TokenRenewalService
  ) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(
      map((event: HttpEvent<any>) => {
        return event;
      }),
      catchError((error: HttpErrorResponse) => {
        if (error instanceof HttpErrorResponse) {
          if (error !== undefined) {
            if (error.status === 401) {
              return this.tokenRenewal(request, next);
            } else {
              return throwError(this.errorHandler.handleError(error));
            }
          }
        } else {
          return throwError(error);
        }
      }));
  }

  tokenRenewal = (request: HttpRequest<any>, next: HttpHandler) => {
    if (!this.flag) {
      this.flag = true;
      const user = this.localStorageCommon.get('auth_user', 'json');
      return this.tokenRenewalServices.put(user.id_users, null).pipe(
        switchMap((response) => {
          this.localStorageCommon.store('auth_token', response.data, 'normal');
          this.flag = false;
          return next.handle(this.setHeadersAuthorization(request));
        })
      );
    }
  };

  setHeadersAuthorization = (request: HttpRequest<any>) => {
    return request.clone({
      setHeaders: {
        Authorization: 'Bearer ' + this.localStorageCommon.get('auth_token', 'normal')
      }
    });
  };
}
