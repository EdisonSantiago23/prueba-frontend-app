import {FormControl, FormGroup, AbstractControl} from '@angular/forms';
import {REGEX_DICTIONARY, VALIDATOR_ENUM} from './Enums';

export const regex = REGEX_DICTIONARY;

export function passwordValidator(control: AbstractControl): { [key: string]: boolean | null } {
  if (control.value !== '' && control.value !== null && control.value !== undefined) {
    const expReg = regex.password;
    if (!expReg.test(control.value) && control.value.length) {
      return {password: true};
    }
  }
  return null;
}

export function samePasswordValidator(control: FormGroup): { [key: string]: boolean | null } {

  if (control.value !== '' && control.value !== null && control.value !== undefined) {

    const password = control.get('password').value;
    const repeatPassword = control.get('passwordconfirm').value;

    if (password !== repeatPassword && repeatPassword !== null) {
      return {samePassword: true};
    }
    return null;
  }
}

export function monetaryValidator(control: FormControl): { [key: string]: boolean | null } {
  if (control.value !== '' && control.value !== null && control.value !== undefined) {
    const numericMonetary = regex.numericMonetary;
    if (!numericMonetary.test(control.value)) {
      return {moneyQuantity: true};
    }
  }
  return null;
}

export function numericValidator(control: FormControl): { [key: string]: boolean | null } {
  if (control.value !== '' && control.value !== null && control.value !== undefined) {
    const numericQuantity = regex.numericQuantity;
    if (!numericQuantity.test(control.value)) {
      return {numericQuantity: true};
    }
  }
  return null;
}

export function notZero(control: FormControl): { [key: string]: boolean | null } {
  if (control.value !== '' && control.value !== null && control.value !== undefined) {
    if (+control.value <= 0) {
      return {notZero: true};
    }
  }
  return null;
}

export function isIntegerValidator(control: FormControl): { [key: string]: boolean | null } {
  if (control.value !== '' && control.value !== null && control.value !== undefined) {
    const v = +control.value;
    if (!Number.isInteger(v) || !regex.threeDigitInt.test(control.value)) {
      return {notInteger: true};
    }
  }
  return null;
}

export function isNumberValidator(control: FormControl): { [key: string]: boolean | null } {
  if (control.value !== '' && control.value !== null && control.value !== undefined) {
    const v = +control.value;
    if (isNaN(v)) {
      return {notNumber: true};
    }
  }
  return null;
}

export function feeValidator(control: FormControl): { [key: string]: boolean | null } {
  if (control.value !== '' && control.value !== null && control.value !== undefined) {
    const v = +control.value;
    if (v < 1 || v > 75 || isNaN(v)) {
      return {numericFee: true};
    }
  }
  return null;
}

export function paidFeeValidator(control: FormControl): { [key: string]: boolean | null } {
  if (control.value !== '' && control.value !== null && control.value !== undefined) {
    const v = +control.value;
    if (v < 0 || v > 74 || isNaN(v)) {
      return {numericPaidFee: true};
    }
  }
  return null;
}

export function alphanumericValidator(control: FormControl): { [key: string]: boolean | null } {
  if (control.value !== '' && control.value !== null && control.value !== undefined) {
    const numericQuantity = regex.alphanumeric;
    if (!numericQuantity.test(control.value)) {
      return {alphanumeric: true};
    }
  }
  return null;
}


export function nameValidator(control: FormControl) {
  if (control.value !== '' && control.value !== null && control.value !== undefined) {
    const names = regex.names;

    if (!names.test(control.value)) {
      return {name: true};
    }
  }
  return null;
}

export function fullNameValidator(control: FormControl) {
  if (control.value !== '' && control.value !== null && control.value !== undefined) {
    const names = regex.fullname;

    if (!names.test(control.value)) {
      return {fullName: true};
    }
  }
  return null;
}

export function lastnameValidator(control: FormControl) {
  if (control.value !== '' && control.value !== null && control.value !== undefined) {
    const names = regex.lastname;

    if (!names.test(control.value)) {
      return {lastname: true};
    }
  }
  return null;
}


export function secondLastnameValidator(control: FormControl) {
  if (control.value !== '' && control.value !== null && control.value !== undefined) {
    const names = regex.secondLastname;

    if (!names.test(control.value)) {
      return {secondLastname: true};
    }
  }
  return null;
}


export function cellphoneEcuValidator(control: FormControl) {
  if (control.value !== '' && control.value !== null && control.value !== undefined) {
    const cellphoneEcu = regex.cellphoneEcu;

    if (!cellphoneEcu.test(control.value)) {
      return {cellphone: true};
    }
  }
  return null;
}

export function telephoneEcuValidator(control: FormControl) {
  if (control.value !== '' && control.value !== null && control.value !== undefined) {
    const telephoneEcu = regex.telephoneEcu;

    if (!telephoneEcu.test(control.value)) {
      return {telephone: true};
    }
  }
  return null;
}

export function identifyIdECValidator(control: FormControl) {
  if (control.value !== '' && control.value !== null && control.value !== undefined) {
    const userId = regex.userId;
    if (!userId.test(control.value) && control.value.length) {
      return {userId: true};
    }
  }
  return null;
}

export function identifyRucECValidator(control: FormControl) {
  if (control.value !== '' && control.value !== null && control.value !== undefined) {
    const userId = regex.userRuc;
    if (!userId.test(control.value) && control.value.length) {
      return {userRuc: true};
    }
  }
  return null;
}

export function identifyPassECValidator(control: FormControl) {
  if (control.value !== '' && control.value !== null && control.value !== undefined) {
    const userId = regex.userPassport;
    if (!userId.test(control.value) && control.value.length) {
      return {userPassport: true};
    }
  }
  return null;
}

export function requiredArrayField(control: FormControl) {
  if (control.value !== '' && control.value !== null && control.value !== undefined) {
    return null;
  }
  return {required_genericArray: true};
}

export function customValidatorHandler(formGroup: AbstractControl, id: string) {
  let errorMessage = '';
  const t = formGroup.get(id) ? formGroup.get(id).errors : formGroup.errors;
  Object.keys(t).forEach(error => {
    let customError: string;
    if (error === 'required') {
      customError = `${error}_${id}`;
    } else {
      customError = error;
    }
    if (formGroup.get(id)) {
      errorMessage = formGroup.get(id).hasError(error) ? VALIDATOR_ENUM[customError] : '';
    } else {
      errorMessage = formGroup.hasError(error) ? VALIDATOR_ENUM[customError] : '';
    }
  });

  return errorMessage;
}


export function customArrayValidatorHandle(formArray: any, id: any) {
  let errorMessage = '';
  Object.keys(formArray.at(id).errors).forEach(error => {

    let customError = '';
    if (error === 'required') {
      if (formArray.at(id).indexOf('address') !== -1) {
        customError = `${error}_address`;
      } else if (formArray.at(id).indexOf('nameCompleteReceive') !== -1) {
        customError = `${error}_nameCompleteReceive`;
      } else if (formArray.at(id).indexOf('nameComplete') !== -1) {
        customError = `${error}_nameComplete`;
      } else if (formArray.at(id).indexOf('Add') !== -1) {
        customError = `${error}_${formArray.substring(0, formArray.indexOf('Add'))}`;
      } else {
        customError = `${error}_${formArray}`;
      }
    } else {
      customError = error;
    }
    errorMessage = formArray.at(id).hasError(error) ? VALIDATOR_ENUM[customError] : '';
  });
  return errorMessage;
}
