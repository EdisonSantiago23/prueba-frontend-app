import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ChipHandler {
  private chips: any[] = [];

  add(chip: any) {
    const exists: any = this.chips.filter(item => item.name === chip.name)[0];
    if (exists !== null && exists !== undefined) {
      this.chips.push(chip);
    }
  }

  remove(name?: string) {
    this.chips = this.chips.filter(item => item.name !== name);
  }

  clear() {
    this.chips = [];
  }

  show(name: string) {
    const chip: any = this.chips.filter(item => item.name === name)[0];
    chip.show();
  }

  hide(name: string) {
    const chip: any = this.chips.filter(item => item.name === name)[0];
    chip.hide();
  }

}
