import {Injectable} from '@angular/core';
import {LocalStorageCommon} from './LocalStorageCommon';
import {JwtHelperService} from '@auth0/angular-jwt';
import {Router} from '@angular/router';
import {TokenRenewalService} from '../services/signin/token-renewal.service';
import {
  AbstractControl,
  AbstractControlOptions,
  FormControl,
  FormGroup,
  ValidationErrors,
  Validators
} from '@angular/forms';
import {customValidatorHandler} from './CustomValidators';
import {IMyDateModel, IMySingleDateModel} from 'angular-mydatepicker';

@Injectable({
  providedIn: 'root'
})
export class Helpers {
  parser = new JwtHelperService();

  constructor(private localStorageCommon: LocalStorageCommon, private router: Router,
              private tokenRenewalService: TokenRenewalService) {
  }

  /**
   * Function that converts numbers to money format
   * @param currency Type of currency($, £, €, etc.)
   * @param value Numerical value (0 or 9.00, etc.)
   * @param decimals Number of decimals (2 by default)
   */
  formatMoney(currency: string, value: any, decimals: number): string {

    if (value !== undefined && value !== '') {
      let n = value;
      n = n.toString();

      if (n === '' || n === '.') {
        n = '0.00';
      }

      const patron = [currency, ' ', ','];
      const longitud = patron.length;

      for (let i = 0; i < longitud; i++) {
        n = n.replace(patron[i], '');
      }
      n = n.replace(patron, '');

      n = parseFloat(n);

      let dec = 2;

      if (decimals !== undefined) {
        dec = decimals;
      }

      const multiplicator = Math.pow(10, dec);
      return currency + ' ' + (Math.round(n * multiplicator) / multiplicator).toFixed(dec);

    }

  }

  /**
   * Function that subtracts two hours
   * @param starthour Start Hour (hh:mm:ss)
   * @param endhour Final Hour (hh:mm:ss)
   */
  subtractHours(starthour: string, endhour: string): string {

    const hourEnd = endhour.split(':');
    const hourStart = starthour.split(':');

    const final = new Date();
    const initial = new Date();

    final.setHours(parseInt(hourEnd[0], 0), parseInt(hourEnd[1], 0), parseInt(hourEnd[2], 0));
    initial.setHours(parseInt(hourStart[0], 0), parseInt(hourStart[1], 0), parseInt(hourStart[2], 0));

    const hours = final.getHours() - initial.getHours();
    const minutes = final.getMinutes() - initial.getMinutes();
    const seconds = final.getSeconds() - initial.getSeconds();

    final.setHours(hours, minutes, seconds);

    return final.getHours() + ':' + final.getMinutes() + ':' + final.getSeconds();

  }

  /**
   * Function that subtracts two dates
   * @param startdate Start date (YYYY-MM-DD)
   * @param enddate Final date (YYYY-MM-DD)
   */
  subtractDates(startdate: string, enddate: string): number {

    let final: string[];
    const initial: Array<string> = startdate.split('-');

    if (enddate !== '') {

      final = enddate.split('-');

    } else {

      const now = new Date();
      final = [String(now.getFullYear()), String(now.getMonth() + 1), String(now.getDate())];

    }

    const dateStart: any = new Date(parseInt(initial[0], 0), (parseInt(initial[1], 0) - 1), parseInt(initial[2], 0));
    const dateEnd: any = new Date(parseInt(final[0], 0), (parseInt(final[1], 0) - 1), parseInt(final[2], 0));

    return Math.floor(((dateEnd - dateStart) / 86400) / 1000);

  }

  /**
   * Function that transforms a number day into a literal date
   * @param day Number of day(s)
   */
  transformDayLiteral(day: number): string {

    let num: any;
    let num2: any;

    if (day / 365.2427 >= 1) {

      num = Math.floor(day / 365.2427);
      num2 = num * 365.2427;
      return num + ' año(s), ' + this.transformDayLiteral(day - num2);

    }

    if (day / 30.437 >= 1) {

      num = Math.floor(day / 30.437);
      num2 = num * 30.437;
      return num + ' mes(es), ' + this.transformDayLiteral(day - num2);

    }

    if (day / 7 >= 1) {

      num = Math.floor(day / 7);
      num2 = num * 7;
      return num + ' semana(s), ' + this.transformDayLiteral(day - num2);

    }

    if (Math.floor(day) !== 0) {

      return Math.floor(day) + ' día(s).';

    } else {

      return '';

    }

  }

  clearStorageSlugIds(slugArray: string[]) {
    slugArray.forEach(slug => {
      if (this.checkSlug(slug)) {
        this.localStorageCommon.destroy(slug);
      }
    });
  }


  checkSlug(slug: string): boolean {
    const candidate = this.localStorageCommon.get(slug, 'normal');
    return !!candidate;
  }


  tokenCheckUp(token: string): boolean {
    const expirationDate = this.parser.getTokenExpirationDate(token);
    if (this.isExpired(token)) {
      this.router.navigate(['/']).catch(e => console.error(e));
    }
    const today = new Date();
    const timeFrame = 60000 * 60;
    const res = expirationDate.getTime() - timeFrame;
    return res < today.getTime();
  }

  isExpired(token: string): boolean {
    return this.parser.isTokenExpired(token);
  }

  /**
   * Function that renews the session token seamlessly
   * @param ctx - the context where to call the options function in side the implemented component
   */

  renewToken(ctx) {
    this.tokenRenewalService.put(ctx.user.id_users, {}).subscribe(
      (response) => {
        if (response.success) {
          this.localStorageCommon.store('auth_token', response.data, 'normal');
        }
      }, (error) => {
        console.error(error);
      }, () => {
        ctx.complete();
      }
    );
  }
  /**
   * This function initializes the error handling
   * @param form - the array of input fields
   * @param ctx -  the component who calls the function
   * @param ctx -  the component who calls the function
   */
  errorInit = (form: string[], ctx) => {
    form.forEach(i => {
      ctx.error[i] = {error: false, change: false, msg: ''};
    });
  }

  /**
   * This function checks the form input state
   * @param error - Error array corresponding tho the form group
   * @param id - Specific form control
   * @param form - Abstract form control to evaluate
   * @param errorOnly - True if we want to only highlight errors
   * @param step - The form step
   */

  validInput(error: any, id: string, form: AbstractControl, errorOnly: boolean = false, step?: string) {
    console.log(error[id])
    error[id].change = true;
    if (step) {
      error[id].step = step;
    }
    let t: ValidationErrors = form.get(id);


    t = t ? t.errors : form.errors;
    if (t) {
      error[id].error = true;
      error[id].msg = customValidatorHandler(form, id);
    } else {
      error[id].error = false;
      error[id].change = !errorOnly;
      error[id].msg = '';
    }
  }

  /**
   * This function validates if the user role possesses the permission
   * @param user - the user profile to check if its an admin
   * @param permission - the slug to validate
   * @param permList - the permission list to iterate
   * @returns boolean corresponding to the condition
   */
  getPermission = (user, permission: string, permList: any[]): boolean => {
    if (user?.role.special !== 'all-access') {
      // tslint:disable-next-line: prefer-for-of
      for (let i = 0; i < permList?.length; i++) {
        if (permList[i].slug === permission) {
          return true;
        }
      }
      return false;
    } else {
      return true;
    }
  }

  /**
   * This function tries to execute statements based on permission
   * @param user - The user object to check if admin
   * @param perm -  The permission slug
   * @param permList -  The list of permission slugs
   * @param run - Optional statements to execute if pass permission test
   * @param elseRun - Optional statements to execute if the test fails
   */


  acidTest(user, perm: string, permList: any[], run?: () => void, elseRun?: () => void) {
    if (this.getPermission(user, perm, permList)) {
      run();
    } else {
      elseRun();
    }
  }

  /**
   * This function uploads a file to a specified file array
   * @param files - The FileList obj to be uploaded
   * @param fieldName - The formControl name
   * @param filesToUpload - the array to temporarily store the file
   * @param form -  the form to set the value
   * @param size - if true checks for a max file of 7mb
   * @param type - if true checks for the file extension type
   * @param docTypes - if checking for type the supported formats, defaults to PDF
   */
  uploadFile(files: FileList,
             fieldName: string,
             filesToUpload: any[],
             form: FormGroup, size: boolean = false,
             type: boolean = false,
             docTypes: string[] = ['application/pdf']) {
    for (let i = 0; i < filesToUpload.length; i++){
      if (filesToUpload[i][0][0] === fieldName) {
        filesToUpload.splice(i);
        break;
      }
    }
    filesToUpload.push([[fieldName], files.item(0)]);
    form.get(fieldName).setValue(files.item(0));
    if (files.item(0)?.size / 1024000 > 7 && size){
      form.get(fieldName).setErrors({maxFile: true});
    }
    if (type) {
      const t = files.item(0)?.type;
      let match = false;

      for (const i of docTypes) {
        if (t === i) {
          match = true;
          break;
        }
      }
      if (!match) {
        if (docTypes.length > 1) {
          form.get(fieldName).setErrors({fileType: true});
        } else if (docTypes.length === 1) {
          switch (docTypes[0]) {
            case 'application/pdf':
              form.get(fieldName).setErrors({fileTypePdf: true});
              break;
            case 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet':
              form.get(fieldName).setErrors({fileTypeExcel: true});
              break;
            case 'text/csv':
              form.get(fieldName).setErrors({fileTypeCsv: true});
          }
        }
      }
    }
  }

  uploadMultipleFiles(files: FileList, fieldName: string, filesToUpload: any[], form: FormGroup) {
    Array.from(files).forEach(i => {
      if (i?.size / 1024000 > 7){
        form.get(fieldName).setErrors({maxFile: true});
      } else {
        filesToUpload.push(i);
        form.get(fieldName).setValue(i);
        form.get(fieldName).updateValueAndValidity();
      }
    });
  }

  /**
   * @param ctx - the parent component to validate de token
   */

  tokenSecurity(ctx) {
    if (ctx.token) {
      if (!this.isExpired(ctx.token)) {
        if (this.tokenCheckUp(ctx.token)) {
          this.renewToken(ctx);
        } else {
          ctx.complete();
        }
      } else {
        ctx.router?.navigate(['/']).catch(e => console.error(e));
      }
    } else {
      ctx.router?.navigate(['/']).catch(e => console.error(e));
    }
  }

  /**
   * This function takes a string formatted day and returns a json date for myDatePicker plugin
   * @param dateString - the string date to be converted ej: yyyy-mm-dd
   * @param prefill - flag to precalculate with today's date
   */
  dateToJson(dateString: string, prefill = false): IMySingleDateModel {
    if (dateString) {
      const dateArr = dateString.split('-');
      return {
        date: {
          year: parseInt(dateArr[0], 0),
          month: parseInt(dateArr[1], 0),
          day: parseInt(dateArr[2], 0),
        },
        jsDate: null
      };
    } else if (prefill) {
      const t = new Date();
      return {
        date: {
          year: t.getFullYear(),
          month: t.getMonth(),
          day: t.getDate()
        },
        jsDate: t,
      } as IMySingleDateModel;
    } else {
      return {
        date: {
          year: null,
          month: null,
          day: null
        },
        jsDate: null,
      } as IMySingleDateModel;
    }

  }

  /**
   * This function takes a date object and transforms it to a string
   * @param dateObj - date object in myDatePicker format
   */
  dateToString(dateObj: IMyDateModel): string {
    if (dateObj) {
      let month = dateObj?.singleDate?.date.month.toString();
      let day = dateObj?.singleDate?.date.day.toString();
      if (parseInt(month, 0) < 10) {
        month = '0' + month;
      }
      if (parseInt(day, 0) < 10) {
        day = '0' + day;
      }
      return dateObj?.singleDate?.date.year + '-' + month + '-' + day;
    } else {
      return null;
    }
  }

  /**
   * This function returns a FormControl configuration object
   * @param params - a string array with the form fields
   */
  generateForm = (params: any[]): {[p: string]: any} =>  {
    const form = {} as any;
    params.forEach(i => {
      let v = /date/i.test(i.field) ? '' : null;
      if (i.field === 'contractProcess' || i.field === 'search') {
        v = '';
      } else if ( i.field === 'typefilterdate' || i.field === 'daterangestart' || i.field === 'daterangeend') {
        v = null;
      }
      form[i.field] = new FormControl(v, i.validatorOrOpts as AbstractControlOptions);
    });
    return form;
  }

  /**
   * Observable function to detect changes and enable submit buttons
   * @param ctx - The component where the function is called
   */
  /**observeForms(ctx) {

    ctx.form.valueChanges.subscribe((i) => {
      setTimeout(() => {
        let flag = false;
        Object.keys(i).forEach(j => {
          if (i[j]) {
            if (ctx.form.get(j).dirty && !flag) {
            
              ctx.enableActivationBtn = !ctx.error[j].error && ctx.error[j].change;
              if (!ctx.enableActivationBtn) {
                flag = true;
              }
            }
          }
        });
      }, 500);
    });
  }*/

  /**
   * This function provides an updated error initialization based on props model for formFields
   * @param form -  the form array containing form objects
   * @param error -  error array to hold the input error states
   */
  errorInitialization = (form, error) => {
    form.forEach(i => {
      error[i.field] = {error: false, change: false, msg: ''};
    });
  }

  /**
   * This function return the color name based on a list
   * @param id - the id_color to match
   * @param colorList - the list of color objects to display
   */
  displayColorName(id: number, colorList: any[]): string {
    for (const c of colorList) {
      if (c.hasOwnProperty('id_color')) {
        if (c.id_color === id) {
          return c.colorname;
        }
      }
    }
    return '';
  }

  displayVehicleClassName(id: number, classList: any[]): string {
    for (const c of classList) {
      if (c.hasOwnProperty('id_vehicle_class')) {
        if (c.id_vehicle_class === id) {
          return c.class_name;
        }
      }
    }
    return '';
  }
  displayVehicleTypeName(id: number, typeList: any[]): string {
    for (const c of typeList) {
      if (c.hasOwnProperty('id_vehicle_type')) {
        if (c.id_vehicle_type === id) {
          return c.type_name;
        }
      }
    }
    return '';
  }
  displayAppraiserName(id: number, appraiserList: any[]): string {
    for (const c of appraiserList) {
      if (c.hasOwnProperty('id_as_appraiser')) {
        if (c.id_as_appraiser === id) {
          return `${c.nameperson} ${c.lastnamepersonfather} ${c.lastnamepersonmother}`;
        }
      }
    }
    return '';
  }

  displayBrandName(id: number, brandList: any[]): string {
    for (const b of brandList) {
      if (b.hasOwnProperty('id_brand')) {
        if (b.id_brand === id) {
          return b.brandname;
        }
      }
    }
    return '';
  }
}
