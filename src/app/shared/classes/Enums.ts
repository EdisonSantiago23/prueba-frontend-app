export const REGEX_DICTIONARY = {
  password: /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$#@$!%*?&-_])[A-Za-z\d$@$#!%*?&-_]{8,}/,
  numericMonetary: /^[0-9]{1,5}(\.[0-9]{2})?$/,
  numericQuantity: /^\d+(\.\d{1,2})?$/,
  names: /[a-zñáéíóúA-ZÑ][^#&<>\"~;$^%{}?\d]{1,20}$/,
  fullname: /[a-zñáéíóúA-ZÑ][^#&<>\"~;$^%{}?\d]+(?!\d)[a-zñáéíóúA-ZÑ]+$/,
  lastname: /((?!\d)[a-zñáéíóúA-ZÑ]{1,20}$)/,
  secondLastname: /((?!\d)[a-zñáéíóúA-ZÑ]{1,20}$)|(.)/,
  cellphoneEcu: /^[09]\d{9}$/,
  telephoneEcu: /^[0][2-7]\d{7}$/,
  userId: /^\d{10}$/,
  userRuc: /^\d{13}$/,
  userPassport: /([a-zA-Z\d]){7,10}/,
  alphanumeric: /^[a-z0-9]+$/i,
  threeDigitInt: /^[1-9]\d{0,2}$/
};

export const VALIDATOR_ENUM = {
  required_email: 'Ingresa el email',
  required_emailBill: 'Ingresa el email para enviar la factura',
  required_password: 'Ingresa la contraseña',
  required_passwordconfirm: 'Confirme la contraseña',
  required_passwordCurrent: 'Ingresa la contraseña actual',
  required_repeatPassword: 'Ingresa la contraseña',
  required_address: 'Ingresa una dirección válida',
  required_name: 'Ingrese el nombre',
  required_fullName: 'Ingrese el nombre completo',
  required_identification: 'Ingresa número de cédula o RUC',
  required_businessName: 'Ingresa tu nombre o razón social',
  required_phone: 'Ingresa un número de teléfono',
  required_celphone: 'Ingresa tu número de celular',
  required_celphoneBill: 'Ingresa tu número de teléfono o celular',


  required_id_category: 'La categoría es requerida',
  required_id_provider: 'El proveedor es requerido',

  required_identificationtype: 'Seleccione el tipo de identificación',
  required_identify: 'Ingrese el número de identificación',
  required_civilstatus: 'Seleccione el estado civil de la persona',
  required_creditinstnumber: 'Ingrese el # Operación',
  required_financedvalue: 'Ingrese el Valor Financiado',
  required_entrypercentage: 'Ingrese el % de entrada',
  required_province: 'Ingrese la Provincia',
  required_provincePerson: 'Ingrese la Provincia',
  required_city: 'Ingrese la Ciudad',
  required_cityPerson: 'Ingrese la Ciudad',
  required_creditdatestart: 'Ingrese la fecha de inicio del crédito',
  required_creditdateend: 'Ingrese la fecha de fin del crédito',
  required_vehiclecost: 'Ingrese el costo del vehículo',
  required_nameperson: 'Ingresa el nombre',
  required_lastnameperson_first: 'Ingrese el apellido paterno de la persona',
  required_lastnameperson_second: 'Ingrese el apellido materno de la persona',
  required_product: 'Seleccione el producto',
  required_nationality: 'Seleccione una nacionalidad',


  required_nameperson_other: 'Ingresa el nombre',
  required_lastnameperson_other_first: 'Ingrese el apellido paterno de la persona',
  required_lastnameperson_other_second: 'Ingrese el apellido materno de la persona',
  required_identificationtype_other: 'Seleccione el tipo de identificación',
  required_identify_other: 'Ingrese el número de identificación',
  required_phone_other: 'Ingresa un número de teléfono',
  required_email_other: 'Ingresa el email',
  required_address_other: 'Ingresa una dirección válida',
  required_nationality_other: 'Seleccione una nacionalidad',
  required_province_other: 'Ingrese la Provincia',
  required_id_city_other: 'Ingrese la Ciudad',
  required_brand: 'Seleccione una marca de vehículo',
  required_modelvehicle: 'Seleccione un modelo de vehículo o ingrese uno nuevo',
  required_year: 'Seleccione el año del vehículo',
  required_licenseplate: 'Ingrese el número de placa / RAMV / CAMV',
  required_chasisnumber: 'Ingrese el número de chasis del vehículo',
  required_motornumber: 'Ingrese el número de motor del vehículo',
  required_color: 'Seleccione el color del vehículo',
  required_billdate: 'Ingrese la fecha de la factura',

  required_file_identify: 'El documento de identidad es requerido',
  required_file_contractbill: 'La factura o el contrato de compraventa son requeridos',

  operation_exists: 'Existe un contrato generado con el mismo número de operación.',
  date_range: 'El rango de fechas seleccionado no es válido, seleccione un rango mayor o igual a un año.',

  name: 'Ingrese nombres o un nombre válido',
  fullName: 'Ingrese nombres y apellidos válidos',
  lastname: 'Ingrese un apellido válido',
  email: 'Ingresa un email válido',
  password: 'Debe contener al menos 8 caracteres, mayúsculas, números y símbolos',
  samePassword: 'Las contraseñas deben ser iguales',
  url: 'Ingrese una url válida',
  moneyQuantity: 'El valor debe ser un número de 2 a 6 dígitos y puede contener 2 decimales',
  validatedate: 'El valor debe ser un número de 2 a 6 dígitos y puede contener 2 decimales',

  numericQuantity: 'El valor debe ser un número y puede contener 2 decimales',
  min: 'El valor debe ser mayor a 1',
  max: 'El valor no debe exceder el',
  userId: 'Ingrese una cédula válida',
  userRuc: 'Ingrese un RUC válido',
  userPassport: 'Ingrese un número de pasaporte válido',
  telephone: 'Ingrese un número de teléfono fijo válido y con código de provincia: 023456789',
  cellphone: 'Ingrese un número de celular válido',

  required_postModel: 'El nombre del modelo es requerido',
  required_institutionname: 'Ingrese el nombre de la institución',
  required_institutiontype: 'Seleccione el tipo de institución',
  required_brandname: 'El nombre de la marca es requerido',
  required_country: 'El país de origen de la marca es requerido',
  required_colorname: 'El nombre del color es requerido',
  required_institution: 'La institución del usuario es requerida',
  required_role: 'El rol del usuario es requerido',
  alphanumeric: 'Solo se permiten caracteres alfanuméricos',
  duplicated_id: 'La identificación se encuentra duplicada',
  maxFile: 'El tamaño del archivo debe estar por debajo de 8 MB',

  // Service activation messages
  required_default_start_date: 'La fecha de inicio de mora es requerida',
  required_fees_paid: 'El número de cuotas pagadas es requerido',
  required_expected_fees: 'El número de cuotas esperadas es requerido',
  required_outstanding_fees: 'El número de cuotas vencidas es requerido',
  required_outstanding_capital: 'El monto de captial pendiente es requerido',
  required_pending_interest: 'El monto de interés pendiente es requerido',
  required_last_payment_date: 'La última fecha de pago es requerida',
  required_note: 'La descripción de la activación es requerida',
  required_cuv_cost: 'El costo del CUV es requerido',
  required_id_province: 'La provincia de custodia es requerida',
  required_id_city: 'La ciudad de custodia es requerida',
  required_fines_cost: 'El monto de multas es requerido',
  required_vehicle_arrangements: 'Los costos de reparación son requeridos',
  required_documentary_cost: 'El costo de la documentación es requerida',
  required_transfer_cost: 'El costo de traslado es requerido',
  required_total_value_settlement: 'El valor de liquidación es requerido',
  required_ifi_payment_date: 'La fecha de pago es requerida',
  required_returned_value: 'El monto devuelto es requerido',
  required_appraisal_cost: 'El costo del avalúo es requerido',
  required_id_vehicle_class: 'La clase de vehículo es requerido',
  required_id_vehicle_type: 'El tipo de vehículo es requerido',
  required_notarial_charges_value: 'Los gastos notariales son requeridos',
  required_sales_commission: 'La comisión en ventas es requerida',
  required_sanitation_date: 'La fecha de saneamiento es requerida',
  required_appraisal_date: 'Fecha de avalúo es requerida',
  numericFee: 'El valor debe ser un número entre 1 y 75',
  numericPaidFee: 'El valor debe ser un número entre 0 y 74',
  minlength: 'La nota debe ser de al menos 150 caracteres',
  feesRange: 'El valor de cuotas pagadas debe ser menor al valor de cuotas previstas',
  // Service activation document forms
  required_opauto_rep_position: 'El cargo del representante OA es requerido',
  required_opauto_rep_name: 'El nombre del representante OA es requerido',
  required_inst_rep_position: 'El cargo del representate IFI es requerido',
  required_inst_rep_name: 'El nombre del representate IFI es requerido',
  required_breakdown: 'Ingrese el desgloce del valor total ejecutado',
  required_contract_number: 'El número de contrato es requerido',
  required_total_executed_value: 'El valor total ejecutado es requerido',
  required_cert_resumen: 'Ingrese el resumen del certificado',
  required_id_color: 'El color del vehículo es requerido',

  // Labour Risk
  required_genericArray: 'El campo es requerido',
  required_date: 'La fecha es requerida',

  // Expected Losses
  required_estimated_delay_rate: 'La tasa de mora estimada es requerida',
  required_probability_no_recovery: 'La probabilidad de no recuperación es requerida',
  required_price_affection: 'La afectación al precio es requerida',
  required_alleged_expenses: 'Los supuestos gastos son requeridos',
  morationValue: 'La suma de la distribución de la mora y la tasa de mora estimada debe ser igual',
  fileTypeCsv: 'El documento debe estar en formato CSV',

  // Appraisal request form
  required_appointmentDate: 'La fecha de avalúo es requerida',
  required_appointmentTime: 'La hora de avalúo es requerida',
  required_contactName: 'El contacto de avalúo es requerido',
  required_contactPhone: 'El teléfono de contacto es requerido',
  required_appointmentAddress: 'La dirección de avalúo es requerida',
  required_appraiser: 'El avaluador es requerido',
  required_id_province_appraisal: 'La provincia de avalúo es requerida',
  required_id_city_appraisal: 'La ciudad de avalúo es requerida',

  // Appraisal update form
  required_commercialValue: 'El valor comercial es requerido',
  required_opportunityValue: 'El valor de oportunidad es requerido',
  required_amountOwed: 'El valor adeudado es requerido',
  required_comment: 'El comentario es requerido',

  // Commerce form
  required_vehicleType: 'Seleccione el tipo de vehículo',
  required_vehicleSubtype: 'Selecciones el subtipo de vehículo',
  required_mileage: 'Ingrese el kilometraje de vehículo',
  required_vehiclePrice: 'Ingrese el precio de venta a anunciar',
  required_adDescription: 'La descripción del vehículo es requerida',
  required_model: 'Ingrese el modelo del vehículo',
  required_vehicleTransmission: 'Seleccione el tipo de transmisión del vehículo',
  required_vehicleStatus: 'Seleccione el estado en el cual se encuentra el vehículo',
  required_fuelType: 'Seleccione el tipo de combustible del vehículo',
  required_vehicleBodywork: 'Seleccione el tipo de chasis del vehículo',

  // Appraiser form
  required_lastnamepersonfather: 'Ingrese el apellido paterno',
  required_lastnamepersonmother: 'Ingrese el apellido materno',
  required_cellphone: 'Ingrese el número de celular',

  // Institution Entity
  required_disclaimer: 'Los terminos y condiciones son requeridos',
  required_contractdate: 'La fecha de contrato es requerida',

  // Score Configuration
  required_last_score: 'El valor de score es requerido',
  required_last_term: 'El plazo es requerido',
  required_first_score: 'El valor de score es requerido',
  required_first_term: 'El plazo es requerido',
  required_new_score: 'El valor de score es requerido',
  required_new_term: 'El plazo es requerido',
  scoreRange: 'El valor de score insertado no es válido',
  termRange: 'El plazo insertado no es válido',
  entryRange: 'La entrada insertada no es válida',
  required_last_entry: 'La entrada es requerida',
  required_first_entry: 'La entrada es requerida',
  required_new_entry: 'La entrada es requerida',
  notNumber: 'El valor debe ser un número con 2 decimales',
  notInteger: 'El valor debe ser un número entero mayor a 1 sin decimales',
  required_activityName: 'El nombre de la actividad es requerido',
  required_homologation: 'El tipo de riesgo es requerido',
  required_riskToMove: 'El tipo de riesgo a asignar es requerido',
  sum_error: 'La sumatoria de configuración debe ser igual a 100',
  notZero: 'El valor debe ser mayor a cero',
  required_newScore: 'El valor de score es requerido',
  required_newCode: 'El código ciiu es requerido',
  maxlength: 'La longitud permitida es'
};

const imgUrlLogin = '/assets/img/login/';
const imgUrlLanding = '/assets/img/landing/';
const imgUrlClients = '/assets/img/clientes/';
const iconsUrl = '/assets/icons/';
const imgUrlUser = '/assets/img/users/';
const btnUrl = '/assets/buttons/';
const dataMissing = iconsUrl + 'data-missing/';
const svgUrl = '/assets/img/SVG/';
const imgUrlLogo = '/assets/img/Logos/';

export const ASSET_SRC = {
  loginBackground: imgUrlLogin + 'login-background.jpg',
  loginBackgroundSrcset: imgUrlLogin + 'login-background@2x.jpg 2x,' + imgUrlLogin + 'login-background@3x.jpg 3x',
  loginLogoVertical: imgUrlLogin + 'angular.png',
  loginLogoVerticalSrcset: imgUrlLogin + 'login-logo-vertical-oa@2x.png 2x, ' + imgUrlLogin + 'login-logo-vertical-oa@3x.png 3x',
  loginArrowBack: iconsUrl + 'arrow-left-circle.svg',
  userPlaceholder: imgUrlUser + 'placeholder.png',
  wizardGray: iconsUrl + 'wizard/wizard-process-icon-gray.svg',
  wizardActive: iconsUrl + 'wizard/wizard-process-icon-active.svg',
  wizardCompleted: iconsUrl + 'wizard/wizard-process-icon-completed.svg',
  nextBtn: btnUrl + 'wizard/next-btn.svg',
  clipboardDataMissing: dataMissing + 'data-missing-icon.svg',
  callBackground: imgUrlLanding + 'call-background.jpg',
  callBackgroundSrcset: imgUrlLanding + 'call-background@2x.jpg 2x, ' + imgUrlLanding + 'call-background@3x.jpg 3x',
  landingCar: imgUrlLanding + 'landing-car.png',
  landingCarSrcset: imgUrlLanding + 'landing-car@2x.png 2x, ' + imgUrlLanding + 'landing-car@3x.png 3x',
  backgroundWorks: svgUrl + 'background-landing.svg',
  howItWorks: {
    step_1: svgUrl + 'howItWorks/step-1.svg',
    step_2: svgUrl + 'howItWorks/step-2.svg',
    step_3: svgUrl + 'howItWorks/step-3.svg',
    step_4: svgUrl + 'howItWorks/step-4.svg',
  },
  beneficiosIfi: imgUrlLanding + 'beneficios-ifi.png',
  beneficiosIfiSrcset: imgUrlLanding + 'beneficios-ifi@2x.png 2x, ' + imgUrlLanding + 'beneficios-ifi@3x.png 3x',
  beneficiosCliente: imgUrlLanding + 'beneficios-clientes.png',
  beneficiosClienteSrcset: imgUrlLanding + 'beneficios-clientes@2x.png 2x, ' + imgUrlLanding + 'beneficios-clientes@3x.png 3x',
  experienciaBackground: imgUrlLanding + 'experiencia-background.jpg',
  experienciaBackgroundSrcset: imgUrlLanding + 'experiencia-background@2x.jpg 2x, ' + imgUrlLanding + 'experiencia-background@3x.jpg 3x',
  contactoBackground: svgUrl + 'background-contacto.svg',
  experienceGreen: svgUrl + 'background-green-experience.svg',
  experienceCar: imgUrlLanding + 'carro-experiencia.png',
  experienceCarSrcset: imgUrlLanding + 'carro-experiencia@2x.png 2x, ' + imgUrlLanding + 'carro-experiencia@3x.png 3x',
  assaLogo: imgUrlClients + 'logo-assa.png',
  assaLogoSrcset: imgUrlClients + 'logo-assa@2x.png 2x, ' + imgUrlClients + 'logo-assa@3x.png 3x',
  capitalLogo: imgUrlClients + 'logo-capital.png',
  capitalLogoSrcset: imgUrlClients + 'logo-capital@2x.png 2x, ' + imgUrlClients + 'logo-capital@3x.png 3x',
  cfcLogo: imgUrlClients + 'logo-cfc.png',
  cfcLogoSrcset: imgUrlClients + 'logo-cfc@2x.png 2x, ' + imgUrlClients + 'logo-cfc@3x.png 3x',
  imbautoLogo: imgUrlClients + 'logo-imbauto.png',
  imbautoLogoSrcset: imgUrlClients + 'logo-imbauto@2x.png 2x, ' + imgUrlClients + 'logo-imbauto@3x.png 3x',
  vallejoLogo: imgUrlClients + 'logo-vallejo.png',
  vallejoLogoSrcset: imgUrlClients + 'logo-vallejo@2x.png 2x, ' + imgUrlClients + 'logo-vallejo@3x.png 3x',
  oaLogo: imgUrlLogo + 'logotipo-oa-footer.png',
  oaLogoSrcset: imgUrlLogo + 'logotipo-oa-footer@2x.png 2x, ' + imgUrlLogo + 'logotipo-oa-footer@3x.png 3x',
  locationIcon: svgUrl + 'icons/location-icon.svg',
  callIcon: svgUrl + 'icons/call-icon.svg',
  mailIcon: svgUrl + 'icons/mail-icon.svg',
  fbIcon: svgUrl + 'icons/facebook-icon.svg',
  liIcon: svgUrl + 'icons/linkedin-icon.svg',
  youtubeIcon: svgUrl + 'icons/youtube-icon.svg',
  imgPlaceholder: svgUrl + 'no_preview.svg'
};

export const STORAGE_SLUGS = [
  'id_color',
  'id_brand',
  'id_institution',
  'id_user',
  'id_appraiser',
  'id_wizardprocess',
  'client_full_name',
  'id_civilstatus',
  'filters',
  'vClassList',
  'vTypeList',
  'colorList',
  'classificationList',
  'approvedList',
  'ptx'
];

export const CONTRACT_UPDATE = [
  // CFC - 0
  ['creditinstnumber',
  'creditdatestart',
  'creditdateend',
  'nameperson',
  'lastnameperson_first',
  'lastnameperson_second',
  'identify',
  'email',
  'address',
  'celphone',
  'nameperson_other',
  'lastnameperson_other_first',
  'lastnameperson_other_second',
  'identify_other',
  'email_other',
  'address_other',
  'brand',
  'modelvehicle',
  'year',
  'licenseplate',
  'chasisnumber',
  'motornumber'],
  // OTHER - 1
  ['creditinstnumber',
    'creditdatestart',
    'creditdateend',
    'city',
    'nameperson',
    'lastnameperson_first',
    'lastnameperson_second',
    'identify',
    'email',
    'address',
    'phone',
    'celphone',
    'cityPerson',
    'nationality',
    'civilstatus',
    'nameperson_other',
    'lastnameperson_other_first',
    'lastnameperson_other_second',
    'identify_other',
    'nationality_other',
    'identify_other',
    'brand',
    'modelvehicle',
    'year',
    'licenseplate',
    'chasisnumber',
    'motornumber',
    'color',
    'billdate'
  ]
];
