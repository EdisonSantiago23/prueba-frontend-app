import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class NotificationAction {
  private modals: any[] = [];

  add(modal: any) {                                                               // add modal to array of active modals
    const modalOld: any = this.modals.filter(x => x.id === modal.id)[0];
    if (modalOld !== null && modalOld !== undefined) {
      this.remove(modalOld.id);
    }
    this.modals.push(modal);
  }

  remove(id?: string) {                                                           // remove modal from array of active modals
    this.modals = this.modals.filter(x => x.id !== id);
  }

  clear() {
    this.modals = [];
  }

  open(id?: string) {                                                             // open modal specified by id
    const modal: any = this.modals.filter(x => x.id === id)[0];
    modal.open();
  }

  close(id?: string) {                                                            // close modal specified by id
    const modal: any = this.modals.filter(x => x.id === id)[0];
    modal.close();
  }
}
