import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { LocalStorageCommon } from './LocalStorageCommon';
import { Console } from 'console';
declare var $: any;

export class Services {

  protected url = environment.baseUrl;
  protected component: string;
  protected http: HttpClient;
  protected httpOptions: any;
  protected httpOptionsFile: any;
  protected authBasic: boolean;
  protected storage: any;

  constructor(component: string, http: HttpClient, authBasic: boolean = false) {
    this.component = component;
    this.http = http;
    this.storage = new LocalStorageCommon();
    this.authBasic = authBasic;
    this.resolvedSchema();
  }

  resolvedSchema(currentSchema: string = null, contentType: string = 'application/x-www-form-urlencoded') {
    let schema = currentSchema;
    if (schema === null) {
      schema = this.storage.get('schema_db', 'normal');
      if (schema === null || schema === '' || !schema) {
        schema = 'public';
      }
    }

    let authorization = 'Bearer ' + this.storage.get('auth_token', 'normal');
    if (this.authBasic) {
      authorization = 'Basic ';
    }

    this.httpOptions = {
      headers: new HttpHeaders({
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': 'true',
        'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, OPTIONS',
        'Access-Control-Allow-Headers': '*',
        Authorization: authorization,
        'schema-db': schema,
      })
    };
    this.httpOptions.headers.set('Content-Type', contentType);
  }
  observeResponse() {
    this.httpOptions.observe = 'response';
  }
  // -----------------------Consultas a la api-----------------------------------------

  get(filter: number | object | string, partUrl: string = null, schema: string = null): Observable<any> {
    this.resolvedSchema(schema);

    let resultURL = this.url + this.component;

    if (partUrl !== null) {
      resultURL += '/' + partUrl;
    }
    if (typeof filter === 'number' || typeof filter === 'string') {
      resultURL += '/' + filter;
    } else {
      var qStr = $.param(filter);
      resultURL += '?' + qStr;

    }
    return this.http.get(resultURL, this.httpOptions);


  }

  post(data: any = null, pageCurrentPaginate: number = null, filter: number | string | object = null, partUrl: string = null, schema: string = null): Observable<any> {
    this.resolvedSchema();
    let resultURL = this.url + this.component;
    if (partUrl !== null) {
      resultURL += '/' + partUrl;
    }
    let options = {
      headers: new HttpHeaders({
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': 'true',
        'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, OPTIONS',
        'Access-Control-Allow-Headers': '*',
        Authorization: 'Bearer eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6ImFuYS5hcnRldGEiLCJuYmYiOjE2Mjk5NTI3MTEsImV4cCI6MTYyOTk1ODExMSwiaWF0IjoxNjI5OTUyNzExLCJpc3MiOiJodHRwOi8vYmlzc3BydWViYXMua2lhZWN1YWRvci5jb20uZWMvIiwiYXVkIjoiaHR0cDovL2Jpc3NwcnVlYmFzLmtpYWVjdWFkb3IuY29tLmVjLyJ9._xbAo5WOOyjBY2pguybFGWAIyXpI2tIATTH7_xGgoAVZwxRA04M2PXPqGPmqbTKDnmxbdJ8E4RDyTX9eCO3bEg',
        'schema-db': schema,
      }).set('Content-Type', 'application/x-www-form-urlencoded')
    };
    return this.http.post(resultURL, data.toString(), options);
  }
  put(id: any, data: any, schema: string = null, partUrl: string = null): Observable<any> {
    this.resolvedSchema(schema);
    let resultURL = this.url + this.component;

    if (partUrl !== null) {
      resultURL += '/' + partUrl;
    }
    return this.http.put(resultURL + id, data, this.httpOptions);

  }
  delete(id: any, schema: string = null, pathUrl: string = ''): Observable<any> {

    this.resolvedSchema(schema);
    if (pathUrl.length) {
      pathUrl = pathUrl + '/';
    }
    console.log(this.url + this.component + '/' + pathUrl + id);
    return this.http.delete(this.url + this.component + '/' + pathUrl + id, this.httpOptions);

  }
}
