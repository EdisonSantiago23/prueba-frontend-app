import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'formatMoneyEc'
})
export class FormatMoneyEcPipe implements PipeTransform {

  transform(value: any, ...args: any[]): any {
    let newResult = value.replace(/,/g, '__COMMA__');
    newResult = newResult.replace(/\./g, ',');
    newResult = newResult.replace(/__COMMA__/g, '.');
    return newResult;
  }

}
