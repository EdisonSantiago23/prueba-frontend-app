import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {LocalStorageCommon} from 'src/app/shared/classes/LocalStorageCommon';

declare var $: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  constructor(private router: Router, private localStorageCommon: LocalStorageCommon) {
  }

  ngOnInit() {
    const token = this.localStorageCommon.get('auth_token', 'normal');
    if (token === null) {
      this.router.navigate(['/']);
    }
  }

}
