import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {AuthComponent} from './auth.component';
import {RouterTestingModule} from '@angular/router/testing';
import {AuthRoutingComponent} from './auth-routing.component';

declare var $: any;

describe('AuthComponent', () => {
  let component: AuthComponent;
  let fixture: ComponentFixture<AuthComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [AuthComponent],
      imports: [RouterTestingModule, AuthRoutingComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuthComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
