import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {SigninComponent} from './signin/signin.component';

export const routesAuth: Routes = [
  {path: '', component: SigninComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routesAuth)],
  exports: [RouterModule]
})
export class AuthRoutingComponent {
}
