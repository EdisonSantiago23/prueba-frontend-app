import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { SigninService } from '../../../services/Auth/signin/signin.service';
import { LocalStorageCommon, ASSET_SRC, NotificationAction, customValidatorHandler, Helpers } from '../../../../shared/classes';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { DashboardService } from '../../../services/workflow/dashboard/dashboard.service';
import { environment } from '../../../../../environments/environment';
import { takeUntil } from 'rxjs/operators';

declare var $: any;

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss']
})
export class SigninComponent implements OnInit, OnDestroy {

  form: FormGroup;
  loggedUser: any;
  logoSrc = ASSET_SRC.loginLogoVertical;
  returnButton = ASSET_SRC.loginArrowBack;
  rememberToggle = false;
  error: any;
  scoreRange = [];
  renderSignin = false;
  bannerEnv = '';
  private destroySubscription$ = new Subject();

  constructor(private formBuilder: FormBuilder, private signIn: SigninService, private notification: NotificationAction,
    private localStorageCommon: LocalStorageCommon, private router: Router,
    private ngxService: NgxUiLoaderService, private helper: Helpers, private dashboardService: DashboardService) {
  }

  ngOnInit() {
    $(() => {
      $('#launcher-frame').hide();
    });

    this.bannerEnv = environment.banner;

    this.loggedUser = this.localStorageCommon.get('auth_user', 'json');
    const token = this.localStorageCommon.get('auth_token', 'normal');

    if (token) {
      const isValid = !this.helper.isExpired(token);
      if (isValid) {
        this.router.navigate(['/admin/workflow/dashboard']).catch((e) => {
          console.error((e));
          // throw new Error(e);
          if (e) {
            this.localStorageCommon.destroy('auth_user');
            this.router.navigate(['/']).catch((res) => {
            });
          }
        });
      } else {
        this.renderSignin = true;
      }
    } else {
      this.renderSignin = true;
    }
    this.errorInit();
    this.form = this.formBuilder.group({
      email: new FormControl('', { validators: [Validators.email, Validators.required] }),
      password: new FormControl('', Validators.required),
      rememberLogin: new FormControl(false)
    });
  }


  errorInit = () => {
    this.error = {
      email: { error: false, change: false, msg: '' },
      password: { error: false, change: false, msg: '' },
    };
  }

  actions = () => {
    this.ngxService.start();

    let body = new URLSearchParams();
    body.set('Dt1', this.form.value.email);
    body.set('Dt2', this.form.value.password);
    this.signIn.post(body, null, 'VmLogOut').pipe(takeUntil(this.destroySubscription$)).subscribe(

      (response) => {

        if (response.success) {
          console.log(response);
        } else {
          this.ngxService.stop();
        }
      },
      (error) => {
        console.log(error);
        this.ngxService.stop();
      }
    );
  }

  action = () => {

    this.ngxService.start();

    let body = new URLSearchParams();
    body.set('Dt1', this.form.value.email);
    body.set('Dt2', this.form.value.password);
    this.signIn.post(body).pipe(takeUntil(this.destroySubscription$)).subscribe(

      (response) => {

        if (response.success) {
          this.localStorageCommon.store('auth_token', response.token, 'normal');
          this.localStorageCommon.store('auth_user', response.user, 'json');
          this.localStorageCommon.store('auth_perm', response.permissions, 'json');
          this.router.navigate(['/admin/workflow/dashboard']).catch((e) => {

            if (e) {
              this.localStorageCommon.destroy('auth_user');
              this.router.navigate(['/']).catch((res) => {
              });
            }
          });
        } else {
          this.ngxService.stop();
        }
      },
      (error) => {

        console.log(error);
        this.ngxService.stop();
      }
    );
  }
  ngOnDestroy(): void {
    this.destroySubscription$.next();     // trigger the unsubscribe
    this.destroySubscription$.complete(); // finalize & clean up the subject stream
  }

}

