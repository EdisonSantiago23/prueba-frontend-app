import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ASSET_SRC } from 'src/app/shared/classes/Enums';
import { Helpers } from 'src/app/shared/classes/Helpers';

declare var $: any;

@Component({
  selector: 'app-workflow',
  templateUrl: './workflow.component.html',
  styleUrls: ['./workflow.component.scss']
})


export class WorkflowComponent implements OnInit {

  institution: string;
  userName: string;
  userInitials: string;
  userPlaceholderImg = ASSET_SRC.userPlaceholder;

  constructor( private router: Router,public helper: Helpers) {}

  ngOnInit() {
    setTimeout(() => {
      $('#launcher-frame').css('left', '.5rem');
    }, 1000);

    this.complete();
  }


  complete() {
    this.userName = "Prueba";
    this.userInitials = "EB";
    $(() => {
      $('.isoNav').hide();
      $('#menu-toggle').click((e) => {
        e.preventDefault();
        $('#wrapper').toggleClass('toggled');
        $('#page-content-wrapper').toggleClass('dynamic-container');
        $('.expanded').toggle(500, 'swing');
        $('.isoNav').toggle(500, 'swing');
        $('#launcher-frame').contents().find('.launcher-text').toggle();
      });


      $(window).resize(() => {
        if ($(window).width() <= 991) {
          $('#wrapper').removeClass('toggled');

          $('#page-content-wrapper').addClass('dynamic-container');
          $('.expanded').hide(500, 'swing');
          $('.isoNav').show(500, 'swing');
          $('#launcher-frame').contents().find('.launcher-text').hide(500, 'swing');
        } else {
          $('#wrapper').addClass('toggled');
          $('#page-content-wrapper').removeClass('dynamic-container');
          $('.expanded').show(500, 'swing');
          $('.isoNav').hide(500, 'swing');
          $('#launcher-frame').contents().find('.launcher-text').show(500, 'swing');
        }
      });
    });
  }


  logOut() {
    this.router.navigate(['/']).catch(e => {
      console.error(e);
    });
  }
}
