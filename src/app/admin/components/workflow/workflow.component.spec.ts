import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import {WorkflowComponent} from './workflow.component';
import {RouterTestingModule} from '@angular/router/testing';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {NgxPaginationModule} from 'ngx-pagination';
import {AngularMyDatePickerModule} from 'angular-mydatepicker';
import {NotificationComponent} from '../../../shared/components/notification/notification.component';
import {NgxUiLoaderModule} from 'ngx-ui-loader';

describe('WorkflowComponent', () => {
  let component: WorkflowComponent;
  let fixture: ComponentFixture<WorkflowComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule, HttpClientTestingModule, NgxPaginationModule, AngularMyDatePickerModule, NgxUiLoaderModule],
      declarations: [WorkflowComponent, NotificationComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkflowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
