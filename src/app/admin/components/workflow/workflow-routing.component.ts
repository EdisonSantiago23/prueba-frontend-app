import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { MapasComponent } from './mapas/mapas.component';

export const routesWorkflow: Routes = [
  { path: '', component: DashboardComponent },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'mapas', component: MapasComponent },

  { path: '**', redirectTo: '404' },
];

@NgModule({
  imports: [RouterModule.forChild(routesWorkflow)],
  exports: [RouterModule]
})
export class WorkflowRoutingComponent {
}
