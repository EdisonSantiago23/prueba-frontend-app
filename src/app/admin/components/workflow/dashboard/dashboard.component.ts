import { Component, OnDestroy, OnInit } from '@angular/core';
import { Color, Label } from 'ng2-charts';
import {  ChartOptions, ChartType } from 'chart.js';



import { NgxUiLoaderService } from 'ngx-ui-loader';
import { Subject } from 'rxjs';

interface PiechartStruct {
  label: Label[];
  data: number[];
  chartType: ChartType;
  legends: boolean;
  plugins: any[];
  colors: Color[];
  options: ChartOptions;
  title: string;
  service: string;
}


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit, OnDestroy {
  departamentos: [] = [];
  supervisores: [] = [];
  empleados: [] = [];
  donutColors = ['#dbd8e3', '#c2dfa6', '#978db7', '#74a47b', '#828285', '#3d7d41',
    '#87bf48', '#d7e5da', '#b2b2b5', '#6c5a97'];

  statusOptions: ChartOptions = {
    tooltips: {
      backgroundColor: 'rgba(0, 0, 0, 0.9)',
      bodyFontFamily: 'Lato-Regular',
      bodyFontColor: 'rgba(255,255, 255, 0.9)',
      bodyFontSize: 12,
      cornerRadius: 2,
      yPadding: 7,
      mode: 'point'
    },
    responsive: true,
    maintainAspectRatio: false,
    legend: {
      position: 'right',
      labels: {
        usePointStyle: true,
        fontSize: 10
      }
    },
    plugins: {
      datalabels: {
        display: false,
        formatter: (value, ctx) => {
          return ctx.chart.data.labels[ctx.dataIndex];
        },
      },
    },
  };

  pieStatusL: PiechartStruct = {
    label: [],
    data: [],
    chartType: 'doughnut',
    legends: true,
    plugins: [],
    colors: [],
    options: this.statusOptions,
    title: '',
    service: ''
  };

  private destroySubscription$ = new Subject();

  constructor( private ngxService: NgxUiLoaderService,) {
  }

  ngOnInit(): void {
    this.getContadores();

  }
  //funcion para traer todos los datos para hacer un contador en el dasboard
  getContadores() {

    this.pieStatusL.options.legend.position = 'right';
    this.pieStatusL.label = ["Empleados", 'Supervisores', 'Departamentos'];
    const backgroundColor = this.donutColors;
    this.pieStatusL.colors = [{ backgroundColor }];
    setTimeout(() => {
      this.pieStatusL.data = [this.empleados.length,this.supervisores.length,this.departamentos.length];
    }, 2000);

  }
  ngOnDestroy(): void {
    this.destroySubscription$.next();     // trigger the unsubscribe
    this.destroySubscription$.complete(); // finalize & clean up the subject stream
  }
}
