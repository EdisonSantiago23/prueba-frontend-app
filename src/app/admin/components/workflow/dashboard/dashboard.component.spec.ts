import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {DashboardComponent} from './dashboard.component';
import {ReactiveFormsModule} from '@angular/forms';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {AngularMyDatePickerModule} from 'angular-mydatepicker';
import {FormatMoneyEcPipe} from '../../../../shared/pipes/format-money-ec.pipe';
import {NotificationComponent} from '../../../../shared/components/notification/notification.component';
import {NgxUiLoaderModule} from 'ngx-ui-loader';
import {ChartsModule} from 'ng2-charts';

declare var $: any;

describe('DashboardComponent', () => {
  let component: DashboardComponent;
  let fixture: ComponentFixture<DashboardComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [ReactiveFormsModule, RouterTestingModule, HttpClientTestingModule,
        AngularMyDatePickerModule, NgxUiLoaderModule, ChartsModule],
      declarations: [DashboardComponent, FormatMoneyEcPipe, NotificationComponent],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });


  it('should create', () => {
    expect(component).toBeTruthy();
  });

});
