import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AdminRoutingModule } from './admin-routing.module';
import { SharedModule } from '../shared/shared.module';
import { HttpConfigInterceptor } from '../shared/interceptors/httpconfig.interceptor';
import { NgSelectModule } from '@ng-select/ng-select';
import { AngularMyDatePickerModule } from 'angular-mydatepicker';
import { NgxPaginationModule } from 'ngx-pagination';
import { ChartsModule } from 'ng2-charts';
import { NgxUiLoaderModule } from 'ngx-ui-loader';
import { GoogleMapsModule } from '@angular/google-maps';

import { ColorPickerModule } from 'ngx-color-picker';
import {
  AdminComponent,
  AuthComponent,
  SigninComponent,
  WorkflowComponent,
  DashboardComponent,
} from './components';
import { MapasComponent } from './components/workflow/mapas/mapas.component';


@NgModule({
  declarations: [AdminComponent, AuthComponent, SigninComponent, WorkflowComponent,
    DashboardComponent,
    MapasComponent
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule, ReactiveFormsModule,
    SharedModule,
    AdminRoutingModule,
    NgSelectModule,
    AngularMyDatePickerModule,
    NgxPaginationModule,
    ChartsModule,
    GoogleMapsModule,
    NgxUiLoaderModule,
    ColorPickerModule
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: HttpConfigInterceptor, multi: true }
  ]
})
export class AdminModule {
}
