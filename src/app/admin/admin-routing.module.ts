import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {routesAuth} from './components/auth/auth-routing.component';
import {routesWorkflow} from './components/workflow/workflow-routing.component';
import {AuthComponent} from './components/auth/auth.component';
import {WorkflowComponent} from './components/workflow/workflow.component';

const routes: Routes = [
  {path: '', component: AuthComponent, children: routesAuth},
  {path: 'workflow', component: WorkflowComponent, children: routesWorkflow},
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule {
}
