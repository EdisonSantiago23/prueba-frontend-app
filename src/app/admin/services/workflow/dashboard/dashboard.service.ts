import {Injectable} from '@angular/core';
import {Services} from 'src/app/shared/classes';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DashboardService extends Services {

  constructor(http: HttpClient) {
    super('dashboard', http);
  }
}
