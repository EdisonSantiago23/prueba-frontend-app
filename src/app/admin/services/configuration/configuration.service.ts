import { Injectable } from '@angular/core';
import {Services} from '../../../shared/classes';
import {HttpClient} from '@angular/common/http';
import {takeUntil} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ConfigurationService extends Services{

  constructor(http: HttpClient) {
    super('catalogue/configuration', http);
  }

  getLossesConfiguration(ctx) {
    ctx.ngxService.startLoader('workflow-loader');
    this.get('EXPECTED_LOSSES').pipe(takeUntil(ctx.destroySubscription$)).subscribe(
      (response) => {
        if (response.success) {
          ctx.configData = response.data;
          ctx.fillForm();
        } else {
          ctx.showNotification('Ooops!', 'fas fa-info-circle', response.message, 'info');
        }
      },
      (error) => {
        console.error(error);
      },
      () => {
        ctx.ngxService.stopLoader('workflow-loader');
      }
    );
  }

  updateLossesConfiguration(data, ctx) {
    ctx.ngxService.startLoader('workflow-loader');
    this.put('EXPECTED_LOSSES', data).pipe(takeUntil(ctx.destroySubscription$)).subscribe(
      (response) => {
        if (response.success) {
          ctx.showNotification('Éxito!', 'fas fa-check', response.message, 'success');
          for (const k of Object.keys(ctx.error)){
            switch (k) {
              case 'moration_distribution':
                ctx.error.moration_distribution.forEach(i => {
                  i.change = false;
                });
                break;
              default:
                ctx.error[k].change = false;
                break;
            }
          }
        }
      },
      (error) => {
        console.error(error);
      },
      () => {
        ctx.ngxService.stopLoader('workflow-loader');
      }
    );
  }

  getUndefinedConfiguration(ctx) {
    ctx.ngxService.startLoader('workflow-loader');
    this.get('UNDEFINED_VARIABLES').pipe(takeUntil(ctx.destroySubscription$)).subscribe(
      (response) => {
        if (response.success) {
          ctx.helper.errorInitialization(ctx.undefForm, ctx.error);
          ctx.configData = response.data.configvalue;
          ctx.configData.prevStateConcurrence = ctx.configData.concurrence;
          ctx.form.get('concurrence').setValue(ctx.configData.concurrence);
          ctx.configData.prevStateEntry = ctx.configData.entry;
          ctx.form.get('entry').setValue(ctx.configData.entry);
          ctx.configData.prevStateMonths = ctx.configData.amount_months;
          ctx.form.get('amount_months').setValue(ctx.configData.amount_months);
          ctx.form.updateValueAndValidity();
          ctx.showUndefined = true;
        }
      },
      (error) => {
        console.error(error);
      },
      () => {
        ctx.ngxService.stopLoader('workflow-loader');
      });
  }

  putUndefinedConfiguration(data, ctx) {
    ctx.ngxService.startLoader('workflow-loader');
    this.put('UNDEFINED_VARIABLES', data).pipe(takeUntil(ctx.destroySubscription$)).subscribe(
      (response) => {
        if (response.success) {
          ctx.showNotification('Éxito!', 'fas fa-check', response.message, 'success');
        } else {
          ctx.showNotification('Ooops!', 'fas fa-info-circle', response.message, 'info');
        }
      },
      (error) => {
        console.error(error);
      },
      () => {
        this.getUndefinedConfiguration(ctx);
      });
  }

  getDefaultVarsList(ctx) {
    ctx.ngxService.startLoader('workflow-loader');
    this.get('DEFAULT_VARIABLES').pipe(takeUntil(ctx.destroySubscription$)).subscribe(
      (response) => {
        if (response.success) {
          ctx.getDefaultVars.clear();
          ctx.errorInit();
          ctx.defaultVarsList = response.data.configvalue.marcas;
          ctx.defaultKeys = Object.keys(ctx.defaultVarsList);
          for (let i = 0; i < ctx.defaultKeys.length; i++) {
            ctx.setDefaultVars();
            ctx.getDefaultVars.at(i).setValue(ctx.defaultVarsList[ctx.defaultKeys[i]].value);
            ctx.getDefaultVars.at(i).updateValueAndValidity();
            ctx.defaultVarsList[ctx.defaultKeys[i]].prevStateValue = ctx.defaultVarsList[ctx.defaultKeys[i]].value;
          }
          ctx.showDefault = true;
        }
      },
      (error) => {
        console.error(error);
      },
      () => {
        ctx.ngxService.stopLoader('workflow-loader');
      });
  }


  putDefaultVars(data, ctx) {
    ctx.ngxService.startLoader('workflow-loader');
    this.put('DEFAULT_VARIABLES', data).pipe(takeUntil(ctx.destroySubscription$)).subscribe(
      (response) => {
        if (response.success) {
          ctx.notificationEmitter.emit({
            title: 'Éxito',
            icon: 'fas fa-check',
            message: response.message,
            type: 'success'
          });
        } else {
          ctx.notificationEmitter.emit({
            title: 'Ooops!',
            icon: 'fas fa-info-circle',
            message: response.message,
            type: 'info'
          });
        }
      },
      (error) => {
        console.error(error);
      },
      () => {
        this.getDefaultVarsList(ctx);
      });
  }
}
