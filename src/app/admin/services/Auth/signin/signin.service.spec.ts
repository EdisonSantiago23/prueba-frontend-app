import {TestBed} from '@angular/core/testing';

import {SigninService} from './signin.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';

declare var $: any;

describe('SigninService', () => {
  let service: SigninService;

  beforeEach(() => {
    TestBed.configureTestingModule({imports: [HttpClientTestingModule]});
    service = TestBed.inject(SigninService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
