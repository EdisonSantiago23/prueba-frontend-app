import {Injectable} from '@angular/core';
import {Services} from 'src/app/shared/classes';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class SigninService extends Services {

  constructor(http: HttpClient) {
    super('LgnUsr/', http, true);
  }
}
