/**
 * Commands to compile for dev
 *
 * @see ng serve --configuration=dev
 * @see ng build --configuration=dev
 *
 */
export const environment = {

  production: true,
  name: 'dev',
  baseUrl: 'http://localhost:8080',
  banner: 'Admin pruebas'

};
