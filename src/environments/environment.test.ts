/**
 * Commands to compile for test
 *
 * @see ng serve --configuration=test
 * @see ng build --configuration=test
 *
 */
export const environment = {

  production: false,
  name: 'test',
  baseUrl: 'http://localhost:8080',
  banner: 'Admin Pruebas'

};
