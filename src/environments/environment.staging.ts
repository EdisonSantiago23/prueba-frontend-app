/**
 * Commands to compile for staging
 *
 * @see ng serve --configuration=staging
 * @see ng build --configuration=staging
 */

export const environment = {
  production: true,
  name: 'staging',
  baseUrl: 'http://localhost:8080',
  banner: 'Admin Capacitaciones'
};
