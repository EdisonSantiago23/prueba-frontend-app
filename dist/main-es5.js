(function () {
  function _createForOfIteratorHelper(o, allowArrayLike) { var it = typeof Symbol !== "undefined" && o[Symbol.iterator] || o["@@iterator"]; if (!it) { if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") { if (it) o = it; var i = 0; var F = function F() {}; return { s: F, n: function n() { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }, e: function e(_e) { throw _e; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var normalCompletion = true, didErr = false, err; return { s: function s() { it = it.call(o); }, n: function n() { var step = it.next(); normalCompletion = step.done; return step; }, e: function e(_e2) { didErr = true; err = _e2; }, f: function f() { try { if (!normalCompletion && it["return"] != null) it["return"](); } finally { if (didErr) throw err; } } }; }

  function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

  function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

  function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

  function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

  function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

  function _possibleConstructorReturn(self, call) { if (call && (typeof call === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

  function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

  function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

  function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"], {
    /***/
    "+2bK":
    /*!******************************************************************************!*\
      !*** ./src/app/admin/components/workflow/supervisor/supervisor.component.ts ***!
      \******************************************************************************/

    /*! exports provided: SupervisorComponent */

    /***/
    function bK(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "SupervisorComponent", function () {
        return SupervisorComponent;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _services_workflow_supervisor_supervisor_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ../../../services/workflow/supervisor/supervisor.service */
      "Itk3");
      /* harmony import */


      var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! rxjs */
      "qCKp");
      /* harmony import */


      var ngx_ui_loader__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ngx-ui-loader */
      "gren");
      /* harmony import */


      var xlsx__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! xlsx */
      "EUZL");
      /* harmony import */


      var xlsx__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(xlsx__WEBPACK_IMPORTED_MODULE_4__);
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");
      /* harmony import */


      var _add_supervisor_add_supervisor_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./add-supervisor/add-supervisor.component */
      "dSvv");
      /* harmony import */


      var _edit_supervisor_edit_supervisor_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! ./edit-supervisor/edit-supervisor.component */
      "182n");

      function SupervisorComponent_div_0_button_18_Template(rf, ctx) {
        if (rf & 1) {
          var _r8 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "button", 26);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function SupervisorComponent_div_0_button_18_Template_button_click_0_listener() {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r8);

            var ctx_r7 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);

            return ctx_r7.addSupervisor();
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "i", 27);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2, " \xA0 CREAR SUPERVISOR ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      function SupervisorComponent_div_0_div_20_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 28);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 29);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 30);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 31);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "p", 32);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, " No tienes ning\xFAn supervisor ingresado ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      function SupervisorComponent_div_0_div_21_tr_29_Template(rf, ctx) {
        if (rf & 1) {
          var _r12 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "tr");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "td", 41);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "td", 41);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "td", 41);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "td", 41);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "div", 42);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "div", 43);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "button", 44);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function SupervisorComponent_div_0_div_21_tr_29_Template_button_click_10_listener() {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r12);

            var item_r10 = ctx.$implicit;

            var ctx_r11 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](3);

            return ctx_r11.eliminarsupervisor(item_r10.id);
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](11, "i", 45);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "div", 43);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "button", 44);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function SupervisorComponent_div_0_div_21_tr_29_Template_button_click_13_listener() {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r12);

            var item_r10 = ctx.$implicit;

            var ctx_r13 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](3);

            return ctx_r13.editsupervisor(item_r10);
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](14, "i", 46);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var item_r10 = ctx.$implicit;

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](item_r10.nombre);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](item_r10.apellido);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](item_r10.correo);
        }
      }

      function SupervisorComponent_div_0_div_21_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 33);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "table", 34);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "thead");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "tr");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "th", 35);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "div", 36);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 37);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](7, "Nombre");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "div", 38);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](9, "|");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "th", 39);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "div", 36);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "div", 40);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](13, "Apellido");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "div", 38);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](15, "|");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "th", 39);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "div", 36);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "div", 40);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](19, "Correo");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "div", 38);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](21, "|");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "th", 39);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "div", 36);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "div", 40);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](25, "Acciones");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](26, "div", 38);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](27, "|");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "tbody");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](29, SupervisorComponent_div_0_div_21_tr_29_Template, 15, 3, "tr", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var ctx_r6 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](29);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx_r6.supervisores);
        }
      }

      function SupervisorComponent_div_0_Template(rf, ctx) {
        if (rf & 1) {
          var _r15 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 9);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 10);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 11);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](3, "ngx-ui-loader", 12);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 13);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "h6");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6, "SUPERVISOR");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "div", 14);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "div", 15);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "div", 16);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "a", 17);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "div", 18);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12, "ACCIONES");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "ul", 19);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "li");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "a", 20);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function SupervisorComponent_div_0_Template_a_click_15_listener() {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r15);

            var ctx_r14 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

            return ctx_r14.exportExcel("excel-table", "Reporte de supervisores");
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](16, "Exportar a excel");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "div", 21);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](18, SupervisorComponent_div_0_button_18_Template, 3, 0, "button", 22);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "div", 23);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](20, SupervisorComponent_div_0_div_20_Template, 6, 0, "div", 24);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](21, SupervisorComponent_div_0_div_21_Template, 30, 1, "div", 25);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("loaderId", "supervisor-loader");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](15);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r0.createSupervisor);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r0.supervisores.length == 0);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r0.supervisores.length > 0);
        }
      }

      function SupervisorComponent_tr_12_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "tr");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "td", 41);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "td", 41);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "td", 41);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var item_r16 = ctx.$implicit;

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](item_r16.nombre);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](item_r16.apellido);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](item_r16.correo);
        }
      }

      function SupervisorComponent_app_add_supervisor_13_Template(rf, ctx) {
        if (rf & 1) {
          var _r19 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "app-add-supervisor", 47);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("goBack", function SupervisorComponent_app_add_supervisor_13_Template_app_add_supervisor_goBack_0_listener($event) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r19);

            var ctx_r18 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

            return ctx_r18.reverse($event);
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      function SupervisorComponent_app_edit_supervisor_14_Template(rf, ctx) {
        if (rf & 1) {
          var _r21 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "app-edit-supervisor", 48);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("goBack", function SupervisorComponent_app_edit_supervisor_14_Template_app_edit_supervisor_goBack_0_listener($event) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r21);

            var ctx_r20 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

            return ctx_r20.reverse($event);
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var ctx_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("supervisor", ctx_r3.supervisor);
        }
      }

      var SupervisorComponent = /*#__PURE__*/function () {
        function SupervisorComponent(supervisorService, ngxService) {
          var _this = this;

          _classCallCheck(this, SupervisorComponent);

          this.supervisorService = supervisorService;
          this.ngxService = ngxService;
          this.supervisores = []; //lista de supervisores

          this.destroySubscription$ = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
          this.createSupervisor = false;
          this.newSupervisor = false;
          this.editSupervisor = false;
          this.listSupervisor = true; //cargar la data inicial del componente del Supervisor

          this.complete = function () {
            _this.createSupervisor = true;

            _this.supervisorService.getAllSupervisores(null, null, _this);
          };

          this.exportExcel = function () {
            var idTable = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
            var nameFile = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
            var idTableHTML = idTable !== null ? idTable : 'excel-table';
            var element = document.getElementById(idTableHTML);
            var ws = xlsx__WEBPACK_IMPORTED_MODULE_4__["utils"].table_to_sheet(element, {
              raw: true
            });
            /* generate workbook and add the worksheet */

            var wb = xlsx__WEBPACK_IMPORTED_MODULE_4__["utils"].book_new();
            xlsx__WEBPACK_IMPORTED_MODULE_4__["utils"].book_append_sheet(wb, ws, 'Sheet1');
            /* save to file */

            var nameFileEXCEL = nameFile !== null ? nameFile : 'Reporte';
            xlsx__WEBPACK_IMPORTED_MODULE_4__["writeFile"](wb, nameFileEXCEL + '.xlsx');
          };
        }

        _createClass(SupervisorComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            this.complete();
          } //activar el componente para agregar el Supervisor

        }, {
          key: "addSupervisor",
          value: function addSupervisor() {
            this.listSupervisor = false;
            this.newSupervisor = true;
          } //retorna de guardar oculpta el formulario y actualiza la data

        }, {
          key: "reverse",
          value: function reverse(back) {
            if (back) {
              this.newSupervisor = false;
              this.editSupervisor = false;
              this.listSupervisor = true;
              this.complete();
            }
          } //eliminar supervisor por id

        }, {
          key: "eliminarsupervisor",
          value: function eliminarsupervisor(idSupervisor) {
            this.supervisorService.deleteSupervisorById(idSupervisor, this);
          } //editar supervisor

        }, {
          key: "editsupervisor",
          value: function editsupervisor(supervisor) {
            this.supervisor = supervisor;
            this.listSupervisor = false;
            this.editSupervisor = true;
          }
        }, {
          key: "ngOnDestroy",
          value: function ngOnDestroy() {
            this.destroySubscription$.next(); // trigger the unsubscribe

            this.destroySubscription$.complete(); // finalize & clean up the subject stream
          }
        }]);

        return SupervisorComponent;
      }();

      SupervisorComponent.ɵfac = function SupervisorComponent_Factory(t) {
        return new (t || SupervisorComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_services_workflow_supervisor_supervisor_service__WEBPACK_IMPORTED_MODULE_1__["SupervisorService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](ngx_ui_loader__WEBPACK_IMPORTED_MODULE_3__["NgxUiLoaderService"]));
      };

      SupervisorComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
        type: SupervisorComponent,
        selectors: [["app-supervisor"]],
        decls: 15,
        vars: 4,
        consts: [["class", "emision-table", "style", "width: 100%; height: 100%; margin-top: 100px", 4, "ngIf"], [2, "display", "none"], ["aria-describedby", "tabla", "id", "excel-table"], ["id", "nump"], ["id", "ced"], ["id", "marca"], [4, "ngFor", "ngForOf"], [3, "goBack", 4, "ngIf"], [3, "supervisor", "goBack", 4, "ngIf"], [1, "emision-table", 2, "width", "100%", "height", "100%", "margin-top", "100px"], [1, "card"], [1, "card-header"], [3, "loaderId"], [1, "row", "align-items-center", 2, "justify-content", "space-between"], [1, "col-md-3", "col-12"], [1, "col-md-auto", "col-12", "d-flex", "justify-content-center"], [1, "dropdown", "mt-4"], ["aria-expanded", "false", "aria-haspopup", "true", "data-toggle", "dropdown", "id", "btn-closure-dropdownMenuExport", "role", "button", 1, "btn", "btn-outline-green-ref", "width-fix", "dropleft"], [1, "texbtn"], ["aria-labelledby", "navbarDropdownMenuLink", 1, "dropdown-menu", "dropdown-table", "dropdown-menu-right"], ["id", "btn-as-export-to", 1, "dropdown-item", 3, "click"], [1, "text-right"], ["class", "btn btn-lg btn-green", "id", "btnAddBrand-vehicleComponent", 3, "click", 4, "ngIf"], [1, "card-body", "table-strech"], ["class", "no-contracts", 4, "ngIf"], ["class", "table-responsive", 4, "ngIf"], ["id", "btnAddBrand-vehicleComponent", 1, "btn", "btn-lg", "btn-green", 3, "click"], [1, "fas", "fa-plus"], [1, "no-contracts"], [1, "container"], [1, "row", "justify-content-center"], [1, "col-4", "text-center", "center-data-missing"], [1, "no-contracts-text", "pt-4"], [1, "table-responsive"], ["aria-describedby", " tabla de supervisores", 1, "table"], ["id", "nunoperacion"], [1, "row"], [1, "col-auto", "head-title"], [1, "col-auto", "separator-title"], ["id", "fechacreada"], [1, "col-auto", "mr-auto", "head-title"], [1, "table-spacing"], [1, "form-group", 2, "text-align", "left"], [2, "display", "inline-block"], [1, "btn", "icon-field", 3, "click"], [1, "fas", "fa-minus-circle", "fa-lg", "font-color-error"], [1, "fas", "fa-history", "fa-lg", "font-color-changed"], [3, "goBack"], [3, "supervisor", "goBack"]],
        template: function SupervisorComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](0, SupervisorComponent_div_0_Template, 22, 4, "div", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "table", 2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "thead");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "tr");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "th", 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6, "NOMBRE");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "th", 4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, "APELLIDO");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "th", 5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10, "CORREO");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "tbody");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](12, SupervisorComponent_tr_12_Template, 7, 3, "tr", 6);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](13, SupervisorComponent_app_add_supervisor_13_Template, 1, 0, "app-add-supervisor", 7);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](14, SupervisorComponent_app_edit_supervisor_14_Template, 1, 1, "app-edit-supervisor", 8);
          }

          if (rf & 2) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.listSupervisor);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](12);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx.supervisores);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.newSupervisor);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.editSupervisor);
          }
        },
        directives: [_angular_common__WEBPACK_IMPORTED_MODULE_5__["NgIf"], _angular_common__WEBPACK_IMPORTED_MODULE_5__["NgForOf"], ngx_ui_loader__WEBPACK_IMPORTED_MODULE_3__["ɵb"], _add_supervisor_add_supervisor_component__WEBPACK_IMPORTED_MODULE_6__["AddSupervisorComponent"], _edit_supervisor_edit_supervisor_component__WEBPACK_IMPORTED_MODULE_7__["EditSupervisorComponent"]],
        styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzdXBlcnZpc29yLmNvbXBvbmVudC5zY3NzIn0= */"]
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](SupervisorComponent, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
          args: [{
            selector: 'app-supervisor',
            templateUrl: './supervisor.component.html',
            styleUrls: ['./supervisor.component.scss']
          }]
        }], function () {
          return [{
            type: _services_workflow_supervisor_supervisor_service__WEBPACK_IMPORTED_MODULE_1__["SupervisorService"]
          }, {
            type: ngx_ui_loader__WEBPACK_IMPORTED_MODULE_3__["NgxUiLoaderService"]
          }];
        }, null);
      })();
      /***/

    },

    /***/
    0:
    /*!***************************!*\
      !*** multi ./src/main.ts ***!
      \***************************/

    /*! no static exports found */

    /***/
    function _(module, exports, __webpack_require__) {
      module.exports = __webpack_require__(
      /*! C:\Users\vicen\Desktop\Prueba\prueba-angular\src\main.ts */
      "zUnb");
      /***/
    },

    /***/
    "0Em7":
    /*!***********************************************!*\
      !*** ./src/app/admin/admin-routing.module.ts ***!
      \***********************************************/

    /*! exports provided: AdminRoutingModule */

    /***/
    function Em7(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AdminRoutingModule", function () {
        return AdminRoutingModule;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var _components_auth_auth_routing_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./components/auth/auth-routing.component */
      "99Gt");
      /* harmony import */


      var _components_workflow_workflow_routing_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./components/workflow/workflow-routing.component */
      "guoY");
      /* harmony import */


      var _components_auth_auth_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ./components/auth/auth.component */
      "sO5X");
      /* harmony import */


      var _components_workflow_workflow_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./components/workflow/workflow.component */
      "pNJG");

      var routes = [{
        path: '',
        component: _components_auth_auth_component__WEBPACK_IMPORTED_MODULE_4__["AuthComponent"],
        children: _components_auth_auth_routing_component__WEBPACK_IMPORTED_MODULE_2__["routesAuth"]
      }, {
        path: 'workflow',
        component: _components_workflow_workflow_component__WEBPACK_IMPORTED_MODULE_5__["WorkflowComponent"],
        children: _components_workflow_workflow_routing_component__WEBPACK_IMPORTED_MODULE_3__["routesWorkflow"]
      }];

      var AdminRoutingModule = function AdminRoutingModule() {
        _classCallCheck(this, AdminRoutingModule);
      };

      AdminRoutingModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({
        type: AdminRoutingModule
      });
      AdminRoutingModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({
        factory: function AdminRoutingModule_Factory(t) {
          return new (t || AdminRoutingModule)();
        },
        imports: [[_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)], _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
      });

      (function () {
        (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](AdminRoutingModule, {
          imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]],
          exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        });
      })();
      /*@__PURE__*/


      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](AdminRoutingModule, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
          args: [{
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
          }]
        }], null, null);
      })();
      /***/

    },

    /***/
    1:
    /*!********************!*\
      !*** fs (ignored) ***!
      \********************/

    /*! no static exports found */

    /***/
    function _(module, exports) {
      /* (ignored) */

      /***/
    },

    /***/
    "118T":
    /*!************************************************************************!*\
      !*** ./src/app/admin/services/workflow/dashboard/dashboard.service.ts ***!
      \************************************************************************/

    /*! exports provided: DashboardService */

    /***/
    function T(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "DashboardService", function () {
        return DashboardService;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var src_app_shared_classes__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! src/app/shared/classes */
      "gtvE");
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common/http */
      "tk/3");

      var DashboardService = /*#__PURE__*/function (_src_app_shared_class) {
        _inherits(DashboardService, _src_app_shared_class);

        var _super = _createSuper(DashboardService);

        function DashboardService(http) {
          _classCallCheck(this, DashboardService);

          return _super.call(this, 'dashboard', http);
        }

        return DashboardService;
      }(src_app_shared_classes__WEBPACK_IMPORTED_MODULE_1__["Services"]);

      DashboardService.ɵfac = function DashboardService_Factory(t) {
        return new (t || DashboardService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]));
      };

      DashboardService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({
        token: DashboardService,
        factory: DashboardService.ɵfac,
        providedIn: 'root'
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](DashboardService, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
          args: [{
            providedIn: 'root'
          }]
        }], function () {
          return [{
            type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]
          }];
        }, null);
      })();
      /***/

    },

    /***/
    "182n":
    /*!***************************************************************************************************!*\
      !*** ./src/app/admin/components/workflow/supervisor/edit-supervisor/edit-supervisor.component.ts ***!
      \***************************************************************************************************/

    /*! exports provided: EditSupervisorComponent */

    /***/
    function n(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "EditSupervisorComponent", function () {
        return EditSupervisorComponent;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/forms */
      "3Pt+");
      /* harmony import */


      var ngx_ui_loader__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ngx-ui-loader */
      "gren");
      /* harmony import */


      var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! rxjs */
      "qCKp");
      /* harmony import */


      var src_app_admin_services_workflow_supervisor_supervisor_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! src/app/admin/services/workflow/supervisor/supervisor.service */
      "Itk3");
      /* harmony import */


      var src_app_shared_classes__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! src/app/shared/classes */
      "gtvE");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");

      function EditSupervisorComponent_div_23_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 26);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", ctx_r0.error.nombre.msg, " ");
        }
      }

      function EditSupervisorComponent_div_24_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 27);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "em", 28);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      function EditSupervisorComponent_div_25_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 27);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "em", 29);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      function EditSupervisorComponent_div_33_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 26);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var ctx_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", ctx_r3.error.apellido.msg, " ");
        }
      }

      function EditSupervisorComponent_div_34_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 27);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "em", 28);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      function EditSupervisorComponent_div_35_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 27);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "em", 29);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      function EditSupervisorComponent_div_43_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 26);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var ctx_r6 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", ctx_r6.error.correo.msg, " ");
        }
      }

      function EditSupervisorComponent_div_44_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 27);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "em", 28);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      function EditSupervisorComponent_div_45_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 27);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "em", 29);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      var _c0 = function _c0(a0, a1) {
        return {
          "is-invalid": a0,
          "is-valid": a1
        };
      };

      var EditSupervisorComponent = /*#__PURE__*/function () {
        function EditSupervisorComponent(formBuilder, supervisorService, helper, ngxService) {
          var _this2 = this;

          _classCallCheck(this, EditSupervisorComponent);

          this.formBuilder = formBuilder;
          this.supervisorService = supervisorService;
          this.helper = helper;
          this.ngxService = ngxService;
          this.goBack = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
          this.destroySubscription$ = new rxjs__WEBPACK_IMPORTED_MODULE_3__["Subject"]();
          this.supervisorForm = ['nombre', 'apellido', 'correo']; //iniciar los errores y las validaciones

          this.errorInit = function () {
            _this2.error = {
              nombre: {
                error: false,
                change: false,
                msg: ''
              },
              apellido: {
                error: false,
                change: false,
                msg: ''
              },
              correo: {
                error: false,
                change: false,
                msg: ''
              }
            };
          }; //enviar data al backend


          this.action = function () {
            _this2.ngxService.start();

            var form = _this2.form.value;
            var data = {
              id: _this2.supervisor.id,
              nombre: form.nombre,
              apellido: form.apellido,
              correo: form.correo
            };

            _this2.supervisorService.updateSupervisor(_this2.supervisor.id, data, _this2);
          };
        }

        _createClass(EditSupervisorComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            //validaciones del formulario
            this.form = this.formBuilder.group({
              nombre: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', {
                validators: [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]
              }),
              apellido: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', {
                validators: [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]
              }),
              correo: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', {
                validators: [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]
              })
            });

            if (this.supervisor) {
              this.fillForm(this.supervisor);
            }

            this.errorInit();
          }
        }, {
          key: "fillForm",
          value: function fillForm(item) {
            var _this3 = this;

            this.supervisorForm.forEach(function (i) {
              _this3.form.get(i).reset(item[i]);
            });
          } //regresar al listado de supervisores

        }, {
          key: "reversePage",
          value: function reversePage() {
            this.goBack.emit(true);
          }
        }, {
          key: "ngOnDestroy",
          value: function ngOnDestroy() {
            this.destroySubscription$.next(); // trigger the unsubscribe

            this.destroySubscription$.complete(); // finalize & clean up the subject stream
          }
        }]);

        return EditSupervisorComponent;
      }();

      EditSupervisorComponent.ɵfac = function EditSupervisorComponent_Factory(t) {
        return new (t || EditSupervisorComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_admin_services_workflow_supervisor_supervisor_service__WEBPACK_IMPORTED_MODULE_4__["SupervisorService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_shared_classes__WEBPACK_IMPORTED_MODULE_5__["Helpers"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](ngx_ui_loader__WEBPACK_IMPORTED_MODULE_2__["NgxUiLoaderService"]));
      };

      EditSupervisorComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
        type: EditSupervisorComponent,
        selectors: [["app-edit-supervisor"]],
        inputs: {
          supervisor: "supervisor"
        },
        outputs: {
          goBack: "goBack"
        },
        decls: 50,
        vars: 23,
        consts: [[1, "row", "wizard-title", 2, "width", "100%", "height", "100%", "margin-top", "100px"], [1, "col-11", "mr-auto", "wizard-fonts"], [1, "required-fields"], [1, "col-1", "align-self-end"], [1, "btn", "btn-clear", 3, "click"], [1, "fas", "fa-angle-left"], [1, "row", "justify-content-center"], [1, "card", "fixed-width"], [1, "card-body"], ["autocomplete", "off", 3, "formGroup"], [1, "row"], [1, "col-4"], [1, "form-group"], ["for", "brandname-addSupervisorComponent"], [1, "fiel-validate"], [1, "field"], ["formControlName", "nombre", "id", "nombre-addSupervisorComponent", "placeholder", "Nombre del supervisor", "type", "text", 1, "form-control", "form-input", "capitalize", 3, "ngClass", "blur", "input"], ["class", "invalid-feedback font-color-error", 4, "ngIf"], ["class", "icon-field", 4, "ngIf"], ["for", "apellido-addSupervisorComponent"], ["formControlName", "apellido", "id", "apellido-addSupervisorComponent", "placeholder", "Apellido del supervisor", "type", "text", 1, "form-control", "form-input", "capitalize", 3, "ngClass", "blur", "input"], ["for", "correo-addSupervisorComponent"], ["formControlName", "correo", "id", "correo-addSupervisorComponent", "placeholder", "correo", "type", "text", 1, "form-control", "form-input", "capitalize", 3, "ngClass", "blur", "input"], [1, "row", "push-green"], [1, "col-12", "text-right"], ["data-target", "#confirmationModal-addVehicleComponent", "id", "confirmationModalBtn-addVehicleComponent", 1, "btn", "btn-save", 3, "disabled", "click"], [1, "invalid-feedback", "font-color-error"], [1, "icon-field"], [1, "fas", "fa-exclamation-circle", "fa-lg", "font-color-error"], [1, "fas", "fa-check-circle", "fa-lg", "font-color-success"]],
        template: function EditSupervisorComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "h4");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3, "Edita un supervisor");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "small", 2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "* Campos requeridos");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "button", 4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function EditSupervisorComponent_Template_button_click_7_listener() {
              return ctx.reversePage();
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](8, "em", 5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](9, " \xA0 Regresar ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](10, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "div", 6);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "div", 7);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "div", 8);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "form", 9);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "div", 10);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "div", 11);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "div", 12);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "label", 13);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](19, "Nombre*");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "div", 14);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "div", 15);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "input", 16);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("blur", function EditSupervisorComponent_Template_input_blur_22_listener() {
              return ctx.helper.validInput(ctx.error, "nombre", ctx.form);
            })("input", function EditSupervisorComponent_Template_input_input_22_listener() {
              return ctx.helper.validInput(ctx.error, "nombre", ctx.form);
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](23, EditSupervisorComponent_div_23_Template, 2, 1, "div", 17);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](24, EditSupervisorComponent_div_24_Template, 2, 0, "div", 18);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](25, EditSupervisorComponent_div_25_Template, 2, 0, "div", 18);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](26, "div", 11);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "div", 12);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "label", 19);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](29, "Apellido *");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](30, "div", 14);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](31, "div", 15);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](32, "input", 20);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("blur", function EditSupervisorComponent_Template_input_blur_32_listener() {
              return ctx.helper.validInput(ctx.error, "apellido", ctx.form);
            })("input", function EditSupervisorComponent_Template_input_input_32_listener() {
              return ctx.helper.validInput(ctx.error, "apellido", ctx.form);
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](33, EditSupervisorComponent_div_33_Template, 2, 1, "div", 17);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](34, EditSupervisorComponent_div_34_Template, 2, 0, "div", 18);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](35, EditSupervisorComponent_div_35_Template, 2, 0, "div", 18);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](36, "div", 11);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](37, "div", 12);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](38, "label", 21);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](39, "Correo *");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](40, "div", 14);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](41, "div", 15);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](42, "input", 22);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("blur", function EditSupervisorComponent_Template_input_blur_42_listener() {
              return ctx.helper.validInput(ctx.error, "correo", ctx.form);
            })("input", function EditSupervisorComponent_Template_input_input_42_listener() {
              return ctx.helper.validInput(ctx.error, "correo", ctx.form);
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](43, EditSupervisorComponent_div_43_Template, 2, 1, "div", 17);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](44, EditSupervisorComponent_div_44_Template, 2, 0, "div", 18);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](45, EditSupervisorComponent_div_45_Template, 2, 0, "div", 18);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](46, "div", 23);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](47, "div", 24);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](48, "button", 25);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function EditSupervisorComponent_Template_button_click_48_listener() {
              return ctx.form.valid && ctx.action();
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](49, " Actualizar ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
          }

          if (rf & 2) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](14);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("formGroup", ctx.form);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](8);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction2"](14, _c0, ctx.error.nombre.error, !ctx.error.nombre.error && ctx.error.nombre.change));

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.form.get("nombre").errors == null ? null : ctx.form.get("nombre").errors.required);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.error.nombre.error && ctx.error.nombre.change);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx.error.nombre.error && ctx.error.nombre.change);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction2"](17, _c0, ctx.error.apellido.error, !ctx.error.apellido.error && ctx.error.apellido.change));

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.form.get("apellido").errors == null ? null : ctx.form.get("apellido").errors.required);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.error.apellido.error && ctx.error.apellido.change);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx.error.apellido.error && ctx.error.apellido.change);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction2"](20, _c0, ctx.error.correo.error, !ctx.error.correo.error && ctx.error.correo.change));

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.form.get("correo").errors == null ? null : ctx.form.get("correo").errors.required);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.error.correo.error && ctx.error.correo.change);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx.error.correo.error && ctx.error.correo.change);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("disabled", !ctx.form.valid);
          }
        },
        directives: [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["ɵangular_packages_forms_forms_y"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NgControlStatusGroup"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroupDirective"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["DefaultValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControlName"], _angular_common__WEBPACK_IMPORTED_MODULE_6__["NgClass"], _angular_common__WEBPACK_IMPORTED_MODULE_6__["NgIf"]],
        styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJlZGl0LXN1cGVydmlzb3IuY29tcG9uZW50LnNjc3MifQ== */"]
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](EditSupervisorComponent, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
          args: [{
            selector: 'app-edit-supervisor',
            templateUrl: './edit-supervisor.component.html',
            styleUrls: ['./edit-supervisor.component.scss']
          }]
        }], function () {
          return [{
            type: _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"]
          }, {
            type: src_app_admin_services_workflow_supervisor_supervisor_service__WEBPACK_IMPORTED_MODULE_4__["SupervisorService"]
          }, {
            type: src_app_shared_classes__WEBPACK_IMPORTED_MODULE_5__["Helpers"]
          }, {
            type: ngx_ui_loader__WEBPACK_IMPORTED_MODULE_2__["NgxUiLoaderService"]
          }];
        }, {
          supervisor: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
          }],
          goBack: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
          }]
        });
      })();
      /***/

    },

    /***/
    "1ua0":
    /*!********************************************!*\
      !*** ./src/app/shared/components/index.ts ***!
      \********************************************/

    /*! exports provided: SharedComponent */

    /***/
    function ua0(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony import */


      var _shared_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! ../shared.component */
      "duh5");
      /* harmony reexport (safe) */


      __webpack_require__.d(__webpack_exports__, "SharedComponent", function () {
        return _shared_component__WEBPACK_IMPORTED_MODULE_0__["SharedComponent"];
      });
      /***/

    },

    /***/
    2:
    /*!************************!*\
      !*** crypto (ignored) ***!
      \************************/

    /*! no static exports found */

    /***/
    function _(module, exports) {
      /* (ignored) */

      /***/
    },

    /***/
    "2Xez":
    /*!*******************************************!*\
      !*** ./src/app/admin/components/index.ts ***!
      \*******************************************/

    /*! exports provided: AdminComponent, AuthComponent, SigninComponent, WorkflowComponent, DashboardComponent, EmpleadoComponent, ReportesComponent, SupervisorComponent, DepartamentoComponent, AddDepartamentoComponent */

    /***/
    function Xez(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony import */


      var _admin_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! ../admin.component */
      "TRGY");
      /* harmony reexport (safe) */


      __webpack_require__.d(__webpack_exports__, "AdminComponent", function () {
        return _admin_component__WEBPACK_IMPORTED_MODULE_0__["AdminComponent"];
      });
      /* harmony import */


      var _auth_auth_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ./auth/auth.component */
      "sO5X");
      /* harmony reexport (safe) */


      __webpack_require__.d(__webpack_exports__, "AuthComponent", function () {
        return _auth_auth_component__WEBPACK_IMPORTED_MODULE_1__["AuthComponent"];
      });
      /* harmony import */


      var _auth_signin_signin_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./auth/signin/signin.component */
      "fFVl");
      /* harmony reexport (safe) */


      __webpack_require__.d(__webpack_exports__, "SigninComponent", function () {
        return _auth_signin_signin_component__WEBPACK_IMPORTED_MODULE_2__["SigninComponent"];
      });
      /* harmony import */


      var _workflow_workflow_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./workflow/workflow.component */
      "pNJG");
      /* harmony reexport (safe) */


      __webpack_require__.d(__webpack_exports__, "WorkflowComponent", function () {
        return _workflow_workflow_component__WEBPACK_IMPORTED_MODULE_3__["WorkflowComponent"];
      });
      /* harmony import */


      var _workflow_dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ./workflow/dashboard/dashboard.component */
      "DDId");
      /* harmony reexport (safe) */


      __webpack_require__.d(__webpack_exports__, "DashboardComponent", function () {
        return _workflow_dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_4__["DashboardComponent"];
      });
      /* harmony import */


      var _workflow_empleado_empleado_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./workflow/empleado/empleado.component */
      "MStx");
      /* harmony reexport (safe) */


      __webpack_require__.d(__webpack_exports__, "EmpleadoComponent", function () {
        return _workflow_empleado_empleado_component__WEBPACK_IMPORTED_MODULE_5__["EmpleadoComponent"];
      });
      /* harmony import */


      var _workflow_reportes_reportes_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./workflow/reportes/reportes.component */
      "W5i3");
      /* harmony reexport (safe) */


      __webpack_require__.d(__webpack_exports__, "ReportesComponent", function () {
        return _workflow_reportes_reportes_component__WEBPACK_IMPORTED_MODULE_6__["ReportesComponent"];
      });
      /* harmony import */


      var _workflow_supervisor_supervisor_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! ./workflow/supervisor/supervisor.component */
      "+2bK");
      /* harmony reexport (safe) */


      __webpack_require__.d(__webpack_exports__, "SupervisorComponent", function () {
        return _workflow_supervisor_supervisor_component__WEBPACK_IMPORTED_MODULE_7__["SupervisorComponent"];
      });
      /* harmony import */


      var _workflow_departamento_departamento_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! ./workflow/departamento/departamento.component */
      "b/wG");
      /* harmony reexport (safe) */


      __webpack_require__.d(__webpack_exports__, "DepartamentoComponent", function () {
        return _workflow_departamento_departamento_component__WEBPACK_IMPORTED_MODULE_8__["DepartamentoComponent"];
      });
      /* harmony import */


      var _workflow_departamento_add_departamento_add_departamento_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
      /*! ./workflow/departamento/add-departamento/add-departamento.component */
      "XBxM");
      /* harmony reexport (safe) */


      __webpack_require__.d(__webpack_exports__, "AddDepartamentoComponent", function () {
        return _workflow_departamento_add_departamento_add_departamento_component__WEBPACK_IMPORTED_MODULE_9__["AddDepartamentoComponent"];
      });
      /***/

    },

    /***/
    "2fU6":
    /*!******************************************************!*\
      !*** ./src/app/shared/classes/NotificationAction.ts ***!
      \******************************************************/

    /*! exports provided: NotificationAction */

    /***/
    function fU6(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "NotificationAction", function () {
        return NotificationAction;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");

      var NotificationAction = /*#__PURE__*/function () {
        function NotificationAction() {
          _classCallCheck(this, NotificationAction);

          this.modals = [];
        }

        _createClass(NotificationAction, [{
          key: "add",
          value: function add(modal) {
            var modalOld = this.modals.filter(function (x) {
              return x.id === modal.id;
            })[0];

            if (modalOld !== null && modalOld !== undefined) {
              this.remove(modalOld.id);
            }

            this.modals.push(modal);
          }
        }, {
          key: "remove",
          value: function remove(id) {
            this.modals = this.modals.filter(function (x) {
              return x.id !== id;
            });
          }
        }, {
          key: "clear",
          value: function clear() {
            this.modals = [];
          }
        }, {
          key: "open",
          value: function open(id) {
            var modal = this.modals.filter(function (x) {
              return x.id === id;
            })[0];
            modal.open();
          }
        }, {
          key: "close",
          value: function close(id) {
            var modal = this.modals.filter(function (x) {
              return x.id === id;
            })[0];
            modal.close();
          }
        }]);

        return NotificationAction;
      }();

      NotificationAction.ɵfac = function NotificationAction_Factory(t) {
        return new (t || NotificationAction)();
      };

      NotificationAction.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({
        token: NotificationAction,
        factory: NotificationAction.ɵfac,
        providedIn: 'root'
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](NotificationAction, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
          args: [{
            providedIn: 'root'
          }]
        }], null, null);
      })();
      /***/

    },

    /***/
    3:
    /*!************************!*\
      !*** stream (ignored) ***!
      \************************/

    /*! no static exports found */

    /***/
    function _(module, exports) {
      /* (ignored) */

      /***/
    },

    /***/
    "99Gt":
    /*!*****************************************************************!*\
      !*** ./src/app/admin/components/auth/auth-routing.component.ts ***!
      \*****************************************************************/

    /*! exports provided: routesAuth, AuthRoutingComponent */

    /***/
    function Gt(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "routesAuth", function () {
        return routesAuth;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AuthRoutingComponent", function () {
        return AuthRoutingComponent;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var _signin_signin_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./signin/signin.component */
      "fFVl");

      var routesAuth = [{
        path: '',
        component: _signin_signin_component__WEBPACK_IMPORTED_MODULE_2__["SigninComponent"]
      }];

      var AuthRoutingComponent = function AuthRoutingComponent() {
        _classCallCheck(this, AuthRoutingComponent);
      };

      AuthRoutingComponent.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({
        type: AuthRoutingComponent
      });
      AuthRoutingComponent.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({
        factory: function AuthRoutingComponent_Factory(t) {
          return new (t || AuthRoutingComponent)();
        },
        imports: [[_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routesAuth)], _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
      });

      (function () {
        (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](AuthRoutingComponent, {
          imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]],
          exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        });
      })();
      /*@__PURE__*/


      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](AuthRoutingComponent, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
          args: [{
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routesAuth)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
          }]
        }], null, null);
      })();
      /***/

    },

    /***/
    "AElR":
    /*!******************************************************!*\
      !*** ./src/app/shared/pipes/format-money-ec.pipe.ts ***!
      \******************************************************/

    /*! exports provided: FormatMoneyEcPipe */

    /***/
    function AElR(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "FormatMoneyEcPipe", function () {
        return FormatMoneyEcPipe;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");

      var FormatMoneyEcPipe = /*#__PURE__*/function () {
        function FormatMoneyEcPipe() {
          _classCallCheck(this, FormatMoneyEcPipe);
        }

        _createClass(FormatMoneyEcPipe, [{
          key: "transform",
          value: function transform(value) {
            var newResult = value.replace(/,/g, '__COMMA__');
            newResult = newResult.replace(/\./g, ',');
            newResult = newResult.replace(/__COMMA__/g, '.');
            return newResult;
          }
        }]);

        return FormatMoneyEcPipe;
      }();

      FormatMoneyEcPipe.ɵfac = function FormatMoneyEcPipe_Factory(t) {
        return new (t || FormatMoneyEcPipe)();
      };

      FormatMoneyEcPipe.ɵpipe = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefinePipe"]({
        name: "formatMoneyEc",
        type: FormatMoneyEcPipe,
        pure: true
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](FormatMoneyEcPipe, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Pipe"],
          args: [{
            name: 'formatMoneyEc'
          }]
        }], null, null);
      })();
      /***/

    },

    /***/
    "AytR":
    /*!*****************************************!*\
      !*** ./src/environments/environment.ts ***!
      \*****************************************/

    /*! exports provided: environment */

    /***/
    function AytR(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "environment", function () {
        return environment;
      }); // This file can be replaced during build by using the `fileReplacements` array.
      // `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
      // The list of file replacements can be found in `angular.json`.


      var environment = {
        production: false,
        name: 'develop',
        baseUrl: 'https://bisspruebas.kiaecuador.com.ec/api/',
        banner: 'Prueba'
      };
      /*
       * For easier debugging in development mode, you can import the following file
       * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
       *
       * This import should be commented out in production mode because it will have a negative impact
       * on performance if an error is thrown.
       */
      // import 'zone.js/dist/zone-error';  // Included with Angular CLI.

      /***/
    },

    /***/
    "BnFf":
    /*!******************************************************************!*\
      !*** ./src/app/admin/services/workflow/sueldo/sueldo.service.ts ***!
      \******************************************************************/

    /*! exports provided: SueldoService */

    /***/
    function BnFf(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "SueldoService", function () {
        return SueldoService;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _shared_classes__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ../../../../shared/classes */
      "gtvE");
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common/http */
      "tk/3");
      /* harmony import */


      var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! rxjs/operators */
      "kU1M");

      var SueldoService = /*#__PURE__*/function (_shared_classes__WEBP) {
        _inherits(SueldoService, _shared_classes__WEBP);

        var _super2 = _createSuper(SueldoService);

        function SueldoService(http) {
          var _this4;

          _classCallCheck(this, SueldoService);

          _this4 = _super2.call(this, "/api/v1/sueldo", http); //lllamado al api para obtener los empleados

          _this4.getAumento = function (data) {
            var page = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
            var ctx = arguments.length > 2 ? arguments[2] : undefined;
            ctx.ngxService.start();

            _this4.get(data, 'newcalculo', page).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["takeUntil"])(ctx.destroySubscription$)).subscribe(function (response) {
              if (response) {
                console.log(response);
                ctx.form.get('salario').reset(response);
                ctx.saldo = response;
                ctx.ngxService.stop();
              }
            }, function (error) {
              console.error(error);
              ctx.ngxService.stop();
            });
          };

          return _this4;
        }

        return SueldoService;
      }(_shared_classes__WEBPACK_IMPORTED_MODULE_1__["Services"]);

      SueldoService.ɵfac = function SueldoService_Factory(t) {
        return new (t || SueldoService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]));
      };

      SueldoService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({
        token: SueldoService,
        factory: SueldoService.ɵfac,
        providedIn: 'root'
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](SueldoService, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
          args: [{
            providedIn: 'root'
          }]
        }], function () {
          return [{
            type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]
          }];
        }, null);
      })();
      /***/

    },

    /***/
    "CLRV":
    /*!*********************************************************************************************!*\
      !*** ./src/app/admin/components/workflow/empleado/edit-empleado/edit-empleado.component.ts ***!
      \*********************************************************************************************/

    /*! exports provided: EditEmpleadoComponent */

    /***/
    function CLRV(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "EditEmpleadoComponent", function () {
        return EditEmpleadoComponent;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");

      var EditEmpleadoComponent = /*#__PURE__*/function () {
        function EditEmpleadoComponent() {
          _classCallCheck(this, EditEmpleadoComponent);
        }

        _createClass(EditEmpleadoComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }]);

        return EditEmpleadoComponent;
      }();

      EditEmpleadoComponent.ɵfac = function EditEmpleadoComponent_Factory(t) {
        return new (t || EditEmpleadoComponent)();
      };

      EditEmpleadoComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
        type: EditEmpleadoComponent,
        selectors: [["app-edit-empleado"]],
        decls: 2,
        vars: 0,
        template: function EditEmpleadoComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "p");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "edit-empleado works!");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
          }
        },
        styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJlZGl0LWVtcGxlYWRvLmNvbXBvbmVudC5zY3NzIn0= */"]
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](EditEmpleadoComponent, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
          args: [{
            selector: 'app-edit-empleado',
            templateUrl: './edit-empleado.component.html',
            styleUrls: ['./edit-empleado.component.scss']
          }]
        }], function () {
          return [];
        }, null);
      })();
      /***/

    },

    /***/
    "DDId":
    /*!****************************************************************************!*\
      !*** ./src/app/admin/components/workflow/dashboard/dashboard.component.ts ***!
      \****************************************************************************/

    /*! exports provided: DashboardComponent */

    /***/
    function DDId(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "DashboardComponent", function () {
        return DashboardComponent;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _services_workflow_departamento_departamento_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ../../../services/workflow/departamento/departamento.service */
      "tmbA");
      /* harmony import */


      var _services_workflow_supervisor_supervisor_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ../../../services/workflow/supervisor/supervisor.service */
      "Itk3");
      /* harmony import */


      var _services_workflow_empleado_empleado_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ../../../services/workflow/empleado/empleado.service */
      "k4qQ");
      /* harmony import */


      var ngx_ui_loader__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ngx-ui-loader */
      "gren");
      /* harmony import */


      var rxjs__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! rxjs */
      "qCKp");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");
      /* harmony import */


      var ng2_charts__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! ng2-charts */
      "LPYB");

      function DashboardComponent_div_37_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 15);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "canvas", 16);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("chartType", ctx_r0.pieStatusL.chartType)("colors", ctx_r0.pieStatusL.colors)("data", ctx_r0.pieStatusL.data)("labels", ctx_r0.pieStatusL.label)("legend", ctx_r0.pieStatusL.legends)("options", ctx_r0.pieStatusL.options)("plugins", ctx_r0.pieStatusL.plugins);
        }
      }

      function DashboardComponent_div_38_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "span", 17);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "i", 18);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "p", 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "small", 19);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "No existen datos relacionados");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      var DashboardComponent = /*#__PURE__*/function () {
        function DashboardComponent(empleadoService, departamentoService, supervisorService, ngxService) {
          _classCallCheck(this, DashboardComponent);

          this.empleadoService = empleadoService;
          this.departamentoService = departamentoService;
          this.supervisorService = supervisorService;
          this.ngxService = ngxService;
          this.departamentos = [];
          this.supervisores = [];
          this.empleados = [];
          this.donutColors = ['#dbd8e3', '#c2dfa6', '#978db7', '#74a47b', '#828285', '#3d7d41', '#87bf48', '#d7e5da', '#b2b2b5', '#6c5a97'];
          this.statusOptions = {
            tooltips: {
              backgroundColor: 'rgba(0, 0, 0, 0.9)',
              bodyFontFamily: 'Lato-Regular',
              bodyFontColor: 'rgba(255,255, 255, 0.9)',
              bodyFontSize: 12,
              cornerRadius: 2,
              yPadding: 7,
              mode: 'point'
            },
            responsive: true,
            maintainAspectRatio: false,
            legend: {
              position: 'right',
              labels: {
                usePointStyle: true,
                fontSize: 10
              }
            },
            plugins: {
              datalabels: {
                display: false,
                formatter: function formatter(value, ctx) {
                  return ctx.chart.data.labels[ctx.dataIndex];
                }
              }
            }
          };
          this.pieStatusL = {
            label: [],
            data: [],
            chartType: 'doughnut',
            legends: true,
            plugins: [],
            colors: [],
            options: this.statusOptions,
            title: '',
            service: ''
          };
          this.destroySubscription$ = new rxjs__WEBPACK_IMPORTED_MODULE_5__["Subject"]();
        }

        _createClass(DashboardComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            this.getContadores();
          } //funcion para traer todos los datos para hacer un contador en el dasboard

        }, {
          key: "getContadores",
          value: function getContadores() {
            var _this5 = this;

            this.departamentoService.getAllDepartamentos(null, null, this);
            this.supervisorService.getAllSupervisores(null, null, this);
            this.empleadoService.getAllEmpleados(null, null, this);
            this.pieStatusL.options.legend.position = 'right';
            this.pieStatusL.label = ["Empleados", 'Supervisores', 'Departamentos'];
            var backgroundColor = this.donutColors;
            this.pieStatusL.colors = [{
              backgroundColor: backgroundColor
            }];
            setTimeout(function () {
              _this5.pieStatusL.data = [_this5.empleados.length, _this5.supervisores.length, _this5.departamentos.length];
            }, 2000);
          }
        }, {
          key: "ngOnDestroy",
          value: function ngOnDestroy() {
            this.destroySubscription$.next(); // trigger the unsubscribe

            this.destroySubscription$.complete(); // finalize & clean up the subject stream
          }
        }]);

        return DashboardComponent;
      }();

      DashboardComponent.ɵfac = function DashboardComponent_Factory(t) {
        return new (t || DashboardComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_services_workflow_empleado_empleado_service__WEBPACK_IMPORTED_MODULE_3__["EmpleadoService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_services_workflow_departamento_departamento_service__WEBPACK_IMPORTED_MODULE_1__["DepartamentoService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_services_workflow_supervisor_supervisor_service__WEBPACK_IMPORTED_MODULE_2__["SupervisorService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](ngx_ui_loader__WEBPACK_IMPORTED_MODULE_4__["NgxUiLoaderService"]));
      };

      DashboardComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
        type: DashboardComponent,
        selectors: [["app-dashboard"]],
        decls: 39,
        vars: 9,
        consts: [[1, "row", "header", "mx-2"], [1, "col-auto", "mr-auto", "heading"], [1, "row"], [1, "card-deck", "stats", "justify-content-center", "w-100"], [1, "card", "text-center"], [3, "loaderId"], [1, "card-body"], [1, "card-title"], [1, "card-text"], [1, "col-6"], [1, "row", "h-100"], [1, "justify-content-center", "w-100", "score"], [1, "card", "h-100", "text-center"], ["class", "status-chart", 4, "ngIf"], [4, "ngIf"], [1, "status-chart"], ["baseChart", "", 3, "chartType", "colors", "data", "labels", "legend", "options", "plugins"], [1, "empty-chart-icon"], [1, "fab", "fa-creative-commons-zero"], [1, "text-muted"]],
        template: function DashboardComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "h4");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3, "Panel de control");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "span");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "Te presentamos un resumen de los registros");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "div", 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "div", 4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](9, "ngx-ui-loader", 5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "div", 6);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "h5", 7);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12, "Departamentos");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "p", 8);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](14);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "div", 4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](16, "ngx-ui-loader", 5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "div", 6);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "h5", 7);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](19, "Supervisores");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "p", 8);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](21);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "div", 4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](23, "ngx-ui-loader", 5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "div", 6);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](25, "h5", 7);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](26, "Empleados");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "p", 8);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](28);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](29, "div", 9);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](30, "div", 10);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](31, "div", 11);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](32, "div", 12);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](33, "ngx-ui-loader", 5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](34, "div", 6);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](35, "h5", 7);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](36, "Personal");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](37, DashboardComponent_div_37_Template, 2, 7, "div", 13);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](38, DashboardComponent_div_38_Template, 6, 0, "div", 14);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
          }

          if (rf & 2) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](9);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("loaderId", "departamentos-loader");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx.departamentos.length);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("loaderId", "supervisores-loader");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx.supervisores.length);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("loaderId", "empleados-loader");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx.empleados.length);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("loaderId", "status-loader");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.pieStatusL.data.length);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx.pieStatusL.data.length);
          }
        },
        directives: [ngx_ui_loader__WEBPACK_IMPORTED_MODULE_4__["ɵb"], _angular_common__WEBPACK_IMPORTED_MODULE_6__["NgIf"], ng2_charts__WEBPACK_IMPORTED_MODULE_7__["BaseChartDirective"]],
        styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJkYXNoYm9hcmQuY29tcG9uZW50LnNjc3MifQ== */"]
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](DashboardComponent, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
          args: [{
            selector: 'app-dashboard',
            templateUrl: './dashboard.component.html',
            styleUrls: ['./dashboard.component.scss']
          }]
        }], function () {
          return [{
            type: _services_workflow_empleado_empleado_service__WEBPACK_IMPORTED_MODULE_3__["EmpleadoService"]
          }, {
            type: _services_workflow_departamento_departamento_service__WEBPACK_IMPORTED_MODULE_1__["DepartamentoService"]
          }, {
            type: _services_workflow_supervisor_supervisor_service__WEBPACK_IMPORTED_MODULE_2__["SupervisorService"]
          }, {
            type: ngx_ui_loader__WEBPACK_IMPORTED_MODULE_4__["NgxUiLoaderService"]
          }];
        }, null);
      })();
      /***/

    },

    /***/
    "Itk3":
    /*!**************************************************************************!*\
      !*** ./src/app/admin/services/workflow/supervisor/supervisor.service.ts ***!
      \**************************************************************************/

    /*! exports provided: SupervisorService */

    /***/
    function Itk3(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "SupervisorService", function () {
        return SupervisorService;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _shared_classes__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ../../../../shared/classes */
      "gtvE");
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common/http */
      "tk/3");
      /* harmony import */


      var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! rxjs/operators */
      "kU1M");

      var SupervisorService = /*#__PURE__*/function (_shared_classes__WEBP2) {
        _inherits(SupervisorService, _shared_classes__WEBP2);

        var _super3 = _createSuper(SupervisorService);

        function SupervisorService(http) {
          var _this6;

          _classCallCheck(this, SupervisorService);

          _this6 = _super3.call(this, "/api/v1/supervisor", http); //lllamado al api para obtener los supervisores

          _this6.getAllSupervisores = function (data) {
            var page = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
            var ctx = arguments.length > 2 ? arguments[2] : undefined;
            ctx.ngxService.start();

            _this6.get(data, '', page).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["takeUntil"])(ctx.destroySubscription$)).subscribe(function (response) {
              if (response) {
                ctx.supervisores = response;
                console.log(response);
                ctx.ngxService.stop();
              }
            }, function (error) {
              console.error(error);
              ctx.ngxService.stop();
            });
          }; //agregar supervisores


          _this6.addSupervisor = function (data, ctx) {
            _this6.post(data, null, '').pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["takeUntil"])(ctx.destroySubscription$)).subscribe(function (response) {
              if (response) {
                ctx.ngxService.stop();
                ctx.reversePage();
              } else {
                ctx.ngxService.stop();
              }
            });
          }; //actualizar supervisor


          _this6.updateSupervisor = function (id, data, ctx) {
            _this6.post(data, null, '').pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["takeUntil"])(ctx.destroySubscription$)).subscribe(function (response) {
              if (response) {
                ctx.ngxService.stop();
                ctx.reversePage();
              } else {
                ctx.ngxService.stop();
              }
            }, function (error) {
              console.error(error);
              ctx.ngxService.stop();
            });
          }; //eliminar supervisor


          _this6.deleteSupervisorById = function (idSupervisor, ctx) {
            ctx.ngxService.start();

            _this6["delete"](idSupervisor, null, '').pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["takeUntil"])(ctx.destroySubscription$)).subscribe(function (response) {
              if (response) {
                ctx.complete();
                ctx.ngxService.stop();
              }
            }, function (error) {
              console.error(error);
              ctx.ngxService.stop();
            });
          };

          return _this6;
        }

        return SupervisorService;
      }(_shared_classes__WEBPACK_IMPORTED_MODULE_1__["Services"]);

      SupervisorService.ɵfac = function SupervisorService_Factory(t) {
        return new (t || SupervisorService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]));
      };

      SupervisorService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({
        token: SupervisorService,
        factory: SupervisorService.ɵfac,
        providedIn: 'root'
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](SupervisorService, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
          args: [{
            providedIn: 'root'
          }]
        }], function () {
          return [{
            type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]
          }];
        }, null);
      })();
      /***/

    },

    /***/
    "MStx":
    /*!**************************************************************************!*\
      !*** ./src/app/admin/components/workflow/empleado/empleado.component.ts ***!
      \**************************************************************************/

    /*! exports provided: EmpleadoComponent */

    /***/
    function MStx(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "EmpleadoComponent", function () {
        return EmpleadoComponent;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _services_workflow_empleado_empleado_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ../../../services/workflow/empleado/empleado.service */
      "k4qQ");
      /* harmony import */


      var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! rxjs */
      "qCKp");
      /* harmony import */


      var ngx_ui_loader__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ngx-ui-loader */
      "gren");
      /* harmony import */


      var xlsx__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! xlsx */
      "EUZL");
      /* harmony import */


      var xlsx__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(xlsx__WEBPACK_IMPORTED_MODULE_4__);
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @angular/forms */
      "3Pt+");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");
      /* harmony import */


      var _add_empleado_add_empleado_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! ./add-empleado/add-empleado.component */
      "My/+");
      /* harmony import */


      var _shared_pipes_format_money_ec_pipe__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! ../../../../shared/pipes/format-money-ec.pipe */
      "AElR");

      function EmpleadoComponent_div_0_div_22_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 30);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 31);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 32);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 33);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "p", 34);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, " No tienes ning\xFAn departamento ingresado ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      function EmpleadoComponent_div_0_div_23_tr_53_Template(rf, ctx) {
        if (rf & 1) {
          var _r8 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "tr");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "td", 43);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "td", 43);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "td", 43);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "td", 43);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "td", 43);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](11, "formatMoneyEc");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](12, "currency");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "td", 43);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](14);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "td", 43);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](16);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "td", 43);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "div", 44);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "div", 45);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "button", 46);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function EmpleadoComponent_div_0_div_23_tr_53_Template_button_click_20_listener() {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r8);

            var item_r6 = ctx.$implicit;

            var ctx_r7 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](3);

            return ctx_r7.eliminardepartamento(item_r6.id);
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](21, "i", 47);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "div", 45);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "button", 46);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function EmpleadoComponent_div_0_div_23_tr_53_Template_button_click_23_listener() {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r8);

            var item_r6 = ctx.$implicit;

            var ctx_r9 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](3);

            return ctx_r9.editempleado(item_r6);
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](24, "i", 48);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var item_r6 = ctx.$implicit;

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](item_r6.nombre);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](item_r6.apellido);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](item_r6.telefono);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](item_r6.email);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](11, 7, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind2"](12, 9, item_r6.salario, "USD")), " ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](item_r6.departamento == null ? null : item_r6.departamento.nombre);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](item_r6.supervisor == null ? null : item_r6.supervisor.nombre);
        }
      }

      function EmpleadoComponent_div_0_div_23_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 35);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "table", 36);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "thead");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "tr");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "th", 37);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "div", 38);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 39);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](7, "Nombre");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "div", 40);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](9, "|");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "th", 41);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "div", 38);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "div", 42);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](13, "Apellido");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "div", 40);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](15, "|");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "th", 41);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "div", 38);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "div", 42);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](19, "Tel\xE9fono");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "div", 40);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](21, "|");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "th", 41);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "div", 38);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "div", 42);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](25, "Correo");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](26, "div", 40);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](27, "|");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "th", 41);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](29, "div", 38);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](30, "div", 42);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](31, "Salario");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](32, "div", 40);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](33, "|");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](34, "th", 41);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](35, "div", 38);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](36, "div", 42);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](37, "Departamento");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](38, "div", 40);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](39, "|");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](40, "th", 41);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](41, "div", 38);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](42, "div", 42);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](43, "Supervisor");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](44, "div", 40);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](45, "|");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](46, "th", 41);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](47, "div", 38);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](48, "div", 42);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](49, "Acciones");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](50, "div", 40);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](51, "|");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](52, "tbody");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](53, EmpleadoComponent_div_0_div_23_tr_53_Template, 25, 12, "tr", 10);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var ctx_r4 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](53);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx_r4.empleados);
        }
      }

      function EmpleadoComponent_div_0_Template(rf, ctx) {
        if (rf & 1) {
          var _r11 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 12);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 13);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 14);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](3, "ngx-ui-loader", 15);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 16);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "h6");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6, "EMPLEADOS");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "div", 17);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "div", 18);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "div", 19);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "a", 20);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "div", 21);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12, "ACCIONES");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "ul", 22);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "li");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "a", 23);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function EmpleadoComponent_div_0_Template_a_click_15_listener() {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r11);

            var ctx_r10 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

            return ctx_r10.exportExcel("excel-table", "Reporte de empleados");
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](16, "Exportar a excel");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "div", 24);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "button", 25);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function EmpleadoComponent_div_0_Template_button_click_18_listener() {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r11);

            var ctx_r12 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

            return ctx_r12.opencreateempleado();
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](19, "i", 26);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](20, " \xA0 CREAR EMPLEADO ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "div", 27);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](22, EmpleadoComponent_div_0_div_22_Template, 6, 0, "div", 28);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](23, EmpleadoComponent_div_0_div_23_Template, 54, 1, "div", 29);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("loaderId", "departamento-loader");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](19);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r0.empleados.length == 0);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r0.empleados.length > 0);
        }
      }

      function EmpleadoComponent_tr_20_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "tr");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "td", 49);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "td", 50);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "td", 51);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "td", 52);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "td", 53);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "td", 54);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "td", 55);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](14);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var item_r13 = ctx.$implicit;

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](item_r13.nombre);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](item_r13.apellido);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](item_r13.telefono);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](item_r13.email);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](item_r13.salario);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](item_r13.departamento == null ? null : item_r13.departamento.nombre);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](item_r13.supervisor == null ? null : item_r13.supervisor.nombre);
        }
      }

      function EmpleadoComponent_app_add_empleado_21_Template(rf, ctx) {
        if (rf & 1) {
          var _r16 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "app-add-empleado", 56);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("hideModalEmitter", function EmpleadoComponent_app_add_empleado_21_Template_app_add_empleado_hideModalEmitter_0_listener() {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r16);

            var ctx_r15 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

            return ctx_r15.hideallModal();
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var ctx_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("empleado", ctx_r2.empleado);
        }
      }

      var EmpleadoComponent = /*#__PURE__*/function () {
        function EmpleadoComponent(empleadoService, formBuilder, ngxService) {
          var _this7 = this;

          _classCallCheck(this, EmpleadoComponent);

          this.empleadoService = empleadoService;
          this.formBuilder = formBuilder;
          this.ngxService = ngxService;
          this.empleados = [];
          this.destroySubscription$ = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
          this.createEmpleado = false;
          this.newEmpleado = false;
          this.editEmpleado = false;
          this.displayNewEmpleado = false;
          this.listEmpleado = true; //cargar la data inicial del componente del departamento

          this.complete = function () {
            _this7.createEmpleado = true;

            _this7.empleadoService.getAllEmpleados(null, null, _this7);
          };

          this.opencreateempleado = function () {
            _this7.displayNewEmpleado = true;
            setTimeout(function () {
              return $('#newEmpleado-empleadoComponent').modal('show');
            });
          };

          this.exportExcel = function () {
            var idTable = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
            var nameFile = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
            var idTableHTML = idTable !== null ? idTable : 'excel-table';
            var element = document.getElementById(idTableHTML);
            var ws = xlsx__WEBPACK_IMPORTED_MODULE_4__["utils"].table_to_sheet(element, {
              raw: true
            });
            /* generate workbook and add the worksheet */

            var wb = xlsx__WEBPACK_IMPORTED_MODULE_4__["utils"].book_new();
            xlsx__WEBPACK_IMPORTED_MODULE_4__["utils"].book_append_sheet(wb, ws, 'Sheet1');
            /* save to file */

            var nameFileEXCEL = nameFile !== null ? nameFile : 'Reporte';
            xlsx__WEBPACK_IMPORTED_MODULE_4__["writeFile"](wb, nameFileEXCEL + '.xlsx');
          };
        }

        _createClass(EmpleadoComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            this.form = this.formBuilder.group({
              tiempo: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"]('', {
                validators: [_angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required]
              }),
              salario: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"]('', {
                validators: [_angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required]
              })
            });
            this.complete();
          } //activar el componente para agregar el departamento

        }, {
          key: "addDepartamento",
          value: function addDepartamento() {
            this.listEmpleado = false;
            this.newEmpleado = true;
          } //retorna de guardar oculpta el formulario y actualiza la data

        }, {
          key: "reverse",
          value: function reverse(back) {
            if (back) {
              this.newEmpleado = false;
              this.editEmpleado = false;
              this.listEmpleado = true;
              this.complete();
            }
          } //eliminar departamento por id

        }, {
          key: "eliminardepartamento",
          value: function eliminardepartamento(idEmpleado) {
            this.empleadoService.deleEmpleadoById(idEmpleado, this);
          } //editar departamento

        }, {
          key: "editempleado",
          value: function editempleado(empleado) {
            this.empleado = empleado;
            this.displayNewEmpleado = true;
            setTimeout(function () {
              return $('#newEmpleado-empleadoComponent').modal('show');
            });
          }
        }, {
          key: "hideallModal",
          value: function hideallModal() {
            console.log('vamos a lokear');
            this.empleado = null;
            this.complete();
            this.displayNewEmpleado = false;
            $('#newEmpleado-empleadoComponent').modal('hide');
          }
        }, {
          key: "ngOnDestroy",
          value: function ngOnDestroy() {
            this.destroySubscription$.next(); // trigger the unsubscribe

            this.destroySubscription$.complete(); // finalize & clean up the subject stream
          }
        }]);

        return EmpleadoComponent;
      }();

      EmpleadoComponent.ɵfac = function EmpleadoComponent_Factory(t) {
        return new (t || EmpleadoComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_services_workflow_empleado_empleado_service__WEBPACK_IMPORTED_MODULE_1__["EmpleadoService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormBuilder"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](ngx_ui_loader__WEBPACK_IMPORTED_MODULE_3__["NgxUiLoaderService"]));
      };

      EmpleadoComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
        type: EmpleadoComponent,
        selectors: [["app-empleado"]],
        decls: 22,
        vars: 3,
        consts: [["class", "emision-table", "style", "width: 100%; height: 100%; margin-top: 100px", 4, "ngIf"], [2, "display", "none"], ["aria-describedby", "tabla", "id", "excel-table"], ["id", "nump"], ["id", "cli"], ["id", "ced"], ["id", "marca"], ["id", "modelo"], ["id", "an"], ["id", "chasis"], [4, "ngFor", "ngForOf"], [3, "empleado", "hideModalEmitter", 4, "ngIf"], [1, "emision-table", 2, "width", "100%", "height", "100%", "margin-top", "100px"], [1, "card"], [1, "card-header"], [3, "loaderId"], [1, "row", "align-items-center", 2, "justify-content", "space-between"], [1, "col-md-3", "col-12"], [1, "col-md-auto", "col-12", "d-flex", "justify-content-center"], [1, "dropdown", "mt-4"], ["aria-expanded", "false", "aria-haspopup", "true", "data-toggle", "dropdown", "id", "btn-closure-dropdownMenuExport", "role", "button", 1, "btn", "btn-outline-green-ref", "width-fix", "dropleft"], [1, "texbtn"], ["aria-labelledby", "navbarDropdownMenuLink", 1, "dropdown-menu", "dropdown-table", "dropdown-menu-right"], ["id", "btn-as-export-to", 1, "dropdown-item", 3, "click"], [1, "text-right"], ["id", "btnAddBrand-vehicleComponent", 1, "btn", "btn-lg", "btn-green", 3, "click"], [1, "fas", "fa-plus"], [1, "card-body", "table-strech"], ["class", "no-contracts", 4, "ngIf"], ["class", "table-responsive", 4, "ngIf"], [1, "no-contracts"], [1, "container"], [1, "row", "justify-content-center"], [1, "col-4", "text-center", "center-data-missing"], [1, "no-contracts-text", "pt-4"], [1, "table-responsive"], ["aria-describedby", " tabla de empleados", 1, "table"], ["id", "nunoperacion"], [1, "row"], [1, "col-auto", "head-title"], [1, "col-auto", "separator-title"], ["id", "fechacreada"], [1, "col-auto", "mr-auto", "head-title"], [1, "table-spacing"], [1, "form-group", 2, "text-align", "left"], [2, "display", "inline-block"], [1, "btn", "icon-field", 3, "click"], [1, "fas", "fa-minus-circle", "fa-lg", "font-color-error"], [1, "fas", "fa-history", "fa-lg", "font-color-changed"], ["id", "nombre"], ["id", "apellido"], ["id", "telefono"], ["id", "correo"], ["id", "salario"], ["id", "nombred"], ["id", "nombres"], [3, "empleado", "hideModalEmitter"]],
        template: function EmpleadoComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](0, EmpleadoComponent_div_0_Template, 24, 3, "div", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "table", 2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "thead");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "tr");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "th", 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6, "NOMBRE");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "th", 4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, "APELLIDO");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "th", 5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10, "TEL\xC9FONO");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "th", 6);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12, "CORREO");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "th", 7);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](14, "SALARIO");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "th", 8);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](16, "DEPARTAMENTO");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "th", 9);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](18, "SUPERVISOR");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "tbody");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](20, EmpleadoComponent_tr_20_Template, 15, 7, "tr", 10);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](21, EmpleadoComponent_app_add_empleado_21_Template, 1, 1, "app-add-empleado", 11);
          }

          if (rf & 2) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.listEmpleado);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](20);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx.empleados);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.displayNewEmpleado);
          }
        },
        directives: [_angular_common__WEBPACK_IMPORTED_MODULE_6__["NgIf"], _angular_common__WEBPACK_IMPORTED_MODULE_6__["NgForOf"], ngx_ui_loader__WEBPACK_IMPORTED_MODULE_3__["ɵb"], _add_empleado_add_empleado_component__WEBPACK_IMPORTED_MODULE_7__["AddEmpleadoComponent"]],
        pipes: [_shared_pipes_format_money_ec_pipe__WEBPACK_IMPORTED_MODULE_8__["FormatMoneyEcPipe"], _angular_common__WEBPACK_IMPORTED_MODULE_6__["CurrencyPipe"]],
        styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJlbXBsZWFkby5jb21wb25lbnQuc2NzcyJ9 */"]
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](EmpleadoComponent, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
          args: [{
            selector: 'app-empleado',
            templateUrl: './empleado.component.html',
            styleUrls: ['./empleado.component.scss']
          }]
        }], function () {
          return [{
            type: _services_workflow_empleado_empleado_service__WEBPACK_IMPORTED_MODULE_1__["EmpleadoService"]
          }, {
            type: _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormBuilder"]
          }, {
            type: ngx_ui_loader__WEBPACK_IMPORTED_MODULE_3__["NgxUiLoaderService"]
          }];
        }, null);
      })();
      /***/

    },

    /***/
    "My/+":
    /*!*******************************************************************************************!*\
      !*** ./src/app/admin/components/workflow/empleado/add-empleado/add-empleado.component.ts ***!
      \*******************************************************************************************/

    /*! exports provided: AddEmpleadoComponent */

    /***/
    function My(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AddEmpleadoComponent", function () {
        return AddEmpleadoComponent;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/forms */
      "3Pt+");
      /* harmony import */


      var ngx_ui_loader__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ngx-ui-loader */
      "gren");
      /* harmony import */


      var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! rxjs */
      "qCKp");
      /* harmony import */


      var src_app_shared_classes__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! src/app/shared/classes */
      "gtvE");
      /* harmony import */


      var _services_workflow_departamento_departamento_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ../../../../services/workflow/departamento/departamento.service */
      "tmbA");
      /* harmony import */


      var _services_workflow_supervisor_supervisor_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ../../../../services/workflow/supervisor/supervisor.service */
      "Itk3");
      /* harmony import */


      var _services_workflow_empleado_empleado_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! ../../../../services/workflow/empleado/empleado.service */
      "k4qQ");
      /* harmony import */


      var _services_workflow_sueldo_sueldo_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! ../../../../services/workflow/sueldo/sueldo.service */
      "BnFf");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");
      /* harmony import */


      var _ng_select_ng_select__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
      /*! @ng-select/ng-select */
      "ZOsW");
      /* harmony import */


      var _shared_pipes_format_money_ec_pipe__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
      /*! ../../../../../shared/pipes/format-money-ec.pipe */
      "AElR");

      function AddEmpleadoComponent_div_57_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 42);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", ctx_r0.error.nombre.msg, " ");
        }
      }

      function AddEmpleadoComponent_div_58_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 43);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "em", 44);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      function AddEmpleadoComponent_div_59_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 43);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "em", 45);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      function AddEmpleadoComponent_div_69_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 42);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var ctx_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", ctx_r3.error.id_departamento.msg, " ");
        }
      }

      function AddEmpleadoComponent_div_70_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 43);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "em", 44);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      function AddEmpleadoComponent_div_71_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 43);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "em", 45);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      function AddEmpleadoComponent_div_87_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 42);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var ctx_r6 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", ctx_r6.error.nombre.msg, " ");
        }
      }

      function AddEmpleadoComponent_div_88_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 43);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "em", 44);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      function AddEmpleadoComponent_div_89_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 43);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "em", 45);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      function AddEmpleadoComponent_div_100_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 42);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var ctx_r9 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", ctx_r9.error.apellido.msg, " ");
        }
      }

      function AddEmpleadoComponent_div_101_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 43);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "em", 44);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      function AddEmpleadoComponent_div_102_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 43);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "em", 45);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      function AddEmpleadoComponent_div_113_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 42);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var ctx_r12 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", ctx_r12.error.email.msg, " ");
        }
      }

      function AddEmpleadoComponent_div_114_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 43);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "em", 44);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      function AddEmpleadoComponent_div_115_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 43);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "em", 45);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      function AddEmpleadoComponent_div_127_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 42);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var ctx_r15 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", ctx_r15.error.telefono.msg, " ");
        }
      }

      function AddEmpleadoComponent_div_128_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 43);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "em", 44);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      function AddEmpleadoComponent_div_129_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 43);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "em", 45);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      function AddEmpleadoComponent_div_140_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 42);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var ctx_r18 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", ctx_r18.error.celular.msg, " ");
        }
      }

      function AddEmpleadoComponent_div_141_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 43);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "em", 44);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      function AddEmpleadoComponent_div_142_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 43);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "em", 45);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      function AddEmpleadoComponent_div_153_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 42);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var ctx_r21 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", ctx_r21.error.salario.msg, " ");
        }
      }

      function AddEmpleadoComponent_div_154_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 43);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "em", 44);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      function AddEmpleadoComponent_div_155_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 43);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "em", 45);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      var _c0 = function _c0(a0, a1) {
        return {
          "is-invalid": a0,
          "is-valid": a1
        };
      };

      var AddEmpleadoComponent = /*#__PURE__*/function () {
        function AddEmpleadoComponent(empleadoService, sueldoService, formBuilder, ngxService, helper, departamentoService, supervisorService) {
          var _this8 = this;

          _classCallCheck(this, AddEmpleadoComponent);

          this.empleadoService = empleadoService;
          this.sueldoService = sueldoService;
          this.formBuilder = formBuilder;
          this.ngxService = ngxService;
          this.helper = helper;
          this.departamentoService = departamentoService;
          this.supervisorService = supervisorService;
          this.hideModalEmitter = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
          this.newForm = true;
          this.destroySubscription$ = new rxjs__WEBPACK_IMPORTED_MODULE_3__["Subject"]();
          this.appraisersList = [];
          this.textbtn = "CREAR";
          this.empleadoForm = ['nombre', 'apellido', 'email', 'telefono', 'celular', 'salario', 'id_departamento', 'id_supervisor']; //iniciar los errores y las validaciones

          this.errorInit = function () {
            _this8.error = {
              nombre: {
                error: false,
                change: false,
                msg: ''
              },
              apellido: {
                error: false,
                change: false,
                msg: ''
              },
              email: {
                error: false,
                change: false,
                msg: ''
              },
              telefono: {
                error: false,
                change: false,
                msg: ''
              },
              celular: {
                error: false,
                change: false,
                msg: ''
              },
              salario: {
                error: false,
                change: false,
                msg: ''
              },
              id_departamento: {
                error: false,
                change: false,
                msg: ''
              },
              id_supervisor: {
                error: false,
                change: false,
                msg: ''
              },
              tiempo: {
                error: false,
                change: false,
                msg: ''
              },
              salarios: {
                error: false,
                change: false,
                msg: ''
              }
            };
          }; //cargar la data inicial del componente del Supervisor


          this.complete = function () {
            _this8.supervisorService.getAllSupervisores(null, null, _this8);

            _this8.departamentoService.getAllDepartamentos(null, null, _this8);
          };

          this.actionAppraisal = function () {
            var _a;

            var form = _this8.form.value;

            var departamento = _this8.departamentos.filter(function (nickname) {
              return nickname.id == form.id_departamento;
            });

            var supervisor = _this8.supervisores.filter(function (nickname) {
              return nickname.id == form.id_supervisor;
            });

            var data = {
              id: (_a = _this8.empleado) === null || _a === void 0 ? void 0 : _a.id,
              nombre: form.nombre,
              apellido: form.apellido,
              email: form.email,
              telefono: form.telefono,
              celular: form.celular,
              salario: form.salario,
              departamento: departamento[0],
              supervisor: supervisor[0]
            };

            _this8.empleadoService.addEmpleado(data, _this8);
          };

          this.calcular = function () {
            var form = _this8.formcal.value;
            var data = {
              tiempo: form.tiempo,
              salario: form.salarios
            };

            _this8.sueldoService.getAumento(data, null, _this8);
          };
        }

        _createClass(AddEmpleadoComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            this.errorInit();
            this.complete(); //validaciones del formulario

            this.form = this.formBuilder.group({
              nombre: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', {
                validators: [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]
              }),
              apellido: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', {
                validators: [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]
              }),
              email: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', {
                validators: [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]
              }),
              telefono: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', {
                validators: [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]
              }),
              celular: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', {
                validators: [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]
              }),
              salario: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', {
                validators: [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]
              }),
              id_departamento: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](null, {
                validators: [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]
              }),
              id_supervisor: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](null, {
                validators: [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]
              })
            });
            this.formcal = this.formBuilder.group({
              tiempo: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', {
                validators: [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]
              }),
              salarios: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', {
                validators: [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]
              })
            });

            if (this.empleado) {
              this.textbtn = "ACTUALIZAR";
              this.fillForm(this.empleado);
            }
          } //cargar data al formulario

        }, {
          key: "fillForm",
          value: function fillForm(item) {
            var _this9 = this;

            console.log(item);
            this.empleadoForm.forEach(function (i) {
              console.log(i);

              switch (i) {
                case 'id_departamento':
                  _this9.form.get('id_departamento').reset(item['departamento'].id);

                  break;

                case 'id_supervisor':
                  _this9.form.get('id_supervisor').reset(item['supervisor'].id);

                  break;

                default:
                  _this9.form.get(i).reset(item[i]);

                  break;
              }
            });
          }
        }, {
          key: "resetServiceBtn",
          value: function resetServiceBtn() {
            var close = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;
            this.hideModalEmitter.emit(false);
          }
        }, {
          key: "hideTooltips",
          value: function hideTooltips() {
            $('[data-toggle="tooltip"]').tooltip('hide');
          }
        }, {
          key: "ngOnDestroy",
          value: function ngOnDestroy() {
            this.destroySubscription$.next(); // trigger the unsubscribe

            this.destroySubscription$.complete(); // finalize & clean up the subject stream
          }
        }]);

        return AddEmpleadoComponent;
      }();

      AddEmpleadoComponent.ɵfac = function AddEmpleadoComponent_Factory(t) {
        return new (t || AddEmpleadoComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_services_workflow_empleado_empleado_service__WEBPACK_IMPORTED_MODULE_7__["EmpleadoService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_services_workflow_sueldo_sueldo_service__WEBPACK_IMPORTED_MODULE_8__["SueldoService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](ngx_ui_loader__WEBPACK_IMPORTED_MODULE_2__["NgxUiLoaderService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_shared_classes__WEBPACK_IMPORTED_MODULE_4__["Helpers"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_services_workflow_departamento_departamento_service__WEBPACK_IMPORTED_MODULE_5__["DepartamentoService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_services_workflow_supervisor_supervisor_service__WEBPACK_IMPORTED_MODULE_6__["SupervisorService"]));
      };

      AddEmpleadoComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
        type: AddEmpleadoComponent,
        selectors: [["app-add-empleado"]],
        inputs: {
          empleado: "empleado"
        },
        outputs: {
          hideModalEmitter: "hideModalEmitter"
        },
        decls: 161,
        vars: 70,
        consts: [["aria-labelledby", "NEW-empleadoComponent", "data-backdrop", "static", "id", "newEmpleado-empleadoComponent", "role", "dialog", "tabindex", "-1", 1, "modal", "right", "fade"], ["role", "document", 1, "modal-dialog"], [1, "modal-content", "empleadoComponent"], [1, "modal-header-ref"], [1, "modal-title"], ["aria-label", "Close", "data-dismiss", "modal", "type", "button", 1, "btn-close-ref", 3, "click"], ["id", "activationRefactorDocumentModal-resetbtn", 1, "fas", "fa-times"], [1, "modal-body", 3, "formGroup"], [1, "appraisalModal-title"], [1, "row"], [1, "col", "pb-1"], [1, "col-md-3", "col-12"], [1, "form-group"], ["type", "text", "data-hj-allow", "", "formControlName", "tiempo", "id", "tiempo-appraisalComponent", "name", "tiempo", "placeholder", "ej: $1", 1, "form-control", "form-input", 3, "ngClass", "blur", "input"], ["for", "nombre-appraisalComponent"], ["type", "text", "data-hj-allow", "", "formControlName", "salarios", "id", "salarios-appraisalComponent", "name", "salarios", "placeholder", "ej: $10000", 1, "form-control", "form-input", 3, "ngClass", "blur", "input"], ["aria-expanded", "false", "aria-haspopup", "true", "data-toggle", "dropdown", "id", "btn-closure-dropdownMenuExport", "role", "button", 1, "btn", "btn-outline-green-ref", "width-fix", "dropleft", 2, "margin-top", "18%", 3, "click"], [1, "col-12"], [1, "d-flex", "flex-column"], [1, "col-4"], [1, "fiel-validate"], [1, "field"], ["bindLabel", "nombre", "bindValue", "id", "clearAllText", "limpiar campo", "formControlName", "id_supervisor", "id", "id_province_appraisal-appraisalComponent", "ngDefaultControl", "", "placeholder", "Seleccione el supervisor", 1, "ng-activate", "strech", 3, "items", "selectOnTab", "blur", "change"], ["class", "invalid-feedback font-color-error", 4, "ngIf"], ["class", "icon-field", 4, "ngIf"], ["bindLabel", "nombre", "bindValue", "id", "clearAllText", "limpiar campo", "formControlName", "id_departamento", "id", "id_province_appraisal-appraisalComponent", "ngDefaultControl", "", "placeholder", "Seleccione el departamento", 1, "ng-activate", "strech", 3, "items", "selectOnTab", "blur", "change"], [1, "inputGroup-wizard"], ["type", "text", "data-hj-allow", "", "formControlName", "nombre", "id", "nombre-appraisalComponent", "name", "nombre", "placeholder", "ej: Edison ", 1, "form-control", "form-input", 3, "ngClass", "blur", "input"], ["for", "apellido-appraisalComponent"], ["type", "text", "data-hj-allow", "", "formControlName", "apellido", "id", "apellido-appraisalComponent", "name", "apellido", "placeholder", "ej: Vicente", 1, "form-control", "form-input", 3, "ngClass", "blur", "input"], ["for", "email-appraisalComponent"], ["type", "text", "data-hj-allow", "", "formControlName", "email", "id", "email-appraisalComponent", "name", "email", "placeholder", "ej: vicenteedison92@gmail.com", 1, "form-control", "form-input", 3, "ngClass", "blur", "input"], ["for", "telefono-appraisalComponent"], ["type", "text", "data-hj-allow", "", "formControlName", "telefono", "id", "telefono-appraisalComponent", "name", "telefono", "placeholder", "ej: 097932320", 1, "form-control", "form-input", 3, "ngClass", "blur", "input"], ["for", "celular-appraisalComponent"], ["type", "text", "data-hj-allow", "", "formControlName", "celular", "id", "celular-appraisalComponent", "name", "celular", "placeholder", "ej: 097932320", 1, "form-control", "form-input", 3, "ngClass", "blur", "input"], ["for", "salario-appraisalComponent"], ["type", "text", "data-hj-allow", "", "formControlName", "salario", "id", "salario-appraisalComponent", "name", "salario", "placeholder", "ej: $1000", 1, "form-control", "form-input", 3, "ngClass", "blur", "input"], [1, "modal-footer"], [1, "row", "text-right", "btn-float"], [1, "col"], [1, "btn", "btn-block", "btn-green", 3, "disabled", "click"], [1, "invalid-feedback", "font-color-error"], [1, "icon-field"], [1, "fas", "fa-exclamation-circle", "fa-lg", "font-color-error"], [1, "fas", "fa-check-circle", "fa-lg", "font-color-success"]],
        template: function AddEmpleadoComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "h4", 4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "NUEVO EMPLEADO");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "button", 5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function AddEmpleadoComponent_Template_button_click_6_listener() {
              return ctx.resetServiceBtn(true);
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](7, "em", 6);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "div", 7);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "h4", 8);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10, "Calcular aumento de salario");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "div", 9);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "div", 10);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "small");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](14, " Ingresa los valores aproximados para calcular el salario final con el aumento, segun su experiencia");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "div", 9);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "div", 11);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "p");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "b");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](19, "Tiempo de trabajo");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "div", 12);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "input", 13);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("blur", function AddEmpleadoComponent_Template_input_blur_21_listener() {
              return ctx.helper.validInput(ctx.error, "tiempo", ctx.form);
            })("input", function AddEmpleadoComponent_Template_input_input_21_listener() {
              return ctx.helper.validInput(ctx.error, "tiempo", ctx.form);
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "div", 11);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "p");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "b");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](25, "Salario");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](26, "label", 14);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "div", 12);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "input", 15);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("blur", function AddEmpleadoComponent_Template_input_blur_28_listener() {
              return ctx.helper.validInput(ctx.error, "salarios", ctx.form);
            })("input", function AddEmpleadoComponent_Template_input_input_28_listener() {
              return ctx.helper.validInput(ctx.error, "salarios", ctx.form);
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](29, "div", 11);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](30, "div", 12);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](31, "a", 16);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function AddEmpleadoComponent_Template_a_click_31_listener() {
              return ctx.calcular();
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](32, "div");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](33, "CALCULAR");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](34, "h1");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](35);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](36, "formatMoneyEc");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](37, "currency");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](38, "div", 7);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](39, "div", 17);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](40, "div", 18);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](41, "h4", 8);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](42, "Datos del empleado");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](43, "div", 9);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](44, "div", 10);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](45, "small");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](46, "* Campos requeridos");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](47, "div", 9);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](48, "div", 19);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](49, "p");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](50, "b");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](51, "Supervisor*");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](52, "div", 12);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](53, "div", 20);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](54, "div", 21);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](55, "label", 14);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](56, "ng-select", 22);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("blur", function AddEmpleadoComponent_Template_ng_select_blur_56_listener() {
              return ctx.helper.validInput(ctx.error, "id_supervisor", ctx.form);
            })("change", function AddEmpleadoComponent_Template_ng_select_change_56_listener() {
              return ctx.helper.validInput(ctx.error, "id_supervisor", ctx.form);
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](57, AddEmpleadoComponent_div_57_Template, 2, 1, "div", 23);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](58, AddEmpleadoComponent_div_58_Template, 2, 0, "div", 24);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](59, AddEmpleadoComponent_div_59_Template, 2, 0, "div", 24);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](60, "div", 19);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](61, "p");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](62, "b");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](63, "Departamento*");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](64, "div", 12);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](65, "div", 20);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](66, "div", 21);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](67, "label", 14);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](68, "ng-select", 25);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("blur", function AddEmpleadoComponent_Template_ng_select_blur_68_listener() {
              return ctx.helper.validInput(ctx.error, "id_departamento", ctx.form);
            })("change", function AddEmpleadoComponent_Template_ng_select_change_68_listener() {
              return ctx.helper.validInput(ctx.error, "id_departamento", ctx.form);
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](69, AddEmpleadoComponent_div_69_Template, 2, 1, "div", 23);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](70, AddEmpleadoComponent_div_70_Template, 2, 0, "div", 24);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](71, AddEmpleadoComponent_div_71_Template, 2, 0, "div", 24);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](72, "div", 17);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](73, "div", 18);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](74, "h4", 8);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](75, "Datos del empleado");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](76, "div", 9);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](77, "div", 19);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](78, "p");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](79, "b");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](80, "Nombre*");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](81, "div", 12);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](82, "div", 20);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](83, "div", 21);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](84, "div", 26);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](85, "label", 14);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](86, "input", 27);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("blur", function AddEmpleadoComponent_Template_input_blur_86_listener() {
              return ctx.helper.validInput(ctx.error, "nombre", ctx.form);
            })("input", function AddEmpleadoComponent_Template_input_input_86_listener() {
              return ctx.helper.validInput(ctx.error, "nombre", ctx.form);
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](87, AddEmpleadoComponent_div_87_Template, 2, 1, "div", 23);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](88, AddEmpleadoComponent_div_88_Template, 2, 0, "div", 24);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](89, AddEmpleadoComponent_div_89_Template, 2, 0, "div", 24);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](90, "div", 19);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](91, "p");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](92, "b");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](93, "Apellido*");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](94, "div", 12);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](95, "div", 20);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](96, "div", 21);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](97, "div", 26);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](98, "label", 28);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](99, "input", 29);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("blur", function AddEmpleadoComponent_Template_input_blur_99_listener() {
              return ctx.helper.validInput(ctx.error, "apellido", ctx.form);
            })("input", function AddEmpleadoComponent_Template_input_input_99_listener() {
              return ctx.helper.validInput(ctx.error, "apellido", ctx.form);
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](100, AddEmpleadoComponent_div_100_Template, 2, 1, "div", 23);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](101, AddEmpleadoComponent_div_101_Template, 2, 0, "div", 24);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](102, AddEmpleadoComponent_div_102_Template, 2, 0, "div", 24);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](103, "div", 19);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](104, "p");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](105, "b");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](106, "Correo *");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](107, "div", 12);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](108, "div", 20);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](109, "div", 21);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](110, "div", 26);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](111, "label", 30);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](112, "input", 31);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("blur", function AddEmpleadoComponent_Template_input_blur_112_listener() {
              return ctx.helper.validInput(ctx.error, "email", ctx.form);
            })("input", function AddEmpleadoComponent_Template_input_input_112_listener() {
              return ctx.helper.validInput(ctx.error, "email", ctx.form);
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](113, AddEmpleadoComponent_div_113_Template, 2, 1, "div", 23);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](114, AddEmpleadoComponent_div_114_Template, 2, 0, "div", 24);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](115, AddEmpleadoComponent_div_115_Template, 2, 0, "div", 24);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](116, "div", 9);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](117, "div", 19);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](118, "p");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](119, "b");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](120, "Tel\xE9fono *");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](121, "div", 12);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](122, "div", 20);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](123, "div", 21);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](124, "div", 26);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](125, "label", 32);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](126, "input", 33);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("blur", function AddEmpleadoComponent_Template_input_blur_126_listener() {
              return ctx.helper.validInput(ctx.error, "telefono", ctx.form);
            })("input", function AddEmpleadoComponent_Template_input_input_126_listener() {
              return ctx.helper.validInput(ctx.error, "telefono", ctx.form);
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](127, AddEmpleadoComponent_div_127_Template, 2, 1, "div", 23);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](128, AddEmpleadoComponent_div_128_Template, 2, 0, "div", 24);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](129, AddEmpleadoComponent_div_129_Template, 2, 0, "div", 24);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](130, "div", 19);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](131, "p");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](132, "b");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](133, "Celular *");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](134, "div", 12);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](135, "div", 20);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](136, "div", 21);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](137, "div", 26);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](138, "label", 34);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](139, "input", 35);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("blur", function AddEmpleadoComponent_Template_input_blur_139_listener() {
              return ctx.helper.validInput(ctx.error, "celular", ctx.form);
            })("input", function AddEmpleadoComponent_Template_input_input_139_listener() {
              return ctx.helper.validInput(ctx.error, "celular", ctx.form);
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](140, AddEmpleadoComponent_div_140_Template, 2, 1, "div", 23);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](141, AddEmpleadoComponent_div_141_Template, 2, 0, "div", 24);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](142, AddEmpleadoComponent_div_142_Template, 2, 0, "div", 24);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](143, "div", 19);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](144, "p");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](145, "b");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](146, "Salario *");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](147, "div", 12);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](148, "div", 20);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](149, "div", 21);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](150, "div", 26);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](151, "label", 36);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](152, "input", 37);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("blur", function AddEmpleadoComponent_Template_input_blur_152_listener() {
              return ctx.helper.validInput(ctx.error, "salario", ctx.form);
            })("input", function AddEmpleadoComponent_Template_input_input_152_listener() {
              return ctx.helper.validInput(ctx.error, "salario", ctx.form);
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](153, AddEmpleadoComponent_div_153_Template, 2, 1, "div", 23);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](154, AddEmpleadoComponent_div_154_Template, 2, 0, "div", 24);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](155, AddEmpleadoComponent_div_155_Template, 2, 0, "div", 24);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](156, "div", 38);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](157, "div", 39);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](158, "div", 40);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](159, "button", 41);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function AddEmpleadoComponent_Template_button_click_159_listener() {
              return ctx.form.valid && ctx.actionAppraisal();
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](160);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
          }

          if (rf & 2) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](8);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("formGroup", ctx.formcal);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](13);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction2"](46, _c0, ctx.error.tiempo.error, !ctx.error.tiempo.error && ctx.error.tiempo.change));

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction2"](49, _c0, ctx.error.salarios.error, !ctx.error.salarios.error && ctx.error.salarios.change));

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](36, 41, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind2"](37, 43, ctx.saldo, "USD")));

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("formGroup", ctx.form);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](18);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("items", ctx.supervisores)("selectOnTab", true);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", (ctx.form.get("nombre").errors == null ? null : ctx.form.get("nombre").errors.required) || (ctx.form.get("nombre").errors == null ? null : ctx.form.get("nombre").errors.fullName));

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.error.nombre.error && ctx.error.nombre.change);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx.error.nombre.error && ctx.error.nombre.change);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](9);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("items", ctx.departamentos)("selectOnTab", true);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", (ctx.form.get("id_departamento").errors == null ? null : ctx.form.get("id_departamento").errors.required) || (ctx.form.get("id_departamento").errors == null ? null : ctx.form.get("id_departamento").errors.fullName));

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.error.id_departamento.error && ctx.error.id_departamento.change);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx.error.id_departamento.error && ctx.error.id_departamento.change);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](15);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction2"](52, _c0, ctx.error.nombre.error, !ctx.error.nombre.error && ctx.error.nombre.change));

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", (ctx.form.get("nombre").errors == null ? null : ctx.form.get("nombre").errors.required) || (ctx.form.get("nombre").errors == null ? null : ctx.form.get("nombre").errors.fullName));

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.error.nombre.error && ctx.error.nombre.change);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx.error.nombre.error && ctx.error.nombre.change);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](10);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction2"](55, _c0, ctx.error.apellido.error, !ctx.error.apellido.error && ctx.error.apellido.change));

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", (ctx.form.get("apellido").errors == null ? null : ctx.form.get("apellido").errors.required) || (ctx.form.get("apellido").errors == null ? null : ctx.form.get("apellido").errors.cellphone));

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.error.apellido.error && ctx.error.apellido.change);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx.error.apellido.error && ctx.error.apellido.change);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](10);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction2"](58, _c0, ctx.error.email.error, !ctx.error.email.error && ctx.error.email.change));

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", (ctx.form.get("email").errors == null ? null : ctx.form.get("email").errors.required) || (ctx.form.get("email").errors == null ? null : ctx.form.get("email").errors.cellphone));

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.error.email.error && ctx.error.email.change);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx.error.email.error && ctx.error.email.change);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](11);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction2"](61, _c0, ctx.error.telefono.error, !ctx.error.telefono.error && ctx.error.telefono.change));

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.form.get("telefono").errors == null ? null : ctx.form.get("telefono").errors.required);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.error.telefono.error && ctx.error.telefono.change);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx.error.telefono.error && ctx.error.telefono.change);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](10);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction2"](64, _c0, ctx.error.celular.error, !ctx.error.celular.error && ctx.error.celular.change));

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.form.get("celular").errors == null ? null : ctx.form.get("celular").errors.required);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.error.celular.error && ctx.error.celular.change);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx.error.celular.error && ctx.error.celular.change);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](10);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction2"](67, _c0, ctx.error.salario.error, !ctx.error.salario.error && ctx.error.salario.change));

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.form.get("salario").errors == null ? null : ctx.form.get("salario").errors.required);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.error.salario.error && ctx.error.salario.change);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx.error.salario.error && ctx.error.salario.change);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("disabled", !ctx.form.valid);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", ctx.textbtn, " ");
          }
        },
        directives: [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["NgControlStatusGroup"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroupDirective"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["DefaultValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControlName"], _angular_common__WEBPACK_IMPORTED_MODULE_9__["NgClass"], _ng_select_ng_select__WEBPACK_IMPORTED_MODULE_10__["NgSelectComponent"], _angular_common__WEBPACK_IMPORTED_MODULE_9__["NgIf"]],
        pipes: [_shared_pipes_format_money_ec_pipe__WEBPACK_IMPORTED_MODULE_11__["FormatMoneyEcPipe"], _angular_common__WEBPACK_IMPORTED_MODULE_9__["CurrencyPipe"]],
        styles: ["@import url(\"https://fonts.googleapis.com/css?family=Hind:300,400&display=swap\");\n.modal.right[_ngcontent-%COMP%]   .modal-dialog[_ngcontent-%COMP%] {\n  position: fixed;\n  margin: auto;\n  max-width: 1000px;\n  width: 65%;\n  height: 100%;\n  transform: translate3d(0%, 0, 0);\n}\n.modal.right.fade[_ngcontent-%COMP%]   .modal-dialog[_ngcontent-%COMP%] {\n  right: 0;\n  transition: opacity 0.3s linear, right 0.3s ease-out;\n}\n.modal.right.fade.in[_ngcontent-%COMP%]   .modal-dialog[_ngcontent-%COMP%] {\n  right: 0;\n}\n.texbtnsss[_ngcontent-%COMP%] {\n  color: #000;\n}\n.card-header[_ngcontent-%COMP%] {\n  background-color: #efefef;\n  color: #000 !important;\n  width: 100%;\n  border: none;\n  text-align: left;\n  height: 50px;\n}\n.table[_ngcontent-%COMP%] {\n  width: 100%;\n}\n.subtitle-refactoriced[_ngcontent-%COMP%] {\n  font-family: Roboto-Regular, sans-serif;\n  color: #414141;\n  font-size: 16px;\n}\n.formdocument[_ngcontent-%COMP%] {\n  margin-top: 2%;\n}\n.subtitle-refactoriceds[_ngcontent-%COMP%] {\n  font-family: Roboto-Regular, sans-serif;\n  color: #ffffff;\n  font-size: 16px;\n}\n.contentmodal[_ngcontent-%COMP%] {\n  margin-top: 5%;\n  width: 95%;\n  margin-left: 3%;\n}\n.card-header[_ngcontent-%COMP%]:after {\n  color: aliceblue;\n  float: right;\n  align-content: center;\n  height: 100%;\n  margin-top: -20px;\n}\n.btn-green-ref[_ngcontent-%COMP%] {\n  width: 142px;\n  height: 36px;\n  margin: 11px 62px 0 60px;\n  padding: 6px 40px 7px 42px;\n  border-radius: 7px;\n  border: solid 1px #4a427f;\n}\n.texbtn[_ngcontent-%COMP%] {\n  font-family: Roboto-Regular, sans-serif;\n  font-size: 14px;\n  font-weight: 500;\n  font-stretch: normal;\n  font-style: normal;\n  line-height: normal;\n  letter-spacing: normal;\n  text-align: center;\n  color: #fff;\n  width: 100%;\n}\n.texbtnd[_ngcontent-%COMP%] {\n  font-family: Roboto-Regular, sans-serif;\n  font-size: 14px;\n  font-weight: 500;\n  font-stretch: normal;\n  font-style: normal;\n  line-height: normal;\n  letter-spacing: normal;\n  text-align: center;\n  color: black;\n  width: 100%;\n}\n.card-header.collapsed[_ngcontent-%COMP%]::after {\n  color: #202020 !important;\n  content: \"+\";\n  font-size: 30px !important;\n  font-family: Roboto-Regular, sans-serif;\n  margin-top: -5%;\n  border-radius: 100px;\n}\n.card-header[_ngcontent-%COMP%]:not(.collapsed)::after {\n  color: #59537f !important;\n  content: \"-\";\n  font-size: 50px !important;\n  transform: rotate(180deg);\n  margin-top: -5%;\n  font-family: Roboto-Regular, sans-serif;\n}\n.title-accordi[_ngcontent-%COMP%] {\n  color: #000 !important;\n  font-family: Roboto-Regular, sans-serif;\n  font-size: 16px;\n}\n.modal.right[_ngcontent-%COMP%]   .modal-content[_ngcontent-%COMP%] {\n  height: 100%;\n  overflow-y: auto;\n}\n.modal.right[_ngcontent-%COMP%]   .modal-body[_ngcontent-%COMP%] {\n  padding: 15px 15px 80px;\n}\n[_ngcontent-%COMP%]:-moz-placeholder-shown {\n  font-family: Roboto-Regular, sans-serif;\n  font-size: 14px;\n  min-height: 36px;\n}\n.activationModal[_ngcontent-%COMP%], [_ngcontent-%COMP%]:placeholder-shown {\n  font-family: Roboto-Regular, sans-serif;\n  font-size: 14px;\n  min-height: 36px;\n}\n[_ngcontent-%COMP%]:-moz-placeholder-shown   .btn-green[_ngcontent-%COMP%] {\n  font-size: 13px;\n  width: 175px;\n  height: 35px;\n}\n.activationModal[_ngcontent-%COMP%]   .btn-green[_ngcontent-%COMP%], [_ngcontent-%COMP%]:placeholder-shown   .btn-green[_ngcontent-%COMP%] {\n  font-size: 13px;\n  width: 175px;\n  height: 35px;\n}\n[_ngcontent-%COMP%]:-moz-placeholder-shown   .fiel-validate[_ngcontent-%COMP%]   .icon-field[_ngcontent-%COMP%] {\n  height: 36px;\n}\n.activationModal[_ngcontent-%COMP%]   .fiel-validate[_ngcontent-%COMP%]   .icon-field[_ngcontent-%COMP%], [_ngcontent-%COMP%]:placeholder-shown   .fiel-validate[_ngcontent-%COMP%]   .icon-field[_ngcontent-%COMP%] {\n  height: 36px;\n}\n.modal-header-ref[_ngcontent-%COMP%] {\n  display: flex;\n  flex-direction: row;\n  align-items: center;\n  justify-content: center;\n}\n.modal-title[_ngcontent-%COMP%] {\n  margin-top: 5%;\n  text-align: center;\n  font-family: Roboto-Regular, sans-serif;\n  font-size: 21px;\n  color: #414141;\n}\n.activationModal-title[_ngcontent-%COMP%] {\n  font-family: Roboto-Regular, sans-serif;\n  font-size: 13px;\n  text-transform: uppercase;\n  font-weight: 700;\n}\n.modarDelete[_ngcontent-%COMP%] {\n  position: relative;\n}\nlabel[_ngcontent-%COMP%] {\n  display: none;\n}\n.addfiles[_ngcontent-%COMP%] {\n  padding: 15px 23px 14px 25px;\n  justify-content: space-between;\n}\n.btn-download[_ngcontent-%COMP%] {\n  width: inherit;\n  height: initial;\n  margin: 10px 0;\n  padding-top: 6px;\n  -webkit-text-size-adjust: 85%;\n     -moz-text-size-adjust: 85%;\n          text-size-adjust: 85%;\n}\n.card-header[_ngcontent-%COMP%] {\n  background-color: #e0e0e0;\n}\n.cardprimary[_ngcontent-%COMP%] {\n  width: 96%;\n  margin-left: 2%;\n}\n.flex-upref[_ngcontent-%COMP%] {\n  min-width: 31%;\n}\n.titlecard[_ngcontent-%COMP%] {\n  font-family: Lato-Regular, sans-serif;\n  font-size: 12px;\n  color: #000000;\n  text-align: left;\n  width: 250px;\n}\n.Rectangle[_ngcontent-%COMP%] {\n  width: 100%;\n  padding: 15px 23px 14px 25px;\n  border-radius: 4px;\n  border: solid 1px #efefef;\n  background-color: #efefef;\n  justify-content: space-between;\n  margin-top: 2%;\n}\n.title-refactoriced[_ngcontent-%COMP%] {\n  font-family: Roboto-Bold, sans-serif;\n  margin-left: 1.5%;\n  font-size: 18px;\n  font-weight: bold;\n  font-stretch: normal;\n  font-style: normal;\n  line-height: normal;\n  letter-spacing: normal;\n  color: #4a4a4a;\n}\n.highlight-field[_ngcontent-%COMP%]   p[_ngcontent-%COMP%] {\n  color: #3d7d41;\n  font-family: Roboto-Bold, sans-serif;\n  font-size: 14px;\n  text-decoration: underline;\n}\n.btn-float[_ngcontent-%COMP%] {\n  position: fixed;\n  bottom: 20px;\n  right: 0;\n}\ntbody[_ngcontent-%COMP%]   td[_ngcontent-%COMP%] {\n  font-size: 13px;\n}\n.header[_ngcontent-%COMP%] {\n  margin-top: 250px;\n}\n*[_ngcontent-%COMP%] {\n  box-sizing: border-box;\n}\n*[_ngcontent-%COMP%]::before, *[_ngcontent-%COMP%]::after {\n  box-sizing: border-box;\n}\nbody[_ngcontent-%COMP%] {\n  margin: 0;\n  padding: 0;\n  font-family: \"Hind\", sans-serif;\n  background: #fff;\n  color: #4d5974;\n  display: flex;\n  min-height: 100vh;\n}\n.container[_ngcontent-%COMP%] {\n  width: 100%;\n}\n.accordion[_ngcontent-%COMP%]   .accordion-item[_ngcontent-%COMP%] {\n  border-bottom: 1px solid #e5e5e5;\n}\n.accordion[_ngcontent-%COMP%]   .accordion-item[_ngcontent-%COMP%]   button[aria-expanded=true][_ngcontent-%COMP%] {\n  border-bottom: 1px solid #4a427f;\n}\n.accordion[_ngcontent-%COMP%]   .btn-cancelar[_ngcontent-%COMP%] {\n  border-radius: 7px;\n  width: 100px;\n  height: 30px;\n  border: solid 1px #979797;\n  display: flex;\n  align-items: center;\n  text-align: center;\n}\n.accordion[_ngcontent-%COMP%]   .btn-des[_ngcontent-%COMP%] {\n  border-radius: 7px;\n  width: 100px;\n  height: 30px;\n  border: solid 1px #4a427f;\n  background-color: #4a427f;\n  display: flex;\n  align-items: center;\n  text-align: center;\n}\n.accordion[_ngcontent-%COMP%]   .btn-cancel[_ngcontent-%COMP%] {\n  border-radius: 7px;\n  width: 100px;\n  height: 30px;\n  border: solid 1px red;\n  background-color: red;\n  display: flex;\n  align-items: center;\n  text-align: center;\n}\n.accordion[_ngcontent-%COMP%]   button[_ngcontent-%COMP%] {\n  position: relative;\n  display: block;\n  text-align: left;\n  width: 100%;\n  padding: 1em 0;\n  color: #000000;\n  font-size: 1.15rem;\n  font-weight: 400;\n  border: none;\n  background: none;\n  outline: none;\n}\n.accordion[_ngcontent-%COMP%]   button[_ngcontent-%COMP%]:hover, .accordion[_ngcontent-%COMP%]   button[_ngcontent-%COMP%]:focus {\n  cursor: pointer;\n  color: #4a427f;\n  font-family: Roboto-Bold, sans-serif;\n  font-size: 18px;\n}\n.accordion[_ngcontent-%COMP%]   button[_ngcontent-%COMP%]:hover::after, .accordion[_ngcontent-%COMP%]   button[_ngcontent-%COMP%]:focus::after {\n  cursor: pointer;\n  color: #4a427f;\n  border: 1px solid #4a427f;\n  font-family: Roboto-Bold, sans-serif;\n  font-size: 18px;\n}\n.accordion[_ngcontent-%COMP%]   button[_ngcontent-%COMP%]   .accordion-title[_ngcontent-%COMP%] {\n  padding: 1em 1.5em 1em 0;\n}\n.accordion[_ngcontent-%COMP%]   button[_ngcontent-%COMP%]   .icon[_ngcontent-%COMP%] {\n  display: inline-block;\n  position: absolute;\n  top: 18px;\n  right: 0;\n  width: 22px;\n  height: 22px;\n  border: 1px solid;\n  border-radius: 22px;\n}\n.accordion[_ngcontent-%COMP%]   button[_ngcontent-%COMP%]   .icon[_ngcontent-%COMP%]::before {\n  display: block;\n  position: absolute;\n  content: \"\";\n  top: 9px;\n  left: 5px;\n  width: 10px;\n  height: 2px;\n  background: currentColor;\n}\n.accordion[_ngcontent-%COMP%]   button[_ngcontent-%COMP%]   .icon[_ngcontent-%COMP%]::after {\n  display: block;\n  position: absolute;\n  content: \"\";\n  top: 5px;\n  left: 9px;\n  width: 2px;\n  height: 10px;\n  background: currentColor;\n}\n.accordion[_ngcontent-%COMP%]   button[aria-expanded=true][_ngcontent-%COMP%] {\n  color: #4a427f;\n  font-size: 19px;\n}\n.accordion[_ngcontent-%COMP%]   button[aria-expanded=true][_ngcontent-%COMP%]   .icon[_ngcontent-%COMP%]::after {\n  width: 0;\n}\n.accordion[_ngcontent-%COMP%]   button[aria-expanded=true][_ngcontent-%COMP%]    + .accordion-content[_ngcontent-%COMP%] {\n  opacity: 1;\n  max-height: 100em;\n  transition: all 200ms linear;\n  will-change: opacity, max-height;\n}\n.accordion[_ngcontent-%COMP%]   .accordion-content[_ngcontent-%COMP%] {\n  opacity: 0;\n  max-height: 0;\n  overflow: hidden;\n  transition: opacity 200ms linear, max-height 200ms linear;\n  will-change: opacity, max-height;\n}\n.accordion[_ngcontent-%COMP%]   .accordion-content[_ngcontent-%COMP%]   p[_ngcontent-%COMP%] {\n  font-size: 1rem;\n  font-weight: 300;\n  margin: 2em 0;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXC4uXFwuLlxcLi5cXC4uXFxhZGQtZW1wbGVhZG8uY29tcG9uZW50LnNjc3MiLCIuLlxcLi5cXC4uXFwuLlxcLi5cXC4uXFwuLlxcLi5cXC4uXFwuLlxcLi5cXHNjc3NcXGNvbG9ycy5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQW9SVSxnRkFBQTtBQWpSVjtFQUNJLGVBQUE7RUFDQSxZQUFBO0VBQ0EsaUJBQUE7RUFDQSxVQUFBO0VBQ0EsWUFBQTtFQUlBLGdDQUFBO0FBREo7QUFJRTtFQUNFLFFBQUE7RUFJQSxvREFBQTtBQURKO0FBSUU7RUFDRSxRQUFBO0FBREo7QUFHRTtFQUNFLFdBQUE7QUFBSjtBQUVFO0VBQ0UseUJBQUE7RUFDQSxzQkFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0EsZ0JBQUE7RUFDQSxZQUFBO0FBQ0o7QUFDRTtFQUNDLFdBQUE7QUFFSDtBQUFFO0VBQ0UsdUNBQUE7RUFDQSxjQUFBO0VBQ0EsZUFBQTtBQUdKO0FBQ0U7RUFDRSxjQUFBO0FBRUo7QUFBRTtFQUNFLHVDQUFBO0VBQ0EsY0FBQTtFQUNBLGVBQUE7QUFHSjtBQUNFO0VBQ0UsY0FBQTtFQUNBLFVBQUE7RUFDQSxlQUFBO0FBRUo7QUFBRTtFQUNFLGdCQUFBO0VBQ0EsWUFBQTtFQUNBLHFCQUFBO0VBQ0EsWUFBQTtFQUNBLGlCQUFBO0FBR0o7QUFFRTtFQUNFLFlBQUE7RUFDQSxZQUFBO0VBQ0Esd0JBQUE7RUFDQSwwQkFBQTtFQUNBLGtCQUFBO0VBQ0EseUJBQUE7QUFDSjtBQUdFO0VBQ0UsdUNBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxvQkFBQTtFQUNBLGtCQUFBO0VBQ0EsbUJBQUE7RUFDQSxzQkFBQTtFQUNBLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLFdBQUE7QUFBSjtBQUdJO0VBQ0UsdUNBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxvQkFBQTtFQUNBLGtCQUFBO0VBQ0EsbUJBQUE7RUFDQSxzQkFBQTtFQUNBLGtCQUFBO0VBQ0EsWUFBQTtFQUNBLFdBQUE7QUFBTjtBQUlJO0VBQ0UseUJBQUE7RUFDQSxZQUFBO0VBQ0EsMEJBQUE7RUFDQSx1Q0FBQTtFQUNBLGVBQUE7RUFDQSxvQkFBQTtBQUROO0FBRUk7RUFDRSx5QkFBQTtFQUNBLFlBQUE7RUFDQSwwQkFBQTtFQUVBLHlCQUFBO0VBQ0EsZUFBQTtFQUNBLHVDQUFBO0FBQ047QUFLRTtFQUNFLHNCQUFBO0VBQ0EsdUNBQUE7RUFDQSxlQUFBO0FBRko7QUFLRTtFQUNFLFlBQUE7RUFDQSxnQkFBQTtBQUZKO0FBS0U7RUFDRSx1QkFBQTtBQUZKO0FBT0U7RUFDRSx1Q0FBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtBQUpKO0FBQ0U7RUFDRSx1Q0FBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtBQUpKO0FBUUk7RUFDRSxlQUFBO0VBQ0EsWUFBQTtFQUNBLFlBQUE7QUFOTjtBQUdJO0VBQ0UsZUFBQTtFQUNBLFlBQUE7RUFDQSxZQUFBO0FBTk47QUFTSTtFQUNFLFlBQUE7QUFQTjtBQU1JO0VBQ0UsWUFBQTtBQVBOO0FBWUU7RUFDRSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSxtQkFBQTtFQUNBLHVCQUFBO0FBVEo7QUFZRTtFQUNFLGNBQUE7RUFDQSxrQkFBQTtFQUNBLHVDQUFBO0VBQ0EsZUFBQTtFQUNBLGNBQUE7QUFUSjtBQWFFO0VBQ0UsdUNBQUE7RUFDQSxlQUFBO0VBQ0EseUJBQUE7RUFDQSxnQkFBQTtBQVZKO0FBYUU7RUFDRSxrQkFBQTtBQVZKO0FBWUU7RUFDRSxhQUFBO0FBVEo7QUFZRTtFQUNFLDRCQUFBO0VBQ0EsOEJBQUE7QUFUSjtBQVdFO0VBQ0UsY0FBQTtFQUNBLGVBQUE7RUFDQSxjQUFBO0VBQ0EsZ0JBQUE7RUFDQSw2QkFBQTtLQUFBLDBCQUFBO1VBQUEscUJBQUE7QUFSSjtBQVdFO0VBQ0UseUJBQUE7QUFSSjtBQVVFO0VBQ0UsVUFBQTtFQUNGLGVBQUE7QUFQRjtBQVNFO0VBQ0UsY0FBQTtBQU5KO0FBUUU7RUFDRSxxQ0FBQTtFQUNBLGVBQUE7RUFDQSxjQUFBO0VBQ0EsZ0JBQUE7RUFDQSxZQUFBO0FBTEo7QUFPRTtFQUNFLFdBQUE7RUFDQSw0QkFBQTtFQUNBLGtCQUFBO0VBQ0EseUJBQUE7RUFDQSx5QkFBQTtFQUNBLDhCQUFBO0VBQ0EsY0FBQTtBQUpKO0FBTUU7RUFDRSxvQ0FBQTtFQUNBLGlCQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0VBQ0Esb0JBQUE7RUFDQSxrQkFBQTtFQUNBLG1CQUFBO0VBQ0Esc0JBQUE7RUFDQSxjQUFBO0FBSEo7QUFVSTtFQUNFLGNDNU9XO0VENk9YLG9DQUFBO0VBQ0EsZUFBQTtFQUNBLDBCQUFBO0FBUE47QUFXRTtFQUNFLGVBQUE7RUFDQSxZQUFBO0VBQ0EsUUFBQTtBQVJKO0FBV0U7RUFDRSxlQUFBO0FBUko7QUFnQkU7RUFDRSxpQkFBQTtBQWJKO0FBd0JFO0VBQ0Usc0JBQUE7QUFyQko7QUFzQkk7RUFDRSxzQkFBQTtBQXBCTjtBQXdCRTtFQUNFLFNBQUE7RUFDQSxVQUFBO0VBQ0EsK0JBQUE7RUFDQSxnQkFqQkc7RUFrQkgsY0FoQks7RUFpQkwsYUFBQTtFQUNBLGlCQUFBO0FBckJKO0FBd0JFO0VBRUUsV0FBQTtBQXRCSjtBQTBCSTtFQUNFLGdDQUFBO0FBdkJOO0FBd0JNO0VBQ0UsZ0NBQUE7QUF0QlI7QUF5Qkk7RUFDRSxrQkFBQTtFQUNBLFlBQUE7RUFDQSxZQUFBO0VBQ0EseUJBQUE7RUFDQSxhQUFBO0VBQ0EsbUJBQUE7RUFDRixrQkFBQTtBQXZCSjtBQXlCSTtFQUNFLGtCQUFBO0VBQ0EsWUFBQTtFQUNBLFlBQUE7RUFDQSx5QkFBQTtFQUNBLHlCQUFBO0VBQ0EsYUFBQTtFQUNBLG1CQUFBO0VBQ0Ysa0JBQUE7QUF2Qko7QUEyQkk7RUFDRSxrQkFBQTtFQUNBLFlBQUE7RUFDQSxZQUFBO0VBQ0EscUJBQUE7RUFDQSxxQkFBQTtFQUNBLGFBQUE7RUFDQSxtQkFBQTtFQUNGLGtCQUFBO0FBekJKO0FBMkJJO0VBQ0Usa0JBQUE7RUFDQSxjQUFBO0VBQ0EsZ0JBQUE7RUFDQSxXQUFBO0VBQ0EsY0FBQTtFQUNBLGNBdkVHO0VBd0VILGtCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxZQUFBO0VBQ0EsZ0JBQUE7RUFDQSxhQUFBO0FBekJOO0FBMEJNO0VBQ0UsZUFBQTtFQUNBLGNBNUVDO0VBNkVELG9DQUFBO0VBQ0EsZUFBQTtBQXhCUjtBQTBCUTtFQUNFLGVBQUE7RUFDQSxjQWxGRDtFQW1GQyx5QkFBQTtFQUNBLG9DQUFBO0VBQ0EsZUFBQTtBQXhCVjtBQTRCTTtFQUNFLHdCQUFBO0FBMUJSO0FBNEJNO0VBQ0UscUJBQUE7RUFDQSxrQkFBQTtFQUNBLFNBQUE7RUFDQSxRQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxpQkFBQTtFQUNBLG1CQUFBO0FBMUJSO0FBMkJRO0VBQ0UsY0FBQTtFQUNBLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLFFBQUE7RUFDQSxTQUFBO0VBQ0EsV0FBQTtFQUNBLFdBQUE7RUFDQSx3QkFBQTtBQXpCVjtBQTJCUTtFQUNFLGNBQUE7RUFDQSxrQkFBQTtFQUNBLFdBQUE7RUFDQSxRQUFBO0VBQ0EsU0FBQTtFQUNBLFVBQUE7RUFDQSxZQUFBO0VBQ0Esd0JBQUE7QUF6QlY7QUE2Qkk7RUFDRSxjQTVIRztFQTZISCxlQUFBO0FBM0JOO0FBOEJRO0VBQ0UsUUFBQTtBQTVCVjtBQStCTTtFQUNFLFVBQUE7RUFDQSxpQkFBQTtFQUNBLDRCQUFBO0VBQ0EsZ0NBQUE7QUE3QlI7QUFnQ0k7RUFDRSxVQUFBO0VBQ0EsYUFBQTtFQUNBLGdCQUFBO0VBQ0EseURBQUE7RUFDQSxnQ0FBQTtBQTlCTjtBQStCTTtFQUNFLGVBQUE7RUFDQSxnQkFBQTtFQUNBLGFBQUE7QUE3QlIiLCJmaWxlIjoiYWRkLWVtcGxlYWRvLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiQGltcG9ydCBcIi4uLy4uLy4uLy4uL3Njc3MvY29sb3JzXCI7XG5cblxuLm1vZGFsLnJpZ2h0IC5tb2RhbC1kaWFsb2cge1xuICAgIHBvc2l0aW9uOiBmaXhlZDtcbiAgICBtYXJnaW46IGF1dG87XG4gICAgbWF4LXdpZHRoOiAxMDAwcHg7XG4gICAgd2lkdGg6IDY1JTtcbiAgICBoZWlnaHQ6IDEwMCU7XG4gICAgLXdlYmtpdC10cmFuc2Zvcm06IHRyYW5zbGF0ZTNkKDAlLCAwLCAwKTtcbiAgICAtbXMtdHJhbnNmb3JtOiB0cmFuc2xhdGUzZCgwJSwgMCwgMCk7XG4gICAgLW8tdHJhbnNmb3JtOiB0cmFuc2xhdGUzZCgwJSwgMCwgMCk7XG4gICAgdHJhbnNmb3JtOiB0cmFuc2xhdGUzZCgwJSwgMCwgMCk7XG4gIH1cbiAgXG4gIC5tb2RhbC5yaWdodC5mYWRlIC5tb2RhbC1kaWFsb2cge1xuICAgIHJpZ2h0OiAwO1xuICAgIC13ZWJraXQtdHJhbnNpdGlvbjogb3BhY2l0eSAwLjNzIGxpbmVhciwgcmlnaHQgMC4zcyBlYXNlLW91dDtcbiAgICAtbW96LXRyYW5zaXRpb246IG9wYWNpdHkgMC4zcyBsaW5lYXIsIHJpZ2h0IDAuM3MgZWFzZS1vdXQ7XG4gICAgLW8tdHJhbnNpdGlvbjogb3BhY2l0eSAwLjNzIGxpbmVhciwgcmlnaHQgMC4zcyBlYXNlLW91dDtcbiAgICB0cmFuc2l0aW9uOiBvcGFjaXR5IDAuM3MgbGluZWFyLCByaWdodCAwLjNzIGVhc2Utb3V0O1xuICB9XG4gIFxuICAubW9kYWwucmlnaHQuZmFkZS5pbiAubW9kYWwtZGlhbG9nIHtcbiAgICByaWdodDogMDtcbiAgfVxuICAudGV4YnRuc3Nze1xuICAgIGNvbG9yOiAjMDAwO1xuICB9XG4gIC5jYXJkLWhlYWRlciB7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2VmZWZlZjtcbiAgICBjb2xvcjogIzAwMCAhaW1wb3J0YW50O1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGJvcmRlcjogbm9uZTtcbiAgICB0ZXh0LWFsaWduOiBsZWZ0O1xuICAgIGhlaWdodDogNTBweDtcbiAgfVxuICAudGFibGUge1xuICAgd2lkdGg6IDEwMCU7XG4gIH1cbiAgLnN1YnRpdGxlLXJlZmFjdG9yaWNlZHtcbiAgICBmb250LWZhbWlseTogUm9ib3RvLVJlZ3VsYXIsIHNhbnMtc2VyaWY7XG4gICAgY29sb3I6ICM0MTQxNDE7XG4gICAgZm9udC1zaXplOiAxNnB4O1xuICBcbiAgICBcbiAgfVxuICAuZm9ybWRvY3VtZW50e1xuICAgIG1hcmdpbi10b3A6IDIlO1xuICB9XG4gIC5zdWJ0aXRsZS1yZWZhY3RvcmljZWRze1xuICAgIGZvbnQtZmFtaWx5OiBSb2JvdG8tUmVndWxhciwgc2Fucy1zZXJpZjtcbiAgICBjb2xvcjogI2ZmZmZmZjtcbiAgICBmb250LXNpemU6IDE2cHg7XG4gIFxuICAgIFxuICB9XG4gIC5jb250ZW50bW9kYWx7XG4gICAgbWFyZ2luLXRvcDogNSU7XG4gICAgd2lkdGg6IDk1JTtcbiAgICBtYXJnaW4tbGVmdDogMyU7XG4gIH1cbiAgLmNhcmQtaGVhZGVyOmFmdGVyIHtcbiAgICBjb2xvcjogYWxpY2VibHVlO1xuICAgIGZsb2F0OiByaWdodDtcbiAgICBhbGlnbi1jb250ZW50OiBjZW50ZXI7XG4gICAgaGVpZ2h0OiAxMDAlO1xuICAgIG1hcmdpbi10b3A6IC0yMHB4O1xuICB9XG4gIFxuICBcbiAgXG4gIC5idG4tZ3JlZW4tcmVmIHtcbiAgICB3aWR0aDogMTQycHg7XG4gICAgaGVpZ2h0OiAzNnB4O1xuICAgIG1hcmdpbjogMTFweCA2MnB4IDAgNjBweDtcbiAgICBwYWRkaW5nOiA2cHggNDBweCA3cHggNDJweDtcbiAgICBib3JkZXItcmFkaXVzOiA3cHg7XG4gICAgYm9yZGVyOiBzb2xpZCAxcHggIzRhNDI3ZjtcbiAgXG4gIH1cbiAgXG4gIC50ZXhidG57XG4gICAgZm9udC1mYW1pbHk6IFJvYm90by1SZWd1bGFyLCBzYW5zLXNlcmlmO1xuICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICBmb250LXdlaWdodDogNTAwO1xuICAgIGZvbnQtc3RyZXRjaDogbm9ybWFsO1xuICAgIGZvbnQtc3R5bGU6IG5vcm1hbDtcbiAgICBsaW5lLWhlaWdodDogbm9ybWFsO1xuICAgIGxldHRlci1zcGFjaW5nOiBub3JtYWw7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIGNvbG9yOiAjZmZmO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIH1cbiAgXG4gICAgLnRleGJ0bmR7XG4gICAgICBmb250LWZhbWlseTogUm9ib3RvLVJlZ3VsYXIsIHNhbnMtc2VyaWY7XG4gICAgICBmb250LXNpemU6IDE0cHg7XG4gICAgICBmb250LXdlaWdodDogNTAwO1xuICAgICAgZm9udC1zdHJldGNoOiBub3JtYWw7XG4gICAgICBmb250LXN0eWxlOiBub3JtYWw7XG4gICAgICBsaW5lLWhlaWdodDogbm9ybWFsO1xuICAgICAgbGV0dGVyLXNwYWNpbmc6IG5vcm1hbDtcbiAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICAgIGNvbG9yOiByZ2IoMCwgMCwgMCk7XG4gICAgICB3aWR0aDogMTAwJTtcbiAgICAgIH1cbiAgICBcbiAgXG4gICAgLmNhcmQtaGVhZGVyLmNvbGxhcHNlZDo6YWZ0ZXIge1xuICAgICAgY29sb3I6ICMyMDIwMjAgIWltcG9ydGFudDtcbiAgICAgIGNvbnRlbnQ6IFwiXFwwMDJCXCI7XG4gICAgICBmb250LXNpemU6IDMwcHggIWltcG9ydGFudDtcbiAgICAgIGZvbnQtZmFtaWx5OiBSb2JvdG8tUmVndWxhciwgc2Fucy1zZXJpZjtcbiAgICAgIG1hcmdpbi10b3A6IC01JTtcbiAgICAgIGJvcmRlci1yYWRpdXM6IDEwMHB4O31cbiAgICAuY2FyZC1oZWFkZXI6bm90KC5jb2xsYXBzZWQpOjphZnRlciB7XG4gICAgICBjb2xvcjogIzU5NTM3ZiAhaW1wb3J0YW50O1xuICAgICAgY29udGVudDogXCJcXDAwMkRcIjtcbiAgICAgIGZvbnQtc2l6ZTogNTBweCAhaW1wb3J0YW50O1xuICAgICAgLXdlYmtpdC10cmFuc2Zvcm06IHJvdGF0ZSgxODBkZWcpO1xuICAgICAgdHJhbnNmb3JtOiByb3RhdGUoMTgwZGVnKTtcbiAgICAgIG1hcmdpbi10b3A6IC01JTtcbiAgICAgIGZvbnQtZmFtaWx5OiBSb2JvdG8tUmVndWxhciwgc2Fucy1zZXJpZjtcbiAgICB9XG4gICAgXG4gIFxuICBcbiAgXG4gIC50aXRsZS1hY2NvcmRpe1xuICAgIGNvbG9yOiAjMDAwICFpbXBvcnRhbnQ7XG4gICAgZm9udC1mYW1pbHk6IFJvYm90by1SZWd1bGFyLCBzYW5zLXNlcmlmO1xuICAgIGZvbnQtc2l6ZTogMTZweDtcbiAgfVxuICBcbiAgLm1vZGFsLnJpZ2h0IC5tb2RhbC1jb250ZW50e1xuICAgIGhlaWdodDogMTAwJTtcbiAgICBvdmVyZmxvdy15OiBhdXRvO1xuICB9XG4gIFxuICAubW9kYWwucmlnaHQgLm1vZGFsLWJvZHkge1xuICAgIHBhZGRpbmc6IDE1cHggMTVweCA4MHB4O1xuICB9XG4gIFxuICBcbiAgXG4gIC5hY3RpdmF0aW9uTW9kYWwsIDpwbGFjZWhvbGRlci1zaG93biB7XG4gICAgZm9udC1mYW1pbHk6IFJvYm90by1SZWd1bGFyLCBzYW5zLXNlcmlmO1xuICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICBtaW4taGVpZ2h0OiAzNnB4O1xuICBcbiAgXG4gIFxuICAgIC5idG4tZ3JlZW4ge1xuICAgICAgZm9udC1zaXplOiAxM3B4O1xuICAgICAgd2lkdGg6IDE3NXB4O1xuICAgICAgaGVpZ2h0OiAzNXB4O1xuICAgIH1cbiAgXG4gICAgLmZpZWwtdmFsaWRhdGUgLmljb24tZmllbGQge1xuICAgICAgaGVpZ2h0OiAzNnB4O1xuICAgIH1cbiAgXG4gIFxuICB9XG4gIC5tb2RhbC1oZWFkZXItcmVme1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBcbiAgfVxuICAubW9kYWwtdGl0bGV7XG4gICAgbWFyZ2luLXRvcDogNSU7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIGZvbnQtZmFtaWx5OiBSb2JvdG8tUmVndWxhciwgc2Fucy1zZXJpZjtcbiAgICBmb250LXNpemU6IDIxcHg7XG4gICAgY29sb3I6ICM0MTQxNDE7XG4gIH1cbiAgXG4gIFxuICAuYWN0aXZhdGlvbk1vZGFsLXRpdGxlIHtcbiAgICBmb250LWZhbWlseTogUm9ib3RvLVJlZ3VsYXIsIHNhbnMtc2VyaWY7XG4gICAgZm9udC1zaXplOiAxM3B4O1xuICAgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gICAgZm9udC13ZWlnaHQ6IDcwMDtcbiAgXG4gIH1cbiAgLm1vZGFyRGVsZXRle1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgfVxuICBsYWJlbCB7XG4gICAgZGlzcGxheTogbm9uZTtcbiAgfVxuICBcbiAgLmFkZGZpbGVze1xuICAgIHBhZGRpbmc6IDE1cHggMjNweCAxNHB4IDI1cHg7XG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICB9XG4gIC5idG4tZG93bmxvYWQge1xuICAgIHdpZHRoOiBpbmhlcml0O1xuICAgIGhlaWdodDogaW5pdGlhbDtcbiAgICBtYXJnaW46IDEwcHggMDtcbiAgICBwYWRkaW5nLXRvcDogNnB4O1xuICAgIHRleHQtc2l6ZS1hZGp1c3Q6IDg1JTtcbiAgfVxuICBcbiAgLmNhcmQtaGVhZGVye1xuICAgIGJhY2tncm91bmQtY29sb3I6ICNlMGUwZTA7XG4gIH1cbiAgLmNhcmRwcmltYXJ5e1xuICAgIHdpZHRoOiA5NiU7XG4gIG1hcmdpbi1sZWZ0OiAyJTtcbiAgfVxuICAuZmxleC11cHJlZiB7XG4gICAgbWluLXdpZHRoOiAzMSU7ICBcbiAgfVxuICAudGl0bGVjYXJke1xuICAgIGZvbnQtZmFtaWx5OiBMYXRvLVJlZ3VsYXIsIHNhbnMtc2VyaWY7XG4gICAgZm9udC1zaXplOiAxMnB4O1xuICAgIGNvbG9yOiAjMDAwMDAwO1xuICAgIHRleHQtYWxpZ246IGxlZnQ7XG4gICAgd2lkdGg6IDI1MHB4O1xuICB9XG4gIC5SZWN0YW5nbGUge1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIHBhZGRpbmc6IDE1cHggMjNweCAxNHB4IDI1cHg7XG4gICAgYm9yZGVyLXJhZGl1czogNHB4O1xuICAgIGJvcmRlcjogc29saWQgMXB4ICNlZmVmZWY7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2VmZWZlZjtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG4gICAgbWFyZ2luLXRvcDogMiU7XG4gICAgfVxuICAudGl0bGUtcmVmYWN0b3JpY2Vke1xuICAgIGZvbnQtZmFtaWx5OiBSb2JvdG8tQm9sZCwgc2Fucy1zZXJpZjtcbiAgICBtYXJnaW4tbGVmdDogMS41JTtcbiAgICBmb250LXNpemU6IDE4cHg7XG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gICAgZm9udC1zdHJldGNoOiBub3JtYWw7XG4gICAgZm9udC1zdHlsZTogbm9ybWFsO1xuICAgIGxpbmUtaGVpZ2h0OiBub3JtYWw7XG4gICAgbGV0dGVyLXNwYWNpbmc6IG5vcm1hbDtcbiAgICBjb2xvcjogIzRhNGE0YTtcbiAgfVxuICBcbiAgXG4gIFxuICAuaGlnaGxpZ2h0LWZpZWxkIHtcbiAgXG4gICAgcCB7XG4gICAgICBjb2xvcjogJHByaW1hcnktYnV0dG9uO1xuICAgICAgZm9udC1mYW1pbHk6IFJvYm90by1Cb2xkLCBzYW5zLXNlcmlmO1xuICAgICAgZm9udC1zaXplOiAxNHB4O1xuICAgICAgdGV4dC1kZWNvcmF0aW9uOiB1bmRlcmxpbmU7XG4gICAgfVxuICB9XG4gIFxuICAuYnRuLWZsb2F0IHtcbiAgICBwb3NpdGlvbjogZml4ZWQ7XG4gICAgYm90dG9tOiAyMHB4O1xuICAgIHJpZ2h0OiAwO1xuICB9XG4gIFxuICB0Ym9keSB0ZCB7XG4gICAgZm9udC1zaXplOiAxM3B4O1xuICB9XG4gIFxuICBcbiAgXG4gIFxuICBcbiAgXG4gIC5oZWFkZXIge1xuICAgIG1hcmdpbi10b3A6IDI1MHB4O1xuICB9XG4gIC8vbmV3XG4gIEBpbXBvcnQgdXJsKCdodHRwczovL2ZvbnRzLmdvb2dsZWFwaXMuY29tL2Nzcz9mYW1pbHk9SGluZDozMDAsNDAwJmRpc3BsYXk9c3dhcCcpO1xuICBcbiAgJGJnOiAjZmZmO1xuICAkdGV4dDogIzAwMDAwMDtcbiAgJGdyYXk6ICM0ZDU5NzQ7XG4gICRsaWdodGdyYXk6ICNlNWU1ZTU7XG4gICRibHVlOiAjNGE0MjdmO1xuICBcbiAgKiB7XG4gICAgYm94LXNpemluZzogYm9yZGVyLWJveDtcbiAgICAmOjpiZWZvcmUsICY6OmFmdGVyIHtcbiAgICAgIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XG4gICAgfVxuICB9XG4gIFxuICBib2R5IHtcbiAgICBtYXJnaW46IDA7XG4gICAgcGFkZGluZzogMDtcbiAgICBmb250LWZhbWlseTogJ0hpbmQnLCBzYW5zLXNlcmlmO1xuICAgIGJhY2tncm91bmQ6ICRiZztcbiAgICBjb2xvcjogJGdyYXk7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBtaW4taGVpZ2h0OiAxMDB2aDtcbiAgfVxuICBcbiAgLmNvbnRhaW5lciB7XG4gIFxuICAgIHdpZHRoOiAxMDAlO1xuICB9XG4gIFxuICAuYWNjb3JkaW9uIHtcbiAgICAuYWNjb3JkaW9uLWl0ZW0ge1xuICAgICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICRsaWdodGdyYXk7XG4gICAgICBidXR0b25bYXJpYS1leHBhbmRlZD0ndHJ1ZSddIHtcbiAgICAgICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICRibHVlO1xuICAgICAgfVxuICAgIH1cbiAgICAuYnRuLWNhbmNlbGFye1xuICAgICAgYm9yZGVyLXJhZGl1czogN3B4O1xuICAgICAgd2lkdGg6IDEwMHB4O1xuICAgICAgaGVpZ2h0OiAzMHB4O1xuICAgICAgYm9yZGVyOiBzb2xpZCAxcHggIzk3OTc5NztcbiAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyOyBcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgfVxuICAgIC5idG4tZGVze1xuICAgICAgYm9yZGVyLXJhZGl1czogN3B4O1xuICAgICAgd2lkdGg6IDEwMHB4O1xuICAgICAgaGVpZ2h0OiAzMHB4O1xuICAgICAgYm9yZGVyOiBzb2xpZCAxcHggIzRhNDI3ZjtcbiAgICAgIGJhY2tncm91bmQtY29sb3I6ICM0YTQyN2Y7XG4gICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjsgXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBcbiAgICBcbiAgICB9XG4gICAgLmJ0bi1jYW5jZWx7XG4gICAgICBib3JkZXItcmFkaXVzOiA3cHg7XG4gICAgICB3aWR0aDogMTAwcHg7XG4gICAgICBoZWlnaHQ6IDMwcHg7XG4gICAgICBib3JkZXI6IHNvbGlkIDFweCByZWQ7XG4gICAgICBiYWNrZ3JvdW5kLWNvbG9yOnJlZDtcbiAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyOyBcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgfVxuICAgIGJ1dHRvbiB7XG4gICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgICBkaXNwbGF5OiBibG9jaztcbiAgICAgIHRleHQtYWxpZ246IGxlZnQ7XG4gICAgICB3aWR0aDogMTAwJTtcbiAgICAgIHBhZGRpbmc6IDFlbSAwO1xuICAgICAgY29sb3I6ICR0ZXh0O1xuICAgICAgZm9udC1zaXplOiAxLjE1cmVtO1xuICAgICAgZm9udC13ZWlnaHQ6IDQwMDtcbiAgICAgIGJvcmRlcjogbm9uZTtcbiAgICAgIGJhY2tncm91bmQ6IG5vbmU7XG4gICAgICBvdXRsaW5lOiBub25lO1xuICAgICAgJjpob3ZlciwgJjpmb2N1cyB7XG4gICAgICAgIGN1cnNvcjogcG9pbnRlcjtcbiAgICAgICAgY29sb3I6ICRibHVlO1xuICAgICAgICBmb250LWZhbWlseTogUm9ib3RvLUJvbGQsIHNhbnMtc2VyaWY7XG4gICAgICAgIGZvbnQtc2l6ZTogMThweDtcbiAgXG4gICAgICAgICY6OmFmdGVyIHtcbiAgICAgICAgICBjdXJzb3I6IHBvaW50ZXI7XG4gICAgICAgICAgY29sb3I6ICRibHVlO1xuICAgICAgICAgIGJvcmRlcjogMXB4IHNvbGlkICRibHVlO1xuICAgICAgICAgIGZvbnQtZmFtaWx5OiBSb2JvdG8tQm9sZCwgc2Fucy1zZXJpZjtcbiAgICAgICAgICBmb250LXNpemU6IDE4cHg7XG4gIFxuICAgICAgICB9XG4gICAgICB9XG4gICAgICAuYWNjb3JkaW9uLXRpdGxlIHtcbiAgICAgICAgcGFkZGluZzogMWVtIDEuNWVtIDFlbSAwO1xuICAgICAgfVxuICAgICAgLmljb24ge1xuICAgICAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAgICAgdG9wOiAxOHB4O1xuICAgICAgICByaWdodDogMDtcbiAgICAgICAgd2lkdGg6IDIycHg7XG4gICAgICAgIGhlaWdodDogMjJweDtcbiAgICAgICAgYm9yZGVyOiAxcHggc29saWQ7XG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDIycHg7XG4gICAgICAgICY6OmJlZm9yZSB7XG4gICAgICAgICAgZGlzcGxheTogYmxvY2s7XG4gICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgICAgICAgIGNvbnRlbnQ6ICcnO1xuICAgICAgICAgIHRvcDogOXB4O1xuICAgICAgICAgIGxlZnQ6IDVweDtcbiAgICAgICAgICB3aWR0aDogMTBweDtcbiAgICAgICAgICBoZWlnaHQ6IDJweDtcbiAgICAgICAgICBiYWNrZ3JvdW5kOiBjdXJyZW50Q29sb3I7XG4gICAgICAgIH1cbiAgICAgICAgJjo6YWZ0ZXIge1xuICAgICAgICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAgICAgICBjb250ZW50OiAnJztcbiAgICAgICAgICB0b3A6IDVweDtcbiAgICAgICAgICBsZWZ0OiA5cHg7XG4gICAgICAgICAgd2lkdGg6IDJweDtcbiAgICAgICAgICBoZWlnaHQ6IDEwcHg7XG4gICAgICAgICAgYmFja2dyb3VuZDogY3VycmVudENvbG9yO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuICAgIGJ1dHRvblthcmlhLWV4cGFuZGVkPSd0cnVlJ10ge1xuICAgICAgY29sb3I6ICRibHVlO1xuICAgICAgZm9udC1zaXplOiAxOXB4O1xuICBcbiAgICAgIC5pY29uIHtcbiAgICAgICAgJjo6YWZ0ZXIge1xuICAgICAgICAgIHdpZHRoOiAwO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgICArIC5hY2NvcmRpb24tY29udGVudCB7XG4gICAgICAgIG9wYWNpdHk6IDE7XG4gICAgICAgIG1heC1oZWlnaHQ6IDEwMGVtO1xuICAgICAgICB0cmFuc2l0aW9uOiBhbGwgMjAwbXMgbGluZWFyO1xuICAgICAgICB3aWxsLWNoYW5nZTogb3BhY2l0eSwgbWF4LWhlaWdodDtcbiAgICAgIH1cbiAgICB9XG4gICAgLmFjY29yZGlvbi1jb250ZW50IHtcbiAgICAgIG9wYWNpdHk6IDA7XG4gICAgICBtYXgtaGVpZ2h0OiAwO1xuICAgICAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgICAgIHRyYW5zaXRpb246IG9wYWNpdHkgMjAwbXMgbGluZWFyLCBtYXgtaGVpZ2h0IDIwMG1zIGxpbmVhcjtcbiAgICAgIHdpbGwtY2hhbmdlOiBvcGFjaXR5LCBtYXgtaGVpZ2h0O1xuICAgICAgcCB7XG4gICAgICAgIGZvbnQtc2l6ZTogMXJlbTtcbiAgICAgICAgZm9udC13ZWlnaHQ6IDMwMDtcbiAgICAgICAgbWFyZ2luOiAyZW0gMDtcbiAgICAgIH1cbiAgICB9XG4gIH1cbiAgIiwiLy8gQ29sb3IgcGFsZXR0ZSBcbiRwcmltYXJ5LWFsZXJ0OiAjM2M0ODc2O1xuJHJlZ2lzdHJ5LWFsZXJ0OiAjNzFjMDE1O1xuJGF0dGVudGlvbi1hbGVydDogI0E3QTlFNjtcbiR3YXJuaW5nLWFsZXJ0OiAjZjljMDA0O1xuJGVycm9yLWFsZXJ0OiAjZmM1NjYzO1xuJGNvbmZpcm1hdGlvbi1hbGVydC1ib3JkZXI6ICM2YzVhOTc7XG4kY29uZmlybWF0aW9uLWFsZXJ0LWJhY2tncm91bmQ6ICNlMWRlZWE7XG4kY29uZmlybWF0aW9uLWJ1dHRvbi1iYWNrZ3JvdW5kOiAjN2I2Y2EyO1xuJEVycm9yLXZhbGlkYXRpb24taWNvbjogI2U0NWU1ZTtcbiRFcnJvci12YWxpZGF0aW9uOiAjZjE1NTYxO1xuJHNlbGVjdGVkLWljb246ICM0NjNkNmU7XG4kY29uZmlybWF0aW9uLWJ1dHRvbi1hbHRlcm5hdGl2ZTogIzc4NmRhNDtcbiR3aGl0ZTogI2ZmZmZmZjtcbiRwcmltYXJ5LWJ1dHRvbjogIzNkN2Q0MTtcbiR0ZXJ0aWFyeS1idXR0b246ICNkN2U1ZGE7XG4kc3VjY2Vzcy12YWxpZGF0aW9uLWJvcmRlcjogIzgyYmUwMDtcbiRiYWNrZ3JvdW5kOiAjZmNmYmZmO1xuJGNhcmQtYm9yZGVyOiAjZWZlY2ZhO1xuJGFsbW9zdC1ibGFjazogIzBhMGEwYjtcbiRsYXJnZS1idXR0b246ICNkYmQ4ZTM7XG4kYm9yZGVyLWNvbG9yOiAjZDlkOWQ5O1xuJG5hdmJhci1ib3JkZXI6ICNmMWYxZjE7XG4kc21hbGwtdGV4dDogIzZiNjQ4YjtcbiRsZWFkOiAjNTk1MDdkO1xuLy8gQmFzZSBjb2xvcnNcbiRwcmltYXJ5OiAkcHJpbWFyeS1idXR0b247XG4kc2Vjb25kYXJ5OiAkc2VsZWN0ZWQtaWNvbjtcbiRzdWNjZXNzOiAkcmVnaXN0cnktYWxlcnQ7XG4kaW5mbzogJGF0dGVudGlvbi1hbGVydDtcbiR3YXJuaW5nOiAkd2FybmluZy1hbGVydDtcbiRkYW5nZXI6ICRlcnJvci1hbGVydDtcbiRkYXJrOiAkYWxtb3N0LWJsYWNrO1xuJHRvdWNoZWQ6ICMzMzk0ZjU7XG4vLyBCb2R5XG4vL1xuLy8gU2V0dGluZ3MgZm9yIHRoZSBgPGJvZHk+YCBlbGVtZW50LlxuJGJvZHktYmc6ICNGQkZDRkY7XG4kYm9keS1jb2xvcjogJGFsbW9zdC1ibGFjaztcblxuLy9ub3RpZmljYWNpb25lc1xuJHN1Y2Nlc3JlZmFjdG86ICM4MzZmZDE7XG4iXX0= */"]
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](AddEmpleadoComponent, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
          args: [{
            selector: 'app-add-empleado',
            templateUrl: './add-empleado.component.html',
            styleUrls: ['./add-empleado.component.scss']
          }]
        }], function () {
          return [{
            type: _services_workflow_empleado_empleado_service__WEBPACK_IMPORTED_MODULE_7__["EmpleadoService"]
          }, {
            type: _services_workflow_sueldo_sueldo_service__WEBPACK_IMPORTED_MODULE_8__["SueldoService"]
          }, {
            type: _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"]
          }, {
            type: ngx_ui_loader__WEBPACK_IMPORTED_MODULE_2__["NgxUiLoaderService"]
          }, {
            type: src_app_shared_classes__WEBPACK_IMPORTED_MODULE_4__["Helpers"]
          }, {
            type: _services_workflow_departamento_departamento_service__WEBPACK_IMPORTED_MODULE_5__["DepartamentoService"]
          }, {
            type: _services_workflow_supervisor_supervisor_service__WEBPACK_IMPORTED_MODULE_6__["SupervisorService"]
          }];
        }, {
          hideModalEmitter: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
          }],
          empleado: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
          }]
        });
      })();
      /***/

    },

    /***/
    "PCNd":
    /*!*****************************************!*\
      !*** ./src/app/shared/shared.module.ts ***!
      \*****************************************/

    /*! exports provided: SharedModule */

    /***/
    function PCNd(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "SharedModule", function () {
        return SharedModule;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/forms */
      "3Pt+");
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/common/http */
      "tk/3");
      /* harmony import */


      var _shared_routing_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ./shared-routing.module */
      "Sj5B");
      /* harmony import */


      var _interceptors_httpconfig_interceptor__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./interceptors/httpconfig.interceptor */
      "nEJ4");
      /* harmony import */


      var _pipes_format_money_ec_pipe__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./pipes/format-money-ec.pipe */
      "AElR");
      /* harmony import */


      var _ng_select_ng_select__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! @ng-select/ng-select */
      "ZOsW");
      /* harmony import */


      var angular_mydatepicker__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! angular-mydatepicker */
      "rXkI");
      /* harmony import */


      var _components__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
      /*! ./components */
      "1ua0");
      /* harmony import */


      var ngx_ui_loader__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
      /*! ngx-ui-loader */
      "gren");

      var SharedModule = function SharedModule() {
        _classCallCheck(this, SharedModule);
      };

      SharedModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({
        type: SharedModule
      });
      SharedModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({
        factory: function SharedModule_Factory(t) {
          return new (t || SharedModule)();
        },
        providers: [{
          provide: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HTTP_INTERCEPTORS"],
          useClass: _interceptors_httpconfig_interceptor__WEBPACK_IMPORTED_MODULE_5__["HttpConfigInterceptor"],
          multi: true
        }],
        imports: [[_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["ReactiveFormsModule"], _shared_routing_module__WEBPACK_IMPORTED_MODULE_4__["SharedRoutingModule"], ngx_ui_loader__WEBPACK_IMPORTED_MODULE_10__["NgxUiLoaderModule"], _ng_select_ng_select__WEBPACK_IMPORTED_MODULE_7__["NgSelectModule"], angular_mydatepicker__WEBPACK_IMPORTED_MODULE_8__["AngularMyDatePickerModule"]]]
      });

      (function () {
        (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](SharedModule, {
          declarations: [_components__WEBPACK_IMPORTED_MODULE_9__["SharedComponent"], _pipes_format_money_ec_pipe__WEBPACK_IMPORTED_MODULE_6__["FormatMoneyEcPipe"]],
          imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["ReactiveFormsModule"], _shared_routing_module__WEBPACK_IMPORTED_MODULE_4__["SharedRoutingModule"], ngx_ui_loader__WEBPACK_IMPORTED_MODULE_10__["NgxUiLoaderModule"], _ng_select_ng_select__WEBPACK_IMPORTED_MODULE_7__["NgSelectModule"], angular_mydatepicker__WEBPACK_IMPORTED_MODULE_8__["AngularMyDatePickerModule"]],
          exports: [_pipes_format_money_ec_pipe__WEBPACK_IMPORTED_MODULE_6__["FormatMoneyEcPipe"]]
        });
      })();
      /*@__PURE__*/


      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](SharedModule, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
          args: [{
            declarations: [_components__WEBPACK_IMPORTED_MODULE_9__["SharedComponent"], _pipes_format_money_ec_pipe__WEBPACK_IMPORTED_MODULE_6__["FormatMoneyEcPipe"]],
            imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["ReactiveFormsModule"], _shared_routing_module__WEBPACK_IMPORTED_MODULE_4__["SharedRoutingModule"], ngx_ui_loader__WEBPACK_IMPORTED_MODULE_10__["NgxUiLoaderModule"], _ng_select_ng_select__WEBPACK_IMPORTED_MODULE_7__["NgSelectModule"], angular_mydatepicker__WEBPACK_IMPORTED_MODULE_8__["AngularMyDatePickerModule"]],
            providers: [{
              provide: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HTTP_INTERCEPTORS"],
              useClass: _interceptors_httpconfig_interceptor__WEBPACK_IMPORTED_MODULE_5__["HttpConfigInterceptor"],
              multi: true
            }],
            exports: [_pipes_format_money_ec_pipe__WEBPACK_IMPORTED_MODULE_6__["FormatMoneyEcPipe"]]
          }]
        }], null, null);
      })();
      /***/

    },

    /***/
    "RbYx":
    /*!******************************************************!*\
      !*** ./src/app/shared/classes/GlobalErrorHandler.ts ***!
      \******************************************************/

    /*! exports provided: GlobalErrorHandler */

    /***/
    function RbYx(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "GlobalErrorHandler", function () {
        return GlobalErrorHandler;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var _LocalStorageCommon__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./LocalStorageCommon */
      "hdVc");

      var GlobalErrorHandler = /*#__PURE__*/function () {
        /**
         * @tutorial Customer Errors
         *
         * @see 400 Bad Request
         * @see 401 Unauthorized
         * @see 402 Payment Required
         * @see 403 Forbidden
         * @see 404 Not Found
         * @see 405 Method Not Allowed
         * @see 406 Not Acceptable
         * @see 407 Proxy Authentication Required
         * @see 408 Request Timeout
         * @see 409 Conflict
         * @see 410 Gone
         * @see 411 Length Required
         * @see 412 Precondition Failed
         * @see 413 Payload Too Large
         * @see 414 URI Too Long
         * @see 415 Unsupported Media Type
         * @see 416 Requested Range Not Satisfiable
         * @see 417 Expectation Failed
         * @see 418 I'm a teapot
         * @see 421 Misdirected Request
         * @see 422 Unprocessable Entity (WebDAV)
         * @see 423 Locked (WebDAV)
         * @see 424 Failed Dependency (WebDAV)
         * @see 426 Upgrade Required
         * @see 428 Precondition Required
         * @see 429 Too Many Requests
         * @see 431 Request Header Fields Too Large
         * @see 451 Unavailable For Legal Reasons
         *
         *
         * @tutorial Server Errors
         *
         * @see 500 Internal Server Error
         * @see 501 Not Implemented
         * @see 502 Bad Gateway
         * @see 503 Service Unavailable
         * @see 504 Gateway Timeout
         * @see 505 HTTP Version Not Supported
         * @see 506 Variant Also Negotiates
         * @see 507 Insufficient Storage
         * @see 508 Loop Detected (WebDAV)
         * @see 510 Not Extended
         * @see 511 Network Authentication Required
         *
         *
         */
        function GlobalErrorHandler(router, localStorage) {
          _classCallCheck(this, GlobalErrorHandler);

          this.router = router;
          this.localStorage = localStorage;
        }

        _createClass(GlobalErrorHandler, [{
          key: "logOut",
          value: function logOut() {
            this.localStorage.clear();
            this.router.navigate(['/'])["catch"](function (e) {
              return console.error(e);
            });
          }
          /**
           * Function that receives from the interceptor the error of the request and personalize message error
           *
           * @param error tslint(no-redundant-jsdoc)
           */

        }, {
          key: "handleError",
          value: function handleError(error) {
            var objectCustomError = {
              title: 'ERROR ' + error.status,
              message: 'Contacte con el administrador del sistema.',
              type: 'danger',
              error: error.error,
              icon: 'fa-dizzy'
            };

            switch (error.status) {
              case 401:
                if (error.error !== undefined) {
                  objectCustomError.message = error.error.message;
                }

                this.logOut();
                break;

              case 403:
                if (error.error !== undefined) {
                  objectCustomError.message = error.error.message;
                }

                break;

              case 406:
                objectCustomError.title = 'ERROR DE AUTENTICACIÓN';

                if (error.error !== undefined) {
                  objectCustomError.message = error.error.message;
                }

                this.logOut();
                break;

              case 413:
                objectCustomError.title = 'ARCHIVO MUY PESADO';
                objectCustomError.message = 'El archivo que trata de cargar excede el límite permitido.';
                break;

              case 422:
                objectCustomError.title = 'ERROR DE VALIDACIÓN';
                var obj = error.error.errors_validations_fields;

                for (var key in obj) {
                  if (obj.hasOwnProperty(key)) {
                    var element = obj[key];
                    objectCustomError.message = element[0];
                    break;
                  }
                }

                break;

              case 0:
                objectCustomError.title = 'ERROR DE CONEXIÓN';
                objectCustomError.message = 'Contacte con el administrador del sistema.';
                break;

              default:
                break;
            }

            return objectCustomError;
          }
        }]);

        return GlobalErrorHandler;
      }();

      GlobalErrorHandler.ɵfac = function GlobalErrorHandler_Factory(t) {
        return new (t || GlobalErrorHandler)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_LocalStorageCommon__WEBPACK_IMPORTED_MODULE_2__["LocalStorageCommon"]));
      };

      GlobalErrorHandler.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({
        token: GlobalErrorHandler,
        factory: GlobalErrorHandler.ɵfac,
        providedIn: 'root'
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](GlobalErrorHandler, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
          args: [{
            providedIn: 'root'
          }]
        }], function () {
          return [{
            type: _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]
          }, {
            type: _LocalStorageCommon__WEBPACK_IMPORTED_MODULE_2__["LocalStorageCommon"]
          }];
        }, null);
      })();
      /***/

    },

    /***/
    "RnhZ":
    /*!**************************************************!*\
      !*** ./node_modules/moment/locale sync ^\.\/.*$ ***!
      \**************************************************/

    /*! no static exports found */

    /***/
    function RnhZ(module, exports, __webpack_require__) {
      var map = {
        "./af": "K/tc",
        "./af.js": "K/tc",
        "./ar": "jnO4",
        "./ar-dz": "o1bE",
        "./ar-dz.js": "o1bE",
        "./ar-kw": "Qj4J",
        "./ar-kw.js": "Qj4J",
        "./ar-ly": "HP3h",
        "./ar-ly.js": "HP3h",
        "./ar-ma": "CoRJ",
        "./ar-ma.js": "CoRJ",
        "./ar-sa": "gjCT",
        "./ar-sa.js": "gjCT",
        "./ar-tn": "bYM6",
        "./ar-tn.js": "bYM6",
        "./ar.js": "jnO4",
        "./az": "SFxW",
        "./az.js": "SFxW",
        "./be": "H8ED",
        "./be.js": "H8ED",
        "./bg": "hKrs",
        "./bg.js": "hKrs",
        "./bm": "p/rL",
        "./bm.js": "p/rL",
        "./bn": "kEOa",
        "./bn-bd": "loYQ",
        "./bn-bd.js": "loYQ",
        "./bn.js": "kEOa",
        "./bo": "0mo+",
        "./bo.js": "0mo+",
        "./br": "aIdf",
        "./br.js": "aIdf",
        "./bs": "JVSJ",
        "./bs.js": "JVSJ",
        "./ca": "1xZ4",
        "./ca.js": "1xZ4",
        "./cs": "PA2r",
        "./cs.js": "PA2r",
        "./cv": "A+xa",
        "./cv.js": "A+xa",
        "./cy": "l5ep",
        "./cy.js": "l5ep",
        "./da": "DxQv",
        "./da.js": "DxQv",
        "./de": "tGlX",
        "./de-at": "s+uk",
        "./de-at.js": "s+uk",
        "./de-ch": "u3GI",
        "./de-ch.js": "u3GI",
        "./de.js": "tGlX",
        "./dv": "WYrj",
        "./dv.js": "WYrj",
        "./el": "jUeY",
        "./el.js": "jUeY",
        "./en-au": "Dmvi",
        "./en-au.js": "Dmvi",
        "./en-ca": "OIYi",
        "./en-ca.js": "OIYi",
        "./en-gb": "Oaa7",
        "./en-gb.js": "Oaa7",
        "./en-ie": "4dOw",
        "./en-ie.js": "4dOw",
        "./en-il": "czMo",
        "./en-il.js": "czMo",
        "./en-in": "7C5Q",
        "./en-in.js": "7C5Q",
        "./en-nz": "b1Dy",
        "./en-nz.js": "b1Dy",
        "./en-sg": "t+mt",
        "./en-sg.js": "t+mt",
        "./eo": "Zduo",
        "./eo.js": "Zduo",
        "./es": "iYuL",
        "./es-do": "CjzT",
        "./es-do.js": "CjzT",
        "./es-mx": "tbfe",
        "./es-mx.js": "tbfe",
        "./es-us": "Vclq",
        "./es-us.js": "Vclq",
        "./es.js": "iYuL",
        "./et": "7BjC",
        "./et.js": "7BjC",
        "./eu": "D/JM",
        "./eu.js": "D/JM",
        "./fa": "jfSC",
        "./fa.js": "jfSC",
        "./fi": "gekB",
        "./fi.js": "gekB",
        "./fil": "1ppg",
        "./fil.js": "1ppg",
        "./fo": "ByF4",
        "./fo.js": "ByF4",
        "./fr": "nyYc",
        "./fr-ca": "2fjn",
        "./fr-ca.js": "2fjn",
        "./fr-ch": "Dkky",
        "./fr-ch.js": "Dkky",
        "./fr.js": "nyYc",
        "./fy": "cRix",
        "./fy.js": "cRix",
        "./ga": "USCx",
        "./ga.js": "USCx",
        "./gd": "9rRi",
        "./gd.js": "9rRi",
        "./gl": "iEDd",
        "./gl.js": "iEDd",
        "./gom-deva": "qvJo",
        "./gom-deva.js": "qvJo",
        "./gom-latn": "DKr+",
        "./gom-latn.js": "DKr+",
        "./gu": "4MV3",
        "./gu.js": "4MV3",
        "./he": "x6pH",
        "./he.js": "x6pH",
        "./hi": "3E1r",
        "./hi.js": "3E1r",
        "./hr": "S6ln",
        "./hr.js": "S6ln",
        "./hu": "WxRl",
        "./hu.js": "WxRl",
        "./hy-am": "1rYy",
        "./hy-am.js": "1rYy",
        "./id": "UDhR",
        "./id.js": "UDhR",
        "./is": "BVg3",
        "./is.js": "BVg3",
        "./it": "bpih",
        "./it-ch": "bxKX",
        "./it-ch.js": "bxKX",
        "./it.js": "bpih",
        "./ja": "B55N",
        "./ja.js": "B55N",
        "./jv": "tUCv",
        "./jv.js": "tUCv",
        "./ka": "IBtZ",
        "./ka.js": "IBtZ",
        "./kk": "bXm7",
        "./kk.js": "bXm7",
        "./km": "6B0Y",
        "./km.js": "6B0Y",
        "./kn": "PpIw",
        "./kn.js": "PpIw",
        "./ko": "Ivi+",
        "./ko.js": "Ivi+",
        "./ku": "JCF/",
        "./ku.js": "JCF/",
        "./ky": "lgnt",
        "./ky.js": "lgnt",
        "./lb": "RAwQ",
        "./lb.js": "RAwQ",
        "./lo": "sp3z",
        "./lo.js": "sp3z",
        "./lt": "JvlW",
        "./lt.js": "JvlW",
        "./lv": "uXwI",
        "./lv.js": "uXwI",
        "./me": "KTz0",
        "./me.js": "KTz0",
        "./mi": "aIsn",
        "./mi.js": "aIsn",
        "./mk": "aQkU",
        "./mk.js": "aQkU",
        "./ml": "AvvY",
        "./ml.js": "AvvY",
        "./mn": "lYtQ",
        "./mn.js": "lYtQ",
        "./mr": "Ob0Z",
        "./mr.js": "Ob0Z",
        "./ms": "6+QB",
        "./ms-my": "ZAMP",
        "./ms-my.js": "ZAMP",
        "./ms.js": "6+QB",
        "./mt": "G0Uy",
        "./mt.js": "G0Uy",
        "./my": "honF",
        "./my.js": "honF",
        "./nb": "bOMt",
        "./nb.js": "bOMt",
        "./ne": "OjkT",
        "./ne.js": "OjkT",
        "./nl": "+s0g",
        "./nl-be": "2ykv",
        "./nl-be.js": "2ykv",
        "./nl.js": "+s0g",
        "./nn": "uEye",
        "./nn.js": "uEye",
        "./oc-lnc": "Fnuy",
        "./oc-lnc.js": "Fnuy",
        "./pa-in": "8/+R",
        "./pa-in.js": "8/+R",
        "./pl": "jVdC",
        "./pl.js": "jVdC",
        "./pt": "8mBD",
        "./pt-br": "0tRk",
        "./pt-br.js": "0tRk",
        "./pt.js": "8mBD",
        "./ro": "lyxo",
        "./ro.js": "lyxo",
        "./ru": "lXzo",
        "./ru.js": "lXzo",
        "./sd": "Z4QM",
        "./sd.js": "Z4QM",
        "./se": "//9w",
        "./se.js": "//9w",
        "./si": "7aV9",
        "./si.js": "7aV9",
        "./sk": "e+ae",
        "./sk.js": "e+ae",
        "./sl": "gVVK",
        "./sl.js": "gVVK",
        "./sq": "yPMs",
        "./sq.js": "yPMs",
        "./sr": "zx6S",
        "./sr-cyrl": "E+lV",
        "./sr-cyrl.js": "E+lV",
        "./sr.js": "zx6S",
        "./ss": "Ur1D",
        "./ss.js": "Ur1D",
        "./sv": "X709",
        "./sv.js": "X709",
        "./sw": "dNwA",
        "./sw.js": "dNwA",
        "./ta": "PeUW",
        "./ta.js": "PeUW",
        "./te": "XLvN",
        "./te.js": "XLvN",
        "./tet": "V2x9",
        "./tet.js": "V2x9",
        "./tg": "Oxv6",
        "./tg.js": "Oxv6",
        "./th": "EOgW",
        "./th.js": "EOgW",
        "./tk": "Wv91",
        "./tk.js": "Wv91",
        "./tl-ph": "Dzi0",
        "./tl-ph.js": "Dzi0",
        "./tlh": "z3Vd",
        "./tlh.js": "z3Vd",
        "./tr": "DoHr",
        "./tr.js": "DoHr",
        "./tzl": "z1FC",
        "./tzl.js": "z1FC",
        "./tzm": "wQk9",
        "./tzm-latn": "tT3J",
        "./tzm-latn.js": "tT3J",
        "./tzm.js": "wQk9",
        "./ug-cn": "YRex",
        "./ug-cn.js": "YRex",
        "./uk": "raLr",
        "./uk.js": "raLr",
        "./ur": "UpQW",
        "./ur.js": "UpQW",
        "./uz": "Loxo",
        "./uz-latn": "AQ68",
        "./uz-latn.js": "AQ68",
        "./uz.js": "Loxo",
        "./vi": "KSF8",
        "./vi.js": "KSF8",
        "./x-pseudo": "/X5v",
        "./x-pseudo.js": "/X5v",
        "./yo": "fzPg",
        "./yo.js": "fzPg",
        "./zh-cn": "XDpg",
        "./zh-cn.js": "XDpg",
        "./zh-hk": "SatO",
        "./zh-hk.js": "SatO",
        "./zh-mo": "OmwH",
        "./zh-mo.js": "OmwH",
        "./zh-tw": "kOpN",
        "./zh-tw.js": "kOpN"
      };

      function webpackContext(req) {
        var id = webpackContextResolve(req);
        return __webpack_require__(id);
      }

      function webpackContextResolve(req) {
        if (!__webpack_require__.o(map, req)) {
          var e = new Error("Cannot find module '" + req + "'");
          e.code = 'MODULE_NOT_FOUND';
          throw e;
        }

        return map[req];
      }

      webpackContext.keys = function webpackContextKeys() {
        return Object.keys(map);
      };

      webpackContext.resolve = webpackContextResolve;
      module.exports = webpackContext;
      webpackContext.id = "RnhZ";
      /***/
    },

    /***/
    "SNli":
    /*!*********************************************************************************************************!*\
      !*** ./src/app/admin/components/workflow/departamento/edit-departamento/edit-departamento.component.ts ***!
      \*********************************************************************************************************/

    /*! exports provided: EditDepartamentoComponent */

    /***/
    function SNli(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "EditDepartamentoComponent", function () {
        return EditDepartamentoComponent;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/forms */
      "3Pt+");
      /* harmony import */


      var ngx_ui_loader__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ngx-ui-loader */
      "gren");
      /* harmony import */


      var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! rxjs */
      "qCKp");
      /* harmony import */


      var src_app_admin_services_workflow_departamento_departamento_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! src/app/admin/services/workflow/departamento/departamento.service */
      "tmbA");
      /* harmony import */


      var src_app_shared_classes__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! src/app/shared/classes */
      "gtvE");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");

      function EditDepartamentoComponent_div_23_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 26);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", ctx_r0.error.nombre.msg, " ");
        }
      }

      function EditDepartamentoComponent_div_24_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 27);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "em", 28);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      function EditDepartamentoComponent_div_25_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 27);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "em", 29);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      function EditDepartamentoComponent_div_33_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 26);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var ctx_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", ctx_r3.error.telefono.msg, " ");
        }
      }

      function EditDepartamentoComponent_div_34_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 27);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "em", 28);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      function EditDepartamentoComponent_div_35_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 27);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "em", 29);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      function EditDepartamentoComponent_div_43_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 26);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var ctx_r6 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", ctx_r6.error.extencion.msg, " ");
        }
      }

      function EditDepartamentoComponent_div_44_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 27);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "em", 28);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      function EditDepartamentoComponent_div_45_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 27);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "em", 29);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      var _c0 = function _c0(a0, a1) {
        return {
          "is-invalid": a0,
          "is-valid": a1
        };
      };

      var EditDepartamentoComponent = /*#__PURE__*/function () {
        function EditDepartamentoComponent(formBuilder, departamentoService, helper, ngxService) {
          var _this10 = this;

          _classCallCheck(this, EditDepartamentoComponent);

          this.formBuilder = formBuilder;
          this.departamentoService = departamentoService;
          this.helper = helper;
          this.ngxService = ngxService;
          this.goBack = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
          this.destroySubscription$ = new rxjs__WEBPACK_IMPORTED_MODULE_3__["Subject"]();
          this.departamentoForm = ['nombre', 'telefono', 'extencion']; //iniciar los errores y las validaciones

          this.errorInit = function () {
            _this10.error = {
              nombre: {
                error: false,
                change: false,
                msg: ''
              },
              telefono: {
                error: false,
                change: false,
                msg: ''
              },
              extencion: {
                error: false,
                change: false,
                msg: ''
              }
            };
          }; //enviar data al backend


          this.action = function () {
            _this10.ngxService.start();

            var form = _this10.form.value;
            var data = {
              id: _this10.departamento.id,
              nombre: form.nombre,
              telefono: form.telefono,
              extencion: form.extencion
            };

            _this10.departamentoService.updateDepartamentos(_this10.departamento.id, data, _this10);
          };
        }

        _createClass(EditDepartamentoComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            //validaciones del formulario
            this.form = this.formBuilder.group({
              nombre: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', {
                validators: [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]
              }),
              telefono: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', {
                validators: [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]
              }),
              extencion: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', {
                validators: [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]
              })
            });

            if (this.departamento) {
              this.fillForm(this.departamento);
            }

            this.errorInit();
          }
        }, {
          key: "fillForm",
          value: function fillForm(item) {
            var _this11 = this;

            this.departamentoForm.forEach(function (i) {
              _this11.form.get(i).reset(item[i]);
            });
          } //regresar al listado de departamentos

        }, {
          key: "reversePage",
          value: function reversePage() {
            this.goBack.emit(true);
          }
        }, {
          key: "ngOnDestroy",
          value: function ngOnDestroy() {
            this.destroySubscription$.next(); // trigger the unsubscribe

            this.destroySubscription$.complete(); // finalize & clean up the subject stream
          }
        }]);

        return EditDepartamentoComponent;
      }();

      EditDepartamentoComponent.ɵfac = function EditDepartamentoComponent_Factory(t) {
        return new (t || EditDepartamentoComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_admin_services_workflow_departamento_departamento_service__WEBPACK_IMPORTED_MODULE_4__["DepartamentoService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_shared_classes__WEBPACK_IMPORTED_MODULE_5__["Helpers"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](ngx_ui_loader__WEBPACK_IMPORTED_MODULE_2__["NgxUiLoaderService"]));
      };

      EditDepartamentoComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
        type: EditDepartamentoComponent,
        selectors: [["app-edit-departamento"]],
        inputs: {
          departamento: "departamento"
        },
        outputs: {
          goBack: "goBack"
        },
        decls: 50,
        vars: 23,
        consts: [[1, "row", "wizard-title", 2, "width", "100%", "height", "100%", "margin-top", "100px"], [1, "col-11", "mr-auto", "wizard-fonts"], [1, "required-fields"], [1, "col-1", "align-self-end"], [1, "btn", "btn-clear", 3, "click"], [1, "fas", "fa-angle-left"], [1, "row", "justify-content-center"], [1, "card", "fixed-width"], [1, "card-body"], ["autocomplete", "off", 3, "formGroup"], [1, "row"], [1, "col-4"], [1, "form-group"], ["for", "brandname-addDepartamentoComponent"], [1, "fiel-validate"], [1, "field"], ["formControlName", "nombre", "id", "nombre-addDepartamentoComponent", "placeholder", "Nombre del departamento", "type", "text", 1, "form-control", "form-input", "capitalize", 3, "ngClass", "blur", "input"], ["class", "invalid-feedback font-color-error", 4, "ngIf"], ["class", "icon-field", 4, "ngIf"], ["for", "telefono-addDepartamentoComponent"], ["formControlName", "telefono", "id", "telefono-addDepartamentoComponent", "placeholder", "telefono del departamento", "type", "text", 1, "form-control", "form-input", "capitalize", 3, "ngClass", "blur", "input"], ["for", "extencion-addDepartamentoComponent"], ["formControlName", "extencion", "id", "extencion-addDepartamentoComponent", "placeholder", "Extencion", "type", "text", 1, "form-control", "form-input", "capitalize", 3, "ngClass", "blur", "input"], [1, "row", "push-green"], [1, "col-12", "text-right"], ["data-target", "#confirmationModal-addVehicleComponent", "id", "confirmationModalBtn-addVehicleComponent", 1, "btn", "btn-save", 3, "disabled", "click"], [1, "invalid-feedback", "font-color-error"], [1, "icon-field"], [1, "fas", "fa-exclamation-circle", "fa-lg", "font-color-error"], [1, "fas", "fa-check-circle", "fa-lg", "font-color-success"]],
        template: function EditDepartamentoComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "h4");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3, "Edita un departamento");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "small", 2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "* Campos requeridos");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "button", 4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function EditDepartamentoComponent_Template_button_click_7_listener() {
              return ctx.reversePage();
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](8, "em", 5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](9, " \xA0 Regresar ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](10, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "div", 6);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "div", 7);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "div", 8);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "form", 9);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "div", 10);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "div", 11);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "div", 12);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "label", 13);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](19, "Nombre*");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "div", 14);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "div", 15);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "input", 16);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("blur", function EditDepartamentoComponent_Template_input_blur_22_listener() {
              return ctx.helper.validInput(ctx.error, "nombre", ctx.form);
            })("input", function EditDepartamentoComponent_Template_input_input_22_listener() {
              return ctx.helper.validInput(ctx.error, "nombre", ctx.form);
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](23, EditDepartamentoComponent_div_23_Template, 2, 1, "div", 17);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](24, EditDepartamentoComponent_div_24_Template, 2, 0, "div", 18);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](25, EditDepartamentoComponent_div_25_Template, 2, 0, "div", 18);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](26, "div", 11);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "div", 12);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "label", 19);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](29, "Tel\xE9fono *");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](30, "div", 14);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](31, "div", 15);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](32, "input", 20);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("blur", function EditDepartamentoComponent_Template_input_blur_32_listener() {
              return ctx.helper.validInput(ctx.error, "telefono", ctx.form);
            })("input", function EditDepartamentoComponent_Template_input_input_32_listener() {
              return ctx.helper.validInput(ctx.error, "telefono", ctx.form);
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](33, EditDepartamentoComponent_div_33_Template, 2, 1, "div", 17);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](34, EditDepartamentoComponent_div_34_Template, 2, 0, "div", 18);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](35, EditDepartamentoComponent_div_35_Template, 2, 0, "div", 18);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](36, "div", 11);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](37, "div", 12);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](38, "label", 21);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](39, "Extencion *");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](40, "div", 14);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](41, "div", 15);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](42, "input", 22);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("blur", function EditDepartamentoComponent_Template_input_blur_42_listener() {
              return ctx.helper.validInput(ctx.error, "extencion", ctx.form);
            })("input", function EditDepartamentoComponent_Template_input_input_42_listener() {
              return ctx.helper.validInput(ctx.error, "extencion", ctx.form);
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](43, EditDepartamentoComponent_div_43_Template, 2, 1, "div", 17);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](44, EditDepartamentoComponent_div_44_Template, 2, 0, "div", 18);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](45, EditDepartamentoComponent_div_45_Template, 2, 0, "div", 18);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](46, "div", 23);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](47, "div", 24);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](48, "button", 25);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function EditDepartamentoComponent_Template_button_click_48_listener() {
              return ctx.form.valid && ctx.action();
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](49, " Actualizar ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
          }

          if (rf & 2) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](14);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("formGroup", ctx.form);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](8);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction2"](14, _c0, ctx.error.nombre.error, !ctx.error.nombre.error && ctx.error.nombre.change));

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.form.get("nombre").errors == null ? null : ctx.form.get("nombre").errors.required);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.error.nombre.error && ctx.error.nombre.change);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx.error.nombre.error && ctx.error.nombre.change);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction2"](17, _c0, ctx.error.telefono.error, !ctx.error.telefono.error && ctx.error.telefono.change));

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.form.get("telefono").errors == null ? null : ctx.form.get("telefono").errors.required);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.error.telefono.error && ctx.error.telefono.change);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx.error.telefono.error && ctx.error.telefono.change);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction2"](20, _c0, ctx.error.extencion.error, !ctx.error.extencion.error && ctx.error.extencion.change));

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.form.get("extencion").errors == null ? null : ctx.form.get("extencion").errors.required);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.error.extencion.error && ctx.error.extencion.change);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx.error.extencion.error && ctx.error.extencion.change);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("disabled", !ctx.form.valid);
          }
        },
        directives: [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["ɵangular_packages_forms_forms_y"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NgControlStatusGroup"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroupDirective"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["DefaultValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControlName"], _angular_common__WEBPACK_IMPORTED_MODULE_6__["NgClass"], _angular_common__WEBPACK_IMPORTED_MODULE_6__["NgIf"]],
        styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJlZGl0LWRlcGFydGFtZW50by5jb21wb25lbnQuc2NzcyJ9 */"]
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](EditDepartamentoComponent, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
          args: [{
            selector: 'app-edit-departamento',
            templateUrl: './edit-departamento.component.html',
            styleUrls: ['./edit-departamento.component.scss']
          }]
        }], function () {
          return [{
            type: _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"]
          }, {
            type: src_app_admin_services_workflow_departamento_departamento_service__WEBPACK_IMPORTED_MODULE_4__["DepartamentoService"]
          }, {
            type: src_app_shared_classes__WEBPACK_IMPORTED_MODULE_5__["Helpers"]
          }, {
            type: ngx_ui_loader__WEBPACK_IMPORTED_MODULE_2__["NgxUiLoaderService"]
          }];
        }, {
          departamento: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
          }],
          goBack: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
          }]
        });
      })();
      /***/

    },

    /***/
    "Sj5B":
    /*!*************************************************!*\
      !*** ./src/app/shared/shared-routing.module.ts ***!
      \*************************************************/

    /*! exports provided: SharedRoutingModule */

    /***/
    function Sj5B(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "SharedRoutingModule", function () {
        return SharedRoutingModule;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");

      var routes = [];

      var SharedRoutingModule = function SharedRoutingModule() {
        _classCallCheck(this, SharedRoutingModule);
      };

      SharedRoutingModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({
        type: SharedRoutingModule
      });
      SharedRoutingModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({
        factory: function SharedRoutingModule_Factory(t) {
          return new (t || SharedRoutingModule)();
        },
        imports: [[_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)], _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
      });

      (function () {
        (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](SharedRoutingModule, {
          imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]],
          exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        });
      })();
      /*@__PURE__*/


      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](SharedRoutingModule, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
          args: [{
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
          }]
        }], null, null);
      })();
      /***/

    },

    /***/
    "Sy1n":
    /*!**********************************!*\
      !*** ./src/app/app.component.ts ***!
      \**********************************/

    /*! exports provided: AppComponent */

    /***/
    function Sy1n(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AppComponent", function () {
        return AppComponent;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var src_app_shared_classes_LocalStorageCommon__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! src/app/shared/classes/LocalStorageCommon */
      "hdVc");

      var AppComponent = /*#__PURE__*/function () {
        function AppComponent(router, localStorageCommon) {
          _classCallCheck(this, AppComponent);

          this.router = router;
          this.localStorageCommon = localStorageCommon;
        }

        _createClass(AppComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            var token = this.localStorageCommon.get('auth_token', 'normal');

            if (token === null) {
              this.router.navigate(['/']);
            }
          }
        }]);

        return AppComponent;
      }();

      AppComponent.ɵfac = function AppComponent_Factory(t) {
        return new (t || AppComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_shared_classes_LocalStorageCommon__WEBPACK_IMPORTED_MODULE_2__["LocalStorageCommon"]));
      };

      AppComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
        type: AppComponent,
        selectors: [["app-root"]],
        decls: 1,
        vars: 0,
        template: function AppComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "router-outlet");
          }
        },
        directives: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterOutlet"]],
        styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJhcHAuY29tcG9uZW50LnNjc3MifQ== */"]
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](AppComponent, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
          args: [{
            selector: 'app-root',
            templateUrl: './app.component.html',
            styleUrls: ['./app.component.scss']
          }]
        }], function () {
          return [{
            type: _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]
          }, {
            type: src_app_shared_classes_LocalStorageCommon__WEBPACK_IMPORTED_MODULE_2__["LocalStorageCommon"]
          }];
        }, null);
      })();
      /***/

    },

    /***/
    "TRGY":
    /*!******************************************!*\
      !*** ./src/app/admin/admin.component.ts ***!
      \******************************************/

    /*! exports provided: AdminComponent */

    /***/
    function TRGY(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AdminComponent", function () {
        return AdminComponent;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");

      var AdminComponent = function AdminComponent() {
        _classCallCheck(this, AdminComponent);
      };

      AdminComponent.ɵfac = function AdminComponent_Factory(t) {
        return new (t || AdminComponent)();
      };

      AdminComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
        type: AdminComponent,
        selectors: [["app-business"]],
        decls: 1,
        vars: 0,
        template: function AdminComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "router-outlet");
          }
        },
        directives: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterOutlet"]],
        styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJhZG1pbi5jb21wb25lbnQuc2NzcyJ9 */"]
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](AdminComponent, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
          args: [{
            selector: 'app-business',
            templateUrl: './admin.component.html',
            styleUrls: ['./admin.component.scss']
          }]
        }], null, null);
      })();
      /***/

    },

    /***/
    "W5i3":
    /*!**************************************************************************!*\
      !*** ./src/app/admin/components/workflow/reportes/reportes.component.ts ***!
      \**************************************************************************/

    /*! exports provided: ReportesComponent */

    /***/
    function W5i3(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ReportesComponent", function () {
        return ReportesComponent;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/forms */
      "3Pt+");
      /* harmony import */


      var _services_workflow_empleado_empleado_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ../../../services/workflow/empleado/empleado.service */
      "k4qQ");
      /* harmony import */


      var _services_workflow_departamento_departamento_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ../../../services/workflow/departamento/departamento.service */
      "tmbA");
      /* harmony import */


      var _services_workflow_supervisor_supervisor_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ../../../services/workflow/supervisor/supervisor.service */
      "Itk3");
      /* harmony import */


      var _services_workflow_reporteria_reporteria_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ../../../services/workflow/reporteria/reporteria.service */
      "tGCW");
      /* harmony import */


      var xlsx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! xlsx */
      "EUZL");
      /* harmony import */


      var xlsx__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(xlsx__WEBPACK_IMPORTED_MODULE_6__);
      /* harmony import */


      var ngx_ui_loader__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! ngx-ui-loader */
      "gren");
      /* harmony import */


      var rxjs__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! rxjs */
      "qCKp");
      /* harmony import */


      var src_app_shared_classes__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
      /*! src/app/shared/classes */
      "gtvE");
      /* harmony import */


      var _ng_select_ng_select__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
      /*! @ng-select/ng-select */
      "ZOsW");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");
      /* harmony import */


      var _shared_pipes_format_money_ec_pipe__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(
      /*! ../../../../shared/pipes/format-money-ec.pipe */
      "AElR");

      function ReportesComponent_div_43_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 34);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 35);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 36);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 37);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "p", 38);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, " No tienes ning\xFAn departamento ingresado ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      function ReportesComponent_div_44_tr_47_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "tr");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "td", 46);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "td", 46);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "td", 46);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "td", 46);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "td", 46);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](11, "formatMoneyEc");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](12, "currency");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "td", 46);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](14);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "td", 46);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](16);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var item_r4 = ctx.$implicit;

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](item_r4.nombre);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](item_r4.apellido);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](item_r4.telefono);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](item_r4.email);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](11, 7, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind2"](12, 9, item_r4.salario, "USD")), " ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](item_r4.departamento == null ? null : item_r4.departamento.nombre);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](item_r4.supervisor == null ? null : item_r4.supervisor.nombre);
        }
      }

      function ReportesComponent_div_44_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 39);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "table", 40);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "thead");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "tr");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "th", 41);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "div", 3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 42);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](7, "Nombre");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "div", 43);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](9, "|");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "th", 44);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "div", 3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "div", 45);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](13, "Apellido");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "div", 43);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](15, "|");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "th", 44);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "div", 3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "div", 45);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](19, "Tel\xE9fono");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "div", 43);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](21, "|");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "th", 44);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "div", 3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "div", 45);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](25, "Correo");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](26, "div", 43);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](27, "|");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "th", 44);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](29, "div", 3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](30, "div", 45);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](31, "Salario");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](32, "div", 43);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](33, "|");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](34, "th", 44);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](35, "div", 3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](36, "div", 45);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](37, "Departamento");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](38, "div", 43);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](39, "|");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](40, "th", 44);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](41, "div", 3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](42, "div", 45);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](43, "Supervisor");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](44, "div", 43);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](45, "|");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](46, "tbody");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](47, ReportesComponent_div_44_tr_47_Template, 17, 12, "tr", 33);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](47);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx_r1.empleados);
        }
      }

      function ReportesComponent_tr_64_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "tr");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "td", 47);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "td", 48);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "td", 49);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "td", 50);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "td", 51);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "td", 52);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "td", 53);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](14);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var item_r5 = ctx.$implicit;

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](item_r5.nombre);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](item_r5.apellido);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](item_r5.telefono);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](item_r5.email);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](item_r5.salario);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](item_r5.departamento == null ? null : item_r5.departamento.nombre);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](item_r5.supervisor == null ? null : item_r5.supervisor.nombre);
        }
      }

      var _c0 = function _c0(a0, a1) {
        return {
          "is-invalid": a0,
          "is-valid": a1
        };
      };

      var ReportesComponent = /*#__PURE__*/function () {
        function ReportesComponent(helper, reporteriaService, ngxService, formBuilder, empleadoService, supervisorService, departamentoService) {
          var _this12 = this;

          _classCallCheck(this, ReportesComponent);

          this.helper = helper;
          this.reporteriaService = reporteriaService;
          this.ngxService = ngxService;
          this.formBuilder = formBuilder;
          this.empleadoService = empleadoService;
          this.supervisorService = supervisorService;
          this.departamentoService = departamentoService;
          this.departamentos = [];
          this.supervisores = [];
          this.empleados = [];
          this.destroySubscription$ = new rxjs__WEBPACK_IMPORTED_MODULE_8__["Subject"](); //iniciar los errores y las validaciones

          this.errorInit = function () {
            _this12.error = {
              id_departamento: {
                error: false,
                change: false,
                msg: ''
              },
              id_supervisor: {
                error: false,
                change: false,
                msg: ''
              },
              salarioinicial: {
                error: false,
                change: false,
                msg: ''
              },
              salariofinal: {
                error: false,
                change: false,
                msg: ''
              }
            };
          };

          this.exportExcel = function () {
            var idTable = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
            var nameFile = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
            var idTableHTML = idTable !== null ? idTable : 'excel-table';
            var element = document.getElementById(idTableHTML);
            var ws = xlsx__WEBPACK_IMPORTED_MODULE_6__["utils"].table_to_sheet(element, {
              raw: true
            });
            /* generate workbook and add the worksheet */

            var wb = xlsx__WEBPACK_IMPORTED_MODULE_6__["utils"].book_new();
            xlsx__WEBPACK_IMPORTED_MODULE_6__["utils"].book_append_sheet(wb, ws, 'Sheet1');
            /* save to file */

            var nameFileEXCEL = nameFile !== null ? nameFile : 'Reporte';
            xlsx__WEBPACK_IMPORTED_MODULE_6__["writeFile"](wb, nameFileEXCEL + '.xlsx');
          };
        }

        _createClass(ReportesComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            this.errorInit();
            this.form = this.formBuilder.group({
              salarioinicial: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](null),
              salariofinal: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](null),
              id_departamento: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](null),
              id_supervisor: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](null)
            });
            this.getContadores();
          } //funcion para traer todos los datos para hacer un contador en el dasboard

        }, {
          key: "getContadores",
          value: function getContadores() {
            this.departamentoService.getAllDepartamentos(null, null, this);
            this.supervisorService.getAllSupervisores(null, null, this);
            this.empleadoService.getAllEmpleados(null, null, this);
          }
        }, {
          key: "saveFilter",
          value: function saveFilter() {
            var form = this.form.value;
            var saldo1 = 0;
            var saldo2 = 100000;

            if (form.salarioinicial) {
              saldo1 = form.salarioinicial;
            }

            if (form.salariofinal) {
              saldo2 = form.salariofinal;
            }

            if (form.id_departamento && !form.id_supervisor) {
              var data = {
                departamento: form.id_departamento,
                salarioinicial: saldo1,
                salariofinal: saldo2
              };
              this.reporteriaService.getallbydepartamento(data, null, this);
            }

            if (form.id_supervisor && !form.id_departamento) {
              var _data = {
                supervisor: form.id_supervisor,
                salarioinicial: saldo1,
                salariofinal: saldo2
              };
              this.reporteriaService.getallbysupervisor(_data, null, this);
            }

            if (form.id_supervisor && form.id_departamento) {
              var _data2 = {
                departamento: form.id_departamento,
                supervisor: form.id_supervisor,
                salarioinicial: saldo1,
                salariofinal: saldo2
              };
              this.reporteriaService.getAllByDepartamentoAndSupervisor(_data2, null, this);
            }

            if (!form.id_supervisor && !form.id_departamento) {
              this.getContadores();
            }
          }
        }, {
          key: "action",
          value: function action(page) {}
        }, {
          key: "ngOnDestroy",
          value: function ngOnDestroy() {
            this.destroySubscription$.next(); // trigger the unsubscribe

            this.destroySubscription$.complete(); // finalize & clean up the subject stream
          }
        }]);

        return ReportesComponent;
      }();

      ReportesComponent.ɵfac = function ReportesComponent_Factory(t) {
        return new (t || ReportesComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_shared_classes__WEBPACK_IMPORTED_MODULE_9__["Helpers"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_services_workflow_reporteria_reporteria_service__WEBPACK_IMPORTED_MODULE_5__["ReporteriaService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](ngx_ui_loader__WEBPACK_IMPORTED_MODULE_7__["NgxUiLoaderService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_services_workflow_empleado_empleado_service__WEBPACK_IMPORTED_MODULE_2__["EmpleadoService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_services_workflow_supervisor_supervisor_service__WEBPACK_IMPORTED_MODULE_4__["SupervisorService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_services_workflow_departamento_departamento_service__WEBPACK_IMPORTED_MODULE_3__["DepartamentoService"]));
      };

      ReportesComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
        type: ReportesComponent,
        selectors: [["app-reportes"]],
        decls: 65,
        vars: 16,
        consts: [[1, "emision-table", 2, "width", "100%", "height", "100%", "margin-top", "100px"], [1, "align-items-center", "headeractivate", "pt-2", "px-3", 3, "formGroup"], ["id", "filter-closureComponent", 1, "col-12"], [1, "row"], [1, "col-md-3", "col-12"], [1, "form-group"], ["for", "input-select-closure-institution"], ["bindLabel", "nombre", "bindValue", "id", "clearAllText", "limpiar campo", "formControlName", "id_departamento", "id", "id_province_appraisal-appraisalComponent", "ngDefaultControl", "", "placeholder", "Seleccione el departamento", 1, "ng-activate", "strech", 3, "items", "selectOnTab", "blur", "change"], ["for", "input-select-closure-contractprocess"], ["bindLabel", "nombre", "bindValue", "id", "clearAllText", "limpiar campo", "formControlName", "id_supervisor", "id", "id_province_appraisal-appraisalComponent", "ngDefaultControl", "", "placeholder", "Seleccione el supervisor", 1, "ng-activate", "strech", 3, "items", "selectOnTab", "blur", "change"], [1, "col-md-2", "col-12"], ["type", "text", "data-hj-allow", "", "formControlName", "salarioinicial", "id", "salarioinicial-appraisalComponent", "name", "salarioinicial", "placeholder", "ej: $1", 1, "form-control", "form-input", 3, "ngClass", "blur", "input"], ["type", "text", "data-hj-allow", "", "formControlName", "salariofinal", "id", "salariofinal-appraisalComponent", "name", "salariofinal", "placeholder", "ej: $10000", 1, "form-control", "form-input", 3, "ngClass", "blur", "input"], [1, "col-md-auto", "col-12", "d-flex", "justify-content-center"], [1, "dropdown", "mt-4"], ["id", "btn-closure-search", 1, "btn", "btn-block", "btn-clear-ref", "mt-4", 3, "click"], [1, "fas", "fa-filter"], ["aria-expanded", "false", "aria-haspopup", "true", "data-toggle", "dropdown", "id", "btn-closure-dropdownMenuExport", "role", "button", 1, "btn", "btn-outline-green-ref", "width-fix", "dropleft"], [1, "texbtn"], ["aria-labelledby", "navbarDropdownMenuLink", 1, "dropdown-menu", "dropdown-table", "dropdown-menu-right"], ["id", "btn-as-export-to", 1, "dropdown-item", 3, "click"], [1, "card-body", "table-strech"], ["class", "no-contracts", 4, "ngIf"], ["class", "table-responsive", 4, "ngIf"], [2, "display", "none"], ["aria-describedby", "tabla", "id", "excel-table"], ["id", "nump"], ["id", "cli"], ["id", "ced"], ["id", "marca"], ["id", "modelo"], ["id", "an"], ["id", "chasis"], [4, "ngFor", "ngForOf"], [1, "no-contracts"], [1, "container"], [1, "row", "justify-content-center"], [1, "col-4", "text-center", "center-data-missing"], [1, "no-contracts-text", "pt-4"], [1, "table-responsive"], ["aria-describedby", " tabla de empleados", 1, "table"], ["id", "nunoperacion"], [1, "col-auto", "head-title"], [1, "col-auto", "separator-title"], ["id", "fechacreada"], [1, "col-auto", "mr-auto", "head-title"], [1, "table-spacing"], ["id", "nombre"], ["id", "apellido"], ["id", "telefono"], ["id", "correo"], ["id", "salario"], ["id", "nombred"], ["id", "nombres"]],
        template: function ReportesComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "div", 5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "label", 6);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](7, "Departamento");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "div", 5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "ng-select", 7);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("blur", function ReportesComponent_Template_ng_select_blur_9_listener() {
              return ctx.helper.validInput(ctx.error, "id_departamento", ctx.form);
            })("change", function ReportesComponent_Template_ng_select_change_9_listener() {
              return ctx.helper.validInput(ctx.error, "id_departamento", ctx.form);
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "div", 4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "div", 5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "label", 8);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](13, "Supervisor");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "div", 5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "ng-select", 9);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("blur", function ReportesComponent_Template_ng_select_blur_15_listener() {
              return ctx.helper.validInput(ctx.error, "id_supervisor", ctx.form);
            })("change", function ReportesComponent_Template_ng_select_change_15_listener() {
              return ctx.helper.validInput(ctx.error, "id_supervisor", ctx.form);
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "div", 10);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "div", 5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "label", 8);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](19, "Salario desde");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "div", 5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "input", 11);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("blur", function ReportesComponent_Template_input_blur_21_listener() {
              return ctx.helper.validInput(ctx.error, "salarioinicial", ctx.form);
            })("input", function ReportesComponent_Template_input_input_21_listener() {
              return ctx.helper.validInput(ctx.error, "salarioinicial", ctx.form);
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "div", 10);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "div", 5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "label", 8);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](25, "Salario hasta");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](26, "div", 5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "input", 12);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("blur", function ReportesComponent_Template_input_blur_27_listener() {
              return ctx.helper.validInput(ctx.error, "salariofinal", ctx.form);
            })("input", function ReportesComponent_Template_input_input_27_listener() {
              return ctx.helper.validInput(ctx.error, "salariofinal", ctx.form);
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "div", 10);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](29, "div", 13);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](30, "div", 14);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](31, "button", 15);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function ReportesComponent_Template_button_click_31_listener() {
              ctx.action(1);
              return ctx.saveFilter();
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](32, "em", 16);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](33, " FILTRAR ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](34, "div", 14);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](35, "a", 17);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](36, "div", 18);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](37, "ACCIONES");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](38, "ul", 19);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](39, "li");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](40, "a", 20);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function ReportesComponent_Template_a_click_40_listener() {
              return ctx.exportExcel("excel-table", "Reporte filtro");
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](41, "Exportar a excel");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](42, "div", 21);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](43, ReportesComponent_div_43_Template, 6, 0, "div", 22);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](44, ReportesComponent_div_44_Template, 48, 1, "div", 23);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](45, "div", 24);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](46, "table", 25);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](47, "thead");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](48, "tr");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](49, "th", 26);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](50, "NOMBRE");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](51, "th", 27);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](52, "APELLIDO");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](53, "th", 28);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](54, "TEL\xC9FONO");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](55, "th", 29);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](56, "CORREO");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](57, "th", 30);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](58, "SALARIO");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](59, "th", 31);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](60, "DEPARTAMENTO");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](61, "th", 32);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](62, "SUPERVISOR");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](63, "tbody");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](64, ReportesComponent_tr_64_Template, 15, 7, "tr", 33);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
          }

          if (rf & 2) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("formGroup", ctx.form);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](8);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("items", ctx.departamentos)("selectOnTab", true);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](6);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("items", ctx.supervisores)("selectOnTab", true);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](6);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction2"](10, _c0, ctx.error.salarioinicial.error, !ctx.error.salarioinicial.error && ctx.error.salarioinicial.change));

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](6);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction2"](13, _c0, ctx.error.salariofinal.error, !ctx.error.salariofinal.error && ctx.error.salariofinal.change));

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](16);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.empleados.length == 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.empleados.length > 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](20);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx.empleados);
          }
        },
        directives: [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["NgControlStatusGroup"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroupDirective"], _ng_select_ng_select__WEBPACK_IMPORTED_MODULE_10__["NgSelectComponent"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControlName"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["DefaultValueAccessor"], _angular_common__WEBPACK_IMPORTED_MODULE_11__["NgClass"], _angular_common__WEBPACK_IMPORTED_MODULE_11__["NgIf"], _angular_common__WEBPACK_IMPORTED_MODULE_11__["NgForOf"]],
        pipes: [_shared_pipes_format_money_ec_pipe__WEBPACK_IMPORTED_MODULE_12__["FormatMoneyEcPipe"], _angular_common__WEBPACK_IMPORTED_MODULE_11__["CurrencyPipe"]],
        styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJyZXBvcnRlcy5jb21wb25lbnQuc2NzcyJ9 */"]
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](ReportesComponent, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
          args: [{
            selector: 'app-reportes',
            templateUrl: './reportes.component.html',
            styleUrls: ['./reportes.component.scss']
          }]
        }], function () {
          return [{
            type: src_app_shared_classes__WEBPACK_IMPORTED_MODULE_9__["Helpers"]
          }, {
            type: _services_workflow_reporteria_reporteria_service__WEBPACK_IMPORTED_MODULE_5__["ReporteriaService"]
          }, {
            type: ngx_ui_loader__WEBPACK_IMPORTED_MODULE_7__["NgxUiLoaderService"]
          }, {
            type: _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"]
          }, {
            type: _services_workflow_empleado_empleado_service__WEBPACK_IMPORTED_MODULE_2__["EmpleadoService"]
          }, {
            type: _services_workflow_supervisor_supervisor_service__WEBPACK_IMPORTED_MODULE_4__["SupervisorService"]
          }, {
            type: _services_workflow_departamento_departamento_service__WEBPACK_IMPORTED_MODULE_3__["DepartamentoService"]
          }];
        }, null);
      })();
      /***/

    },

    /***/
    "XBxM":
    /*!*******************************************************************************************************!*\
      !*** ./src/app/admin/components/workflow/departamento/add-departamento/add-departamento.component.ts ***!
      \*******************************************************************************************************/

    /*! exports provided: AddDepartamentoComponent */

    /***/
    function XBxM(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AddDepartamentoComponent", function () {
        return AddDepartamentoComponent;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/forms */
      "3Pt+");
      /* harmony import */


      var ngx_ui_loader__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ngx-ui-loader */
      "gren");
      /* harmony import */


      var _shared_classes_Helpers__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ../../../../../shared/classes/Helpers */
      "sFNd");
      /* harmony import */


      var _services_workflow_departamento_departamento_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ../../../../services/workflow/departamento/departamento.service */
      "tmbA");
      /* harmony import */


      var rxjs__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! rxjs */
      "qCKp");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");

      function AddDepartamentoComponent_div_23_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 26);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", ctx_r0.error.nombre.msg, " ");
        }
      }

      function AddDepartamentoComponent_div_24_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 27);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "em", 28);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      function AddDepartamentoComponent_div_25_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 27);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "em", 29);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      function AddDepartamentoComponent_div_33_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 26);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var ctx_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", ctx_r3.error.telefono.msg, " ");
        }
      }

      function AddDepartamentoComponent_div_34_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 27);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "em", 28);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      function AddDepartamentoComponent_div_35_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 27);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "em", 29);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      function AddDepartamentoComponent_div_43_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 26);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var ctx_r6 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", ctx_r6.error.extencion.msg, " ");
        }
      }

      function AddDepartamentoComponent_div_44_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 27);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "em", 28);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      function AddDepartamentoComponent_div_45_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 27);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "em", 29);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      var _c0 = function _c0(a0, a1) {
        return {
          "is-invalid": a0,
          "is-valid": a1
        };
      };

      var AddDepartamentoComponent = /*#__PURE__*/function () {
        function AddDepartamentoComponent(formBuilder, departamentoService, helper, ngxService) {
          var _this13 = this;

          _classCallCheck(this, AddDepartamentoComponent);

          this.formBuilder = formBuilder;
          this.departamentoService = departamentoService;
          this.helper = helper;
          this.ngxService = ngxService;
          this.goBack = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
          this.destroySubscription$ = new rxjs__WEBPACK_IMPORTED_MODULE_5__["Subject"](); //iniciar los errores y las validaciones

          this.errorInit = function () {
            _this13.error = {
              nombre: {
                error: false,
                change: false,
                msg: ''
              },
              telefono: {
                error: false,
                change: false,
                msg: ''
              },
              extencion: {
                error: false,
                change: false,
                msg: ''
              }
            };
          }; //enviar data al backend


          this.action = function () {
            _this13.ngxService.start();

            var form = _this13.form.value;
            var data = {
              nombre: form.nombre,
              telefono: form.telefono,
              extencion: form.extencion
            };

            _this13.departamentoService.addDepartamentos(data, _this13);
          };
        }

        _createClass(AddDepartamentoComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            this.errorInit(); //validaciones del formulario

            this.form = this.formBuilder.group({
              nombre: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', {
                validators: [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]
              }),
              telefono: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', {
                validators: [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]
              }),
              extencion: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', {
                validators: [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]
              })
            });
          } //regresar al listado de departamentos

        }, {
          key: "reversePage",
          value: function reversePage() {
            this.goBack.emit(true);
          }
        }, {
          key: "ngOnDestroy",
          value: function ngOnDestroy() {
            this.destroySubscription$.next(); // trigger the unsubscribe

            this.destroySubscription$.complete(); // finalize & clean up the subject stream
          }
        }]);

        return AddDepartamentoComponent;
      }();

      AddDepartamentoComponent.ɵfac = function AddDepartamentoComponent_Factory(t) {
        return new (t || AddDepartamentoComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_services_workflow_departamento_departamento_service__WEBPACK_IMPORTED_MODULE_4__["DepartamentoService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_shared_classes_Helpers__WEBPACK_IMPORTED_MODULE_3__["Helpers"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](ngx_ui_loader__WEBPACK_IMPORTED_MODULE_2__["NgxUiLoaderService"]));
      };

      AddDepartamentoComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
        type: AddDepartamentoComponent,
        selectors: [["app-add-departamento"]],
        outputs: {
          goBack: "goBack"
        },
        decls: 50,
        vars: 23,
        consts: [[1, "row", "wizard-title", 2, "width", "100%", "height", "100%", "margin-top", "100px"], [1, "col-11", "mr-auto", "wizard-fonts"], [1, "required-fields"], [1, "col-1", "align-self-end"], [1, "btn", "btn-clear", 3, "click"], [1, "fas", "fa-angle-left"], [1, "row", "justify-content-center"], [1, "card", "fixed-width"], [1, "card-body"], ["autocomplete", "off", 3, "formGroup"], [1, "row"], [1, "col-4"], [1, "form-group"], ["for", "brandname-addDepartamentoComponent"], [1, "fiel-validate"], [1, "field"], ["formControlName", "nombre", "id", "nombre-addDepartamentoComponent", "placeholder", "Nombre del departamento", "type", "text", 1, "form-control", "form-input", "capitalize", 3, "ngClass", "blur", "input"], ["class", "invalid-feedback font-color-error", 4, "ngIf"], ["class", "icon-field", 4, "ngIf"], ["for", "telefono-addDepartamentoComponent"], ["formControlName", "telefono", "id", "telefono-addDepartamentoComponent", "placeholder", "telefono del departamento", "type", "text", 1, "form-control", "form-input", "capitalize", 3, "ngClass", "blur", "input"], ["for", "extencion-addDepartamentoComponent"], ["formControlName", "extencion", "id", "extencion-addDepartamentoComponent", "placeholder", "Extencion", "type", "text", 1, "form-control", "form-input", "capitalize", 3, "ngClass", "blur", "input"], [1, "row", "push-green"], [1, "col-12", "text-right"], ["data-target", "#confirmationModal-addVehicleComponent", "id", "confirmationModalBtn-addVehicleComponent", 1, "btn", "btn-save", 3, "disabled", "click"], [1, "invalid-feedback", "font-color-error"], [1, "icon-field"], [1, "fas", "fa-exclamation-circle", "fa-lg", "font-color-error"], [1, "fas", "fa-check-circle", "fa-lg", "font-color-success"]],
        template: function AddDepartamentoComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "h4");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3, "Agrega un nuevo departamento");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "small", 2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "* Campos requeridos");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "button", 4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function AddDepartamentoComponent_Template_button_click_7_listener() {
              return ctx.reversePage();
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](8, "em", 5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](9, " \xA0 Regresar ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](10, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "div", 6);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "div", 7);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "div", 8);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "form", 9);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "div", 10);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "div", 11);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "div", 12);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "label", 13);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](19, "Nombre*");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "div", 14);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "div", 15);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "input", 16);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("blur", function AddDepartamentoComponent_Template_input_blur_22_listener() {
              return ctx.helper.validInput(ctx.error, "nombre", ctx.form);
            })("input", function AddDepartamentoComponent_Template_input_input_22_listener() {
              return ctx.helper.validInput(ctx.error, "nombre", ctx.form);
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](23, AddDepartamentoComponent_div_23_Template, 2, 1, "div", 17);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](24, AddDepartamentoComponent_div_24_Template, 2, 0, "div", 18);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](25, AddDepartamentoComponent_div_25_Template, 2, 0, "div", 18);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](26, "div", 11);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "div", 12);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "label", 19);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](29, "Tel\xE9fono *");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](30, "div", 14);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](31, "div", 15);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](32, "input", 20);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("blur", function AddDepartamentoComponent_Template_input_blur_32_listener() {
              return ctx.helper.validInput(ctx.error, "telefono", ctx.form);
            })("input", function AddDepartamentoComponent_Template_input_input_32_listener() {
              return ctx.helper.validInput(ctx.error, "telefono", ctx.form);
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](33, AddDepartamentoComponent_div_33_Template, 2, 1, "div", 17);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](34, AddDepartamentoComponent_div_34_Template, 2, 0, "div", 18);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](35, AddDepartamentoComponent_div_35_Template, 2, 0, "div", 18);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](36, "div", 11);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](37, "div", 12);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](38, "label", 21);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](39, "Extencion *");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](40, "div", 14);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](41, "div", 15);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](42, "input", 22);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("blur", function AddDepartamentoComponent_Template_input_blur_42_listener() {
              return ctx.helper.validInput(ctx.error, "extencion", ctx.form);
            })("input", function AddDepartamentoComponent_Template_input_input_42_listener() {
              return ctx.helper.validInput(ctx.error, "extencion", ctx.form);
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](43, AddDepartamentoComponent_div_43_Template, 2, 1, "div", 17);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](44, AddDepartamentoComponent_div_44_Template, 2, 0, "div", 18);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](45, AddDepartamentoComponent_div_45_Template, 2, 0, "div", 18);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](46, "div", 23);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](47, "div", 24);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](48, "button", 25);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function AddDepartamentoComponent_Template_button_click_48_listener() {
              return ctx.form.valid && ctx.action();
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](49, " Guardar ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
          }

          if (rf & 2) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](14);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("formGroup", ctx.form);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](8);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction2"](14, _c0, ctx.error.nombre.error, !ctx.error.nombre.error && ctx.error.nombre.change));

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.form.get("nombre").errors == null ? null : ctx.form.get("nombre").errors.required);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.error.nombre.error && ctx.error.nombre.change);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx.error.nombre.error && ctx.error.nombre.change);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction2"](17, _c0, ctx.error.telefono.error, !ctx.error.telefono.error && ctx.error.telefono.change));

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.form.get("telefono").errors == null ? null : ctx.form.get("telefono").errors.required);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.error.telefono.error && ctx.error.telefono.change);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx.error.telefono.error && ctx.error.telefono.change);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction2"](20, _c0, ctx.error.extencion.error, !ctx.error.extencion.error && ctx.error.extencion.change));

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.form.get("extencion").errors == null ? null : ctx.form.get("extencion").errors.required);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.error.extencion.error && ctx.error.extencion.change);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx.error.extencion.error && ctx.error.extencion.change);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("disabled", !ctx.form.valid);
          }
        },
        directives: [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["ɵangular_packages_forms_forms_y"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NgControlStatusGroup"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroupDirective"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["DefaultValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControlName"], _angular_common__WEBPACK_IMPORTED_MODULE_6__["NgClass"], _angular_common__WEBPACK_IMPORTED_MODULE_6__["NgIf"]],
        styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJhZGQtZGVwYXJ0YW1lbnRvLmNvbXBvbmVudC5zY3NzIn0= */"]
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](AddDepartamentoComponent, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
          args: [{
            selector: 'app-add-departamento',
            templateUrl: './add-departamento.component.html',
            styleUrls: ['./add-departamento.component.scss']
          }]
        }], function () {
          return [{
            type: _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"]
          }, {
            type: _services_workflow_departamento_departamento_service__WEBPACK_IMPORTED_MODULE_4__["DepartamentoService"]
          }, {
            type: _shared_classes_Helpers__WEBPACK_IMPORTED_MODULE_3__["Helpers"]
          }, {
            type: ngx_ui_loader__WEBPACK_IMPORTED_MODULE_2__["NgxUiLoaderService"]
          }];
        }, {
          goBack: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
          }]
        });
      })();
      /***/

    },

    /***/
    "ZAI4":
    /*!*******************************!*\
      !*** ./src/app/app.module.ts ***!
      \*******************************/

    /*! exports provided: AppModule */

    /***/
    function ZAI4(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AppModule", function () {
        return AppModule;
      });
      /* harmony import */


      var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/platform-browser */
      "jhN1");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/forms */
      "3Pt+");
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/common/http */
      "tk/3");
      /* harmony import */


      var _shared_interceptors_httpconfig_interceptor__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ./shared/interceptors/httpconfig.interceptor */
      "nEJ4");
      /* harmony import */


      var _app_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./app-routing.module */
      "vY5A");
      /* harmony import */


      var _app_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./app.component */
      "Sy1n");
      /* harmony import */


      var _shared_shared_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! ./shared/shared.module */
      "PCNd");
      /* harmony import */


      var _admin_admin_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! ./admin/admin.module */
      "jkDv");

      var AppModule = function AppModule() {
        _classCallCheck(this, AppModule);
      };

      AppModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineNgModule"]({
        type: AppModule,
        bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_6__["AppComponent"]]
      });
      AppModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineInjector"]({
        factory: function AppModule_Factory(t) {
          return new (t || AppModule)();
        },
        providers: [{
          provide: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HTTP_INTERCEPTORS"],
          useClass: _shared_interceptors_httpconfig_interceptor__WEBPACK_IMPORTED_MODULE_4__["HttpConfigInterceptor"],
          multi: true
        }],
        imports: [[_angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"], _app_routing_module__WEBPACK_IMPORTED_MODULE_5__["AppRoutingModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["ReactiveFormsModule"], _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClientModule"], _admin_admin_module__WEBPACK_IMPORTED_MODULE_8__["AdminModule"], _shared_shared_module__WEBPACK_IMPORTED_MODULE_7__["SharedModule"]]]
      });

      (function () {
        (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵsetNgModuleScope"](AppModule, {
          declarations: [_app_component__WEBPACK_IMPORTED_MODULE_6__["AppComponent"]],
          imports: [_angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"], _app_routing_module__WEBPACK_IMPORTED_MODULE_5__["AppRoutingModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["ReactiveFormsModule"], _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClientModule"], _admin_admin_module__WEBPACK_IMPORTED_MODULE_8__["AdminModule"], _shared_shared_module__WEBPACK_IMPORTED_MODULE_7__["SharedModule"]]
        });
      })();
      /*@__PURE__*/


      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵsetClassMetadata"](AppModule, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"],
          args: [{
            declarations: [_app_component__WEBPACK_IMPORTED_MODULE_6__["AppComponent"]],
            imports: [_angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"], _app_routing_module__WEBPACK_IMPORTED_MODULE_5__["AppRoutingModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["ReactiveFormsModule"], _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClientModule"], _admin_admin_module__WEBPACK_IMPORTED_MODULE_8__["AdminModule"], _shared_shared_module__WEBPACK_IMPORTED_MODULE_7__["SharedModule"]],
            providers: [{
              provide: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HTTP_INTERCEPTORS"],
              useClass: _shared_interceptors_httpconfig_interceptor__WEBPACK_IMPORTED_MODULE_4__["HttpConfigInterceptor"],
              multi: true
            }],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_6__["AppComponent"]]
          }]
        }], null, null);
      })();
      /***/

    },

    /***/
    "b/wG":
    /*!**********************************************************************************!*\
      !*** ./src/app/admin/components/workflow/departamento/departamento.component.ts ***!
      \**********************************************************************************/

    /*! exports provided: DepartamentoComponent */

    /***/
    function bWG(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "DepartamentoComponent", function () {
        return DepartamentoComponent;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _services_workflow_departamento_departamento_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ../../../services/workflow/departamento/departamento.service */
      "tmbA");
      /* harmony import */


      var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! rxjs */
      "qCKp");
      /* harmony import */


      var ngx_ui_loader__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ngx-ui-loader */
      "gren");
      /* harmony import */


      var xlsx__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! xlsx */
      "EUZL");
      /* harmony import */


      var xlsx__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(xlsx__WEBPACK_IMPORTED_MODULE_4__);
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");
      /* harmony import */


      var _add_departamento_add_departamento_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./add-departamento/add-departamento.component */
      "XBxM");
      /* harmony import */


      var _edit_departamento_edit_departamento_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! ./edit-departamento/edit-departamento.component */
      "SNli");

      function DepartamentoComponent_div_0_button_18_Template(rf, ctx) {
        if (rf & 1) {
          var _r8 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "button", 26);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function DepartamentoComponent_div_0_button_18_Template_button_click_0_listener() {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r8);

            var ctx_r7 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);

            return ctx_r7.addDepartamento();
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "i", 27);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2, " \xA0 CREAR DEPARTAMENTO ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      function DepartamentoComponent_div_0_div_20_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 28);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 29);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 30);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 31);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "p", 32);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, " No tienes ning\xFAn departamento ingresado ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      function DepartamentoComponent_div_0_div_21_tr_29_Template(rf, ctx) {
        if (rf & 1) {
          var _r12 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "tr");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "td", 41);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "td", 41);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "td", 41);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "td", 41);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "div", 42);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "div", 43);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "button", 44);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function DepartamentoComponent_div_0_div_21_tr_29_Template_button_click_10_listener() {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r12);

            var item_r10 = ctx.$implicit;

            var ctx_r11 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](3);

            return ctx_r11.eliminardepartamento(item_r10.id);
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](11, "i", 45);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "div", 43);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "button", 44);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function DepartamentoComponent_div_0_div_21_tr_29_Template_button_click_13_listener() {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r12);

            var item_r10 = ctx.$implicit;

            var ctx_r13 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](3);

            return ctx_r13.editdepartamento(item_r10);
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](14, "i", 46);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var item_r10 = ctx.$implicit;

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](item_r10.nombre);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](item_r10.telefono);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](item_r10.extencion);
        }
      }

      function DepartamentoComponent_div_0_div_21_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 33);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "table", 34);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "thead");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "tr");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "th", 35);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "div", 36);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 37);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](7, "Nombre");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "div", 38);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](9, "|");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "th", 39);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "div", 36);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "div", 40);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](13, "Tel\xE9fono");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "div", 38);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](15, "|");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "th", 39);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "div", 36);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "div", 40);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](19, "Extencion");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "div", 38);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](21, "|");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "th", 39);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "div", 36);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "div", 40);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](25, "Acciones");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](26, "div", 38);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](27, "|");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "tbody");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](29, DepartamentoComponent_div_0_div_21_tr_29_Template, 15, 3, "tr", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var ctx_r6 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](29);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx_r6.departamentos);
        }
      }

      function DepartamentoComponent_div_0_Template(rf, ctx) {
        if (rf & 1) {
          var _r15 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 9);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 10);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 11);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](3, "ngx-ui-loader", 12);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 13);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "h6");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6, "DEPARTAMENTO");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "div", 14);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "div", 15);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "div", 16);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "a", 17);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "div", 18);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12, "ACCIONES");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "ul", 19);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "li");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "a", 20);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function DepartamentoComponent_div_0_Template_a_click_15_listener() {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r15);

            var ctx_r14 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

            return ctx_r14.exportExcel("excel-table", "Reporte de departamentos");
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](16, "Exportar a excel");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "div", 21);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](18, DepartamentoComponent_div_0_button_18_Template, 3, 0, "button", 22);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "div", 23);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](20, DepartamentoComponent_div_0_div_20_Template, 6, 0, "div", 24);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](21, DepartamentoComponent_div_0_div_21_Template, 30, 1, "div", 25);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("loaderId", "departamento-loader");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](15);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r0.createDepartamento);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r0.departamentos.length == 0);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r0.departamentos.length > 0);
        }
      }

      function DepartamentoComponent_tr_12_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "tr");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "td", 41);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "td", 41);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "td", 41);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var item_r16 = ctx.$implicit;

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](item_r16.nombre);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](item_r16.telefono);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](item_r16.extencion);
        }
      }

      function DepartamentoComponent_app_add_departamento_13_Template(rf, ctx) {
        if (rf & 1) {
          var _r19 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "app-add-departamento", 47);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("goBack", function DepartamentoComponent_app_add_departamento_13_Template_app_add_departamento_goBack_0_listener($event) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r19);

            var ctx_r18 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

            return ctx_r18.reverse($event);
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      function DepartamentoComponent_app_edit_departamento_14_Template(rf, ctx) {
        if (rf & 1) {
          var _r21 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "app-edit-departamento", 48);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("goBack", function DepartamentoComponent_app_edit_departamento_14_Template_app_edit_departamento_goBack_0_listener($event) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r21);

            var ctx_r20 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

            return ctx_r20.reverse($event);
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var ctx_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("departamento", ctx_r3.departamento);
        }
      }

      var DepartamentoComponent = /*#__PURE__*/function () {
        function DepartamentoComponent(departamentoService, ngxService) {
          var _this14 = this;

          _classCallCheck(this, DepartamentoComponent);

          this.departamentoService = departamentoService;
          this.ngxService = ngxService;
          this.departamentos = [];
          this.destroySubscription$ = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
          this.createDepartamento = false;
          this.newDepartamento = false;
          this.editDepartamento = false;
          this.listDepartamento = true; //cargar la data inicial del componente del departamento

          this.complete = function () {
            _this14.createDepartamento = true;

            _this14.departamentoService.getAllDepartamentos(null, null, _this14);
          };

          this.exportExcel = function () {
            var idTable = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
            var nameFile = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
            var idTableHTML = idTable !== null ? idTable : 'excel-table';
            var element = document.getElementById(idTableHTML);
            var ws = xlsx__WEBPACK_IMPORTED_MODULE_4__["utils"].table_to_sheet(element, {
              raw: true
            });
            /* generate workbook and add the worksheet */

            var wb = xlsx__WEBPACK_IMPORTED_MODULE_4__["utils"].book_new();
            xlsx__WEBPACK_IMPORTED_MODULE_4__["utils"].book_append_sheet(wb, ws, 'Sheet1');
            /* save to file */

            var nameFileEXCEL = nameFile !== null ? nameFile : 'Reporte';
            xlsx__WEBPACK_IMPORTED_MODULE_4__["writeFile"](wb, nameFileEXCEL + '.xlsx');
          };
        }

        _createClass(DepartamentoComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            this.complete();
          } //activar el componente para agregar el departamento

        }, {
          key: "addDepartamento",
          value: function addDepartamento() {
            this.listDepartamento = false;
            this.newDepartamento = true;
          } //retorna de guardar oculpta el formulario y actualiza la data

        }, {
          key: "reverse",
          value: function reverse(back) {
            if (back) {
              this.newDepartamento = false;
              this.editDepartamento = false;
              this.listDepartamento = true;
              this.complete();
            }
          } //eliminar departamento por id

        }, {
          key: "eliminardepartamento",
          value: function eliminardepartamento(idDepartamento) {
            this.departamentoService.deleteDepartamentoById(idDepartamento, this);
          } //editar departamento

        }, {
          key: "editdepartamento",
          value: function editdepartamento(departamento) {
            this.departamento = departamento;
            this.listDepartamento = false;
            this.editDepartamento = true;
          }
        }, {
          key: "ngOnDestroy",
          value: function ngOnDestroy() {
            this.destroySubscription$.next(); // trigger the unsubscribe

            this.destroySubscription$.complete(); // finalize & clean up the subject stream
          }
        }]);

        return DepartamentoComponent;
      }();

      DepartamentoComponent.ɵfac = function DepartamentoComponent_Factory(t) {
        return new (t || DepartamentoComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_services_workflow_departamento_departamento_service__WEBPACK_IMPORTED_MODULE_1__["DepartamentoService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](ngx_ui_loader__WEBPACK_IMPORTED_MODULE_3__["NgxUiLoaderService"]));
      };

      DepartamentoComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
        type: DepartamentoComponent,
        selectors: [["app-departamento"]],
        decls: 15,
        vars: 4,
        consts: [["class", "emision-table", "style", "width: 100%; height: 100%; margin-top: 100px", 4, "ngIf"], [2, "display", "none"], ["aria-describedby", "tabla", "id", "excel-table"], ["id", "nump"], ["id", "ced"], ["id", "marca"], [4, "ngFor", "ngForOf"], [3, "goBack", 4, "ngIf"], [3, "departamento", "goBack", 4, "ngIf"], [1, "emision-table", 2, "width", "100%", "height", "100%", "margin-top", "100px"], [1, "card"], [1, "card-header"], [3, "loaderId"], [1, "row", "align-items-center", 2, "justify-content", "space-between"], [1, "col-md-3", "col-12"], [1, "col-md-auto", "col-12", "d-flex", "justify-content-center"], [1, "dropdown", "mt-4"], ["aria-expanded", "false", "aria-haspopup", "true", "data-toggle", "dropdown", "id", "btn-closure-dropdownMenuExport", "role", "button", 1, "btn", "btn-outline-green-ref", "width-fix", "dropleft"], [1, "texbtn"], ["aria-labelledby", "navbarDropdownMenuLink", 1, "dropdown-menu", "dropdown-table", "dropdown-menu-right"], ["id", "btn-as-export-to", 1, "dropdown-item", 3, "click"], [1, "text-right"], ["class", "btn btn-lg btn-green", "id", "btnAddBrand-vehicleComponent", 3, "click", 4, "ngIf"], [1, "card-body", "table-strech"], ["class", "no-contracts", 4, "ngIf"], ["class", "table-responsive", 4, "ngIf"], ["id", "btnAddBrand-vehicleComponent", 1, "btn", "btn-lg", "btn-green", 3, "click"], [1, "fas", "fa-plus"], [1, "no-contracts"], [1, "container"], [1, "row", "justify-content-center"], [1, "col-4", "text-center", "center-data-missing"], [1, "no-contracts-text", "pt-4"], [1, "table-responsive"], ["aria-describedby", " tabla de departamentos", 1, "table"], ["id", "nunoperacion"], [1, "row"], [1, "col-auto", "head-title"], [1, "col-auto", "separator-title"], ["id", "fechacreada"], [1, "col-auto", "mr-auto", "head-title"], [1, "table-spacing"], [1, "form-group", 2, "text-align", "left"], [2, "display", "inline-block"], [1, "btn", "icon-field", 3, "click"], [1, "fas", "fa-minus-circle", "fa-lg", "font-color-error"], [1, "fas", "fa-history", "fa-lg", "font-color-changed"], [3, "goBack"], [3, "departamento", "goBack"]],
        template: function DepartamentoComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](0, DepartamentoComponent_div_0_Template, 22, 4, "div", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "table", 2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "thead");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "tr");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "th", 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6, "NOMBRE");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "th", 4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, "TEL\xC9FONO");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "th", 5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10, "EXTENCI\xF3N");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "tbody");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](12, DepartamentoComponent_tr_12_Template, 7, 3, "tr", 6);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](13, DepartamentoComponent_app_add_departamento_13_Template, 1, 0, "app-add-departamento", 7);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](14, DepartamentoComponent_app_edit_departamento_14_Template, 1, 1, "app-edit-departamento", 8);
          }

          if (rf & 2) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.listDepartamento);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](12);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx.departamentos);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.newDepartamento);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.editDepartamento);
          }
        },
        directives: [_angular_common__WEBPACK_IMPORTED_MODULE_5__["NgIf"], _angular_common__WEBPACK_IMPORTED_MODULE_5__["NgForOf"], ngx_ui_loader__WEBPACK_IMPORTED_MODULE_3__["ɵb"], _add_departamento_add_departamento_component__WEBPACK_IMPORTED_MODULE_6__["AddDepartamentoComponent"], _edit_departamento_edit_departamento_component__WEBPACK_IMPORTED_MODULE_7__["EditDepartamentoComponent"]],
        styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJkZXBhcnRhbWVudG8uY29tcG9uZW50LnNjc3MifQ== */"]
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](DepartamentoComponent, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
          args: [{
            selector: 'app-departamento',
            templateUrl: './departamento.component.html',
            styleUrls: ['./departamento.component.scss']
          }]
        }], function () {
          return [{
            type: _services_workflow_departamento_departamento_service__WEBPACK_IMPORTED_MODULE_1__["DepartamentoService"]
          }, {
            type: ngx_ui_loader__WEBPACK_IMPORTED_MODULE_3__["NgxUiLoaderService"]
          }];
        }, null);
      })();
      /***/

    },

    /***/
    "crnd":
    /*!**********************************************************!*\
      !*** ./src/$$_lazy_route_resource lazy namespace object ***!
      \**********************************************************/

    /*! no static exports found */

    /***/
    function crnd(module, exports) {
      function webpackEmptyAsyncContext(req) {
        // Here Promise.resolve().then() is used instead of new Promise() to prevent
        // uncaught exception popping up in devtools
        return Promise.resolve().then(function () {
          var e = new Error("Cannot find module '" + req + "'");
          e.code = 'MODULE_NOT_FOUND';
          throw e;
        });
      }

      webpackEmptyAsyncContext.keys = function () {
        return [];
      };

      webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
      module.exports = webpackEmptyAsyncContext;
      webpackEmptyAsyncContext.id = "crnd";
      /***/
    },

    /***/
    "dSvv":
    /*!*************************************************************************************************!*\
      !*** ./src/app/admin/components/workflow/supervisor/add-supervisor/add-supervisor.component.ts ***!
      \*************************************************************************************************/

    /*! exports provided: AddSupervisorComponent */

    /***/
    function dSvv(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AddSupervisorComponent", function () {
        return AddSupervisorComponent;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/forms */
      "3Pt+");
      /* harmony import */


      var ngx_ui_loader__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ngx-ui-loader */
      "gren");
      /* harmony import */


      var _shared_classes_Helpers__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ../../../../../shared/classes/Helpers */
      "sFNd");
      /* harmony import */


      var _services_workflow_supervisor_supervisor_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ../../../../services/workflow/supervisor/supervisor.service */
      "Itk3");
      /* harmony import */


      var rxjs__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! rxjs */
      "qCKp");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");

      function AddSupervisorComponent_div_23_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 26);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", ctx_r0.error.nombre.msg, " ");
        }
      }

      function AddSupervisorComponent_div_24_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 27);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "em", 28);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      function AddSupervisorComponent_div_25_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 27);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "em", 29);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      function AddSupervisorComponent_div_33_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 26);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var ctx_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", ctx_r3.error.apellido.msg, " ");
        }
      }

      function AddSupervisorComponent_div_34_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 27);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "em", 28);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      function AddSupervisorComponent_div_35_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 27);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "em", 29);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      function AddSupervisorComponent_div_43_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 26);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var ctx_r6 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", ctx_r6.error.correo.msg, " ");
        }
      }

      function AddSupervisorComponent_div_44_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 27);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "em", 28);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      function AddSupervisorComponent_div_45_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 27);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "em", 29);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      var _c0 = function _c0(a0, a1) {
        return {
          "is-invalid": a0,
          "is-valid": a1
        };
      };

      var AddSupervisorComponent = /*#__PURE__*/function () {
        function AddSupervisorComponent(formBuilder, supervisorService, helper, ngxService) {
          var _this15 = this;

          _classCallCheck(this, AddSupervisorComponent);

          this.formBuilder = formBuilder;
          this.supervisorService = supervisorService;
          this.helper = helper;
          this.ngxService = ngxService;
          this.goBack = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
          this.destroySubscription$ = new rxjs__WEBPACK_IMPORTED_MODULE_5__["Subject"](); //iniciar los errores y las validaciones

          this.errorInit = function () {
            _this15.error = {
              nombre: {
                error: false,
                change: false,
                msg: ''
              },
              apellido: {
                error: false,
                change: false,
                msg: ''
              },
              correo: {
                error: false,
                change: false,
                msg: ''
              }
            };
          }; //enviar data al backend


          this.action = function () {
            _this15.ngxService.start();

            var form = _this15.form.value;
            var data = {
              nombre: form.nombre,
              apellido: form.apellido,
              correo: form.correo
            };

            _this15.supervisorService.addSupervisor(data, _this15);
          };
        }

        _createClass(AddSupervisorComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            this.errorInit(); //validaciones del formulario

            this.form = this.formBuilder.group({
              nombre: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', {
                validators: [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]
              }),
              apellido: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', {
                validators: [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]
              }),
              correo: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', {
                validators: [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]
              })
            });
          } //regresar al listado de supervisores

        }, {
          key: "reversePage",
          value: function reversePage() {
            this.goBack.emit(true);
          }
        }, {
          key: "ngOnDestroy",
          value: function ngOnDestroy() {
            this.destroySubscription$.next(); // trigger the unsubscribe

            this.destroySubscription$.complete(); // finalize & clean up the subject stream
          }
        }]);

        return AddSupervisorComponent;
      }();

      AddSupervisorComponent.ɵfac = function AddSupervisorComponent_Factory(t) {
        return new (t || AddSupervisorComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_services_workflow_supervisor_supervisor_service__WEBPACK_IMPORTED_MODULE_4__["SupervisorService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_shared_classes_Helpers__WEBPACK_IMPORTED_MODULE_3__["Helpers"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](ngx_ui_loader__WEBPACK_IMPORTED_MODULE_2__["NgxUiLoaderService"]));
      };

      AddSupervisorComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
        type: AddSupervisorComponent,
        selectors: [["app-add-supervisor"]],
        outputs: {
          goBack: "goBack"
        },
        decls: 50,
        vars: 23,
        consts: [[1, "row", "wizard-title", 2, "width", "100%", "height", "100%", "margin-top", "100px"], [1, "col-11", "mr-auto", "wizard-fonts"], [1, "required-fields"], [1, "col-1", "align-self-end"], [1, "btn", "btn-clear", 3, "click"], [1, "fas", "fa-angle-left"], [1, "row", "justify-content-center"], [1, "card", "fixed-width"], [1, "card-body"], ["autocomplete", "off", 3, "formGroup"], [1, "row"], [1, "col-4"], [1, "form-group"], ["for", "brandname-addSupervisorComponent"], [1, "fiel-validate"], [1, "field"], ["formControlName", "nombre", "id", "nombre-addSupervisorComponent", "placeholder", "Nombre del supervisor", "type", "text", 1, "form-control", "form-input", "capitalize", 3, "ngClass", "blur", "input"], ["class", "invalid-feedback font-color-error", 4, "ngIf"], ["class", "icon-field", 4, "ngIf"], ["for", "apellido-addSupervisorComponent"], ["formControlName", "apellido", "id", "apellido-addSupervisorComponent", "placeholder", "apellido del supervisor", "type", "text", 1, "form-control", "form-input", "capitalize", 3, "ngClass", "blur", "input"], ["for", "correo-addSupervisorComponent"], ["formControlName", "correo", "id", "correo-addSupervisorComponent", "placeholder", "Correo", "type", "text", 1, "form-control", "form-input", "capitalize", 3, "ngClass", "blur", "input"], [1, "row", "push-green"], [1, "col-12", "text-right"], ["data-target", "#confirmationModal-addVehicleComponent", "id", "confirmationModalBtn-addVehicleComponent", 1, "btn", "btn-save", 3, "disabled", "click"], [1, "invalid-feedback", "font-color-error"], [1, "icon-field"], [1, "fas", "fa-exclamation-circle", "fa-lg", "font-color-error"], [1, "fas", "fa-check-circle", "fa-lg", "font-color-success"]],
        template: function AddSupervisorComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "h4");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3, "Agrega un nuevo supervisor");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "small", 2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "* Campos requeridos");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "button", 4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function AddSupervisorComponent_Template_button_click_7_listener() {
              return ctx.reversePage();
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](8, "em", 5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](9, " \xA0 Regresar ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](10, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "div", 6);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "div", 7);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "div", 8);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "form", 9);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "div", 10);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "div", 11);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "div", 12);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "label", 13);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](19, "Nombre*");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "div", 14);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "div", 15);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "input", 16);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("blur", function AddSupervisorComponent_Template_input_blur_22_listener() {
              return ctx.helper.validInput(ctx.error, "nombre", ctx.form);
            })("input", function AddSupervisorComponent_Template_input_input_22_listener() {
              return ctx.helper.validInput(ctx.error, "nombre", ctx.form);
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](23, AddSupervisorComponent_div_23_Template, 2, 1, "div", 17);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](24, AddSupervisorComponent_div_24_Template, 2, 0, "div", 18);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](25, AddSupervisorComponent_div_25_Template, 2, 0, "div", 18);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](26, "div", 11);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "div", 12);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "label", 19);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](29, "Apellido *");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](30, "div", 14);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](31, "div", 15);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](32, "input", 20);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("blur", function AddSupervisorComponent_Template_input_blur_32_listener() {
              return ctx.helper.validInput(ctx.error, "apellido", ctx.form);
            })("input", function AddSupervisorComponent_Template_input_input_32_listener() {
              return ctx.helper.validInput(ctx.error, "apellido", ctx.form);
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](33, AddSupervisorComponent_div_33_Template, 2, 1, "div", 17);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](34, AddSupervisorComponent_div_34_Template, 2, 0, "div", 18);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](35, AddSupervisorComponent_div_35_Template, 2, 0, "div", 18);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](36, "div", 11);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](37, "div", 12);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](38, "label", 21);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](39, "Correo *");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](40, "div", 14);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](41, "div", 15);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](42, "input", 22);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("blur", function AddSupervisorComponent_Template_input_blur_42_listener() {
              return ctx.helper.validInput(ctx.error, "correo", ctx.form);
            })("input", function AddSupervisorComponent_Template_input_input_42_listener() {
              return ctx.helper.validInput(ctx.error, "correo", ctx.form);
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](43, AddSupervisorComponent_div_43_Template, 2, 1, "div", 17);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](44, AddSupervisorComponent_div_44_Template, 2, 0, "div", 18);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](45, AddSupervisorComponent_div_45_Template, 2, 0, "div", 18);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](46, "div", 23);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](47, "div", 24);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](48, "button", 25);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function AddSupervisorComponent_Template_button_click_48_listener() {
              return ctx.form.valid && ctx.action();
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](49, " Guardar ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
          }

          if (rf & 2) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](14);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("formGroup", ctx.form);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](8);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction2"](14, _c0, ctx.error.nombre.error, !ctx.error.nombre.error && ctx.error.nombre.change));

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.form.get("nombre").errors == null ? null : ctx.form.get("nombre").errors.required);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.error.nombre.error && ctx.error.nombre.change);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx.error.nombre.error && ctx.error.nombre.change);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction2"](17, _c0, ctx.error.apellido.error, !ctx.error.apellido.error && ctx.error.apellido.change));

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.form.get("apellido").errors == null ? null : ctx.form.get("apellido").errors.required);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.error.apellido.error && ctx.error.apellido.change);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx.error.apellido.error && ctx.error.apellido.change);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction2"](20, _c0, ctx.error.correo.error, !ctx.error.correo.error && ctx.error.correo.change));

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.form.get("correo").errors == null ? null : ctx.form.get("correo").errors.required);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.error.correo.error && ctx.error.correo.change);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx.error.correo.error && ctx.error.correo.change);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("disabled", !ctx.form.valid);
          }
        },
        directives: [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["ɵangular_packages_forms_forms_y"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NgControlStatusGroup"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroupDirective"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["DefaultValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControlName"], _angular_common__WEBPACK_IMPORTED_MODULE_6__["NgClass"], _angular_common__WEBPACK_IMPORTED_MODULE_6__["NgIf"]],
        styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJhZGQtc3VwZXJ2aXNvci5jb21wb25lbnQuc2NzcyJ9 */"]
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](AddSupervisorComponent, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
          args: [{
            selector: 'app-add-supervisor',
            templateUrl: './add-supervisor.component.html',
            styleUrls: ['./add-supervisor.component.scss']
          }]
        }], function () {
          return [{
            type: _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"]
          }, {
            type: _services_workflow_supervisor_supervisor_service__WEBPACK_IMPORTED_MODULE_4__["SupervisorService"]
          }, {
            type: _shared_classes_Helpers__WEBPACK_IMPORTED_MODULE_3__["Helpers"]
          }, {
            type: ngx_ui_loader__WEBPACK_IMPORTED_MODULE_2__["NgxUiLoaderService"]
          }];
        }, {
          goBack: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
          }]
        });
      })();
      /***/

    },

    /***/
    "duh5":
    /*!********************************************!*\
      !*** ./src/app/shared/shared.component.ts ***!
      \********************************************/

    /*! exports provided: SharedComponent */

    /***/
    function duh5(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "SharedComponent", function () {
        return SharedComponent;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");

      var SharedComponent = /*#__PURE__*/function () {
        function SharedComponent() {
          _classCallCheck(this, SharedComponent);
        }

        _createClass(SharedComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }]);

        return SharedComponent;
      }();

      SharedComponent.ɵfac = function SharedComponent_Factory(t) {
        return new (t || SharedComponent)();
      };

      SharedComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
        type: SharedComponent,
        selectors: [["app-shared"]],
        decls: 2,
        vars: 0,
        template: function SharedComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "p");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, " shared works!\n");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
          }
        },
        styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzaGFyZWQuY29tcG9uZW50LnNjc3MifQ== */"]
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](SharedComponent, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
          args: [{
            selector: 'app-shared',
            templateUrl: './shared.component.html',
            styleUrls: ['./shared.component.scss']
          }]
        }], function () {
          return [];
        }, null);
      })();
      /***/

    },

    /***/
    "fFVl":
    /*!******************************************************************!*\
      !*** ./src/app/admin/components/auth/signin/signin.component.ts ***!
      \******************************************************************/

    /*! exports provided: SigninComponent */

    /***/
    function fFVl(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "SigninComponent", function () {
        return SigninComponent;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var rxjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! rxjs */
      "qCKp");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/forms */
      "3Pt+");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var _services_Auth_signin_signin_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ../../../services/Auth/signin/signin.service */
      "ikX8");
      /* harmony import */


      var _shared_classes__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ../../../../shared/classes */
      "gtvE");
      /* harmony import */


      var ngx_ui_loader__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ngx-ui-loader */
      "gren");
      /* harmony import */


      var _services_workflow_dashboard_dashboard_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! ../../../services/workflow/dashboard/dashboard.service */
      "118T");
      /* harmony import */


      var _environments_environment__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! ../../../../../environments/environment */
      "AytR");
      /* harmony import */


      var rxjs_operators__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
      /*! rxjs/operators */
      "kU1M");

      var SigninComponent = /*#__PURE__*/function () {
        function SigninComponent(formBuilder, signIn, notification, localStorageCommon, router, ngxService, helper, dashboardService) {
          var _this16 = this;

          _classCallCheck(this, SigninComponent);

          this.formBuilder = formBuilder;
          this.signIn = signIn;
          this.notification = notification;
          this.localStorageCommon = localStorageCommon;
          this.router = router;
          this.ngxService = ngxService;
          this.helper = helper;
          this.dashboardService = dashboardService;
          this.logoSrc = _shared_classes__WEBPACK_IMPORTED_MODULE_5__["ASSET_SRC"].loginLogoVertical;
          this.returnButton = _shared_classes__WEBPACK_IMPORTED_MODULE_5__["ASSET_SRC"].loginArrowBack;
          this.rememberToggle = false;
          this.scoreRange = [];
          this.renderSignin = false;
          this.bannerEnv = '';
          this.destroySubscription$ = new rxjs__WEBPACK_IMPORTED_MODULE_1__["Subject"]();

          this.errorInit = function () {
            _this16.error = {
              email: {
                error: false,
                change: false,
                msg: ''
              },
              password: {
                error: false,
                change: false,
                msg: ''
              }
            };
          };

          this.action = function () {
            _this16.ngxService.start();

            var data = {
              dt1: _this16.form.value.email,
              dt2: _this16.form.value.password
            };

            _this16.signIn.post(data).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_9__["takeUntil"])(_this16.destroySubscription$)).subscribe(function (response) {
              console.log('aaaaxx');
              console.log(response);

              if (response.success) {
                _this16.localStorageCommon.store('auth_token', response.token, 'normal');

                _this16.localStorageCommon.store('auth_user', response.user, 'json');

                _this16.localStorageCommon.store('auth_perm', response.permissions, 'json');

                _this16.router.navigate(['/admin/workflow/dashboard'])["catch"](function (e) {
                  if (e) {
                    _this16.localStorageCommon.destroy('auth_user');

                    _this16.router.navigate(['/'])["catch"](function (res) {});
                  }
                });
              } else {
                _this16.ngxService.stop();
              }
            }, function (error) {
              console.log('xxxxxxxxxxxxxxxxx');
              console.log(error);

              _this16.ngxService.stop();
            });
          };
        }

        _createClass(SigninComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            var _this17 = this;

            $(function () {
              $('#launcher-frame').hide();
            });
            this.bannerEnv = _environments_environment__WEBPACK_IMPORTED_MODULE_8__["environment"].banner;
            this.loggedUser = this.localStorageCommon.get('auth_user', 'json');
            var token = this.localStorageCommon.get('auth_token', 'normal');

            if (token) {
              var isValid = !this.helper.isExpired(token);

              if (isValid) {
                this.router.navigate(['/admin/workflow/dashboard'])["catch"](function (e) {
                  console.error(e); // throw new Error(e);

                  if (e) {
                    _this17.localStorageCommon.destroy('auth_user');

                    _this17.router.navigate(['/'])["catch"](function (res) {});
                  }
                });
              } else {
                this.renderSignin = true;
              }
            } else {
              this.renderSignin = true;
            }

            this.errorInit();
            this.form = this.formBuilder.group({
              email: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', {
                validators: [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].email, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]
              }),
              password: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
              rememberLogin: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](false)
            });
          }
        }, {
          key: "ngOnDestroy",
          value: function ngOnDestroy() {
            this.destroySubscription$.next(); // trigger the unsubscribe

            this.destroySubscription$.complete(); // finalize & clean up the subject stream
          }
        }]);

        return SigninComponent;
      }();

      SigninComponent.ɵfac = function SigninComponent_Factory(t) {
        return new (t || SigninComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_services_Auth_signin_signin_service__WEBPACK_IMPORTED_MODULE_4__["SigninService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_shared_classes__WEBPACK_IMPORTED_MODULE_5__["NotificationAction"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_shared_classes__WEBPACK_IMPORTED_MODULE_5__["LocalStorageCommon"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](ngx_ui_loader__WEBPACK_IMPORTED_MODULE_6__["NgxUiLoaderService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_shared_classes__WEBPACK_IMPORTED_MODULE_5__["Helpers"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_services_workflow_dashboard_dashboard_service__WEBPACK_IMPORTED_MODULE_7__["DashboardService"]));
      };

      SigninComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
        type: SigninComponent,
        selectors: [["app-signin"]],
        decls: 24,
        vars: 3,
        consts: [[1, "main"], [1, "col-6", "col-sm-12"], [1, "login-form"], ["alt", "Ejemplo", "width", "100px", "height", "100px", 1, "login-logo", "mx-auto", "d-block", 3, "src"], [1, "text-center", "banner"], [1, "login-form-group", 3, "formGroup"], [1, "form-group"], ["for", "inputEmail-signinComponent"], [1, "fiel-validate"], [1, "field"], ["for", "inputEmail-signinComponent", 2, "display", "none"], ["autofocus", "", "formControlName", "email", "id", "inputEmail-signinComponent", "placeholder", "alguien@empresa.com", "required", "", "type", "email", 1, "form-control", "login-input"], ["for", "inputPassword-signinComponent", 2, "display", "none"], ["formControlName", "password", "id", "inputPassword-signinComponent", "placeholder", "Palabra Clave", "required", "", "type", "password", 1, "form-control", "login-input", 3, "keydown.enter"], ["id", "loginBtn-signinComponent", "type", "button", 1, "btn", "btn-lg", "btn-primary", "btn-block", 3, "click"], [1, "sidenav"]],
        template: function SigninComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "ngx-ui-loader");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](4, "img", 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "h2", 4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "form", 5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "div", 6);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "label", 7);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10, "Email");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "div", 8);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "div", 9);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](13, "label", 10);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](14, "label", 10);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](15, "input", 11);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "div", 6);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "div", 8);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](18, "label", 12);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "div", 9);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "input", 13);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("keydown.enter", function SigninComponent_Template_input_keydown_enter_20_listener() {
              return ctx.action();
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "button", 14);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function SigninComponent_Template_button_click_21_listener() {
              return ctx.action();
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](22, " Iniciar Sesi\xF3n ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](23, "div", 15);
          }

          if (rf & 2) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate"]("src", ctx.logoSrc, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsanitizeUrl"]);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx.bannerEnv);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("formGroup", ctx.form);
          }
        },
        directives: [ngx_ui_loader__WEBPACK_IMPORTED_MODULE_6__["ɵb"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["ɵangular_packages_forms_forms_y"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["NgControlStatusGroup"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroupDirective"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["DefaultValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControlName"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["RequiredValidator"]],
        styles: [".banner[_ngcontent-%COMP%] {\n  font-family: Roboto-Bold, sans-serif;\n  color: #0030f0;\n  padding-top: 5%;\n  padding-bottom: 5%;\n}\n\n.return-button[_ngcontent-%COMP%] {\n  display: flex;\n  flex-direction: row;\n  align-items: center;\n  justify-content: center;\n}\n\n.return-button[_ngcontent-%COMP%]   div[_ngcontent-%COMP%] {\n  width: 21%;\n}\n\n@media screen and (max-width: 1400px) {\n  .return-button[_ngcontent-%COMP%]   div[_ngcontent-%COMP%] {\n    width: 22%;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXC4uXFwuLlxcLi5cXC4uXFxhdXRoLmNvbXBvbmVudC5zY3NzIiwiLi5cXC4uXFwuLlxcLi5cXC4uXFwuLlxcLi5cXC4uXFwuLlxcLi5cXC4uXFxzY3NzXFxjb2xvcnMuc2NzcyIsIi4uXFwuLlxcLi5cXC4uXFwuLlxcLi5cXHNpZ25pbi5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFFQTtFQUNFLG9DQUFBO0VBQ0EsY0NIYztFRElkLGVBQUE7RUFDQSxrQkFBQTtBRURGOztBQUZBO0VBQ0UsYUFBQTtFQUNBLG1CQUFBO0VBQ0EsbUJBQUE7RUFDQSx1QkFBQTtBQUtGOztBQUhFO0VBQ0UsVUFBQTtBQUtKOztBQURBO0VBQ0U7SUFDRSxVQUFBO0VBSUY7QUFDRiIsImZpbGUiOiJzaWduaW4uY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJAaW1wb3J0IFwiLi4vLi4vLi4vLi4vc2Nzcy9jb2xvcnNcIjtcblxuLmJhbm5lciB7XG4gIGZvbnQtZmFtaWx5OiBSb2JvdG8tQm9sZCwgc2Fucy1zZXJpZjtcbiAgY29sb3I6ICRwcmltYXJ5LWFsZXJ0O1xuICBwYWRkaW5nLXRvcDogNSU7XG4gIHBhZGRpbmctYm90dG9tOiA1JTtcbn1cbiIsIi8vIENvbG9yIHBhbGV0dGUgXG4kcHJpbWFyeS1hbGVydDogIzAwMzBmMDtcbiRyZWdpc3RyeS1hbGVydDogIzcxYzAxNTtcbiRhdHRlbnRpb24tYWxlcnQ6ICNBN0E5RTY7XG4kd2FybmluZy1hbGVydDogI2Y5YzAwNDtcbiRlcnJvci1hbGVydDogI2ZjNTY2MztcbiRjb25maXJtYXRpb24tYWxlcnQtYm9yZGVyOiAjNmM1YTk3O1xuJGNvbmZpcm1hdGlvbi1hbGVydC1iYWNrZ3JvdW5kOiAjZTFkZWVhO1xuJGNvbmZpcm1hdGlvbi1idXR0b24tYmFja2dyb3VuZDogIzdiNmNhMjtcbiRFcnJvci12YWxpZGF0aW9uLWljb246ICNlNDVlNWU7XG4kRXJyb3ItdmFsaWRhdGlvbjogI2YxNTU2MTtcbiRzZWxlY3RlZC1pY29uOiAjNDYzZDZlO1xuJGNvbmZpcm1hdGlvbi1idXR0b24tYWx0ZXJuYXRpdmU6ICM3ODZkYTQ7XG4kd2hpdGU6ICNmZmZmZmY7XG4kcHJpbWFyeS1idXR0b246ICMzZDdkNDE7XG4kdGVydGlhcnktYnV0dG9uOiAjZDdlNWRhO1xuJHN1Y2Nlc3MtdmFsaWRhdGlvbi1ib3JkZXI6ICM4MmJlMDA7XG4kYmFja2dyb3VuZDogI2ZjZmJmZjtcbiRjYXJkLWJvcmRlcjogI2VmZWNmYTtcbiRhbG1vc3QtYmxhY2s6ICMwYTBhMGI7XG4kbGFyZ2UtYnV0dG9uOiAjZGJkOGUzO1xuJGJvcmRlci1jb2xvcjogI2Q5ZDlkOTtcbiRuYXZiYXItYm9yZGVyOiAjZjFmMWYxO1xuJHNtYWxsLXRleHQ6ICM2YjY0OGI7XG4kbGVhZDogIzU5NTA3ZDtcbi8vIEJhc2UgY29sb3JzXG4kcHJpbWFyeTogJHByaW1hcnktYnV0dG9uO1xuJHNlY29uZGFyeTogJHNlbGVjdGVkLWljb247XG4kc3VjY2VzczogJHJlZ2lzdHJ5LWFsZXJ0O1xuJGluZm86ICRhdHRlbnRpb24tYWxlcnQ7XG4kd2FybmluZzogJHdhcm5pbmctYWxlcnQ7XG4kZGFuZ2VyOiAkZXJyb3ItYWxlcnQ7XG4kZGFyazogJGFsbW9zdC1ibGFjaztcbiR0b3VjaGVkOiAjMzM5NGY1O1xuLy8gQm9keVxuLy9cbi8vIFNldHRpbmdzIGZvciB0aGUgYDxib2R5PmAgZWxlbWVudC5cbiRib2R5LWJnOiAjRkJGQ0ZGO1xuJGJvZHktY29sb3I6ICRhbG1vc3QtYmxhY2s7XG5cbi8vbm90aWZpY2FjaW9uZXNcbiRzdWNjZXNyZWZhY3RvOiAjYmViZWJlO1xuIiwiLmJhbm5lciB7XG4gIGZvbnQtZmFtaWx5OiBSb2JvdG8tQm9sZCwgc2Fucy1zZXJpZjtcbiAgY29sb3I6ICMwMDMwZjA7XG4gIHBhZGRpbmctdG9wOiA1JTtcbiAgcGFkZGluZy1ib3R0b206IDUlO1xufVxuXG4ucmV0dXJuLWJ1dHRvbiB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xufVxuLnJldHVybi1idXR0b24gZGl2IHtcbiAgd2lkdGg6IDIxJTtcbn1cblxuQG1lZGlhIHNjcmVlbiBhbmQgKG1heC13aWR0aDogMTQwMHB4KSB7XG4gIC5yZXR1cm4tYnV0dG9uIGRpdiB7XG4gICAgd2lkdGg6IDIyJTtcbiAgfVxufSJdfQ== */"]
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](SigninComponent, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
          args: [{
            selector: 'app-signin',
            templateUrl: './signin.component.html',
            styleUrls: ['./signin.component.scss']
          }]
        }], function () {
          return [{
            type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"]
          }, {
            type: _services_Auth_signin_signin_service__WEBPACK_IMPORTED_MODULE_4__["SigninService"]
          }, {
            type: _shared_classes__WEBPACK_IMPORTED_MODULE_5__["NotificationAction"]
          }, {
            type: _shared_classes__WEBPACK_IMPORTED_MODULE_5__["LocalStorageCommon"]
          }, {
            type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]
          }, {
            type: ngx_ui_loader__WEBPACK_IMPORTED_MODULE_6__["NgxUiLoaderService"]
          }, {
            type: _shared_classes__WEBPACK_IMPORTED_MODULE_5__["Helpers"]
          }, {
            type: _services_workflow_dashboard_dashboard_service__WEBPACK_IMPORTED_MODULE_7__["DashboardService"]
          }];
        }, null);
      })();
      /***/

    },

    /***/
    "gtvE":
    /*!*****************************************!*\
      !*** ./src/app/shared/classes/index.ts ***!
      \*****************************************/

    /*! exports provided: ChipHandler, regex, passwordValidator, samePasswordValidator, monetaryValidator, numericValidator, notZero, isIntegerValidator, isNumberValidator, feeValidator, paidFeeValidator, alphanumericValidator, nameValidator, fullNameValidator, lastnameValidator, secondLastnameValidator, cellphoneEcuValidator, telephoneEcuValidator, identifyIdECValidator, identifyRucECValidator, identifyPassECValidator, requiredArrayField, customValidatorHandler, customArrayValidatorHandle, REGEX_DICTIONARY, VALIDATOR_ENUM, ASSET_SRC, STORAGE_SLUGS, CONTRACT_UPDATE, GlobalErrorHandler, Helpers, LocalStorageCommon, NotificationAction, Services */

    /***/
    function gtvE(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony import */


      var _chipHandler__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! ./chipHandler */
      "uWDe");
      /* harmony reexport (safe) */


      __webpack_require__.d(__webpack_exports__, "ChipHandler", function () {
        return _chipHandler__WEBPACK_IMPORTED_MODULE_0__["ChipHandler"];
      });
      /* harmony import */


      var _CustomValidators__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ./CustomValidators */
      "vj/p");
      /* harmony reexport (safe) */


      __webpack_require__.d(__webpack_exports__, "regex", function () {
        return _CustomValidators__WEBPACK_IMPORTED_MODULE_1__["regex"];
      });
      /* harmony reexport (safe) */


      __webpack_require__.d(__webpack_exports__, "passwordValidator", function () {
        return _CustomValidators__WEBPACK_IMPORTED_MODULE_1__["passwordValidator"];
      });
      /* harmony reexport (safe) */


      __webpack_require__.d(__webpack_exports__, "samePasswordValidator", function () {
        return _CustomValidators__WEBPACK_IMPORTED_MODULE_1__["samePasswordValidator"];
      });
      /* harmony reexport (safe) */


      __webpack_require__.d(__webpack_exports__, "monetaryValidator", function () {
        return _CustomValidators__WEBPACK_IMPORTED_MODULE_1__["monetaryValidator"];
      });
      /* harmony reexport (safe) */


      __webpack_require__.d(__webpack_exports__, "numericValidator", function () {
        return _CustomValidators__WEBPACK_IMPORTED_MODULE_1__["numericValidator"];
      });
      /* harmony reexport (safe) */


      __webpack_require__.d(__webpack_exports__, "notZero", function () {
        return _CustomValidators__WEBPACK_IMPORTED_MODULE_1__["notZero"];
      });
      /* harmony reexport (safe) */


      __webpack_require__.d(__webpack_exports__, "isIntegerValidator", function () {
        return _CustomValidators__WEBPACK_IMPORTED_MODULE_1__["isIntegerValidator"];
      });
      /* harmony reexport (safe) */


      __webpack_require__.d(__webpack_exports__, "isNumberValidator", function () {
        return _CustomValidators__WEBPACK_IMPORTED_MODULE_1__["isNumberValidator"];
      });
      /* harmony reexport (safe) */


      __webpack_require__.d(__webpack_exports__, "feeValidator", function () {
        return _CustomValidators__WEBPACK_IMPORTED_MODULE_1__["feeValidator"];
      });
      /* harmony reexport (safe) */


      __webpack_require__.d(__webpack_exports__, "paidFeeValidator", function () {
        return _CustomValidators__WEBPACK_IMPORTED_MODULE_1__["paidFeeValidator"];
      });
      /* harmony reexport (safe) */


      __webpack_require__.d(__webpack_exports__, "alphanumericValidator", function () {
        return _CustomValidators__WEBPACK_IMPORTED_MODULE_1__["alphanumericValidator"];
      });
      /* harmony reexport (safe) */


      __webpack_require__.d(__webpack_exports__, "nameValidator", function () {
        return _CustomValidators__WEBPACK_IMPORTED_MODULE_1__["nameValidator"];
      });
      /* harmony reexport (safe) */


      __webpack_require__.d(__webpack_exports__, "fullNameValidator", function () {
        return _CustomValidators__WEBPACK_IMPORTED_MODULE_1__["fullNameValidator"];
      });
      /* harmony reexport (safe) */


      __webpack_require__.d(__webpack_exports__, "lastnameValidator", function () {
        return _CustomValidators__WEBPACK_IMPORTED_MODULE_1__["lastnameValidator"];
      });
      /* harmony reexport (safe) */


      __webpack_require__.d(__webpack_exports__, "secondLastnameValidator", function () {
        return _CustomValidators__WEBPACK_IMPORTED_MODULE_1__["secondLastnameValidator"];
      });
      /* harmony reexport (safe) */


      __webpack_require__.d(__webpack_exports__, "cellphoneEcuValidator", function () {
        return _CustomValidators__WEBPACK_IMPORTED_MODULE_1__["cellphoneEcuValidator"];
      });
      /* harmony reexport (safe) */


      __webpack_require__.d(__webpack_exports__, "telephoneEcuValidator", function () {
        return _CustomValidators__WEBPACK_IMPORTED_MODULE_1__["telephoneEcuValidator"];
      });
      /* harmony reexport (safe) */


      __webpack_require__.d(__webpack_exports__, "identifyIdECValidator", function () {
        return _CustomValidators__WEBPACK_IMPORTED_MODULE_1__["identifyIdECValidator"];
      });
      /* harmony reexport (safe) */


      __webpack_require__.d(__webpack_exports__, "identifyRucECValidator", function () {
        return _CustomValidators__WEBPACK_IMPORTED_MODULE_1__["identifyRucECValidator"];
      });
      /* harmony reexport (safe) */


      __webpack_require__.d(__webpack_exports__, "identifyPassECValidator", function () {
        return _CustomValidators__WEBPACK_IMPORTED_MODULE_1__["identifyPassECValidator"];
      });
      /* harmony reexport (safe) */


      __webpack_require__.d(__webpack_exports__, "requiredArrayField", function () {
        return _CustomValidators__WEBPACK_IMPORTED_MODULE_1__["requiredArrayField"];
      });
      /* harmony reexport (safe) */


      __webpack_require__.d(__webpack_exports__, "customValidatorHandler", function () {
        return _CustomValidators__WEBPACK_IMPORTED_MODULE_1__["customValidatorHandler"];
      });
      /* harmony reexport (safe) */


      __webpack_require__.d(__webpack_exports__, "customArrayValidatorHandle", function () {
        return _CustomValidators__WEBPACK_IMPORTED_MODULE_1__["customArrayValidatorHandle"];
      });
      /* harmony import */


      var _Enums__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./Enums */
      "xQgS");
      /* harmony reexport (safe) */


      __webpack_require__.d(__webpack_exports__, "REGEX_DICTIONARY", function () {
        return _Enums__WEBPACK_IMPORTED_MODULE_2__["REGEX_DICTIONARY"];
      });
      /* harmony reexport (safe) */


      __webpack_require__.d(__webpack_exports__, "VALIDATOR_ENUM", function () {
        return _Enums__WEBPACK_IMPORTED_MODULE_2__["VALIDATOR_ENUM"];
      });
      /* harmony reexport (safe) */


      __webpack_require__.d(__webpack_exports__, "ASSET_SRC", function () {
        return _Enums__WEBPACK_IMPORTED_MODULE_2__["ASSET_SRC"];
      });
      /* harmony reexport (safe) */


      __webpack_require__.d(__webpack_exports__, "STORAGE_SLUGS", function () {
        return _Enums__WEBPACK_IMPORTED_MODULE_2__["STORAGE_SLUGS"];
      });
      /* harmony reexport (safe) */


      __webpack_require__.d(__webpack_exports__, "CONTRACT_UPDATE", function () {
        return _Enums__WEBPACK_IMPORTED_MODULE_2__["CONTRACT_UPDATE"];
      });
      /* harmony import */


      var _GlobalErrorHandler__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./GlobalErrorHandler */
      "RbYx");
      /* harmony reexport (safe) */


      __webpack_require__.d(__webpack_exports__, "GlobalErrorHandler", function () {
        return _GlobalErrorHandler__WEBPACK_IMPORTED_MODULE_3__["GlobalErrorHandler"];
      });
      /* harmony import */


      var _Helpers__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ./Helpers */
      "sFNd");
      /* harmony reexport (safe) */


      __webpack_require__.d(__webpack_exports__, "Helpers", function () {
        return _Helpers__WEBPACK_IMPORTED_MODULE_4__["Helpers"];
      });
      /* harmony import */


      var _LocalStorageCommon__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./LocalStorageCommon */
      "hdVc");
      /* harmony reexport (safe) */


      __webpack_require__.d(__webpack_exports__, "LocalStorageCommon", function () {
        return _LocalStorageCommon__WEBPACK_IMPORTED_MODULE_5__["LocalStorageCommon"];
      });
      /* harmony import */


      var _NotificationAction__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./NotificationAction */
      "2fU6");
      /* harmony reexport (safe) */


      __webpack_require__.d(__webpack_exports__, "NotificationAction", function () {
        return _NotificationAction__WEBPACK_IMPORTED_MODULE_6__["NotificationAction"];
      });
      /* harmony import */


      var _Services__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! ./Services */
      "q4S7");
      /* harmony reexport (safe) */


      __webpack_require__.d(__webpack_exports__, "Services", function () {
        return _Services__WEBPACK_IMPORTED_MODULE_7__["Services"];
      });
      /***/

    },

    /***/
    "guoY":
    /*!*************************************************************************!*\
      !*** ./src/app/admin/components/workflow/workflow-routing.component.ts ***!
      \*************************************************************************/

    /*! exports provided: routesWorkflow, WorkflowRoutingComponent */

    /***/
    function guoY(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "routesWorkflow", function () {
        return routesWorkflow;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "WorkflowRoutingComponent", function () {
        return WorkflowRoutingComponent;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var _dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./dashboard/dashboard.component */
      "DDId");
      /* harmony import */


      var _departamento_departamento_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./departamento/departamento.component */
      "b/wG");
      /* harmony import */


      var _empleado_empleado_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ./empleado/empleado.component */
      "MStx");
      /* harmony import */


      var _supervisor_supervisor_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./supervisor/supervisor.component */
      "+2bK");
      /* harmony import */


      var _reportes_reportes_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./reportes/reportes.component */
      "W5i3");

      var routesWorkflow = [{
        path: '',
        component: _dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_2__["DashboardComponent"]
      }, {
        path: 'dashboard',
        component: _dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_2__["DashboardComponent"]
      }, {
        path: 'departamento',
        component: _departamento_departamento_component__WEBPACK_IMPORTED_MODULE_3__["DepartamentoComponent"]
      }, {
        path: 'empleado',
        component: _empleado_empleado_component__WEBPACK_IMPORTED_MODULE_4__["EmpleadoComponent"]
      }, {
        path: 'supervisor',
        component: _supervisor_supervisor_component__WEBPACK_IMPORTED_MODULE_5__["SupervisorComponent"]
      }, {
        path: 'reporte',
        component: _reportes_reportes_component__WEBPACK_IMPORTED_MODULE_6__["ReportesComponent"]
      }, {
        path: '**',
        redirectTo: '404'
      }];

      var WorkflowRoutingComponent = function WorkflowRoutingComponent() {
        _classCallCheck(this, WorkflowRoutingComponent);
      };

      WorkflowRoutingComponent.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({
        type: WorkflowRoutingComponent
      });
      WorkflowRoutingComponent.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({
        factory: function WorkflowRoutingComponent_Factory(t) {
          return new (t || WorkflowRoutingComponent)();
        },
        imports: [[_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routesWorkflow)], _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
      });

      (function () {
        (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](WorkflowRoutingComponent, {
          imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]],
          exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        });
      })();
      /*@__PURE__*/


      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](WorkflowRoutingComponent, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
          args: [{
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routesWorkflow)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
          }]
        }], null, null);
      })();
      /***/

    },

    /***/
    "hdVc":
    /*!******************************************************!*\
      !*** ./src/app/shared/classes/LocalStorageCommon.ts ***!
      \******************************************************/

    /*! exports provided: LocalStorageCommon */

    /***/
    function hdVc(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "LocalStorageCommon", function () {
        return LocalStorageCommon;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");

      var LocalStorageCommon = /*#__PURE__*/function () {
        function LocalStorageCommon() {
          _classCallCheck(this, LocalStorageCommon);
        }
        /**
         * Function that obtains the value of a specific key, in the specific format
         * @param key Name of the key
         * @param format Return format (normal, json)
         */


        _createClass(LocalStorageCommon, [{
          key: "get",
          value: function get(key, format) {
            var data = localStorage.getItem(key);

            switch (format) {
              case 'normal':
                {
                  return data;
                }

              case 'json':
                {
                  return data !== null ? JSON.parse(data) : null;
                }

              default:
                {
                  return data;
                }
            }
          }
          /**
           * Function to store a value to a specific key and to a specific format
           * @param key Name of the key
           * @param data Value to store
           * @param format Type of format (normal, json)
           */

        }, {
          key: "store",
          value: function store(key, data, format) {
            switch (format) {
              case 'normal':
                {
                  localStorage.setItem(key, data);
                  break;
                }

              case 'json':
                {
                  localStorage.setItem(key, JSON.stringify(data));
                  break;
                }

              default:
                {
                  localStorage.setItem(key, data);
                  break;
                }
            }
          }
          /**
           * function that eliminates a specific value by means of its key
           * @param key Name of the key
           */

        }, {
          key: "destroy",
          value: function destroy(key) {
            localStorage.removeItem(key);
          }
          /**
           * function that cleans all storage
           */

        }, {
          key: "clear",
          value: function clear() {
            localStorage.clear();
          }
        }]);

        return LocalStorageCommon;
      }();

      LocalStorageCommon.ɵfac = function LocalStorageCommon_Factory(t) {
        return new (t || LocalStorageCommon)();
      };

      LocalStorageCommon.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({
        token: LocalStorageCommon,
        factory: LocalStorageCommon.ɵfac,
        providedIn: 'root'
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](LocalStorageCommon, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
          args: [{
            providedIn: 'root'
          }]
        }], function () {
          return [];
        }, null);
      })();
      /***/

    },

    /***/
    "ikX8":
    /*!**************************************************************!*\
      !*** ./src/app/admin/services/Auth/signin/signin.service.ts ***!
      \**************************************************************/

    /*! exports provided: SigninService */

    /***/
    function ikX8(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "SigninService", function () {
        return SigninService;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var src_app_shared_classes__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! src/app/shared/classes */
      "gtvE");
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common/http */
      "tk/3");

      var SigninService = /*#__PURE__*/function (_src_app_shared_class2) {
        _inherits(SigninService, _src_app_shared_class2);

        var _super4 = _createSuper(SigninService);

        function SigninService(http) {
          _classCallCheck(this, SigninService);

          return _super4.call(this, 'LgnUsr/VmLgnUsr', http, true);
        }

        return SigninService;
      }(src_app_shared_classes__WEBPACK_IMPORTED_MODULE_1__["Services"]);

      SigninService.ɵfac = function SigninService_Factory(t) {
        return new (t || SigninService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]));
      };

      SigninService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({
        token: SigninService,
        factory: SigninService.ɵfac,
        providedIn: 'root'
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](SigninService, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
          args: [{
            providedIn: 'root'
          }]
        }], function () {
          return [{
            type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]
          }];
        }, null);
      })();
      /***/

    },

    /***/
    "jkDv":
    /*!***************************************!*\
      !*** ./src/app/admin/admin.module.ts ***!
      \***************************************/

    /*! exports provided: AdminModule */

    /***/
    function jkDv(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AdminModule", function () {
        return AdminModule;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common/http */
      "tk/3");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "3Pt+");
      /* harmony import */


      var _admin_routing_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ./admin-routing.module */
      "0Em7");
      /* harmony import */


      var _shared_shared_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ../shared/shared.module */
      "PCNd");
      /* harmony import */


      var _shared_interceptors_httpconfig_interceptor__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ../shared/interceptors/httpconfig.interceptor */
      "nEJ4");
      /* harmony import */


      var _ng_select_ng_select__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! @ng-select/ng-select */
      "ZOsW");
      /* harmony import */


      var angular_mydatepicker__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! angular-mydatepicker */
      "rXkI");
      /* harmony import */


      var ngx_pagination__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
      /*! ngx-pagination */
      "oOf3");
      /* harmony import */


      var ng2_charts__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
      /*! ng2-charts */
      "LPYB");
      /* harmony import */


      var ngx_ui_loader__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
      /*! ngx-ui-loader */
      "gren");
      /* harmony import */


      var ngx_color_picker__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(
      /*! ngx-color-picker */
      "R9Cn");
      /* harmony import */


      var _components__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(
      /*! ./components */
      "2Xez");
      /* harmony import */


      var _components_workflow_departamento_edit_departamento_edit_departamento_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(
      /*! ./components/workflow/departamento/edit-departamento/edit-departamento.component */
      "SNli");
      /* harmony import */


      var _components_workflow_supervisor_add_supervisor_add_supervisor_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(
      /*! ./components/workflow/supervisor/add-supervisor/add-supervisor.component */
      "dSvv");
      /* harmony import */


      var _components_workflow_supervisor_edit_supervisor_edit_supervisor_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(
      /*! ./components/workflow/supervisor/edit-supervisor/edit-supervisor.component */
      "182n");
      /* harmony import */


      var _components_workflow_empleado_add_empleado_add_empleado_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(
      /*! ./components/workflow/empleado/add-empleado/add-empleado.component */
      "My/+");
      /* harmony import */


      var _components_workflow_empleado_edit_empleado_edit_empleado_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(
      /*! ./components/workflow/empleado/edit-empleado/edit-empleado.component */
      "CLRV");

      var AdminModule = function AdminModule() {
        _classCallCheck(this, AdminModule);
      };

      AdminModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({
        type: AdminModule
      });
      AdminModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({
        factory: function AdminModule_Factory(t) {
          return new (t || AdminModule)();
        },
        providers: [{
          provide: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HTTP_INTERCEPTORS"],
          useClass: _shared_interceptors_httpconfig_interceptor__WEBPACK_IMPORTED_MODULE_6__["HttpConfigInterceptor"],
          multi: true
        }],
        imports: [[_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClientModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"], _shared_shared_module__WEBPACK_IMPORTED_MODULE_5__["SharedModule"], _admin_routing_module__WEBPACK_IMPORTED_MODULE_4__["AdminRoutingModule"], _ng_select_ng_select__WEBPACK_IMPORTED_MODULE_7__["NgSelectModule"], angular_mydatepicker__WEBPACK_IMPORTED_MODULE_8__["AngularMyDatePickerModule"], ngx_pagination__WEBPACK_IMPORTED_MODULE_9__["NgxPaginationModule"], ng2_charts__WEBPACK_IMPORTED_MODULE_10__["ChartsModule"], ngx_ui_loader__WEBPACK_IMPORTED_MODULE_11__["NgxUiLoaderModule"], ngx_color_picker__WEBPACK_IMPORTED_MODULE_12__["ColorPickerModule"]]]
      });

      (function () {
        (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](AdminModule, {
          declarations: [_components__WEBPACK_IMPORTED_MODULE_13__["AdminComponent"], _components__WEBPACK_IMPORTED_MODULE_13__["AuthComponent"], _components__WEBPACK_IMPORTED_MODULE_13__["SigninComponent"], _components__WEBPACK_IMPORTED_MODULE_13__["WorkflowComponent"], _components__WEBPACK_IMPORTED_MODULE_13__["DashboardComponent"], _components__WEBPACK_IMPORTED_MODULE_13__["DepartamentoComponent"], _components__WEBPACK_IMPORTED_MODULE_13__["AddDepartamentoComponent"], _components__WEBPACK_IMPORTED_MODULE_13__["EmpleadoComponent"], _components__WEBPACK_IMPORTED_MODULE_13__["SupervisorComponent"], _components__WEBPACK_IMPORTED_MODULE_13__["ReportesComponent"], _components_workflow_departamento_edit_departamento_edit_departamento_component__WEBPACK_IMPORTED_MODULE_14__["EditDepartamentoComponent"], _components_workflow_supervisor_add_supervisor_add_supervisor_component__WEBPACK_IMPORTED_MODULE_15__["AddSupervisorComponent"], _components_workflow_supervisor_edit_supervisor_edit_supervisor_component__WEBPACK_IMPORTED_MODULE_16__["EditSupervisorComponent"], _components_workflow_empleado_add_empleado_add_empleado_component__WEBPACK_IMPORTED_MODULE_17__["AddEmpleadoComponent"], _components_workflow_empleado_edit_empleado_edit_empleado_component__WEBPACK_IMPORTED_MODULE_18__["EditEmpleadoComponent"]],
          imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClientModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"], _shared_shared_module__WEBPACK_IMPORTED_MODULE_5__["SharedModule"], _admin_routing_module__WEBPACK_IMPORTED_MODULE_4__["AdminRoutingModule"], _ng_select_ng_select__WEBPACK_IMPORTED_MODULE_7__["NgSelectModule"], angular_mydatepicker__WEBPACK_IMPORTED_MODULE_8__["AngularMyDatePickerModule"], ngx_pagination__WEBPACK_IMPORTED_MODULE_9__["NgxPaginationModule"], ng2_charts__WEBPACK_IMPORTED_MODULE_10__["ChartsModule"], ngx_ui_loader__WEBPACK_IMPORTED_MODULE_11__["NgxUiLoaderModule"], ngx_color_picker__WEBPACK_IMPORTED_MODULE_12__["ColorPickerModule"]]
        });
      })();
      /*@__PURE__*/


      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](AdminModule, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
          args: [{
            declarations: [_components__WEBPACK_IMPORTED_MODULE_13__["AdminComponent"], _components__WEBPACK_IMPORTED_MODULE_13__["AuthComponent"], _components__WEBPACK_IMPORTED_MODULE_13__["SigninComponent"], _components__WEBPACK_IMPORTED_MODULE_13__["WorkflowComponent"], _components__WEBPACK_IMPORTED_MODULE_13__["DashboardComponent"], _components__WEBPACK_IMPORTED_MODULE_13__["DepartamentoComponent"], _components__WEBPACK_IMPORTED_MODULE_13__["AddDepartamentoComponent"], _components__WEBPACK_IMPORTED_MODULE_13__["EmpleadoComponent"], _components__WEBPACK_IMPORTED_MODULE_13__["SupervisorComponent"], _components__WEBPACK_IMPORTED_MODULE_13__["ReportesComponent"], _components_workflow_departamento_edit_departamento_edit_departamento_component__WEBPACK_IMPORTED_MODULE_14__["EditDepartamentoComponent"], _components_workflow_supervisor_add_supervisor_add_supervisor_component__WEBPACK_IMPORTED_MODULE_15__["AddSupervisorComponent"], _components_workflow_supervisor_edit_supervisor_edit_supervisor_component__WEBPACK_IMPORTED_MODULE_16__["EditSupervisorComponent"], _components_workflow_empleado_add_empleado_add_empleado_component__WEBPACK_IMPORTED_MODULE_17__["AddEmpleadoComponent"], _components_workflow_empleado_edit_empleado_edit_empleado_component__WEBPACK_IMPORTED_MODULE_18__["EditEmpleadoComponent"]],
            imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClientModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"], _shared_shared_module__WEBPACK_IMPORTED_MODULE_5__["SharedModule"], _admin_routing_module__WEBPACK_IMPORTED_MODULE_4__["AdminRoutingModule"], _ng_select_ng_select__WEBPACK_IMPORTED_MODULE_7__["NgSelectModule"], angular_mydatepicker__WEBPACK_IMPORTED_MODULE_8__["AngularMyDatePickerModule"], ngx_pagination__WEBPACK_IMPORTED_MODULE_9__["NgxPaginationModule"], ng2_charts__WEBPACK_IMPORTED_MODULE_10__["ChartsModule"], ngx_ui_loader__WEBPACK_IMPORTED_MODULE_11__["NgxUiLoaderModule"], ngx_color_picker__WEBPACK_IMPORTED_MODULE_12__["ColorPickerModule"]],
            providers: [{
              provide: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HTTP_INTERCEPTORS"],
              useClass: _shared_interceptors_httpconfig_interceptor__WEBPACK_IMPORTED_MODULE_6__["HttpConfigInterceptor"],
              multi: true
            }]
          }]
        }], null, null);
      })();
      /***/

    },

    /***/
    "k4qQ":
    /*!**********************************************************************!*\
      !*** ./src/app/admin/services/workflow/empleado/empleado.service.ts ***!
      \**********************************************************************/

    /*! exports provided: EmpleadoService */

    /***/
    function k4qQ(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "EmpleadoService", function () {
        return EmpleadoService;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _shared_classes__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ../../../../shared/classes */
      "gtvE");
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common/http */
      "tk/3");
      /* harmony import */


      var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! rxjs/operators */
      "kU1M");

      var EmpleadoService = /*#__PURE__*/function (_shared_classes__WEBP3) {
        _inherits(EmpleadoService, _shared_classes__WEBP3);

        var _super5 = _createSuper(EmpleadoService);

        function EmpleadoService(http) {
          var _this18;

          _classCallCheck(this, EmpleadoService);

          _this18 = _super5.call(this, "/api/v1/empleado", http); //lllamado al api para obtener los empleados

          _this18.getAllEmpleados = function (data) {
            var page = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
            var ctx = arguments.length > 2 ? arguments[2] : undefined;
            ctx.ngxService.start();

            _this18.get(data, '', page).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["takeUntil"])(ctx.destroySubscription$)).subscribe(function (response) {
              if (response) {
                ctx.empleados = response;
                ctx.ngxService.stop();
              }
            }, function (error) {
              console.error(error);
              ctx.ngxService.stop();
            });
          }; //agregar supervisores


          _this18.addEmpleado = function (data, ctx) {
            _this18.post(data, null, '').pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["takeUntil"])(ctx.destroySubscription$)).subscribe(function (response) {
              console.log("hgola");
              console.log(response);

              if (response) {
                ctx.ngxService.stop();
                ctx.resetServiceBtn();
              } else {
                ctx.ngxService.stop();
              }
            });
          }; //actualizar supervisor


          _this18.updateSupervisor = function (id, data, ctx) {
            _this18.post(data, null, '').pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["takeUntil"])(ctx.destroySubscription$)).subscribe(function (response) {
              if (response) {
                ctx.ngxService.stop();
                ctx.reversePage();
              } else {
                ctx.ngxService.stop();
              }
            }, function (error) {
              console.error(error);
              ctx.ngxService.stop();
            });
          }; //eliminar supervisor


          _this18.deleEmpleadoById = function (idEmpleado, ctx) {
            ctx.ngxService.start();

            _this18["delete"](idEmpleado, null, '').pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["takeUntil"])(ctx.destroySubscription$)).subscribe(function (response) {
              if (response) {
                ctx.complete();
                ctx.ngxService.stop();
              }
            }, function (error) {
              console.error(error);
              ctx.ngxService.stop();
            });
          };

          return _this18;
        }

        return EmpleadoService;
      }(_shared_classes__WEBPACK_IMPORTED_MODULE_1__["Services"]);

      EmpleadoService.ɵfac = function EmpleadoService_Factory(t) {
        return new (t || EmpleadoService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]));
      };

      EmpleadoService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({
        token: EmpleadoService,
        factory: EmpleadoService.ɵfac,
        providedIn: 'root'
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](EmpleadoService, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
          args: [{
            providedIn: 'root'
          }]
        }], function () {
          return [{
            type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]
          }];
        }, null);
      })();
      /***/

    },

    /***/
    "nEJ4":
    /*!***************************************************************!*\
      !*** ./src/app/shared/interceptors/httpconfig.interceptor.ts ***!
      \***************************************************************/

    /*! exports provided: HttpConfigInterceptor */

    /***/
    function nEJ4(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "HttpConfigInterceptor", function () {
        return HttpConfigInterceptor;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/common/http */
      "tk/3");
      /* harmony import */


      var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! rxjs */
      "qCKp");
      /* harmony import */


      var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! rxjs/operators */
      "kU1M");
      /* harmony import */


      var _classes_GlobalErrorHandler__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ../classes/GlobalErrorHandler */
      "RbYx");
      /* harmony import */


      var _classes_LocalStorageCommon__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ../classes/LocalStorageCommon */
      "hdVc");
      /* harmony import */


      var _services_signin_token_renewal_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ../services/signin/token-renewal.service */
      "qhoq");

      var HttpConfigInterceptor = /*#__PURE__*/function () {
        function HttpConfigInterceptor(errorHandler, localStorageCommon, tokenRenewalServices) {
          var _this19 = this;

          _classCallCheck(this, HttpConfigInterceptor);

          this.errorHandler = errorHandler;
          this.localStorageCommon = localStorageCommon;
          this.tokenRenewalServices = tokenRenewalServices;
          this.flag = false;

          this.tokenRenewal = function (request, next) {
            if (!_this19.flag) {
              _this19.flag = true;

              var user = _this19.localStorageCommon.get('auth_user', 'json');

              return _this19.tokenRenewalServices.put(user.id_users, null).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["switchMap"])(function (response) {
                _this19.localStorageCommon.store('auth_token', response.data, 'normal');

                _this19.flag = false;
                return next.handle(_this19.setHeadersAuthorization(request));
              }));
            }
          };

          this.setHeadersAuthorization = function (request) {
            return request.clone({
              setHeaders: {
                Authorization: 'Bearer ' + _this19.localStorageCommon.get('auth_token', 'normal')
              }
            });
          };
        }

        _createClass(HttpConfigInterceptor, [{
          key: "intercept",
          value: function intercept(request, next) {
            var _this20 = this;

            return next.handle(request).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (event) {
              return event;
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(function (error) {
              if (error instanceof _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpErrorResponse"]) {
                if (error !== undefined) {
                  if (error.status === 401) {
                    return _this20.tokenRenewal(request, next);
                  } else {
                    return Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["throwError"])(_this20.errorHandler.handleError(error));
                  }
                }
              } else {
                return Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["throwError"])(error);
              }
            }));
          }
        }]);

        return HttpConfigInterceptor;
      }();

      HttpConfigInterceptor.ɵfac = function HttpConfigInterceptor_Factory(t) {
        return new (t || HttpConfigInterceptor)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_classes_GlobalErrorHandler__WEBPACK_IMPORTED_MODULE_4__["GlobalErrorHandler"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_classes_LocalStorageCommon__WEBPACK_IMPORTED_MODULE_5__["LocalStorageCommon"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_services_signin_token_renewal_service__WEBPACK_IMPORTED_MODULE_6__["TokenRenewalService"]));
      };

      HttpConfigInterceptor.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({
        token: HttpConfigInterceptor,
        factory: HttpConfigInterceptor.ɵfac
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](HttpConfigInterceptor, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"]
        }], function () {
          return [{
            type: _classes_GlobalErrorHandler__WEBPACK_IMPORTED_MODULE_4__["GlobalErrorHandler"]
          }, {
            type: _classes_LocalStorageCommon__WEBPACK_IMPORTED_MODULE_5__["LocalStorageCommon"]
          }, {
            type: _services_signin_token_renewal_service__WEBPACK_IMPORTED_MODULE_6__["TokenRenewalService"]
          }];
        }, null);
      })();
      /***/

    },

    /***/
    "pNJG":
    /*!*****************************************************************!*\
      !*** ./src/app/admin/components/workflow/workflow.component.ts ***!
      \*****************************************************************/

    /*! exports provided: WorkflowComponent */

    /***/
    function pNJG(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "WorkflowComponent", function () {
        return WorkflowComponent;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var src_app_shared_classes_Enums__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! src/app/shared/classes/Enums */
      "xQgS");
      /* harmony import */


      var src_app_shared_classes_Helpers__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! src/app/shared/classes/Helpers */
      "sFNd");
      /* harmony import */


      var ngx_ui_loader__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ngx-ui-loader */
      "gren");

      var _c0 = function _c0() {
        return ["/admin/workflow/dashboard"];
      };

      var _c1 = function _c1() {
        return ["/admin/workflow/empleado"];
      };

      var _c2 = function _c2() {
        return ["/admin/workflow/supervisor"];
      };

      var _c3 = function _c3() {
        return ["/admin/workflow/departamento"];
      };

      var _c4 = function _c4() {
        return ["/admin/workflow/reporte"];
      };

      var WorkflowComponent = /*#__PURE__*/function () {
        function WorkflowComponent(router, helper) {
          _classCallCheck(this, WorkflowComponent);

          this.router = router;
          this.helper = helper;
          this.userPlaceholderImg = src_app_shared_classes_Enums__WEBPACK_IMPORTED_MODULE_2__["ASSET_SRC"].userPlaceholder;
        }

        _createClass(WorkflowComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            setTimeout(function () {
              $('#launcher-frame').css('left', '.5rem');
            }, 1000);
            this.complete();
          }
        }, {
          key: "complete",
          value: function complete() {
            this.userName = "Prueba";
            this.userInitials = "EB";
            $(function () {
              $('.isoNav').hide();
              $('#menu-toggle').click(function (e) {
                e.preventDefault();
                $('#wrapper').toggleClass('toggled');
                $('#page-content-wrapper').toggleClass('dynamic-container');
                $('.expanded').toggle(500, 'swing');
                $('.isoNav').toggle(500, 'swing');
                $('#launcher-frame').contents().find('.launcher-text').toggle();
              });
              $(window).resize(function () {
                if ($(window).width() <= 991) {
                  $('#wrapper').removeClass('toggled');
                  $('#page-content-wrapper').addClass('dynamic-container');
                  $('.expanded').hide(500, 'swing');
                  $('.isoNav').show(500, 'swing');
                  $('#launcher-frame').contents().find('.launcher-text').hide(500, 'swing');
                } else {
                  $('#wrapper').addClass('toggled');
                  $('#page-content-wrapper').removeClass('dynamic-container');
                  $('.expanded').show(500, 'swing');
                  $('.isoNav').hide(500, 'swing');
                  $('#launcher-frame').contents().find('.launcher-text').show(500, 'swing');
                }
              });
            });
          }
        }, {
          key: "logOut",
          value: function logOut() {
            this.router.navigate(['/'])["catch"](function (e) {
              console.error(e);
            });
          }
        }]);

        return WorkflowComponent;
      }();

      WorkflowComponent.ɵfac = function WorkflowComponent_Factory(t) {
        return new (t || WorkflowComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_shared_classes_Helpers__WEBPACK_IMPORTED_MODULE_3__["Helpers"]));
      };

      WorkflowComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
        type: WorkflowComponent,
        selectors: [["app-workflow"]],
        decls: 67,
        vars: 18,
        consts: [[1, "navbar", "navbar-expand", "fixed-top", "navbar-light", "bg-white"], ["id", "home-btn", 1, "navbar-brand", 3, "routerLink"], ["alt", "Ejemploo", "src", "assets/img/Logos/angular.png", 1, "isoNav", "img-responsive", "float-left"], ["id", "home-btn-responsive", 1, "navbar-brand", 3, "routerLink"], ["alt", "Ejemplo", "src", "assets/img/Logos/angular_logo.png", 1, "logoNav", "img-responsive", "expanded"], ["href", "#menu-toggle", "id", "menu-toggle", 1, "navbar-brand"], [1, "navbar-toggler-icon"], ["aria-controls", "navbarsExample02", "aria-expanded", "false", "aria-label", "Toggle navigation", "data-target", "#navbarsExample02", "data-toggle", "collapse", "id", "btn-toggler", "type", "button", 1, "navbar-toggler"], ["aria-controls", "navbarNavDropdown", "aria-expanded", "false", "aria-label", "Toggle navigation", "data-target", "#navbarNavDropdown", "data-toggle", "collapse", "id", "toggler-btn", "type", "button", 1, "navbar-toggler", "custom-toggler"], ["id", "navbarNavDropdown", 1, "navbar-collapse", "collapse", "text-right"], [1, "navbar-nav", "ml-auto"], [1, "row", "align-items-center"], [1, "col-auto", "user-info", "text-right"], [1, "user-name"], [1, "col", "text-center", "user-image"], ["alt", "User logo placeholder", 1, "user-placeholder", 3, "src"], [1, "centered-initials"], [1, "col"], [1, "dropdown", "show"], ["aria-expanded", "false", "aria-haspopup", "true", "data-offset", "1", "data-toggle", "dropdown", "href", "", "id", "menu-btn", "id", "dropdownMenuLink", "role", "button", 1, "user-menu"], [1, "fas", "fa-angle-down"], ["aria-labelledby", "dropdownMenuLink", 1, "dropdown-menu", "dropdown-menu-right", "menu-box"], [1, "dropdown-divider", 2, "display", "none"], ["id", "logout-btn", 1, "dropdown-item", 3, "click"], ["id", "wrapper", 1, "toggled"], ["id", "sidebar-wrapper"], [1, "expanded"], [1, "sidebar-nav"], ["id", "dashboard-btn", "routerLinkActive", "router-link-active", 3, "routerLink"], [1, "fas", "fa-tachometer-alt"], [1, "fas", "fa-user-friends"], [1, "fas", "fa-id-card-alt"], [1, "fas", "fa-house-user"], [1, "fas", "fa-chart-pie"], ["id", "page-content-wrapper"], [1, "container-fluid", "viewPort"], [1, "loader-container"], [3, "loaderId"]],
        template: function WorkflowComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "ngx-ui-loader");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "nav", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "a", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](4, "img", 2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "a", 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](6, "img", 4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "a", 5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](8, "span", 6);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "button", 7);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](10, "span", 6);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "button", 8);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](12, "span", 6);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "div", 9);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "ul", 10);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "div", 11);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "div", 12);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "p", 13);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](18);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "div", 14);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](20, "img", 15);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "div", 16);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](22);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "div", 17);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "div", 18);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](25, "a", 19);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](26, "i", 20);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "div", 21);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](28, "div", 22);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](29, "a", 23);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function WorkflowComponent_Template_a_click_29_listener() {
              return ctx.logOut();
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](30, "Cerrar Sesi\xF3n");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](31, "div", 24);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](32, "div", 25);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](33, "div");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](34, "h6", 26);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](35, "MEN\xDA PRINCIPAL");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](36, "ul", 27);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](37, "li");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](38, "a", 28);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](39, "i", 29);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](40, "div", 26);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](41, "Panel de control");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](42, "li");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](43, "a", 28);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](44, "i", 30);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](45, "div", 26);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](46, "Empleados");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](47, "li");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](48, "a", 28);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](49, "i", 31);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](50, "div", 26);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](51, "Supervisores");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](52, "li");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](53, "a", 28);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](54, "i", 32);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](55, "div", 26);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](56, "Departamentos");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](57, "li");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](58, "a", 28);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](59, "i", 33);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](60, "div", 26);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](61, "Reportes");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](62, "div", 34);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](63, "div", 35);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](64, "router-outlet");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](65, "div", 36);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](66, "ngx-ui-loader", 37);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
          }

          if (rf & 2) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](11, _c0));

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](12, _c0));

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](13);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx.userName);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate"]("src", ctx.userPlaceholderImg, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsanitizeUrl"]);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx.userInitials);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](16);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](13, _c0));

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](14, _c1));

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](15, _c2));

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](16, _c3));

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](17, _c4));

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](8);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("loaderId", "workflow-loader");
          }
        },
        directives: [ngx_ui_loader__WEBPACK_IMPORTED_MODULE_4__["ɵb"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterLinkWithHref"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterLinkActive"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterOutlet"]],
        styles: [".navbar[_ngcontent-%COMP%] {\n  padding-right: 3rem;\n  z-index: 1000;\n}\n\n.navbar-brand[_ngcontent-%COMP%] {\n  margin-left: -0.65rem;\n}\n\n.isoNav[_ngcontent-%COMP%] {\n  width: 35px;\n}\n\nbutton[_ngcontent-%COMP%] {\n  border-radius: 5px !important;\n  height: 40px;\n}\n\n.router-link-active[_ngcontent-%COMP%] {\n  color: #463d6e !important;\n  background: #dbd8e3;\n}\n\na.nav-link[_ngcontent-%COMP%], button.btn.btn-outline-primary[_ngcontent-%COMP%] {\n  font-size: 1rem;\n  display: inline-block;\n  color: #3d3769 !important;\n}\n\n.fa-sign-out-alt[_ngcontent-%COMP%] {\n  position: relative;\n}\n\n.user-placeholder[_ngcontent-%COMP%] {\n  border-radius: 50px;\n}\n\n.bg-white[_ngcontent-%COMP%] {\n  background-color: white !important;\n  border: solid 1px #f1f1f1;\n}\n\n.custom-toggler.navbar-toggler[_ngcontent-%COMP%] {\n  border-color: #3d3769 !important;\n}\n\n.custom-toggler[_ngcontent-%COMP%]   .navbar-toggler-icon[_ngcontent-%COMP%] {\n  background-image: url(\"data:image/svg+xml;charset=utf8,%3Csvg viewBox='0 0 32 32' xmlns='http://www.w3.org/2000/svg'%3E%3Cpath stroke='rgba(61,55,105, 0.7)' stroke-width='2' stroke-linecap='round' stroke-miterlimit='10' d='M4 8h24M4 16h24M4 24h24'/%3E%3C/svg%3E\") !important;\n}\n\n@media (max-width: 1635px) {\n  a.nav-link[_ngcontent-%COMP%] {\n    text-align: center;\n  }\n}\n\n@media (max-width: 991px) {\n  .navbar-nav[_ngcontent-%COMP%] {\n    padding-left: initial;\n  }\n\n  \n  .nav-item[_ngcontent-%COMP%]    > button[_ngcontent-%COMP%] {\n    margin-top: 0.5rem;\n    position: relative;\n    left: -10px;\n  }\n}\n\n@media (max-width: 414px) {\n  .navbar-nav[_ngcontent-%COMP%] {\n    align-items: flex-end !important;\n  }\n\n  .navbar[_ngcontent-%COMP%] {\n    padding: 1rem 1.5rem !important;\n  }\n\n  .nav-link[_ngcontent-%COMP%] {\n    padding-right: 0 !important;\n  }\n}\n\n#wrapper[_ngcontent-%COMP%] {\n  overflow: hidden;\n  padding-left: 0;\n  transition: all 0.5s ease;\n  background: #fff;\n}\n\n#wrapper.toggled[_ngcontent-%COMP%] {\n  padding-right: 225px;\n}\n\n.user-name[_ngcontent-%COMP%] {\n  display: inline-block;\n  height: 16px;\n  font-family: Roboto-Regular, sans-serif;\n  font-size: 14px;\n  text-align: right;\n  color: #0a0a0b;\n  margin: 0;\n  white-space: nowrap;\n  overflow: hidden;\n}\n\n.user-info[_ngcontent-%COMP%], .user-image[_ngcontent-%COMP%] {\n  padding: 0;\n  vertical-align: middle;\n}\n\n.user-info[_ngcontent-%COMP%] {\n  padding-right: 0.75rem;\n}\n\n.user-image[_ngcontent-%COMP%] {\n  position: relative;\n  text-align: center;\n  min-width: 40px;\n}\n\n#page-content[_ngcontent-%COMP%] {\n  flex: 1 0 auto;\n}\n\n#sticky-footer[_ngcontent-%COMP%] {\n  flex-shrink: none;\n  border: solid 1px #f1f1f1;\n  background-color: #fff;\n  z-index: 2;\n  display: none;\n}\n\n.user-menu[_ngcontent-%COMP%] {\n  color: #0a0a0b;\n  font-size: 18px;\n}\n\n.centered-initials[_ngcontent-%COMP%] {\n  position: absolute;\n  top: 50%;\n  left: 50%;\n  transform: translate(-50%, -50%);\n  color: #463d6e;\n  font-family: Roboto-Regular, sans-serif;\n  font-size: 14px;\n}\n\n.institution-name[_ngcontent-%COMP%] {\n  height: 14px;\n  opacity: 0.49;\n  font-family: Roboto-Regular, sans-serif;\n  font-size: 12px;\n  text-align: right;\n  white-space: nowrap;\n  overflow: hidden;\n  color: #0a0a0b;\n  margin: 0;\n}\n\n.menu-box[_ngcontent-%COMP%] {\n  width: 200px;\n  font-family: Roboto-Regular, sans-serif;\n  font-size: 14px;\n  color: #0a0a0b;\n}\n\n.menu-box[_ngcontent-%COMP%]   a[_ngcontent-%COMP%] {\n  padding: 0.45rem 1.5rem;\n}\n\n.footer-separators[_ngcontent-%COMP%] {\n  opacity: 0.3;\n  color: #423d72;\n  padding: 0 1.25rem;\n}\n\n.copy-text[_ngcontent-%COMP%] {\n  opacity: 0.4;\n  font-family: Roboto-Regular, sans-serif;\n  font-size: 12px;\n  color: #443d70;\n}\n\n.footer-links[_ngcontent-%COMP%] {\n  margin-right: 2.5%;\n}\n\n.footer-links[_ngcontent-%COMP%]    > a[_ngcontent-%COMP%] {\n  font-family: Roboto-Regular, sans-serif;\n  font-size: 12px;\n  color: #443d70;\n}\n\n#sidebar-wrapper[_ngcontent-%COMP%] {\n  z-index: 4;\n  position: fixed;\n  top: 61px;\n  width: 66px;\n  margin-right: -250px;\n  overflow-y: scroll;\n  background: #fff;\n  box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.1);\n  transition: all 0.5s ease;\n  max-height: 90%;\n  -ms-overflow-style: none;\n  scrollbar-width: none;\n}\n\n#sidebar-wrapper[_ngcontent-%COMP%]::-webkit-scrollbar {\n  display: none;\n}\n\n#wrapper.toggled[_ngcontent-%COMP%]   #sidebar-wrapper[_ngcontent-%COMP%] {\n  width: 225px;\n  box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.1);\n}\n\n#page-content-wrapper[_ngcontent-%COMP%] {\n  width: 100%;\n  height: auto;\n  position: absolute;\n  padding: 15px;\n  -ms-overflow-style: none;\n  scrollbar-width: none;\n}\n\n.viewPort[_ngcontent-%COMP%] {\n  padding-top: 0;\n  padding-bottom: 0;\n  -ms-overflow-style: none;\n  scrollbar-width: none;\n}\n\n.viewPort[_ngcontent-%COMP%]::-webkit-scrollbar {\n  display: none;\n}\n\n#wrapper.toggled[_ngcontent-%COMP%]   #page-content-wrapper[_ngcontent-%COMP%] {\n  position: absolute;\n  margin-left: 225px;\n  -ms-overflow-style: none;\n  scrollbar-width: none;\n}\n\n#page-content-wrapper.dynamic-container[_ngcontent-%COMP%] {\n  position: relative;\n  padding-right: 0;\n  margin-right: 0;\n  width: 95%;\n  left: 3.6vw;\n  top: -7px;\n  -ms-overflow-style: none;\n  scrollbar-width: none;\n}\n\n#page-content-wrapper[_ngcontent-%COMP%]::-webkit-scrollbar, #wrapper.toggled[_ngcontent-%COMP%]   #page-content-wrapper[_ngcontent-%COMP%]::-webkit-scrollbar, #page-content-wrapper.dynamic-container[_ngcontent-%COMP%]::-webkit-scrollbar {\n  display: none;\n}\n\n\n\n.sidebar-nav[_ngcontent-%COMP%] {\n  margin: 0;\n  padding: 0;\n  list-style: none;\n}\n\n.sidebar-nav[_ngcontent-%COMP%]   li[_ngcontent-%COMP%] {\n  line-height: 50px;\n}\n\n#sidebar-wrapper[_ngcontent-%COMP%]   h6[_ngcontent-%COMP%] {\n  padding-left: 17px;\n  margin-top: 20px;\n  font-family: Roboto-Regular, sans-serif;\n  font-size: 14px;\n  color: #b5b1c5;\n}\n\n.sidebar-nav[_ngcontent-%COMP%]   li[_ngcontent-%COMP%]   a[_ngcontent-%COMP%] {\n  display: flex;\n  align-items: center;\n  text-decoration: none;\n  font-family: Roboto-Regular, sans-serif;\n  font-size: 14px;\n  color: #0a0a0b;\n  border-radius: 5px;\n  margin: 10px 14px;\n  height: 34px;\n}\n\n.sidebar-nav[_ngcontent-%COMP%]   li[_ngcontent-%COMP%]   a[_ngcontent-%COMP%]   i[_ngcontent-%COMP%] {\n  font-size: 18px;\n  padding: 0 10px;\n  margin-right: 5px;\n  min-width: 40px;\n  text-align: center;\n}\n\n.sidebar-nav[_ngcontent-%COMP%]   li[_ngcontent-%COMP%]   a[_ngcontent-%COMP%]:hover, .sidebar-nav[_ngcontent-%COMP%]   li[_ngcontent-%COMP%]   a[_ngcontent-%COMP%]:active {\n  text-decoration: none;\n  color: #463d6e;\n  background: #dbd8e3;\n}\n\n.sidebar-nav[_ngcontent-%COMP%]   li[_ngcontent-%COMP%]   a[_ngcontent-%COMP%]:active, .sidebar-nav[_ngcontent-%COMP%]   li[_ngcontent-%COMP%]   a[_ngcontent-%COMP%]:focus {\n  text-decoration: none;\n}\n\n.sidebar-nav[_ngcontent-%COMP%]    > .sidebar-brand[_ngcontent-%COMP%] {\n  height: 65px;\n  font-size: 18px;\n  line-height: 60px;\n}\n\n.sidebar-nav[_ngcontent-%COMP%]    > .sidebar-brand[_ngcontent-%COMP%]   a[_ngcontent-%COMP%] {\n  color: #999999;\n}\n\n.sidebar-nav[_ngcontent-%COMP%]    > .sidebar-brand[_ngcontent-%COMP%]   a[_ngcontent-%COMP%]:hover {\n  color: #fff;\n  background: none;\n}\n\n.toggled[_ngcontent-%COMP%]   .loader-container[_ngcontent-%COMP%] {\n  position: fixed;\n  left: 14rem;\n  top: 4.4rem;\n  width: 100%;\n  height: 100.6%;\n  z-index: 1060;\n  pointer-events: none;\n}\n\n.loader-container[_ngcontent-%COMP%] {\n  position: fixed;\n  left: 4.2rem;\n  top: 3.9rem;\n  width: 100%;\n  height: 100.6%;\n  z-index: 1060;\n  pointer-events: none !important;\n}\n\n@media screen and (max-width: 2000px) {\n  #wrapper[_ngcontent-%COMP%] {\n    width: 100%;\n  }\n\n  .toggled[_ngcontent-%COMP%]   .loader-container[_ngcontent-%COMP%] {\n    width: 88.5%;\n    left: 14rem;\n    top: 4.4rem;\n  }\n\n  .loader-container[_ngcontent-%COMP%] {\n    top: 3.9rem;\n    left: 4.2rem;\n    width: 100%;\n  }\n}\n\n@media screen and (min-height: 716px) {\n  #sidebar-wrapper[_ngcontent-%COMP%] {\n    min-height: -webkit-fill-available;\n    min-height: 100%;\n  }\n}\n\n@media screen and (max-height: 715px) {\n  #sidebar-wrapper[_ngcontent-%COMP%] {\n    min-height: 90%;\n  }\n}\n\n@media screen and (max-width: 1540px) {\n  .toggled[_ngcontent-%COMP%]   .loader-container[_ngcontent-%COMP%] {\n    left: 14rem;\n    width: 83.5%;\n    top: 4.4rem;\n  }\n\n  .loader-container[_ngcontent-%COMP%] {\n    left: 4.2rem;\n    top: 3.9rem;\n    width: 100%;\n  }\n\n  .dynamic-container[_ngcontent-%COMP%]   .viewPort[_ngcontent-%COMP%] {\n    margin-left: 1rem;\n  }\n}\n\n@media (min-width: 768px) {\n  #wrapper[_ngcontent-%COMP%] {\n    padding-left: 0;\n  }\n\n  #wrapper.toggled[_ngcontent-%COMP%] {\n    padding-right: 225px;\n  }\n\n  #sidebar-wrapper[_ngcontent-%COMP%] {\n    width: 66px;\n    box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.1);\n  }\n\n  #wrapper.toggled[_ngcontent-%COMP%]   #sidebar-wrapper[_ngcontent-%COMP%] {\n    width: 225px;\n  }\n\n  #page-content-wrapper[_ngcontent-%COMP%] {\n    padding: 20px;\n    position: relative;\n    margin-bottom: 5rem;\n  }\n\n  #wrapper.toggled[_ngcontent-%COMP%]   #page-content-wrapper[_ngcontent-%COMP%] {\n    position: relative;\n    margin-right: 0;\n    width: 100%;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXC4uXFwuLlxcd29ya2Zsb3cuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0E7RUFDRSxtQkFBQTtFQUNBLGFBQUE7QUFBRjs7QUFHQTtFQUNFLHFCQUFBO0FBQUY7O0FBR0E7RUFFRSxXQUFBO0FBREY7O0FBSUE7RUFDRSw2QkFBQTtFQUNBLFlBQUE7QUFERjs7QUFJQTtFQUNFLHlCQUFBO0VBQ0EsbUJBQUE7QUFERjs7QUFJQTs7RUFFRSxlQUFBO0VBQ0EscUJBQUE7RUFDQSx5QkFBQTtBQURGOztBQUlBO0VBQ0Usa0JBQUE7QUFERjs7QUFJQTtFQUNFLG1CQUFBO0FBREY7O0FBSUE7RUFDRSxrQ0FBQTtFQUNBLHlCQUFBO0FBREY7O0FBSUE7RUFDRSxnQ0FBQTtBQURGOztBQUlBO0VBQ0Usa1JBQUE7QUFERjs7QUFJQTtFQUNFO0lBQ0Usa0JBQUE7RUFERjtBQUNGOztBQUlBO0VBQ0U7SUFDRSxxQkFBQTtFQUZGOztFQUlBOzs7OztJQUFBO0VBTUE7SUFDRSxrQkFBQTtJQUNBLGtCQUFBO0lBQ0EsV0FBQTtFQURGO0FBQ0Y7O0FBSUE7RUFDRTtJQUNFLGdDQUFBO0VBRkY7O0VBSUE7SUFDRSwrQkFBQTtFQURGOztFQUdBO0lBQ0UsMkJBQUE7RUFBRjtBQUNGOztBQUdBO0VBQ0UsZ0JBQUE7RUFDQSxlQUFBO0VBSUEseUJBQUE7RUFDQSxnQkFBQTtBQURGOztBQUlBO0VBQ0Usb0JBQUE7QUFERjs7QUFJQTtFQUNFLHFCQUFBO0VBQ0EsWUFBQTtFQUNBLHVDQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0VBQ0EsY0E1R2E7RUE2R2IsU0FBQTtFQUNBLG1CQUFBO0VBQ0EsZ0JBQUE7QUFERjs7QUFJQTs7RUFFRSxVQUFBO0VBQ0Esc0JBQUE7QUFERjs7QUFJQTtFQUNFLHNCQUFBO0FBREY7O0FBSUE7RUFDRSxrQkFBQTtFQUNBLGtCQUFBO0VBQ0EsZUFBQTtBQURGOztBQUlBO0VBQ0UsY0FBQTtBQURGOztBQUlBO0VBQ0UsaUJBQUE7RUFDQSx5QkFBQTtFQUNBLHNCQUFBO0VBQ0EsVUFBQTtFQUNBLGFBQUE7QUFERjs7QUFJQTtFQUNFLGNBL0lhO0VBZ0piLGVBQUE7QUFERjs7QUFJQTtFQUNFLGtCQUFBO0VBQ0EsUUFBQTtFQUNBLFNBQUE7RUFDQSxnQ0FBQTtFQUNBLGNBQUE7RUFDQSx1Q0FBQTtFQUNBLGVBQUE7QUFERjs7QUFJQTtFQUVFLFlBQUE7RUFDQSxhQUFBO0VBQ0EsdUNBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxtQkFBQTtFQUNBLGdCQUFBO0VBQ0EsY0F0S2E7RUF1S2IsU0FBQTtBQUZGOztBQUtBO0VBQ0UsWUFBQTtFQUNBLHVDQUFBO0VBQ0EsZUFBQTtFQUNBLGNBOUthO0FBNEtmOztBQUlFO0VBQ0UsdUJBQUE7QUFGSjs7QUFNQTtFQUNFLFlBQUE7RUFDQSxjQUFBO0VBQ0Esa0JBQUE7QUFIRjs7QUFNQTtFQUNFLFlBQUE7RUFDQSx1Q0FBQTtFQUNBLGVBQUE7RUFDQSxjQUFBO0FBSEY7O0FBTUE7RUFDRSxrQkFBQTtBQUhGOztBQU1BO0VBQ0UsdUNBQUE7RUFDQSxlQUFBO0VBQ0EsY0FBQTtBQUhGOztBQU1BO0VBQ0UsVUFBQTtFQUNBLGVBQUE7RUFDQSxTQUFBO0VBQ0EsV0FBQTtFQUNBLG9CQUFBO0VBQ0Esa0JBQUE7RUFDQSxnQkFBQTtFQUNBLDBDQUFBO0VBSUEseUJBQUE7RUFDQSxlQUFBO0VBQ0Esd0JBQUE7RUFDQSxxQkFBQTtBQUhGOztBQU1BO0VBQ0UsYUFBQTtBQUhGOztBQU1BO0VBQ0UsWUFBQTtFQUNBLDBDQUFBO0FBSEY7O0FBTUE7RUFDRSxXQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0VBQ0EsYUFBQTtFQUNBLHdCQUFBO0VBQ0EscUJBQUE7QUFIRjs7QUFNQTtFQUNFLGNBQUE7RUFDQSxpQkFBQTtFQUNBLHdCQUFBO0VBQ0EscUJBQUE7QUFIRjs7QUFNQTtFQUNFLGFBQUE7QUFIRjs7QUFNQTtFQUNFLGtCQUFBO0VBQ0Esa0JBQUE7RUFDQSx3QkFBQTtFQUNBLHFCQUFBO0FBSEY7O0FBTUE7RUFDRSxrQkFBQTtFQUNBLGdCQUFBO0VBQ0EsZUFBQTtFQUNBLFVBQUE7RUFDQSxXQUFBO0VBQ0EsU0FBQTtFQUVBLHdCQUFBO0VBQ0EscUJBQUE7QUFKRjs7QUFNQTtFQUNFLGFBQUE7QUFIRjs7QUFNQSxtQkFBQTs7QUFFQTtFQUNFLFNBQUE7RUFDQSxVQUFBO0VBQ0EsZ0JBQUE7QUFKRjs7QUFNRTtFQUNFLGlCQUFBO0FBSko7O0FBU0U7RUFDRSxrQkFBQTtFQUNBLGdCQUFBO0VBQ0EsdUNBQUE7RUFDQSxlQUFBO0VBQ0EsY0FBQTtBQU5KOztBQVVBO0VBQ0UsYUFBQTtFQUNBLG1CQUFBO0VBQ0EscUJBQUE7RUFDQSx1Q0FBQTtFQUNBLGVBQUE7RUFDQSxjQTdTYTtFQThTYixrQkFBQTtFQUNBLGlCQUFBO0VBQ0EsWUFBQTtBQVBGOztBQVNFO0VBQ0UsZUFBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtFQUNBLGVBQUE7RUFDQSxrQkFBQTtBQVBKOztBQVdBOztFQUVFLHFCQUFBO0VBQ0EsY0FBQTtFQUNBLG1CQUFBO0FBUkY7O0FBV0E7O0VBRUUscUJBQUE7QUFSRjs7QUFXQTtFQUNFLFlBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7QUFSRjs7QUFXQTtFQUNFLGNBQUE7QUFSRjs7QUFXQTtFQUNFLFdBQUE7RUFDQSxnQkFBQTtBQVJGOztBQVdBO0VBQ0UsZUFBQTtFQUNBLFdBQUE7RUFDQSxXQUFBO0VBQ0EsV0FBQTtFQUNBLGNBQUE7RUFDQSxhQUFBO0VBQ0Esb0JBQUE7QUFSRjs7QUFXQTtFQUNFLGVBQUE7RUFDQSxZQUFBO0VBQ0EsV0FBQTtFQUNBLFdBQUE7RUFDQSxjQUFBO0VBQ0EsYUFBQTtFQUNBLCtCQUFBO0FBUkY7O0FBZUE7RUFDRTtJQUNFLFdBQUE7RUFaRjs7RUFjQTtJQUNFLFlBQUE7SUFDQSxXQUFBO0lBQ0EsV0FBQTtFQVhGOztFQWFBO0lBQ0UsV0FBQTtJQUNBLFlBQUE7SUFDQSxXQUFBO0VBVkY7QUFDRjs7QUFjQTtFQUNFO0lBQ0Usa0NBQUE7SUFDQSxnQkFBQTtFQVpGO0FBQ0Y7O0FBZ0JBO0VBQ0U7SUFDRSxlQUFBO0VBZEY7QUFDRjs7QUFvQkE7RUFDRTtJQUNFLFdBQUE7SUFDQSxZQUFBO0lBQ0EsV0FBQTtFQWxCRjs7RUFvQkE7SUFDRSxZQUFBO0lBQ0EsV0FBQTtJQUNBLFdBQUE7RUFqQkY7O0VBbUJBO0lBQ0UsaUJBQUE7RUFoQkY7QUFDRjs7QUFtQkE7RUFDRTtJQUNFLGVBQUE7RUFqQkY7O0VBbUJBO0lBQ0Usb0JBQUE7RUFoQkY7O0VBa0JBO0lBQ0UsV0FBQTtJQUNBLDBDQUFBO0VBZkY7O0VBaUJBO0lBQ0UsWUFBQTtFQWRGOztFQWdCQTtJQUNFLGFBQUE7SUFDQSxrQkFBQTtJQUNBLG1CQUFBO0VBYkY7O0VBZUE7SUFDRSxrQkFBQTtJQUNBLGVBQUE7SUFDQSxXQUFBO0VBWkY7QUFDRiIsImZpbGUiOiJ3b3JrZmxvdy5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIiRhbG1vc3QtYmxhY2s6ICMwYTBhMGI7XG4ubmF2YmFyIHtcbiAgcGFkZGluZy1yaWdodDogM3JlbTtcbiAgei1pbmRleDogMTAwMDtcbn1cblxuLm5hdmJhci1icmFuZCB7XG4gIG1hcmdpbi1sZWZ0OiAtLjY1cmVtO1xufVxuXG4uaXNvTmF2IHtcbiAgLy8gZGlzcGxheTogbm9uZTtcbiAgd2lkdGg6IDM1cHg7XG59XG5cbmJ1dHRvbiB7XG4gIGJvcmRlci1yYWRpdXM6IDVweCAhaW1wb3J0YW50O1xuICBoZWlnaHQ6IDQwcHg7XG59XG5cbi5yb3V0ZXItbGluay1hY3RpdmUge1xuICBjb2xvcjogIzQ2M2Q2ZSAhaW1wb3J0YW50O1xuICBiYWNrZ3JvdW5kOiAjZGJkOGUzO1xufVxuXG5hLm5hdi1saW5rLFxuYnV0dG9uLmJ0bi5idG4tb3V0bGluZS1wcmltYXJ5IHtcbiAgZm9udC1zaXplOiAxcmVtO1xuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gIGNvbG9yOiAjM2QzNzY5ICFpbXBvcnRhbnQ7XG59XG5cbi5mYS1zaWduLW91dC1hbHQge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG59XG5cbi51c2VyLXBsYWNlaG9sZGVyIHtcbiAgYm9yZGVyLXJhZGl1czogNTBweDtcbn1cblxuLmJnLXdoaXRlIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGUgIWltcG9ydGFudDtcbiAgYm9yZGVyOiBzb2xpZCAxcHggI2YxZjFmMTtcbn1cblxuLmN1c3RvbS10b2dnbGVyLm5hdmJhci10b2dnbGVyIHtcbiAgYm9yZGVyLWNvbG9yOiByZ2IoNjEsIDU1LCAxMDUpICFpbXBvcnRhbnQ7XG59XG5cbi5jdXN0b20tdG9nZ2xlciAubmF2YmFyLXRvZ2dsZXItaWNvbiB7XG4gIGJhY2tncm91bmQtaW1hZ2U6IHVybChcImRhdGE6aW1hZ2Uvc3ZnK3htbDtjaGFyc2V0PXV0ZjgsJTNDc3ZnIHZpZXdCb3g9JzAgMCAzMiAzMicgeG1sbnM9J2h0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnJyUzRSUzQ3BhdGggc3Ryb2tlPSdyZ2JhKDYxLDU1LDEwNSwgMC43KScgc3Ryb2tlLXdpZHRoPScyJyBzdHJva2UtbGluZWNhcD0ncm91bmQnIHN0cm9rZS1taXRlcmxpbWl0PScxMCcgZD0nTTQgOGgyNE00IDE2aDI0TTQgMjRoMjQnLyUzRSUzQy9zdmclM0VcIikgIWltcG9ydGFudDtcbn1cblxuQG1lZGlhIChtYXgtd2lkdGg6IDE2MzVweCkge1xuICBhLm5hdi1saW5rIHtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIH1cbn1cblxuQG1lZGlhIChtYXgtd2lkdGg6IDk5MXB4KSB7XG4gIC5uYXZiYXItbmF2IHtcbiAgICBwYWRkaW5nLWxlZnQ6IGluaXRpYWw7XG4gIH1cbiAgLyogIC5sb2dvTmF2IHtcbiAgICAgIGRpc3BsYXk6IG5vbmU7XG4gIH1cbiAgLmlzb05hdiB7XG4gICAgICBkaXNwbGF5OiBibG9jaztcbiAgfSovXG4gIC5uYXYtaXRlbSA+IGJ1dHRvbiB7XG4gICAgbWFyZ2luLXRvcDogLjVyZW07XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIGxlZnQ6IC0xMHB4O1xuICB9XG59XG5cbkBtZWRpYSAobWF4LXdpZHRoOiA0MTRweCkge1xuICAubmF2YmFyLW5hdiB7XG4gICAgYWxpZ24taXRlbXM6IGZsZXgtZW5kICFpbXBvcnRhbnQ7XG4gIH1cbiAgLm5hdmJhciB7XG4gICAgcGFkZGluZzogMXJlbSAxLjVyZW0gIWltcG9ydGFudDtcbiAgfVxuICAubmF2LWxpbmsge1xuICAgIHBhZGRpbmctcmlnaHQ6IDAgIWltcG9ydGFudDtcbiAgfVxufVxuXG4jd3JhcHBlciB7XG4gIG92ZXJmbG93OiBoaWRkZW47XG4gIHBhZGRpbmctbGVmdDogMDtcbiAgLXdlYmtpdC10cmFuc2l0aW9uOiBhbGwgMC41cyBlYXNlO1xuICAtbW96LXRyYW5zaXRpb246IGFsbCAwLjVzIGVhc2U7XG4gIC1vLXRyYW5zaXRpb246IGFsbCAwLjVzIGVhc2U7XG4gIHRyYW5zaXRpb246IGFsbCAwLjVzIGVhc2U7XG4gIGJhY2tncm91bmQ6ICNmZmY7XG59XG5cbiN3cmFwcGVyLnRvZ2dsZWQge1xuICBwYWRkaW5nLXJpZ2h0OiAyMjVweDtcbn1cblxuLnVzZXItbmFtZSB7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgaGVpZ2h0OiAxNnB4O1xuICBmb250LWZhbWlseTogUm9ib3RvLVJlZ3VsYXIsIHNhbnMtc2VyaWY7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgdGV4dC1hbGlnbjogcmlnaHQ7XG4gIGNvbG9yOiAkYWxtb3N0LWJsYWNrO1xuICBtYXJnaW46IDA7XG4gIHdoaXRlLXNwYWNlOiBub3dyYXA7XG4gIG92ZXJmbG93OiBoaWRkZW5cbn1cblxuLnVzZXItaW5mbyxcbi51c2VyLWltYWdlIHtcbiAgcGFkZGluZzogMDtcbiAgdmVydGljYWwtYWxpZ246IG1pZGRsZTtcbn1cblxuLnVzZXItaW5mbyB7XG4gIHBhZGRpbmctcmlnaHQ6IDAuNzVyZW07XG59XG5cbi51c2VyLWltYWdlIHtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIG1pbi13aWR0aDogNDBweDtcbn1cblxuI3BhZ2UtY29udGVudCB7XG4gIGZsZXg6IDEgMCBhdXRvO1xufVxuXG4jc3RpY2t5LWZvb3RlciB7XG4gIGZsZXgtc2hyaW5rOiBub25lO1xuICBib3JkZXI6IHNvbGlkIDFweCAjZjFmMWYxO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xuICB6LWluZGV4OiAyO1xuICBkaXNwbGF5OiBub25lO1xufVxuXG4udXNlci1tZW51IHtcbiAgY29sb3I6ICRhbG1vc3QtYmxhY2s7XG4gIGZvbnQtc2l6ZTogMThweDtcbn1cblxuLmNlbnRlcmVkLWluaXRpYWxzIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0b3A6IDUwJTtcbiAgbGVmdDogNTAlO1xuICB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgtNTAlLCAtNTAlKTtcbiAgY29sb3I6ICM0NjNkNmU7XG4gIGZvbnQtZmFtaWx5OiBSb2JvdG8tUmVndWxhciwgc2Fucy1zZXJpZjtcbiAgZm9udC1zaXplOiAxNHB4O1xufVxuXG4uaW5zdGl0dXRpb24tbmFtZSB7XG4gIC8vIHdpZHRoOiAxMzhweDtcbiAgaGVpZ2h0OiAxNHB4O1xuICBvcGFjaXR5OiAwLjQ5O1xuICBmb250LWZhbWlseTogUm9ib3RvLVJlZ3VsYXIsIHNhbnMtc2VyaWY7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgdGV4dC1hbGlnbjogcmlnaHQ7XG4gIHdoaXRlLXNwYWNlOiBub3dyYXA7XG4gIG92ZXJmbG93OiBoaWRkZW47XG4gIGNvbG9yOiAkYWxtb3N0LWJsYWNrO1xuICBtYXJnaW46IDA7XG59XG5cbi5tZW51LWJveCB7XG4gIHdpZHRoOiAyMDBweDtcbiAgZm9udC1mYW1pbHk6IFJvYm90by1SZWd1bGFyLCBzYW5zLXNlcmlmO1xuICBmb250LXNpemU6IDE0cHg7XG4gIGNvbG9yOiAkYWxtb3N0LWJsYWNrO1xuXG4gIGEge1xuICAgIHBhZGRpbmc6IC40NXJlbSAxLjVyZW07XG4gIH1cbn1cblxuLmZvb3Rlci1zZXBhcmF0b3JzIHtcbiAgb3BhY2l0eTogMC4zO1xuICBjb2xvcjogIzQyM2Q3MjtcbiAgcGFkZGluZzogMCAxLjI1cmVtO1xufVxuXG4uY29weS10ZXh0IHtcbiAgb3BhY2l0eTogMC40O1xuICBmb250LWZhbWlseTogUm9ib3RvLVJlZ3VsYXIsIHNhbnMtc2VyaWY7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgY29sb3I6ICM0NDNkNzA7XG59XG5cbi5mb290ZXItbGlua3Mge1xuICBtYXJnaW4tcmlnaHQ6IDIuNSU7XG59XG5cbi5mb290ZXItbGlua3MgPiBhIHtcbiAgZm9udC1mYW1pbHk6IFJvYm90by1SZWd1bGFyLCBzYW5zLXNlcmlmO1xuICBmb250LXNpemU6IDEycHg7XG4gIGNvbG9yOiAjNDQzZDcwO1xufVxuXG4jc2lkZWJhci13cmFwcGVyIHtcbiAgei1pbmRleDogNDtcbiAgcG9zaXRpb246IGZpeGVkO1xuICB0b3A6IDYxcHg7XG4gIHdpZHRoOiA2NnB4O1xuICBtYXJnaW4tcmlnaHQ6IC0yNTBweDtcbiAgb3ZlcmZsb3cteTogc2Nyb2xsO1xuICBiYWNrZ3JvdW5kOiAjZmZmO1xuICBib3gtc2hhZG93OiAwIDJweCA0cHggMCByZ2JhKDAsIDAsIDAsIDAuMSk7XG4gIC13ZWJraXQtdHJhbnNpdGlvbjogYWxsIDAuNXMgZWFzZTtcbiAgLW1vei10cmFuc2l0aW9uOiBhbGwgMC41cyBlYXNlO1xuICAtby10cmFuc2l0aW9uOiBhbGwgMC41cyBlYXNlO1xuICB0cmFuc2l0aW9uOiBhbGwgMC41cyBlYXNlO1xuICBtYXgtaGVpZ2h0OiA5MCU7XG4gIC1tcy1vdmVyZmxvdy1zdHlsZTogbm9uZTtcbiAgc2Nyb2xsYmFyLXdpZHRoOiBub25lO1xufVxuXG4jc2lkZWJhci13cmFwcGVyOjotd2Via2l0LXNjcm9sbGJhciB7XG4gIGRpc3BsYXk6IG5vbmU7XG59XG5cbiN3cmFwcGVyLnRvZ2dsZWQgI3NpZGViYXItd3JhcHBlciB7XG4gIHdpZHRoOiAyMjVweDtcbiAgYm94LXNoYWRvdzogMCAycHggNHB4IDAgcmdiYSgwLCAwLCAwLCAwLjEpO1xufVxuXG4jcGFnZS1jb250ZW50LXdyYXBwZXIge1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiBhdXRvO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHBhZGRpbmc6IDE1cHg7XG4gIC1tcy1vdmVyZmxvdy1zdHlsZTogbm9uZTtcbiAgc2Nyb2xsYmFyLXdpZHRoOiBub25lO1xufVxuXG4udmlld1BvcnQge1xuICBwYWRkaW5nLXRvcDogMDtcbiAgcGFkZGluZy1ib3R0b206IDA7XG4gIC1tcy1vdmVyZmxvdy1zdHlsZTogbm9uZTtcbiAgc2Nyb2xsYmFyLXdpZHRoOiBub25lO1xufVxuXG4udmlld1BvcnQ6Oi13ZWJraXQtc2Nyb2xsYmFyIHtcbiAgZGlzcGxheTogbm9uZTtcbn1cblxuI3dyYXBwZXIudG9nZ2xlZCAjcGFnZS1jb250ZW50LXdyYXBwZXIge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIG1hcmdpbi1sZWZ0OiAyMjVweDtcbiAgLW1zLW92ZXJmbG93LXN0eWxlOiBub25lO1xuICBzY3JvbGxiYXItd2lkdGg6IG5vbmU7XG59XG5cbiNwYWdlLWNvbnRlbnQtd3JhcHBlci5keW5hbWljLWNvbnRhaW5lciB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgcGFkZGluZy1yaWdodDogMDtcbiAgbWFyZ2luLXJpZ2h0OiAwO1xuICB3aWR0aDogOTUlO1xuICBsZWZ0OiAzLjZ2dztcbiAgdG9wOiAtN3B4O1xuICAvLy8gb3ZlcmZsb3cteDogaGlkZGVuO1xuICAtbXMtb3ZlcmZsb3ctc3R5bGU6IG5vbmU7XG4gIHNjcm9sbGJhci13aWR0aDogbm9uZTtcbn1cbiNwYWdlLWNvbnRlbnQtd3JhcHBlcjo6LXdlYmtpdC1zY3JvbGxiYXIsICN3cmFwcGVyLnRvZ2dsZWQgI3BhZ2UtY29udGVudC13cmFwcGVyOjotd2Via2l0LXNjcm9sbGJhciwgI3BhZ2UtY29udGVudC13cmFwcGVyLmR5bmFtaWMtY29udGFpbmVyOjotd2Via2l0LXNjcm9sbGJhciB7XG4gIGRpc3BsYXk6IG5vbmU7XG59XG5cbi8qIFNpZGViYXIgU3R5bGVzICovXG5cbi5zaWRlYmFyLW5hdiB7XG4gIG1hcmdpbjogMDtcbiAgcGFkZGluZzogMDtcbiAgbGlzdC1zdHlsZTogbm9uZTtcblxuICBsaSB7XG4gICAgbGluZS1oZWlnaHQ6IDUwcHg7XG4gIH1cbn1cblxuI3NpZGViYXItd3JhcHBlciB7XG4gIGg2IHtcbiAgICBwYWRkaW5nLWxlZnQ6IDE3cHg7XG4gICAgbWFyZ2luLXRvcDogMjBweDtcbiAgICBmb250LWZhbWlseTogUm9ib3RvLVJlZ3VsYXIsIHNhbnMtc2VyaWY7XG4gICAgZm9udC1zaXplOiAxNHB4O1xuICAgIGNvbG9yOiAjYjViMWM1O1xuICB9XG59XG5cbi5zaWRlYmFyLW5hdiBsaSBhIHtcbiAgZGlzcGxheTogZmxleDtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xuICBmb250LWZhbWlseTogUm9ib3RvLVJlZ3VsYXIsIHNhbnMtc2VyaWY7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgY29sb3I6ICRhbG1vc3QtYmxhY2s7XG4gIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgbWFyZ2luOiAxMHB4IDE0cHg7XG4gIGhlaWdodDogMzRweDtcblxuICBpIHtcbiAgICBmb250LXNpemU6IDE4cHg7XG4gICAgcGFkZGluZzogMCAxMHB4O1xuICAgIG1hcmdpbi1yaWdodDogNXB4O1xuICAgIG1pbi13aWR0aDogNDBweDtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIH1cbn1cblxuLnNpZGViYXItbmF2IGxpIGE6aG92ZXIsXG4uc2lkZWJhci1uYXYgbGkgYTphY3RpdmUge1xuICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XG4gIGNvbG9yOiAjNDYzZDZlO1xuICBiYWNrZ3JvdW5kOiAjZGJkOGUzO1xufVxuXG4uc2lkZWJhci1uYXYgbGkgYTphY3RpdmUsXG4uc2lkZWJhci1uYXYgbGkgYTpmb2N1cyB7XG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcbn1cblxuLnNpZGViYXItbmF2ID4gLnNpZGViYXItYnJhbmQge1xuICBoZWlnaHQ6IDY1cHg7XG4gIGZvbnQtc2l6ZTogMThweDtcbiAgbGluZS1oZWlnaHQ6IDYwcHg7XG59XG5cbi5zaWRlYmFyLW5hdiA+IC5zaWRlYmFyLWJyYW5kIGEge1xuICBjb2xvcjogIzk5OTk5OTtcbn1cblxuLnNpZGViYXItbmF2ID4gLnNpZGViYXItYnJhbmQgYTpob3ZlciB7XG4gIGNvbG9yOiAjZmZmO1xuICBiYWNrZ3JvdW5kOiBub25lO1xufVxuXG4udG9nZ2xlZCAubG9hZGVyLWNvbnRhaW5lciB7XG4gIHBvc2l0aW9uOiBmaXhlZDtcbiAgbGVmdDogMTRyZW07XG4gIHRvcDogNC40cmVtO1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiAxMDAuNiU7XG4gIHotaW5kZXg6IDEwNjA7XG4gIHBvaW50ZXItZXZlbnRzOiBub25lO1xufVxuXG4ubG9hZGVyLWNvbnRhaW5lciB7XG4gIHBvc2l0aW9uOiBmaXhlZDtcbiAgbGVmdDogNC4ycmVtO1xuICB0b3A6IDMuOXJlbTtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogMTAwLjYlO1xuICB6LWluZGV4OiAxMDYwO1xuICBwb2ludGVyLWV2ZW50czogbm9uZSAhaW1wb3J0YW50O1xufVxuXG4jd3JhcHBlciB7XG4gIC8vICBvdmVyZmxvdzogaGlkZGVuO1xufVxuXG5AbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiAyMDAwcHgpIHtcbiAgI3dyYXBwZXIge1xuICAgIHdpZHRoOiAxMDAlO1xuICB9XG4gIC50b2dnbGVkIC5sb2FkZXItY29udGFpbmVyIHtcbiAgICB3aWR0aDogODguNSU7XG4gICAgbGVmdDogMTRyZW07XG4gICAgdG9wOiA0LjRyZW07XG4gIH1cbiAgLmxvYWRlci1jb250YWluZXIge1xuICAgIHRvcDogMy45cmVtO1xuICAgIGxlZnQ6IDQuMnJlbTtcbiAgICB3aWR0aDogMTAwJTtcbiAgfVxuXG5cbn1cbkBtZWRpYSBzY3JlZW4gYW5kIChtaW4taGVpZ2h0OiA3MTZweCkge1xuICAjc2lkZWJhci13cmFwcGVyIHtcbiAgICBtaW4taGVpZ2h0OiAtd2Via2l0LWZpbGwtYXZhaWxhYmxlO1xuICAgIG1pbi1oZWlnaHQ6IDEwMCU7XG5cbiAgfVxufVxuXG5AbWVkaWEgc2NyZWVuIGFuZCAobWF4LWhlaWdodDogNzE1cHgpIHtcbiAgI3NpZGViYXItd3JhcHBlciB7XG4gICAgbWluLWhlaWdodDogOTAlO1xuICB9XG59XG5cblxuXG5cbkBtZWRpYSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDE1NDBweCkge1xuICAudG9nZ2xlZCAubG9hZGVyLWNvbnRhaW5lciB7XG4gICAgbGVmdDogMTRyZW07XG4gICAgd2lkdGg6IDgzLjUlO1xuICAgIHRvcDogNC40cmVtO1xuICB9XG4gIC5sb2FkZXItY29udGFpbmVyIHtcbiAgICBsZWZ0OiA0LjJyZW07XG4gICAgdG9wOiAzLjlyZW07XG4gICAgd2lkdGg6IDEwMCU7XG4gIH1cbiAgLmR5bmFtaWMtY29udGFpbmVyIC52aWV3UG9ydCB7XG4gICAgbWFyZ2luLWxlZnQ6IDFyZW07XG4gIH1cbn1cblxuQG1lZGlhKG1pbi13aWR0aDogNzY4cHgpIHtcbiAgI3dyYXBwZXIge1xuICAgIHBhZGRpbmctbGVmdDogMDtcbiAgfVxuICAjd3JhcHBlci50b2dnbGVkIHtcbiAgICBwYWRkaW5nLXJpZ2h0OiAyMjVweDtcbiAgfVxuICAjc2lkZWJhci13cmFwcGVyIHtcbiAgICB3aWR0aDogNjZweDtcbiAgICBib3gtc2hhZG93OiAwIDJweCA0cHggMCByZ2JhKDAsIDAsIDAsIDAuMSk7XG4gIH1cbiAgI3dyYXBwZXIudG9nZ2xlZCAjc2lkZWJhci13cmFwcGVyIHtcbiAgICB3aWR0aDogMjI1cHg7XG4gIH1cbiAgI3BhZ2UtY29udGVudC13cmFwcGVyIHtcbiAgICBwYWRkaW5nOiAyMHB4O1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICBtYXJnaW4tYm90dG9tOiA1cmVtO1xuICB9XG4gICN3cmFwcGVyLnRvZ2dsZWQgI3BhZ2UtY29udGVudC13cmFwcGVyIHtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgbWFyZ2luLXJpZ2h0OiAwO1xuICAgIHdpZHRoOiAxMDAlO1xuICB9XG59XG4iXX0= */"]
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](WorkflowComponent, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
          args: [{
            selector: 'app-workflow',
            templateUrl: './workflow.component.html',
            styleUrls: ['./workflow.component.scss']
          }]
        }], function () {
          return [{
            type: _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]
          }, {
            type: src_app_shared_classes_Helpers__WEBPACK_IMPORTED_MODULE_3__["Helpers"]
          }];
        }, null);
      })();
      /***/

    },

    /***/
    "q4S7":
    /*!********************************************!*\
      !*** ./src/app/shared/classes/Services.ts ***!
      \********************************************/

    /*! exports provided: Services */

    /***/
    function q4S7(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "Services", function () {
        return Services;
      });
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/common/http */
      "tk/3");
      /* harmony import */


      var src_environments_environment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! src/environments/environment */
      "AytR");
      /* harmony import */


      var _LocalStorageCommon__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./LocalStorageCommon */
      "hdVc");

      var Services = /*#__PURE__*/function () {
        function Services(component, http) {
          var authBasic = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;

          _classCallCheck(this, Services);

          this.url = src_environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].baseUrl;
          this.component = component;
          this.http = http;
          this.storage = new _LocalStorageCommon__WEBPACK_IMPORTED_MODULE_2__["LocalStorageCommon"]();
          this.authBasic = authBasic;
          this.resolvedSchema();
        }

        _createClass(Services, [{
          key: "resolvedSchema",
          value: function resolvedSchema() {
            var currentSchema = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
            var contentType = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'application/json';
            var schema = currentSchema;

            if (schema === null) {
              schema = this.storage.get('schema_db', 'normal');

              if (schema === null || schema === '' || !schema) {
                schema = 'public';
              }
            }

            var authorization = 'Bearer ' + this.storage.get('auth_token', 'normal');

            if (this.authBasic) {
              authorization = 'Basic ';
            }

            this.httpOptions = {
              headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HttpHeaders"]({
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': 'true',
                'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, OPTIONS',
                'Access-Control-Allow-Headers': '*',
                Authorization: authorization,
                'schema-db': schema
              })
            };
            this.httpOptions.headers.set('Content-Type', contentType);
          }
        }, {
          key: "observeResponse",
          value: function observeResponse() {
            this.httpOptions.observe = 'response';
          } // -------------------------------------------------------------------------------------------------

        }, {
          key: "get",
          value: function get(filter) {
            var partUrl = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
            var pageCurrentPaginate = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;
            var schema = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : null;
            this.resolvedSchema(schema);
            var resultURL = this.url + this.component;

            if (partUrl !== null) {
              resultURL += '/' + partUrl;
            }

            if (typeof filter === 'number' || typeof filter === 'string') {
              resultURL += '/' + filter;
            } else {
              resultURL += '?filter=' + JSON.stringify(filter);

              if (pageCurrentPaginate !== null) {
                resultURL += '&page=' + pageCurrentPaginate;
              }
            }

            console.log('holaa');
            console.log(resultURL);
            return this.http.get(resultURL, this.httpOptions);
          }
        }, {
          key: "getVersion",
          value: function getVersion(filter) {
            var partUrl = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
            var schema = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;
            this.resolvedSchema(schema);
            var resultURL = this.url + this.component;

            if (partUrl !== null) {
              resultURL += '/' + partUrl;
            }

            if (typeof filter === 'number' || typeof filter === 'string') {
              resultURL += '/' + filter;
            } else {
              var qStr = $.param(filter);
              resultURL += '?' + qStr;
            }

            console.log(resultURL);
            return this.http.get(resultURL, this.httpOptions);
          }
        }, {
          key: "getDeep",
          value: function getDeep(filter) {
            var partUrl = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
            var pageCurrentPaginate = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;
            var schema = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : null;
            this.resolvedSchema(schema);
            this.observeResponse();
            var resultURL = this.url + this.component;

            if (partUrl !== null) {
              resultURL += '/' + partUrl;
            }

            if (typeof filter === 'number' || typeof filter === 'string') {
              resultURL += '/' + filter;
            } else {
              resultURL += '?filter=' + JSON.stringify(filter);

              if (pageCurrentPaginate !== null) {
                resultURL += '&page=' + pageCurrentPaginate;
              }
            }

            return this.http.get(resultURL, this.httpOptions);
          }
        }, {
          key: "getPublic",
          value: function getPublic(filter) {
            var partUrl = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
            var pageCurrentPaginate = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;
            var schema = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : null;
            this.resolvedSchema(schema);
            var resultURL = this.url + this.component;

            if (partUrl !== null) {
              resultURL += '/' + partUrl;
            }

            resultURL += '?' + filter;
            return this.http.get(resultURL, this.httpOptions);
          } // tslint:disable-next-line: max-line-length

        }, {
          key: "post",
          value: function post() {
            var data = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
            var pageCurrentPaginate = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
            var filter = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;
            var partUrl = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : null;
            var schema = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : null;
            this.resolvedSchema(schema);
            var resultURL = this.url + this.component;

            if (partUrl !== null) {
              resultURL += '/' + partUrl;
            }

            if (typeof filter === 'number' || typeof filter === 'string') {
              resultURL += '/' + filter;
            } else if (filter !== null) {
              resultURL += '?filter=' + JSON.stringify(filter);

              if (pageCurrentPaginate !== null) {
                resultURL += '&page=' + pageCurrentPaginate;
              }
            }

            console.log(resultURL);
            console.log(data);
            console.log(this.httpOptions);
            return this.http.post(resultURL, data, this.httpOptions);
          }
        }, {
          key: "postBlob",
          value: function postBlob() {
            var data = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
            var pathUrl = arguments.length > 1 ? arguments[1] : undefined;
            this.resolvedSchema(null);
            var auth = 'Bearer ' + this.storage.get('auth_token', 'normal');
            this.httpOptions = {
              headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HttpHeaders"]({
                'Content-Type': 'application/json',
                Accept: 'application/json',
                Authorization: auth
              }),
              responseType: 'blob'
            };
            var r = this.url + this.component + '/' + pathUrl;
            return this.http.post(r, data, this.httpOptions);
          }
        }, {
          key: "put",
          value: function put(id, data) {
            var schema = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;
            var partUrl = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : null;
            this.resolvedSchema(schema);
            var resultURL = this.url + this.component;

            if (partUrl !== null) {
              resultURL += '/' + partUrl;
            }

            return this.http.put(resultURL + '/' + id, data, this.httpOptions);
          }
        }, {
          key: "delete",
          value: function _delete(id) {
            var schema = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
            var pathUrl = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : '';
            this.resolvedSchema(schema);

            if (pathUrl.length) {
              pathUrl = pathUrl + '/';
            }

            console.log('holis');
            console.log(this.url + this.component + '/' + pathUrl + id);
            return this.http["delete"](this.url + this.component + '/' + pathUrl + id, this.httpOptions);
          }
        }, {
          key: "state",
          value: function state(id) {
            var schema = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
            this.resolvedSchema(schema);
            return this.http.put(this.url + this.component + '/state/' + id, {}, this.httpOptions);
          }
        }, {
          key: "exportComplete",
          value: function exportComplete() {
            var schema = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
            this.resolvedSchema(schema);
            return this.http.get(this.url + this.component + '/exportComplete', this.httpOptions);
          }
        }, {
          key: "uploadMultiplesFiles",
          value: function uploadMultiplesFiles(data, files) {
            var partUrl = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;
            var schema = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : null;
            this.resolvedSchema(schema);
            var resultURL = this.url + this.component;

            if (partUrl !== null) {
              resultURL += '/' + partUrl;
            }

            var formData = new FormData();
            files.forEach(function (file) {
              formData.append(file[0], file[1], file[1].name);
            });
            var wizkey = Object.keys(data);
            formData.append(wizkey[0], data.id_wizardprocess);
            return this.http.post(resultURL, formData, this.httpOptions);
          }
        }, {
          key: "recover",
          value: function recover(data) {
            var schema = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
            this.resolvedSchema(schema);
            return this.http.post(this.url + this.component + '/resetPassword', data, this.httpOptions);
          }
        }, {
          key: "show",
          value: function show(id) {
            var schema = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
            this.resolvedSchema(schema);
            return this.http.get(this.url + this.component + '/show/' + id, this.httpOptions);
          }
        }]);

        return Services;
      }();
      /***/

    },

    /***/
    "qhoq":
    /*!*****************************************************************!*\
      !*** ./src/app/shared/services/signin/token-renewal.service.ts ***!
      \*****************************************************************/

    /*! exports provided: TokenRenewalService */

    /***/
    function qhoq(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "TokenRenewalService", function () {
        return TokenRenewalService;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/common/http */
      "tk/3");
      /* harmony import */


      var src_app_shared_classes_Services__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! src/app/shared/classes/Services */
      "q4S7");

      var TokenRenewalService = /*#__PURE__*/function (_src_app_shared_class3) {
        _inherits(TokenRenewalService, _src_app_shared_class3);

        var _super6 = _createSuper(TokenRenewalService);

        function TokenRenewalService(http) {
          _classCallCheck(this, TokenRenewalService);

          return _super6.call(this, 'auth/renewal-token', http, true);
        }

        return TokenRenewalService;
      }(src_app_shared_classes_Services__WEBPACK_IMPORTED_MODULE_2__["Services"]);

      TokenRenewalService.ɵfac = function TokenRenewalService_Factory(t) {
        return new (t || TokenRenewalService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]));
      };

      TokenRenewalService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({
        token: TokenRenewalService,
        factory: TokenRenewalService.ɵfac,
        providedIn: 'root'
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](TokenRenewalService, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
          args: [{
            providedIn: 'root'
          }]
        }], function () {
          return [{
            type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]
          }];
        }, null);
      })();
      /***/

    },

    /***/
    "sFNd":
    /*!*******************************************!*\
      !*** ./src/app/shared/classes/Helpers.ts ***!
      \*******************************************/

    /*! exports provided: Helpers */

    /***/
    function sFNd(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "Helpers", function () {
        return Helpers;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _LocalStorageCommon__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ./LocalStorageCommon */
      "hdVc");
      /* harmony import */


      var _auth0_angular_jwt__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @auth0/angular-jwt */
      "Nm8O");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var _services_signin_token_renewal_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ../services/signin/token-renewal.service */
      "qhoq");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @angular/forms */
      "3Pt+");
      /* harmony import */


      var _CustomValidators__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./CustomValidators */
      "vj/p");

      var Helpers = /*#__PURE__*/function () {
        function Helpers(localStorageCommon, router, tokenRenewalService) {
          _classCallCheck(this, Helpers);

          this.localStorageCommon = localStorageCommon;
          this.router = router;
          this.tokenRenewalService = tokenRenewalService;
          this.parser = new _auth0_angular_jwt__WEBPACK_IMPORTED_MODULE_2__["JwtHelperService"]();
          /**
           * This function initializes the error handling
           * @param form - the array of input fields
           * @param ctx -  the component who calls the function
           * @param ctx -  the component who calls the function
           */

          this.errorInit = function (form, ctx) {
            form.forEach(function (i) {
              ctx.error[i] = {
                error: false,
                change: false,
                msg: ''
              };
            });
          };
          /**
           * This function validates if the user role possesses the permission
           * @param user - the user profile to check if its an admin
           * @param permission - the slug to validate
           * @param permList - the permission list to iterate
           * @returns boolean corresponding to the condition
           */


          this.getPermission = function (user, permission, permList) {
            if ((user === null || user === void 0 ? void 0 : user.role.special) !== 'all-access') {
              // tslint:disable-next-line: prefer-for-of
              for (var i = 0; i < (permList === null || permList === void 0 ? void 0 : permList.length); i++) {
                if (permList[i].slug === permission) {
                  return true;
                }
              }

              return false;
            } else {
              return true;
            }
          };
          /**
           * This function returns a FormControl configuration object
           * @param params - a string array with the form fields
           */


          this.generateForm = function (params) {
            var form = {};
            params.forEach(function (i) {
              var v = /date/i.test(i.field) ? '' : null;

              if (i.field === 'contractProcess' || i.field === 'search') {
                v = '';
              } else if (i.field === 'typefilterdate' || i.field === 'daterangestart' || i.field === 'daterangeend') {
                v = null;
              }

              form[i.field] = new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](v, i.validatorOrOpts);
            });
            return form;
          };
          /**
           * Observable function to detect changes and enable submit buttons
           * @param ctx - The component where the function is called
           */

          /**observeForms(ctx) {
                    ctx.form.valueChanges.subscribe((i) => {
              setTimeout(() => {
                let flag = false;
                Object.keys(i).forEach(j => {
                  if (i[j]) {
                    if (ctx.form.get(j).dirty && !flag) {
                    
                      ctx.enableActivationBtn = !ctx.error[j].error && ctx.error[j].change;
                      if (!ctx.enableActivationBtn) {
                        flag = true;
                      }
                    }
                  }
                });
              }, 500);
            });
          }*/

          /**
           * This function provides an updated error initialization based on props model for formFields
           * @param form -  the form array containing form objects
           * @param error -  error array to hold the input error states
           */


          this.errorInitialization = function (form, error) {
            form.forEach(function (i) {
              error[i.field] = {
                error: false,
                change: false,
                msg: ''
              };
            });
          };
        }
        /**
         * Function that converts numbers to money format
         * @param currency Type of currency($, £, €, etc.)
         * @param value Numerical value (0 or 9.00, etc.)
         * @param decimals Number of decimals (2 by default)
         */


        _createClass(Helpers, [{
          key: "formatMoney",
          value: function formatMoney(currency, value, decimals) {
            if (value !== undefined && value !== '') {
              var n = value;
              n = n.toString();

              if (n === '' || n === '.') {
                n = '0.00';
              }

              var patron = [currency, ' ', ','];
              var longitud = patron.length;

              for (var i = 0; i < longitud; i++) {
                n = n.replace(patron[i], '');
              }

              n = n.replace(patron, '');
              n = parseFloat(n);
              var dec = 2;

              if (decimals !== undefined) {
                dec = decimals;
              }

              var multiplicator = Math.pow(10, dec);
              return currency + ' ' + (Math.round(n * multiplicator) / multiplicator).toFixed(dec);
            }
          }
          /**
           * Function that subtracts two hours
           * @param starthour Start Hour (hh:mm:ss)
           * @param endhour Final Hour (hh:mm:ss)
           */

        }, {
          key: "subtractHours",
          value: function subtractHours(starthour, endhour) {
            var hourEnd = endhour.split(':');
            var hourStart = starthour.split(':');

            var _final = new Date();

            var initial = new Date();

            _final.setHours(parseInt(hourEnd[0], 0), parseInt(hourEnd[1], 0), parseInt(hourEnd[2], 0));

            initial.setHours(parseInt(hourStart[0], 0), parseInt(hourStart[1], 0), parseInt(hourStart[2], 0));
            var hours = _final.getHours() - initial.getHours();
            var minutes = _final.getMinutes() - initial.getMinutes();
            var seconds = _final.getSeconds() - initial.getSeconds();

            _final.setHours(hours, minutes, seconds);

            return _final.getHours() + ':' + _final.getMinutes() + ':' + _final.getSeconds();
          }
          /**
           * Function that subtracts two dates
           * @param startdate Start date (YYYY-MM-DD)
           * @param enddate Final date (YYYY-MM-DD)
           */

        }, {
          key: "subtractDates",
          value: function subtractDates(startdate, enddate) {
            var _final2;

            var initial = startdate.split('-');

            if (enddate !== '') {
              _final2 = enddate.split('-');
            } else {
              var now = new Date();
              _final2 = [String(now.getFullYear()), String(now.getMonth() + 1), String(now.getDate())];
            }

            var dateStart = new Date(parseInt(initial[0], 0), parseInt(initial[1], 0) - 1, parseInt(initial[2], 0));
            var dateEnd = new Date(parseInt(_final2[0], 0), parseInt(_final2[1], 0) - 1, parseInt(_final2[2], 0));
            return Math.floor((dateEnd - dateStart) / 86400 / 1000);
          }
          /**
           * Function that transforms a number day into a literal date
           * @param day Number of day(s)
           */

        }, {
          key: "transformDayLiteral",
          value: function transformDayLiteral(day) {
            var num;
            var num2;

            if (day / 365.2427 >= 1) {
              num = Math.floor(day / 365.2427);
              num2 = num * 365.2427;
              return num + ' año(s), ' + this.transformDayLiteral(day - num2);
            }

            if (day / 30.437 >= 1) {
              num = Math.floor(day / 30.437);
              num2 = num * 30.437;
              return num + ' mes(es), ' + this.transformDayLiteral(day - num2);
            }

            if (day / 7 >= 1) {
              num = Math.floor(day / 7);
              num2 = num * 7;
              return num + ' semana(s), ' + this.transformDayLiteral(day - num2);
            }

            if (Math.floor(day) !== 0) {
              return Math.floor(day) + ' día(s).';
            } else {
              return '';
            }
          }
        }, {
          key: "clearStorageSlugIds",
          value: function clearStorageSlugIds(slugArray) {
            var _this21 = this;

            slugArray.forEach(function (slug) {
              if (_this21.checkSlug(slug)) {
                _this21.localStorageCommon.destroy(slug);
              }
            });
          }
        }, {
          key: "checkSlug",
          value: function checkSlug(slug) {
            var candidate = this.localStorageCommon.get(slug, 'normal');
            return !!candidate;
          }
        }, {
          key: "tokenCheckUp",
          value: function tokenCheckUp(token) {
            var expirationDate = this.parser.getTokenExpirationDate(token);

            if (this.isExpired(token)) {
              this.router.navigate(['/'])["catch"](function (e) {
                return console.error(e);
              });
            }

            var today = new Date();
            var timeFrame = 60000 * 60;
            var res = expirationDate.getTime() - timeFrame;
            return res < today.getTime();
          }
        }, {
          key: "isExpired",
          value: function isExpired(token) {
            return this.parser.isTokenExpired(token);
          }
          /**
           * Function that renews the session token seamlessly
           * @param ctx - the context where to call the options function in side the implemented component
           */

        }, {
          key: "renewToken",
          value: function renewToken(ctx) {
            var _this22 = this;

            this.tokenRenewalService.put(ctx.user.id_users, {}).subscribe(function (response) {
              if (response.success) {
                _this22.localStorageCommon.store('auth_token', response.data, 'normal');
              }
            }, function (error) {
              console.error(error);
            }, function () {
              ctx.complete();
            });
          }
          /**
           * This function checks the form input state
           * @param error - Error array corresponding tho the form group
           * @param id - Specific form control
           * @param form - Abstract form control to evaluate
           * @param errorOnly - True if we want to only highlight errors
           * @param step - The form step
           */

        }, {
          key: "validInput",
          value: function validInput(error, id, form) {
            var errorOnly = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : false;
            var step = arguments.length > 4 ? arguments[4] : undefined;
            console.log(error[id]);
            error[id].change = true;

            if (step) {
              error[id].step = step;
            }

            var t = form.get(id);
            t = t ? t.errors : form.errors;

            if (t) {
              error[id].error = true;
              error[id].msg = Object(_CustomValidators__WEBPACK_IMPORTED_MODULE_6__["customValidatorHandler"])(form, id);
            } else {
              error[id].error = false;
              error[id].change = !errorOnly;
              error[id].msg = '';
            }
          }
          /**
           * This function tries to execute statements based on permission
           * @param user - The user object to check if admin
           * @param perm -  The permission slug
           * @param permList -  The list of permission slugs
           * @param run - Optional statements to execute if pass permission test
           * @param elseRun - Optional statements to execute if the test fails
           */

        }, {
          key: "acidTest",
          value: function acidTest(user, perm, permList, run, elseRun) {
            if (this.getPermission(user, perm, permList)) {
              run();
            } else {
              elseRun();
            }
          }
          /**
           * This function uploads a file to a specified file array
           * @param files - The FileList obj to be uploaded
           * @param fieldName - The formControl name
           * @param filesToUpload - the array to temporarily store the file
           * @param form -  the form to set the value
           * @param size - if true checks for a max file of 7mb
           * @param type - if true checks for the file extension type
           * @param docTypes - if checking for type the supported formats, defaults to PDF
           */

        }, {
          key: "uploadFile",
          value: function uploadFile(files, fieldName, filesToUpload, form) {
            var size = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : false;
            var type = arguments.length > 5 && arguments[5] !== undefined ? arguments[5] : false;
            var docTypes = arguments.length > 6 && arguments[6] !== undefined ? arguments[6] : ['application/pdf'];

            var _a, _b;

            for (var i = 0; i < filesToUpload.length; i++) {
              if (filesToUpload[i][0][0] === fieldName) {
                filesToUpload.splice(i);
                break;
              }
            }

            filesToUpload.push([[fieldName], files.item(0)]);
            form.get(fieldName).setValue(files.item(0));

            if (((_a = files.item(0)) === null || _a === void 0 ? void 0 : _a.size) / 1024000 > 7 && size) {
              form.get(fieldName).setErrors({
                maxFile: true
              });
            }

            if (type) {
              var t = (_b = files.item(0)) === null || _b === void 0 ? void 0 : _b.type;
              var match = false;

              var _iterator = _createForOfIteratorHelper(docTypes),
                  _step;

              try {
                for (_iterator.s(); !(_step = _iterator.n()).done;) {
                  var _i = _step.value;

                  if (t === _i) {
                    match = true;
                    break;
                  }
                }
              } catch (err) {
                _iterator.e(err);
              } finally {
                _iterator.f();
              }

              if (!match) {
                if (docTypes.length > 1) {
                  form.get(fieldName).setErrors({
                    fileType: true
                  });
                } else if (docTypes.length === 1) {
                  switch (docTypes[0]) {
                    case 'application/pdf':
                      form.get(fieldName).setErrors({
                        fileTypePdf: true
                      });
                      break;

                    case 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet':
                      form.get(fieldName).setErrors({
                        fileTypeExcel: true
                      });
                      break;

                    case 'text/csv':
                      form.get(fieldName).setErrors({
                        fileTypeCsv: true
                      });
                  }
                }
              }
            }
          }
        }, {
          key: "uploadMultipleFiles",
          value: function uploadMultipleFiles(files, fieldName, filesToUpload, form) {
            Array.from(files).forEach(function (i) {
              if ((i === null || i === void 0 ? void 0 : i.size) / 1024000 > 7) {
                form.get(fieldName).setErrors({
                  maxFile: true
                });
              } else {
                filesToUpload.push(i);
                form.get(fieldName).setValue(i);
                form.get(fieldName).updateValueAndValidity();
              }
            });
          }
          /**
           * @param ctx - the parent component to validate de token
           */

        }, {
          key: "tokenSecurity",
          value: function tokenSecurity(ctx) {
            var _a, _b;

            if (ctx.token) {
              if (!this.isExpired(ctx.token)) {
                if (this.tokenCheckUp(ctx.token)) {
                  this.renewToken(ctx);
                } else {
                  ctx.complete();
                }
              } else {
                (_a = ctx.router) === null || _a === void 0 ? void 0 : _a.navigate(['/'])["catch"](function (e) {
                  return console.error(e);
                });
              }
            } else {
              (_b = ctx.router) === null || _b === void 0 ? void 0 : _b.navigate(['/'])["catch"](function (e) {
                return console.error(e);
              });
            }
          }
          /**
           * This function takes a string formatted day and returns a json date for myDatePicker plugin
           * @param dateString - the string date to be converted ej: yyyy-mm-dd
           * @param prefill - flag to precalculate with today's date
           */

        }, {
          key: "dateToJson",
          value: function dateToJson(dateString) {
            var prefill = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

            if (dateString) {
              var dateArr = dateString.split('-');
              return {
                date: {
                  year: parseInt(dateArr[0], 0),
                  month: parseInt(dateArr[1], 0),
                  day: parseInt(dateArr[2], 0)
                },
                jsDate: null
              };
            } else if (prefill) {
              var t = new Date();
              return {
                date: {
                  year: t.getFullYear(),
                  month: t.getMonth(),
                  day: t.getDate()
                },
                jsDate: t
              };
            } else {
              return {
                date: {
                  year: null,
                  month: null,
                  day: null
                },
                jsDate: null
              };
            }
          }
          /**
           * This function takes a date object and transforms it to a string
           * @param dateObj - date object in myDatePicker format
           */

        }, {
          key: "dateToString",
          value: function dateToString(dateObj) {
            var _a, _b, _c;

            if (dateObj) {
              var month = (_a = dateObj === null || dateObj === void 0 ? void 0 : dateObj.singleDate) === null || _a === void 0 ? void 0 : _a.date.month.toString();
              var day = (_b = dateObj === null || dateObj === void 0 ? void 0 : dateObj.singleDate) === null || _b === void 0 ? void 0 : _b.date.day.toString();

              if (parseInt(month, 0) < 10) {
                month = '0' + month;
              }

              if (parseInt(day, 0) < 10) {
                day = '0' + day;
              }

              return ((_c = dateObj === null || dateObj === void 0 ? void 0 : dateObj.singleDate) === null || _c === void 0 ? void 0 : _c.date.year) + '-' + month + '-' + day;
            } else {
              return null;
            }
          }
          /**
           * This function return the color name based on a list
           * @param id - the id_color to match
           * @param colorList - the list of color objects to display
           */

        }, {
          key: "displayColorName",
          value: function displayColorName(id, colorList) {
            var _iterator2 = _createForOfIteratorHelper(colorList),
                _step2;

            try {
              for (_iterator2.s(); !(_step2 = _iterator2.n()).done;) {
                var c = _step2.value;

                if (c.hasOwnProperty('id_color')) {
                  if (c.id_color === id) {
                    return c.colorname;
                  }
                }
              }
            } catch (err) {
              _iterator2.e(err);
            } finally {
              _iterator2.f();
            }

            return '';
          }
        }, {
          key: "displayVehicleClassName",
          value: function displayVehicleClassName(id, classList) {
            var _iterator3 = _createForOfIteratorHelper(classList),
                _step3;

            try {
              for (_iterator3.s(); !(_step3 = _iterator3.n()).done;) {
                var c = _step3.value;

                if (c.hasOwnProperty('id_vehicle_class')) {
                  if (c.id_vehicle_class === id) {
                    return c.class_name;
                  }
                }
              }
            } catch (err) {
              _iterator3.e(err);
            } finally {
              _iterator3.f();
            }

            return '';
          }
        }, {
          key: "displayVehicleTypeName",
          value: function displayVehicleTypeName(id, typeList) {
            var _iterator4 = _createForOfIteratorHelper(typeList),
                _step4;

            try {
              for (_iterator4.s(); !(_step4 = _iterator4.n()).done;) {
                var c = _step4.value;

                if (c.hasOwnProperty('id_vehicle_type')) {
                  if (c.id_vehicle_type === id) {
                    return c.type_name;
                  }
                }
              }
            } catch (err) {
              _iterator4.e(err);
            } finally {
              _iterator4.f();
            }

            return '';
          }
        }, {
          key: "displayAppraiserName",
          value: function displayAppraiserName(id, appraiserList) {
            var _iterator5 = _createForOfIteratorHelper(appraiserList),
                _step5;

            try {
              for (_iterator5.s(); !(_step5 = _iterator5.n()).done;) {
                var c = _step5.value;

                if (c.hasOwnProperty('id_as_appraiser')) {
                  if (c.id_as_appraiser === id) {
                    return "".concat(c.nameperson, " ").concat(c.lastnamepersonfather, " ").concat(c.lastnamepersonmother);
                  }
                }
              }
            } catch (err) {
              _iterator5.e(err);
            } finally {
              _iterator5.f();
            }

            return '';
          }
        }, {
          key: "displayBrandName",
          value: function displayBrandName(id, brandList) {
            var _iterator6 = _createForOfIteratorHelper(brandList),
                _step6;

            try {
              for (_iterator6.s(); !(_step6 = _iterator6.n()).done;) {
                var b = _step6.value;

                if (b.hasOwnProperty('id_brand')) {
                  if (b.id_brand === id) {
                    return b.brandname;
                  }
                }
              }
            } catch (err) {
              _iterator6.e(err);
            } finally {
              _iterator6.f();
            }

            return '';
          }
        }]);

        return Helpers;
      }();

      Helpers.ɵfac = function Helpers_Factory(t) {
        return new (t || Helpers)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_LocalStorageCommon__WEBPACK_IMPORTED_MODULE_1__["LocalStorageCommon"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_services_signin_token_renewal_service__WEBPACK_IMPORTED_MODULE_4__["TokenRenewalService"]));
      };

      Helpers.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({
        token: Helpers,
        factory: Helpers.ɵfac,
        providedIn: 'root'
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](Helpers, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
          args: [{
            providedIn: 'root'
          }]
        }], function () {
          return [{
            type: _LocalStorageCommon__WEBPACK_IMPORTED_MODULE_1__["LocalStorageCommon"]
          }, {
            type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]
          }, {
            type: _services_signin_token_renewal_service__WEBPACK_IMPORTED_MODULE_4__["TokenRenewalService"]
          }];
        }, null);
      })();
      /***/

    },

    /***/
    "sO5X":
    /*!*********************************************************!*\
      !*** ./src/app/admin/components/auth/auth.component.ts ***!
      \*********************************************************/

    /*! exports provided: AuthComponent */

    /***/
    function sO5X(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AuthComponent", function () {
        return AuthComponent;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");

      var AuthComponent = /*#__PURE__*/function () {
        function AuthComponent() {
          _classCallCheck(this, AuthComponent);
        }

        _createClass(AuthComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }]);

        return AuthComponent;
      }();

      AuthComponent.ɵfac = function AuthComponent_Factory(t) {
        return new (t || AuthComponent)();
      };

      AuthComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
        type: AuthComponent,
        selectors: [["app-auth"]],
        decls: 1,
        vars: 0,
        template: function AuthComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "router-outlet");
          }
        },
        directives: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterOutlet"]],
        styles: [".banner[_ngcontent-%COMP%] {\n  font-family: Roboto-Bold, sans-serif;\n  color: #0030f0;\n  padding-top: 5%;\n  padding-bottom: 5%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXC4uXFwuLlxcYXV0aC5jb21wb25lbnQuc2NzcyIsIi4uXFwuLlxcLi5cXC4uXFwuLlxcLi5cXC4uXFwuLlxcLi5cXHNjc3NcXGNvbG9ycy5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUVBO0VBQ0Usb0NBQUE7RUFDQSxjQ0hjO0VESWQsZUFBQTtFQUNBLGtCQUFBO0FBREYiLCJmaWxlIjoiYXV0aC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIkBpbXBvcnQgXCIuLi8uLi8uLi8uLi9zY3NzL2NvbG9yc1wiO1xuXG4uYmFubmVyIHtcbiAgZm9udC1mYW1pbHk6IFJvYm90by1Cb2xkLCBzYW5zLXNlcmlmO1xuICBjb2xvcjogJHByaW1hcnktYWxlcnQ7XG4gIHBhZGRpbmctdG9wOiA1JTtcbiAgcGFkZGluZy1ib3R0b206IDUlO1xufVxuIiwiLy8gQ29sb3IgcGFsZXR0ZSBcbiRwcmltYXJ5LWFsZXJ0OiAjMDAzMGYwO1xuJHJlZ2lzdHJ5LWFsZXJ0OiAjNzFjMDE1O1xuJGF0dGVudGlvbi1hbGVydDogI0E3QTlFNjtcbiR3YXJuaW5nLWFsZXJ0OiAjZjljMDA0O1xuJGVycm9yLWFsZXJ0OiAjZmM1NjYzO1xuJGNvbmZpcm1hdGlvbi1hbGVydC1ib3JkZXI6ICM2YzVhOTc7XG4kY29uZmlybWF0aW9uLWFsZXJ0LWJhY2tncm91bmQ6ICNlMWRlZWE7XG4kY29uZmlybWF0aW9uLWJ1dHRvbi1iYWNrZ3JvdW5kOiAjN2I2Y2EyO1xuJEVycm9yLXZhbGlkYXRpb24taWNvbjogI2U0NWU1ZTtcbiRFcnJvci12YWxpZGF0aW9uOiAjZjE1NTYxO1xuJHNlbGVjdGVkLWljb246ICM0NjNkNmU7XG4kY29uZmlybWF0aW9uLWJ1dHRvbi1hbHRlcm5hdGl2ZTogIzc4NmRhNDtcbiR3aGl0ZTogI2ZmZmZmZjtcbiRwcmltYXJ5LWJ1dHRvbjogIzNkN2Q0MTtcbiR0ZXJ0aWFyeS1idXR0b246ICNkN2U1ZGE7XG4kc3VjY2Vzcy12YWxpZGF0aW9uLWJvcmRlcjogIzgyYmUwMDtcbiRiYWNrZ3JvdW5kOiAjZmNmYmZmO1xuJGNhcmQtYm9yZGVyOiAjZWZlY2ZhO1xuJGFsbW9zdC1ibGFjazogIzBhMGEwYjtcbiRsYXJnZS1idXR0b246ICNkYmQ4ZTM7XG4kYm9yZGVyLWNvbG9yOiAjZDlkOWQ5O1xuJG5hdmJhci1ib3JkZXI6ICNmMWYxZjE7XG4kc21hbGwtdGV4dDogIzZiNjQ4YjtcbiRsZWFkOiAjNTk1MDdkO1xuLy8gQmFzZSBjb2xvcnNcbiRwcmltYXJ5OiAkcHJpbWFyeS1idXR0b247XG4kc2Vjb25kYXJ5OiAkc2VsZWN0ZWQtaWNvbjtcbiRzdWNjZXNzOiAkcmVnaXN0cnktYWxlcnQ7XG4kaW5mbzogJGF0dGVudGlvbi1hbGVydDtcbiR3YXJuaW5nOiAkd2FybmluZy1hbGVydDtcbiRkYW5nZXI6ICRlcnJvci1hbGVydDtcbiRkYXJrOiAkYWxtb3N0LWJsYWNrO1xuJHRvdWNoZWQ6ICMzMzk0ZjU7XG4vLyBCb2R5XG4vL1xuLy8gU2V0dGluZ3MgZm9yIHRoZSBgPGJvZHk+YCBlbGVtZW50LlxuJGJvZHktYmc6ICNGQkZDRkY7XG4kYm9keS1jb2xvcjogJGFsbW9zdC1ibGFjaztcblxuLy9ub3RpZmljYWNpb25lc1xuJHN1Y2Nlc3JlZmFjdG86ICNiZWJlYmU7XG4iXX0= */"]
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](AuthComponent, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
          args: [{
            selector: 'app-auth',
            templateUrl: './auth.component.html',
            styleUrls: ['./auth.component.scss']
          }]
        }], function () {
          return [];
        }, null);
      })();
      /***/

    },

    /***/
    "tGCW":
    /*!**************************************************************************!*\
      !*** ./src/app/admin/services/workflow/reporteria/reporteria.service.ts ***!
      \**************************************************************************/

    /*! exports provided: ReporteriaService */

    /***/
    function tGCW(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ReporteriaService", function () {
        return ReporteriaService;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _shared_classes__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ../../../../shared/classes */
      "gtvE");
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common/http */
      "tk/3");
      /* harmony import */


      var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! rxjs/operators */
      "kU1M");

      var ReporteriaService = /*#__PURE__*/function (_shared_classes__WEBP4) {
        _inherits(ReporteriaService, _shared_classes__WEBP4);

        var _super7 = _createSuper(ReporteriaService);

        function ReporteriaService(http) {
          var _this23;

          _classCallCheck(this, ReporteriaService);

          _this23 = _super7.call(this, "/api/v1/reporteria", http); //lllamado al api para obtener los empleados

          _this23.getAllByDepartamentoAndSupervisor = function (data) {
            var page = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
            var ctx = arguments.length > 2 ? arguments[2] : undefined;
            ctx.ngxService.start();

            _this23.get(data, 'getallbydepartamentobysupervisor', page).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["takeUntil"])(ctx.destroySubscription$)).subscribe(function (response) {
              if (response) {
                console.log(response);
                ctx.empleados = response;
                ctx.ngxService.stop();
              }
            }, function (error) {
              console.error(error);
              ctx.ngxService.stop();
            });
          }; //lllamado al api para obtener los empleados


          _this23.getallbydepartamento = function (data) {
            var page = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
            var ctx = arguments.length > 2 ? arguments[2] : undefined;
            ctx.ngxService.start();

            _this23.get(data, 'getallbydepartamento', page).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["takeUntil"])(ctx.destroySubscription$)).subscribe(function (response) {
              if (response) {
                console.log(response);
                ctx.empleados = response;
                ctx.ngxService.stop();
              }
            }, function (error) {
              console.error(error);
              ctx.ngxService.stop();
            });
          }; //lllamado al api para obtener los empleados


          _this23.getallbysupervisor = function (data) {
            var page = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
            var ctx = arguments.length > 2 ? arguments[2] : undefined;
            ctx.ngxService.start();

            _this23.get(data, 'getallbysupervisor', page).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["takeUntil"])(ctx.destroySubscription$)).subscribe(function (response) {
              if (response) {
                console.log(response);
                ctx.empleados = response;
                ctx.ngxService.stop();
              }
            }, function (error) {
              console.error(error);
              ctx.ngxService.stop();
            });
          };

          return _this23;
        }

        return ReporteriaService;
      }(_shared_classes__WEBPACK_IMPORTED_MODULE_1__["Services"]);

      ReporteriaService.ɵfac = function ReporteriaService_Factory(t) {
        return new (t || ReporteriaService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]));
      };

      ReporteriaService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({
        token: ReporteriaService,
        factory: ReporteriaService.ɵfac,
        providedIn: 'root'
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](ReporteriaService, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
          args: [{
            providedIn: 'root'
          }]
        }], function () {
          return [{
            type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]
          }];
        }, null);
      })();
      /***/

    },

    /***/
    "tmbA":
    /*!******************************************************************************!*\
      !*** ./src/app/admin/services/workflow/departamento/departamento.service.ts ***!
      \******************************************************************************/

    /*! exports provided: DepartamentoService */

    /***/
    function tmbA(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "DepartamentoService", function () {
        return DepartamentoService;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _shared_classes__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ../../../../shared/classes */
      "gtvE");
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common/http */
      "tk/3");
      /* harmony import */


      var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! rxjs/operators */
      "kU1M");

      var DepartamentoService = /*#__PURE__*/function (_shared_classes__WEBP5) {
        _inherits(DepartamentoService, _shared_classes__WEBP5);

        var _super8 = _createSuper(DepartamentoService);

        function DepartamentoService(http) {
          var _this24;

          _classCallCheck(this, DepartamentoService);

          _this24 = _super8.call(this, "/api/v1/departamento", http);

          _this24.getAllDepartamentos = function (data) {
            var page = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
            var ctx = arguments.length > 2 ? arguments[2] : undefined;
            ctx.ngxService.start();

            _this24.get(data, '', page).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["takeUntil"])(ctx.destroySubscription$)).subscribe(function (response) {
              if (response) {
                ctx.departamentos = response;
                ctx.ngxService.stop();
              }
            }, function (error) {
              console.error(error);
              ctx.ngxService.stop();
            });
          };

          _this24.addDepartamentos = function (data, ctx) {
            _this24.post(data, null, '').pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["takeUntil"])(ctx.destroySubscription$)).subscribe(function (response) {
              if (response) {
                ctx.ngxService.stop();
                ctx.reversePage();
              } else {
                ctx.ngxService.stop();
              }
            });
          };

          _this24.updateDepartamentos = function (id, data, ctx) {
            _this24.post(data, null, '').pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["takeUntil"])(ctx.destroySubscription$)).subscribe(function (response) {
              if (response) {
                ctx.ngxService.stop();
                ctx.reversePage();
              } else {
                ctx.ngxService.stop();
              }
            }, function (error) {
              console.error(error);
              ctx.ngxService.stop();
            });
          };

          _this24.deleteDepartamentoById = function (idDepartamento, ctx) {
            ctx.ngxService.start();

            _this24["delete"](idDepartamento, null, '').pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["takeUntil"])(ctx.destroySubscription$)).subscribe(function (response) {
              if (response) {
                ctx.complete();
                ctx.ngxService.stop();
              }
            }, function (error) {
              console.error(error);
              ctx.ngxService.stop();
            });
          };

          return _this24;
        }

        return DepartamentoService;
      }(_shared_classes__WEBPACK_IMPORTED_MODULE_1__["Services"]);

      DepartamentoService.ɵfac = function DepartamentoService_Factory(t) {
        return new (t || DepartamentoService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]));
      };

      DepartamentoService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({
        token: DepartamentoService,
        factory: DepartamentoService.ɵfac,
        providedIn: 'root'
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](DepartamentoService, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
          args: [{
            providedIn: 'root'
          }]
        }], function () {
          return [{
            type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]
          }];
        }, null);
      })();
      /***/

    },

    /***/
    "uWDe":
    /*!***********************************************!*\
      !*** ./src/app/shared/classes/chipHandler.ts ***!
      \***********************************************/

    /*! exports provided: ChipHandler */

    /***/
    function uWDe(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ChipHandler", function () {
        return ChipHandler;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");

      var ChipHandler = /*#__PURE__*/function () {
        function ChipHandler() {
          _classCallCheck(this, ChipHandler);

          this.chips = [];
        }

        _createClass(ChipHandler, [{
          key: "add",
          value: function add(chip) {
            var exists = this.chips.filter(function (item) {
              return item.name === chip.name;
            })[0];

            if (exists !== null && exists !== undefined) {
              this.chips.push(chip);
            }
          }
        }, {
          key: "remove",
          value: function remove(name) {
            this.chips = this.chips.filter(function (item) {
              return item.name !== name;
            });
          }
        }, {
          key: "clear",
          value: function clear() {
            this.chips = [];
          }
        }, {
          key: "show",
          value: function show(name) {
            var chip = this.chips.filter(function (item) {
              return item.name === name;
            })[0];
            chip.show();
          }
        }, {
          key: "hide",
          value: function hide(name) {
            var chip = this.chips.filter(function (item) {
              return item.name === name;
            })[0];
            chip.hide();
          }
        }]);

        return ChipHandler;
      }();

      ChipHandler.ɵfac = function ChipHandler_Factory(t) {
        return new (t || ChipHandler)();
      };

      ChipHandler.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({
        token: ChipHandler,
        factory: ChipHandler.ɵfac,
        providedIn: 'root'
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](ChipHandler, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
          args: [{
            providedIn: 'root'
          }]
        }], null, null);
      })();
      /***/

    },

    /***/
    "vY5A":
    /*!***************************************!*\
      !*** ./src/app/app-routing.module.ts ***!
      \***************************************/

    /*! exports provided: AppRoutingModule */

    /***/
    function vY5A(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function () {
        return AppRoutingModule;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var _admin_components_auth_auth_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./admin/components/auth/auth.component */
      "sO5X");
      /* harmony import */


      var _admin_components_auth_auth_routing_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./admin/components/auth/auth-routing.component */
      "99Gt");

      var routes = [{
        path: '',
        component: _admin_components_auth_auth_component__WEBPACK_IMPORTED_MODULE_2__["AuthComponent"],
        children: _admin_components_auth_auth_routing_component__WEBPACK_IMPORTED_MODULE_3__["routesAuth"]
      }, {
        path: 'admin',
        loadChildren: function loadChildren() {
          return Promise.resolve().then(__webpack_require__.bind(null,
          /*! ./admin/admin.module */
          "jkDv")).then(function (m) {
            return m.AdminModule;
          });
        }
      }, {
        path: '**',
        redirectTo: '404'
      }];

      var AppRoutingModule = function AppRoutingModule() {
        _classCallCheck(this, AppRoutingModule);
      };

      AppRoutingModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({
        type: AppRoutingModule
      });
      AppRoutingModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({
        factory: function AppRoutingModule_Factory(t) {
          return new (t || AppRoutingModule)();
        },
        imports: [[_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forRoot(routes, {
          scrollPositionRestoration: 'disabled',
          enableTracing: false,
          relativeLinkResolution: 'legacy'
        })], _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
      });

      (function () {
        (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](AppRoutingModule, {
          imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]],
          exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        });
      })();
      /*@__PURE__*/


      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](AppRoutingModule, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
          args: [{
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forRoot(routes, {
              scrollPositionRestoration: 'disabled',
              enableTracing: false,
              relativeLinkResolution: 'legacy'
            })],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
          }]
        }], null, null);
      })();
      /***/

    },

    /***/
    "vj/p":
    /*!****************************************************!*\
      !*** ./src/app/shared/classes/CustomValidators.ts ***!
      \****************************************************/

    /*! exports provided: regex, passwordValidator, samePasswordValidator, monetaryValidator, numericValidator, notZero, isIntegerValidator, isNumberValidator, feeValidator, paidFeeValidator, alphanumericValidator, nameValidator, fullNameValidator, lastnameValidator, secondLastnameValidator, cellphoneEcuValidator, telephoneEcuValidator, identifyIdECValidator, identifyRucECValidator, identifyPassECValidator, requiredArrayField, customValidatorHandler, customArrayValidatorHandle */

    /***/
    function vjP(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "regex", function () {
        return regex;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "passwordValidator", function () {
        return passwordValidator;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "samePasswordValidator", function () {
        return samePasswordValidator;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "monetaryValidator", function () {
        return monetaryValidator;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "numericValidator", function () {
        return numericValidator;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "notZero", function () {
        return notZero;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "isIntegerValidator", function () {
        return isIntegerValidator;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "isNumberValidator", function () {
        return isNumberValidator;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "feeValidator", function () {
        return feeValidator;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "paidFeeValidator", function () {
        return paidFeeValidator;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "alphanumericValidator", function () {
        return alphanumericValidator;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "nameValidator", function () {
        return nameValidator;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "fullNameValidator", function () {
        return fullNameValidator;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "lastnameValidator", function () {
        return lastnameValidator;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "secondLastnameValidator", function () {
        return secondLastnameValidator;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "cellphoneEcuValidator", function () {
        return cellphoneEcuValidator;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "telephoneEcuValidator", function () {
        return telephoneEcuValidator;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "identifyIdECValidator", function () {
        return identifyIdECValidator;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "identifyRucECValidator", function () {
        return identifyRucECValidator;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "identifyPassECValidator", function () {
        return identifyPassECValidator;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "requiredArrayField", function () {
        return requiredArrayField;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "customValidatorHandler", function () {
        return customValidatorHandler;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "customArrayValidatorHandle", function () {
        return customArrayValidatorHandle;
      });
      /* harmony import */


      var _Enums__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! ./Enums */
      "xQgS");

      var regex = _Enums__WEBPACK_IMPORTED_MODULE_0__["REGEX_DICTIONARY"];

      function passwordValidator(control) {
        if (control.value !== '' && control.value !== null && control.value !== undefined) {
          var expReg = regex.password;

          if (!expReg.test(control.value) && control.value.length) {
            return {
              password: true
            };
          }
        }

        return null;
      }

      function samePasswordValidator(control) {
        if (control.value !== '' && control.value !== null && control.value !== undefined) {
          var password = control.get('password').value;
          var repeatPassword = control.get('passwordconfirm').value;

          if (password !== repeatPassword && repeatPassword !== null) {
            return {
              samePassword: true
            };
          }

          return null;
        }
      }

      function monetaryValidator(control) {
        if (control.value !== '' && control.value !== null && control.value !== undefined) {
          var numericMonetary = regex.numericMonetary;

          if (!numericMonetary.test(control.value)) {
            return {
              moneyQuantity: true
            };
          }
        }

        return null;
      }

      function numericValidator(control) {
        if (control.value !== '' && control.value !== null && control.value !== undefined) {
          var numericQuantity = regex.numericQuantity;

          if (!numericQuantity.test(control.value)) {
            return {
              numericQuantity: true
            };
          }
        }

        return null;
      }

      function notZero(control) {
        if (control.value !== '' && control.value !== null && control.value !== undefined) {
          if (+control.value <= 0) {
            return {
              notZero: true
            };
          }
        }

        return null;
      }

      function isIntegerValidator(control) {
        if (control.value !== '' && control.value !== null && control.value !== undefined) {
          var v = +control.value;

          if (!Number.isInteger(v) || !regex.threeDigitInt.test(control.value)) {
            return {
              notInteger: true
            };
          }
        }

        return null;
      }

      function isNumberValidator(control) {
        if (control.value !== '' && control.value !== null && control.value !== undefined) {
          var v = +control.value;

          if (isNaN(v)) {
            return {
              notNumber: true
            };
          }
        }

        return null;
      }

      function feeValidator(control) {
        if (control.value !== '' && control.value !== null && control.value !== undefined) {
          var v = +control.value;

          if (v < 1 || v > 75 || isNaN(v)) {
            return {
              numericFee: true
            };
          }
        }

        return null;
      }

      function paidFeeValidator(control) {
        if (control.value !== '' && control.value !== null && control.value !== undefined) {
          var v = +control.value;

          if (v < 0 || v > 74 || isNaN(v)) {
            return {
              numericPaidFee: true
            };
          }
        }

        return null;
      }

      function alphanumericValidator(control) {
        if (control.value !== '' && control.value !== null && control.value !== undefined) {
          var numericQuantity = regex.alphanumeric;

          if (!numericQuantity.test(control.value)) {
            return {
              alphanumeric: true
            };
          }
        }

        return null;
      }

      function nameValidator(control) {
        if (control.value !== '' && control.value !== null && control.value !== undefined) {
          var names = regex.names;

          if (!names.test(control.value)) {
            return {
              name: true
            };
          }
        }

        return null;
      }

      function fullNameValidator(control) {
        if (control.value !== '' && control.value !== null && control.value !== undefined) {
          var names = regex.fullname;

          if (!names.test(control.value)) {
            return {
              fullName: true
            };
          }
        }

        return null;
      }

      function lastnameValidator(control) {
        if (control.value !== '' && control.value !== null && control.value !== undefined) {
          var names = regex.lastname;

          if (!names.test(control.value)) {
            return {
              lastname: true
            };
          }
        }

        return null;
      }

      function secondLastnameValidator(control) {
        if (control.value !== '' && control.value !== null && control.value !== undefined) {
          var names = regex.secondLastname;

          if (!names.test(control.value)) {
            return {
              secondLastname: true
            };
          }
        }

        return null;
      }

      function cellphoneEcuValidator(control) {
        if (control.value !== '' && control.value !== null && control.value !== undefined) {
          var cellphoneEcu = regex.cellphoneEcu;

          if (!cellphoneEcu.test(control.value)) {
            return {
              cellphone: true
            };
          }
        }

        return null;
      }

      function telephoneEcuValidator(control) {
        if (control.value !== '' && control.value !== null && control.value !== undefined) {
          var telephoneEcu = regex.telephoneEcu;

          if (!telephoneEcu.test(control.value)) {
            return {
              telephone: true
            };
          }
        }

        return null;
      }

      function identifyIdECValidator(control) {
        if (control.value !== '' && control.value !== null && control.value !== undefined) {
          var userId = regex.userId;

          if (!userId.test(control.value) && control.value.length) {
            return {
              userId: true
            };
          }
        }

        return null;
      }

      function identifyRucECValidator(control) {
        if (control.value !== '' && control.value !== null && control.value !== undefined) {
          var userId = regex.userRuc;

          if (!userId.test(control.value) && control.value.length) {
            return {
              userRuc: true
            };
          }
        }

        return null;
      }

      function identifyPassECValidator(control) {
        if (control.value !== '' && control.value !== null && control.value !== undefined) {
          var userId = regex.userPassport;

          if (!userId.test(control.value) && control.value.length) {
            return {
              userPassport: true
            };
          }
        }

        return null;
      }

      function requiredArrayField(control) {
        if (control.value !== '' && control.value !== null && control.value !== undefined) {
          return null;
        }

        return {
          required_genericArray: true
        };
      }

      function customValidatorHandler(formGroup, id) {
        var errorMessage = '';
        var t = formGroup.get(id) ? formGroup.get(id).errors : formGroup.errors;
        Object.keys(t).forEach(function (error) {
          var customError;

          if (error === 'required') {
            customError = "".concat(error, "_").concat(id);
          } else {
            customError = error;
          }

          if (formGroup.get(id)) {
            errorMessage = formGroup.get(id).hasError(error) ? _Enums__WEBPACK_IMPORTED_MODULE_0__["VALIDATOR_ENUM"][customError] : '';
          } else {
            errorMessage = formGroup.hasError(error) ? _Enums__WEBPACK_IMPORTED_MODULE_0__["VALIDATOR_ENUM"][customError] : '';
          }
        });
        return errorMessage;
      }

      function customArrayValidatorHandle(formArray, id) {
        var errorMessage = '';
        Object.keys(formArray.at(id).errors).forEach(function (error) {
          var customError = '';

          if (error === 'required') {
            if (formArray.at(id).indexOf('address') !== -1) {
              customError = "".concat(error, "_address");
            } else if (formArray.at(id).indexOf('nameCompleteReceive') !== -1) {
              customError = "".concat(error, "_nameCompleteReceive");
            } else if (formArray.at(id).indexOf('nameComplete') !== -1) {
              customError = "".concat(error, "_nameComplete");
            } else if (formArray.at(id).indexOf('Add') !== -1) {
              customError = "".concat(error, "_").concat(formArray.substring(0, formArray.indexOf('Add')));
            } else {
              customError = "".concat(error, "_").concat(formArray);
            }
          } else {
            customError = error;
          }

          errorMessage = formArray.at(id).hasError(error) ? _Enums__WEBPACK_IMPORTED_MODULE_0__["VALIDATOR_ENUM"][customError] : '';
        });
        return errorMessage;
      }
      /***/

    },

    /***/
    "xQgS":
    /*!*****************************************!*\
      !*** ./src/app/shared/classes/Enums.ts ***!
      \*****************************************/

    /*! exports provided: REGEX_DICTIONARY, VALIDATOR_ENUM, ASSET_SRC, STORAGE_SLUGS, CONTRACT_UPDATE */

    /***/
    function xQgS(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "REGEX_DICTIONARY", function () {
        return REGEX_DICTIONARY;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "VALIDATOR_ENUM", function () {
        return VALIDATOR_ENUM;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ASSET_SRC", function () {
        return ASSET_SRC;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "STORAGE_SLUGS", function () {
        return STORAGE_SLUGS;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "CONTRACT_UPDATE", function () {
        return CONTRACT_UPDATE;
      });

      var REGEX_DICTIONARY = {
        password: /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$#@$!%*?&-_])[A-Za-z\d$@$#!%*?&-_]{8,}/,
        numericMonetary: /^[0-9]{1,5}(\.[0-9]{2})?$/,
        numericQuantity: /^\d+(\.\d{1,2})?$/,
        names: /[a-zñáéíóúA-ZÑ][^#&<>\"~;$^%{}?\d]{1,20}$/,
        fullname: /[a-zñáéíóúA-ZÑ][^#&<>\"~;$^%{}?\d]+(?!\d)[a-zñáéíóúA-ZÑ]+$/,
        lastname: /((?!\d)[a-zñáéíóúA-ZÑ]{1,20}$)/,
        secondLastname: /((?!\d)[a-zñáéíóúA-ZÑ]{1,20}$)|(.)/,
        cellphoneEcu: /^[09]\d{9}$/,
        telephoneEcu: /^[0][2-7]\d{7}$/,
        userId: /^\d{10}$/,
        userRuc: /^\d{13}$/,
        userPassport: /([a-zA-Z\d]){7,10}/,
        alphanumeric: /^[a-z0-9]+$/i,
        threeDigitInt: /^[1-9]\d{0,2}$/
      };
      var VALIDATOR_ENUM = {
        required_email: 'Ingresa el email',
        required_emailBill: 'Ingresa el email para enviar la factura',
        required_password: 'Ingresa la contraseña',
        required_passwordconfirm: 'Confirme la contraseña',
        required_passwordCurrent: 'Ingresa la contraseña actual',
        required_repeatPassword: 'Ingresa la contraseña',
        required_address: 'Ingresa una dirección válida',
        required_name: 'Ingrese el nombre',
        required_fullName: 'Ingrese el nombre completo',
        required_identification: 'Ingresa número de cédula o RUC',
        required_businessName: 'Ingresa tu nombre o razón social',
        required_phone: 'Ingresa un número de teléfono',
        required_celphone: 'Ingresa tu número de celular',
        required_celphoneBill: 'Ingresa tu número de teléfono o celular',
        required_id_category: 'La categoría es requerida',
        required_id_provider: 'El proveedor es requerido',
        required_identificationtype: 'Seleccione el tipo de identificación',
        required_identify: 'Ingrese el número de identificación',
        required_civilstatus: 'Seleccione el estado civil de la persona',
        required_creditinstnumber: 'Ingrese el # Operación',
        required_financedvalue: 'Ingrese el Valor Financiado',
        required_entrypercentage: 'Ingrese el % de entrada',
        required_province: 'Ingrese la Provincia',
        required_provincePerson: 'Ingrese la Provincia',
        required_city: 'Ingrese la Ciudad',
        required_cityPerson: 'Ingrese la Ciudad',
        required_creditdatestart: 'Ingrese la fecha de inicio del crédito',
        required_creditdateend: 'Ingrese la fecha de fin del crédito',
        required_vehiclecost: 'Ingrese el costo del vehículo',
        required_nameperson: 'Ingresa el nombre',
        required_lastnameperson_first: 'Ingrese el apellido paterno de la persona',
        required_lastnameperson_second: 'Ingrese el apellido materno de la persona',
        required_product: 'Seleccione el producto',
        required_nationality: 'Seleccione una nacionalidad',
        required_nameperson_other: 'Ingresa el nombre',
        required_lastnameperson_other_first: 'Ingrese el apellido paterno de la persona',
        required_lastnameperson_other_second: 'Ingrese el apellido materno de la persona',
        required_identificationtype_other: 'Seleccione el tipo de identificación',
        required_identify_other: 'Ingrese el número de identificación',
        required_phone_other: 'Ingresa un número de teléfono',
        required_email_other: 'Ingresa el email',
        required_address_other: 'Ingresa una dirección válida',
        required_nationality_other: 'Seleccione una nacionalidad',
        required_province_other: 'Ingrese la Provincia',
        required_id_city_other: 'Ingrese la Ciudad',
        required_brand: 'Seleccione una marca de vehículo',
        required_modelvehicle: 'Seleccione un modelo de vehículo o ingrese uno nuevo',
        required_year: 'Seleccione el año del vehículo',
        required_licenseplate: 'Ingrese el número de placa / RAMV / CAMV',
        required_chasisnumber: 'Ingrese el número de chasis del vehículo',
        required_motornumber: 'Ingrese el número de motor del vehículo',
        required_color: 'Seleccione el color del vehículo',
        required_billdate: 'Ingrese la fecha de la factura',
        required_file_identify: 'El documento de identidad es requerido',
        required_file_contractbill: 'La factura o el contrato de compraventa son requeridos',
        operation_exists: 'Existe un contrato generado con el mismo número de operación.',
        date_range: 'El rango de fechas seleccionado no es válido, seleccione un rango mayor o igual a un año.',
        name: 'Ingrese nombres o un nombre válido',
        fullName: 'Ingrese nombres y apellidos válidos',
        lastname: 'Ingrese un apellido válido',
        email: 'Ingresa un email válido',
        password: 'Debe contener al menos 8 caracteres, mayúsculas, números y símbolos',
        samePassword: 'Las contraseñas deben ser iguales',
        url: 'Ingrese una url válida',
        moneyQuantity: 'El valor debe ser un número de 2 a 6 dígitos y puede contener 2 decimales',
        validatedate: 'El valor debe ser un número de 2 a 6 dígitos y puede contener 2 decimales',
        numericQuantity: 'El valor debe ser un número y puede contener 2 decimales',
        min: 'El valor debe ser mayor a 1',
        max: 'El valor no debe exceder el',
        userId: 'Ingrese una cédula válida',
        userRuc: 'Ingrese un RUC válido',
        userPassport: 'Ingrese un número de pasaporte válido',
        telephone: 'Ingrese un número de teléfono fijo válido y con código de provincia: 023456789',
        cellphone: 'Ingrese un número de celular válido',
        required_postModel: 'El nombre del modelo es requerido',
        required_institutionname: 'Ingrese el nombre de la institución',
        required_institutiontype: 'Seleccione el tipo de institución',
        required_brandname: 'El nombre de la marca es requerido',
        required_country: 'El país de origen de la marca es requerido',
        required_colorname: 'El nombre del color es requerido',
        required_institution: 'La institución del usuario es requerida',
        required_role: 'El rol del usuario es requerido',
        alphanumeric: 'Solo se permiten caracteres alfanuméricos',
        duplicated_id: 'La identificación se encuentra duplicada',
        maxFile: 'El tamaño del archivo debe estar por debajo de 8 MB',
        // Service activation messages
        required_default_start_date: 'La fecha de inicio de mora es requerida',
        required_fees_paid: 'El número de cuotas pagadas es requerido',
        required_expected_fees: 'El número de cuotas esperadas es requerido',
        required_outstanding_fees: 'El número de cuotas vencidas es requerido',
        required_outstanding_capital: 'El monto de captial pendiente es requerido',
        required_pending_interest: 'El monto de interés pendiente es requerido',
        required_last_payment_date: 'La última fecha de pago es requerida',
        required_note: 'La descripción de la activación es requerida',
        required_cuv_cost: 'El costo del CUV es requerido',
        required_id_province: 'La provincia de custodia es requerida',
        required_id_city: 'La ciudad de custodia es requerida',
        required_fines_cost: 'El monto de multas es requerido',
        required_vehicle_arrangements: 'Los costos de reparación son requeridos',
        required_documentary_cost: 'El costo de la documentación es requerida',
        required_transfer_cost: 'El costo de traslado es requerido',
        required_total_value_settlement: 'El valor de liquidación es requerido',
        required_ifi_payment_date: 'La fecha de pago es requerida',
        required_returned_value: 'El monto devuelto es requerido',
        required_appraisal_cost: 'El costo del avalúo es requerido',
        required_id_vehicle_class: 'La clase de vehículo es requerido',
        required_id_vehicle_type: 'El tipo de vehículo es requerido',
        required_notarial_charges_value: 'Los gastos notariales son requeridos',
        required_sales_commission: 'La comisión en ventas es requerida',
        required_sanitation_date: 'La fecha de saneamiento es requerida',
        required_appraisal_date: 'Fecha de avalúo es requerida',
        numericFee: 'El valor debe ser un número entre 1 y 75',
        numericPaidFee: 'El valor debe ser un número entre 0 y 74',
        minlength: 'La nota debe ser de al menos 150 caracteres',
        feesRange: 'El valor de cuotas pagadas debe ser menor al valor de cuotas previstas',
        // Service activation document forms
        required_opauto_rep_position: 'El cargo del representante OA es requerido',
        required_opauto_rep_name: 'El nombre del representante OA es requerido',
        required_inst_rep_position: 'El cargo del representate IFI es requerido',
        required_inst_rep_name: 'El nombre del representate IFI es requerido',
        required_breakdown: 'Ingrese el desgloce del valor total ejecutado',
        required_contract_number: 'El número de contrato es requerido',
        required_total_executed_value: 'El valor total ejecutado es requerido',
        required_cert_resumen: 'Ingrese el resumen del certificado',
        required_id_color: 'El color del vehículo es requerido',
        // Labour Risk
        required_genericArray: 'El campo es requerido',
        required_date: 'La fecha es requerida',
        // Expected Losses
        required_estimated_delay_rate: 'La tasa de mora estimada es requerida',
        required_probability_no_recovery: 'La probabilidad de no recuperación es requerida',
        required_price_affection: 'La afectación al precio es requerida',
        required_alleged_expenses: 'Los supuestos gastos son requeridos',
        morationValue: 'La suma de la distribución de la mora y la tasa de mora estimada debe ser igual',
        fileTypeCsv: 'El documento debe estar en formato CSV',
        // Appraisal request form
        required_appointmentDate: 'La fecha de avalúo es requerida',
        required_appointmentTime: 'La hora de avalúo es requerida',
        required_contactName: 'El contacto de avalúo es requerido',
        required_contactPhone: 'El teléfono de contacto es requerido',
        required_appointmentAddress: 'La dirección de avalúo es requerida',
        required_appraiser: 'El avaluador es requerido',
        required_id_province_appraisal: 'La provincia de avalúo es requerida',
        required_id_city_appraisal: 'La ciudad de avalúo es requerida',
        // Appraisal update form
        required_commercialValue: 'El valor comercial es requerido',
        required_opportunityValue: 'El valor de oportunidad es requerido',
        required_amountOwed: 'El valor adeudado es requerido',
        required_comment: 'El comentario es requerido',
        // Commerce form
        required_vehicleType: 'Seleccione el tipo de vehículo',
        required_vehicleSubtype: 'Selecciones el subtipo de vehículo',
        required_mileage: 'Ingrese el kilometraje de vehículo',
        required_vehiclePrice: 'Ingrese el precio de venta a anunciar',
        required_adDescription: 'La descripción del vehículo es requerida',
        required_model: 'Ingrese el modelo del vehículo',
        required_vehicleTransmission: 'Seleccione el tipo de transmisión del vehículo',
        required_vehicleStatus: 'Seleccione el estado en el cual se encuentra el vehículo',
        required_fuelType: 'Seleccione el tipo de combustible del vehículo',
        required_vehicleBodywork: 'Seleccione el tipo de chasis del vehículo',
        // Appraiser form
        required_lastnamepersonfather: 'Ingrese el apellido paterno',
        required_lastnamepersonmother: 'Ingrese el apellido materno',
        required_cellphone: 'Ingrese el número de celular',
        // Institution Entity
        required_disclaimer: 'Los terminos y condiciones son requeridos',
        required_contractdate: 'La fecha de contrato es requerida',
        // Score Configuration
        required_last_score: 'El valor de score es requerido',
        required_last_term: 'El plazo es requerido',
        required_first_score: 'El valor de score es requerido',
        required_first_term: 'El plazo es requerido',
        required_new_score: 'El valor de score es requerido',
        required_new_term: 'El plazo es requerido',
        scoreRange: 'El valor de score insertado no es válido',
        termRange: 'El plazo insertado no es válido',
        entryRange: 'La entrada insertada no es válida',
        required_last_entry: 'La entrada es requerida',
        required_first_entry: 'La entrada es requerida',
        required_new_entry: 'La entrada es requerida',
        notNumber: 'El valor debe ser un número con 2 decimales',
        notInteger: 'El valor debe ser un número entero mayor a 1 sin decimales',
        required_activityName: 'El nombre de la actividad es requerido',
        required_homologation: 'El tipo de riesgo es requerido',
        required_riskToMove: 'El tipo de riesgo a asignar es requerido',
        sum_error: 'La sumatoria de configuración debe ser igual a 100',
        notZero: 'El valor debe ser mayor a cero',
        required_newScore: 'El valor de score es requerido',
        required_newCode: 'El código ciiu es requerido',
        maxlength: 'La longitud permitida es'
      };
      var imgUrlLogin = '/assets/img/login/';
      var imgUrlLanding = '/assets/img/landing/';
      var imgUrlClients = '/assets/img/clientes/';
      var iconsUrl = '/assets/icons/';
      var imgUrlUser = '/assets/img/users/';
      var btnUrl = '/assets/buttons/';
      var dataMissing = iconsUrl + 'data-missing/';
      var svgUrl = '/assets/img/SVG/';
      var imgUrlLogo = '/assets/img/Logos/';
      var ASSET_SRC = {
        loginBackground: imgUrlLogin + 'login-background.jpg',
        loginBackgroundSrcset: imgUrlLogin + 'login-background@2x.jpg 2x,' + imgUrlLogin + 'login-background@3x.jpg 3x',
        loginLogoVertical: imgUrlLogin + 'angular.png',
        loginLogoVerticalSrcset: imgUrlLogin + 'login-logo-vertical-oa@2x.png 2x, ' + imgUrlLogin + 'login-logo-vertical-oa@3x.png 3x',
        loginArrowBack: iconsUrl + 'arrow-left-circle.svg',
        userPlaceholder: imgUrlUser + 'placeholder.png',
        wizardGray: iconsUrl + 'wizard/wizard-process-icon-gray.svg',
        wizardActive: iconsUrl + 'wizard/wizard-process-icon-active.svg',
        wizardCompleted: iconsUrl + 'wizard/wizard-process-icon-completed.svg',
        nextBtn: btnUrl + 'wizard/next-btn.svg',
        clipboardDataMissing: dataMissing + 'data-missing-icon.svg',
        callBackground: imgUrlLanding + 'call-background.jpg',
        callBackgroundSrcset: imgUrlLanding + 'call-background@2x.jpg 2x, ' + imgUrlLanding + 'call-background@3x.jpg 3x',
        landingCar: imgUrlLanding + 'landing-car.png',
        landingCarSrcset: imgUrlLanding + 'landing-car@2x.png 2x, ' + imgUrlLanding + 'landing-car@3x.png 3x',
        backgroundWorks: svgUrl + 'background-landing.svg',
        howItWorks: {
          step_1: svgUrl + 'howItWorks/step-1.svg',
          step_2: svgUrl + 'howItWorks/step-2.svg',
          step_3: svgUrl + 'howItWorks/step-3.svg',
          step_4: svgUrl + 'howItWorks/step-4.svg'
        },
        beneficiosIfi: imgUrlLanding + 'beneficios-ifi.png',
        beneficiosIfiSrcset: imgUrlLanding + 'beneficios-ifi@2x.png 2x, ' + imgUrlLanding + 'beneficios-ifi@3x.png 3x',
        beneficiosCliente: imgUrlLanding + 'beneficios-clientes.png',
        beneficiosClienteSrcset: imgUrlLanding + 'beneficios-clientes@2x.png 2x, ' + imgUrlLanding + 'beneficios-clientes@3x.png 3x',
        experienciaBackground: imgUrlLanding + 'experiencia-background.jpg',
        experienciaBackgroundSrcset: imgUrlLanding + 'experiencia-background@2x.jpg 2x, ' + imgUrlLanding + 'experiencia-background@3x.jpg 3x',
        contactoBackground: svgUrl + 'background-contacto.svg',
        experienceGreen: svgUrl + 'background-green-experience.svg',
        experienceCar: imgUrlLanding + 'carro-experiencia.png',
        experienceCarSrcset: imgUrlLanding + 'carro-experiencia@2x.png 2x, ' + imgUrlLanding + 'carro-experiencia@3x.png 3x',
        assaLogo: imgUrlClients + 'logo-assa.png',
        assaLogoSrcset: imgUrlClients + 'logo-assa@2x.png 2x, ' + imgUrlClients + 'logo-assa@3x.png 3x',
        capitalLogo: imgUrlClients + 'logo-capital.png',
        capitalLogoSrcset: imgUrlClients + 'logo-capital@2x.png 2x, ' + imgUrlClients + 'logo-capital@3x.png 3x',
        cfcLogo: imgUrlClients + 'logo-cfc.png',
        cfcLogoSrcset: imgUrlClients + 'logo-cfc@2x.png 2x, ' + imgUrlClients + 'logo-cfc@3x.png 3x',
        imbautoLogo: imgUrlClients + 'logo-imbauto.png',
        imbautoLogoSrcset: imgUrlClients + 'logo-imbauto@2x.png 2x, ' + imgUrlClients + 'logo-imbauto@3x.png 3x',
        vallejoLogo: imgUrlClients + 'logo-vallejo.png',
        vallejoLogoSrcset: imgUrlClients + 'logo-vallejo@2x.png 2x, ' + imgUrlClients + 'logo-vallejo@3x.png 3x',
        oaLogo: imgUrlLogo + 'logotipo-oa-footer.png',
        oaLogoSrcset: imgUrlLogo + 'logotipo-oa-footer@2x.png 2x, ' + imgUrlLogo + 'logotipo-oa-footer@3x.png 3x',
        locationIcon: svgUrl + 'icons/location-icon.svg',
        callIcon: svgUrl + 'icons/call-icon.svg',
        mailIcon: svgUrl + 'icons/mail-icon.svg',
        fbIcon: svgUrl + 'icons/facebook-icon.svg',
        liIcon: svgUrl + 'icons/linkedin-icon.svg',
        youtubeIcon: svgUrl + 'icons/youtube-icon.svg',
        imgPlaceholder: svgUrl + 'no_preview.svg'
      };
      var STORAGE_SLUGS = ['id_color', 'id_brand', 'id_institution', 'id_user', 'id_appraiser', 'id_wizardprocess', 'client_full_name', 'id_civilstatus', 'filters', 'vClassList', 'vTypeList', 'colorList', 'classificationList', 'approvedList', 'ptx'];
      var CONTRACT_UPDATE = [// CFC - 0
      ['creditinstnumber', 'creditdatestart', 'creditdateend', 'nameperson', 'lastnameperson_first', 'lastnameperson_second', 'identify', 'email', 'address', 'celphone', 'nameperson_other', 'lastnameperson_other_first', 'lastnameperson_other_second', 'identify_other', 'email_other', 'address_other', 'brand', 'modelvehicle', 'year', 'licenseplate', 'chasisnumber', 'motornumber'], // OTHER - 1
      ['creditinstnumber', 'creditdatestart', 'creditdateend', 'city', 'nameperson', 'lastnameperson_first', 'lastnameperson_second', 'identify', 'email', 'address', 'phone', 'celphone', 'cityPerson', 'nationality', 'civilstatus', 'nameperson_other', 'lastnameperson_other_first', 'lastnameperson_other_second', 'identify_other', 'nationality_other', 'identify_other', 'brand', 'modelvehicle', 'year', 'licenseplate', 'chasisnumber', 'motornumber', 'color', 'billdate']];
      /***/
    },

    /***/
    "zUnb":
    /*!*********************!*\
      !*** ./src/main.ts ***!
      \*********************/

    /*! no exports provided */

    /***/
    function zUnb(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _environments_environment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ./environments/environment */
      "AytR");
      /* harmony import */


      var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./app/app.module */
      "ZAI4");
      /* harmony import */


      var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/platform-browser */
      "jhN1");

      if (_environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].production) {
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
      }

      var oaStyle = ['background: transparent', 'color: #87bf48', 'display: block', 'text-align: center', 'font-family: \'Roboto-Regular\', sans-serif', 'font-size: 14px'].join(';');

      _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["platformBrowser"]().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"]).then(function (res) {
        return console.log('%c ' + String.fromCodePoint(0x1F680) + ' Admin up and running ', oaStyle);
      }, function (err) {
        return console.error(err);
      });
      /***/

    }
  }, [[0, "runtime", "vendor"]]]);
})();
//# sourceMappingURL=main-es5.js.map